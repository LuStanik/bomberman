func @_Tests.RandomBombsTests.BasicTest$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Tests\\EditMode\\RandomBombsTests.cs" :19 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Tests\\EditMode\\RandomBombsTests.cs" :23 :28) // false
%1 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Tests\\EditMode\\RandomBombsTests.cs" :23 :17) // isActive
cbde.store %0, %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Tests\\EditMode\\RandomBombsTests.cs" :23 :17)
// Entity from another assembly: Assert
%2 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Tests\\EditMode\\RandomBombsTests.cs" :25 :28) // false
%3 = cbde.load %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Tests\\EditMode\\RandomBombsTests.cs" :25 :35)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Tests\\EditMode\\RandomBombsTests.cs" :25 :12) // Assert.AreEqual(false, isActive) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Tests.RandomBombsTests.NewTestScriptSimplePasses$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Tests\\EditMode\\RandomBombsTests.cs" :30 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
func @_Tests.RandomBombsTests.BombPower$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Tests\\EditMode\\RandomBombsTests.cs" :36 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = constant 3 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Tests\\EditMode\\RandomBombsTests.cs" :39 :24)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Tests\\EditMode\\RandomBombsTests.cs" :39 :16) // power
cbde.store %0, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Tests\\EditMode\\RandomBombsTests.cs" :39 :16)
// Entity from another assembly: MonoBehaviour
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Tests\\EditMode\\RandomBombsTests.cs" :40 :56) // Not a variable of known type: bombPrefab
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Tests\\EditMode\\RandomBombsTests.cs" :40 :30) // MonoBehaviour.Instantiate(bombPrefab) (InvocationExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Tests\\EditMode\\RandomBombsTests.cs" :41 :29) // Not a variable of known type: bomb
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Tests\\EditMode\\RandomBombsTests.cs" :41 :29) // bomb.GetComponent<Bomb>() (InvocationExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Tests\\EditMode\\RandomBombsTests.cs" :42 :12) // Not a variable of known type: classBomb
%9 = constant 4 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Tests\\EditMode\\RandomBombsTests.cs" :42 :27)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Tests\\EditMode\\RandomBombsTests.cs" :42 :12) // classBomb.Boom(4) (InvocationExpression)
// Entity from another assembly: Assert
%11 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Tests\\EditMode\\RandomBombsTests.cs" :43 :28)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Tests\\EditMode\\RandomBombsTests.cs" :43 :35) // Not a variable of known type: classBomb
%13 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Tests\\EditMode\\RandomBombsTests.cs" :43 :35) // classBomb.GetPower() (InvocationExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Tests\\EditMode\\RandomBombsTests.cs" :43 :12) // Assert.AreEqual(power, classBomb.GetPower()) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function NewTestScriptWithEnumeratorPasses(), it contains poisonous unsupported syntaxes

