func @_Telepathy.SafeQueue$T$.Enqueue$T$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :30 :8) {
^entry (%_item : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :30 :28)
cbde.store %_item, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :30 :28)
br ^0

^0: // LockBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :32 :18) // Not a variable of known type: queue
br ^1

^1: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :34 :16) // Not a variable of known type: queue
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :34 :30) // Not a variable of known type: item
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :34 :16) // queue.Enqueue(item) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Telepathy.SafeQueue$T$.TryDequeue$outT$(none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :40 :8) {
^entry (%_result : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :40 :31)
cbde.store %_result, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :40 :31)
br ^0

^0: // LockBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :42 :18) // Not a variable of known type: queue
br ^1

^1: // BinaryBranchBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :44 :25) // default(T) (DefaultExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :45 :20) // Not a variable of known type: queue
%4 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :45 :20) // queue.Count (SimpleMemberAccessExpression)
%5 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :45 :34)
%6 = cmpi "sgt", %4, %5 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :45 :20)
cond_br %6, ^2, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :45 :20)

^2: // JumpBlock
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :47 :29) // Not a variable of known type: queue
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :47 :29) // queue.Dequeue() (InvocationExpression)
%9 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :48 :27) // true
return %9 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :48 :20)

^3: // JumpBlock
%10 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :50 :23) // false
return %10 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :50 :16)

^4: // ExitBlock
cbde.unreachable

}
func @_Telepathy.SafeQueue$T$.TryDequeueAll$outT$$$(none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :56 :8) {
^entry (%_result : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :56 :34)
cbde.store %_result, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :56 :34)
br ^0

^0: // LockBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :58 :18) // Not a variable of known type: queue
br ^1

^1: // JumpBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :60 :25) // Not a variable of known type: queue
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :60 :25) // queue.ToArray() (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :61 :16) // Not a variable of known type: queue
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :61 :16) // queue.Clear() (InvocationExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :62 :23) // Not a variable of known type: result
%7 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :62 :23) // result.Length (SimpleMemberAccessExpression)
%8 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :62 :39)
%9 = cmpi "sgt", %7, %8 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :62 :23)
return %9 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :62 :16)

^2: // ExitBlock
cbde.unreachable

}
func @_Telepathy.SafeQueue$T$.Clear$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :66 :8) {
^entry :
br ^0

^0: // LockBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :68 :18) // Not a variable of known type: queue
br ^1

^1: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :70 :16) // Not a variable of known type: queue
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\SafeQueue.cs" :70 :16) // queue.Clear() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
