func @_Telepathy.ThreadExtensions.AbortAndJoin$System.Threading.Thread$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\ThreadExtensions.cs" :7 :8) {
^entry (%_thread : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\ThreadExtensions.cs" :7 :40)
cbde.store %_thread, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\ThreadExtensions.cs" :7 :40)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\ThreadExtensions.cs" :13 :12) // Not a variable of known type: thread
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\ThreadExtensions.cs" :13 :12) // thread.Abort() (InvocationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\ThreadExtensions.cs" :22 :12) // Not a variable of known type: thread
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\ThreadExtensions.cs" :22 :12) // thread.Join() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
