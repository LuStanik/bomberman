// Skipping function ReadSafely(none, none, i32, i32), it contains poisonous unsupported syntaxes

func @_Telepathy.NetworkStreamExtensions.ReadExactly$System.Net.Sockets.NetworkStream.byte$$.int$(none, none, i32) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :30 :8) {
^entry (%_stream : none, %_buffer : none, %_amount : i32):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :30 :39)
cbde.store %_stream, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :30 :39)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :30 :66)
cbde.store %_buffer, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :30 :66)
%2 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :30 :81)
cbde.store %_amount, %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :30 :81)
br ^0

^0: // SimpleBlock
%3 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :41 :28)
%4 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :41 :16) // bytesRead
cbde.store %3, %4 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :41 :16)
br ^1

^1: // BinaryBranchBlock
%5 = cbde.load %4 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :42 :19)
%6 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :42 :31)
%7 = cmpi "slt", %5, %6 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :42 :19)
cond_br %7, ^2, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :42 :19)

^2: // BinaryBranchBlock
%8 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :45 :32)
%9 = cbde.load %4 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :45 :41)
%10 = subi %8, %9 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :45 :32)
%11 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :45 :20) // remaining
cbde.store %10, %11 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :45 :20)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :46 :29) // Not a variable of known type: stream
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :46 :47) // Not a variable of known type: buffer
%14 = cbde.load %4 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :46 :55)
%15 = cbde.load %11 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :46 :66)
%16 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :46 :29) // stream.ReadSafely(buffer, bytesRead, remaining) (InvocationExpression)
%17 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :46 :20) // result
cbde.store %16, %17 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :46 :20)
%18 = cbde.load %17 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :49 :20)
%19 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :49 :30)
%20 = cmpi "eq", %18, %19 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :49 :20)
cond_br %20, ^4, ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :49 :20)

^4: // JumpBlock
%21 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :50 :27) // false
return %21 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :50 :20)

^5: // SimpleBlock
%22 = cbde.load %4 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :53 :16)
%23 = cbde.load %17 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :53 :29)
%24 = addi %22, %23 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :53 :16)
cbde.store %24, %4 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :53 :16)
br ^1

^3: // JumpBlock
%25 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :55 :19) // true
return %25 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\NetworkStreamExtensions.cs" :55 :12)

^6: // ExitBlock
cbde.unreachable

}
