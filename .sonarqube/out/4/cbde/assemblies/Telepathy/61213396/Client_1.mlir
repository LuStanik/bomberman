// Skipping function ReceiveThreadFunction(none, i32), it contains poisonous unsupported syntaxes

// Skipping function Connect(none, i32), it contains poisonous unsupported syntaxes

// Skipping function Disconnect(), it contains poisonous unsupported syntaxes

func @_Telepathy.Client.Send$byte$$$(none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Client.cs" :190 :8) {
^entry (%_data : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Client.cs" :190 :25)
cbde.store %_data, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Client.cs" :190 :25)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Client.cs" :192 :16) // Not a variable of known type: Connected
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Client.cs" :192 :16)

^1: // BinaryBranchBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Client.cs" :195 :20) // Not a variable of known type: data
%3 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Client.cs" :195 :20) // data.Length (SimpleMemberAccessExpression)
%4 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Client.cs" :195 :35) // Not a variable of known type: MaxMessageSize
%5 = cmpi "sle", %3, %4 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Client.cs" :195 :20)
cond_br %5, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Client.cs" :195 :20)

^3: // JumpBlock
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Client.cs" :200 :20) // Not a variable of known type: sendQueue
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Client.cs" :200 :38) // Not a variable of known type: data
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Client.cs" :200 :20) // sendQueue.Enqueue(data) (InvocationExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Client.cs" :202 :20) // Not a variable of known type: sendPending
%10 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Client.cs" :202 :20) // sendPending.Set() (InvocationExpression)
%11 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Client.cs" :203 :27) // true
return %11 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Client.cs" :203 :20)

^4: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Logger
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Client.cs" :205 :16) // Logger.LogError (SimpleMemberAccessExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Client.cs" :205 :65) // Not a variable of known type: data
%14 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Client.cs" :205 :65) // data.Length (SimpleMemberAccessExpression)
%15 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Client.cs" :205 :87) // Not a variable of known type: MaxMessageSize
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Client.cs" :205 :32) // $"Client.Send: message too big: {data.Length}. Limit: {MaxMessageSize}" (InterpolatedStringExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Client.cs" :205 :16) // Logger.LogError($"Client.Send: message too big: {data.Length}. Limit: {MaxMessageSize}") (InvocationExpression)
%18 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Client.cs" :206 :23) // false
return %18 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Client.cs" :206 :16)

^2: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Logger
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Client.cs" :208 :12) // Logger.LogWarning (SimpleMemberAccessExpression)
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Client.cs" :208 :30) // "Client.Send: not connected!" (StringLiteralExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Client.cs" :208 :12) // Logger.LogWarning("Client.Send: not connected!") (InvocationExpression)
%22 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Client.cs" :209 :19) // false
return %22 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Client.cs" :209 :12)

^5: // ExitBlock
cbde.unreachable

}
