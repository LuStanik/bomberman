func @_Telepathy.Utils.IntToBytesBigEndian$int$(i32) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :14 :8) {
^entry (%_value : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :14 :49)
cbde.store %_value, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :14 :49)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :16 :28) //  (OmittedArraySizeExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :16 :23) // byte[] (ArrayType)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :16 :19) // new byte[] {                 (byte)(value >> 24),                 (byte)(value >> 16),                 (byte)(value >> 8),                 (byte)value             } (ArrayCreationExpression)
%4 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :17 :23)
%5 = constant 24 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :17 :32)
%6 = cbde.neg %5 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :17 :23)
%7 = shlis %4, %6 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :17 :23)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :17 :16) // (byte)(value >> 24) (CastExpression)
%9 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :18 :23)
%10 = constant 16 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :18 :32)
%11 = cbde.neg %10 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :18 :23)
%12 = shlis %9, %11 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :18 :23)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :18 :16) // (byte)(value >> 16) (CastExpression)
%14 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :19 :23)
%15 = constant 8 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :19 :32)
%16 = cbde.neg %15 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :19 :23)
%17 = shlis %14, %16 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :19 :23)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :19 :16) // (byte)(value >> 8) (CastExpression)
%19 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :20 :22)
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :20 :16) // (byte)value (CastExpression)
return %3 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :16 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Telepathy.Utils.IntToBytesBigEndianNonAlloc$int.byte$$$(i32, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :26 :8) {
^entry (%_value : i32, %_bytes : none):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :26 :55)
cbde.store %_value, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :26 :55)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :26 :66)
cbde.store %_bytes, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :26 :66)
br ^0

^0: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :28 :12) // Not a variable of known type: bytes
%3 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :28 :18)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :28 :12) // bytes[0] (ElementAccessExpression)
%5 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :28 :30)
%6 = constant 24 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :28 :39)
%7 = cbde.neg %6 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :28 :30)
%8 = shlis %5, %7 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :28 :30)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :28 :23) // (byte)(value >> 24) (CastExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :29 :12) // Not a variable of known type: bytes
%11 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :29 :18)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :29 :12) // bytes[1] (ElementAccessExpression)
%13 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :29 :30)
%14 = constant 16 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :29 :39)
%15 = cbde.neg %14 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :29 :30)
%16 = shlis %13, %15 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :29 :30)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :29 :23) // (byte)(value >> 16) (CastExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :30 :12) // Not a variable of known type: bytes
%19 = constant 2 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :30 :18)
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :30 :12) // bytes[2] (ElementAccessExpression)
%21 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :30 :30)
%22 = constant 8 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :30 :39)
%23 = cbde.neg %22 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :30 :30)
%24 = shlis %21, %23 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :30 :30)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :30 :23) // (byte)(value >> 8) (CastExpression)
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :31 :12) // Not a variable of known type: bytes
%27 = constant 3 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :31 :18)
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :31 :12) // bytes[3] (ElementAccessExpression)
%29 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :31 :29)
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :31 :23) // (byte)value (CastExpression)
br ^1

^1: // ExitBlock
return

}
func @_Telepathy.Utils.BytesToIntBigEndian$byte$$$(none) -> i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :34 :8) {
^entry (%_bytes : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :34 :46)
cbde.store %_bytes, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :34 :46)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :37 :17) // Not a variable of known type: bytes
%2 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :37 :23)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :37 :17) // bytes[0] (ElementAccessExpression)
%4 = constant 24 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :37 :29)
%5 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :37 :17) // Binary expression on unsupported types bytes[0] << 24
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :38 :17) // Not a variable of known type: bytes
%7 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :38 :23)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :38 :17) // bytes[1] (ElementAccessExpression)
%9 = constant 16 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :38 :29)
%10 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :38 :17) // Binary expression on unsupported types bytes[1] << 16
%11 = or %5, %10 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :37 :16)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :39 :17) // Not a variable of known type: bytes
%13 = constant 2 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :39 :23)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :39 :17) // bytes[2] (ElementAccessExpression)
%15 = constant 8 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :39 :29)
%16 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :39 :17) // Binary expression on unsupported types bytes[2] << 8
%17 = or %11, %16 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :37 :16)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :40 :16) // Not a variable of known type: bytes
%19 = constant 3 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :40 :22)
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :40 :16) // bytes[3] (ElementAccessExpression)
%21 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :37 :16) // Binary expression on unsupported types (bytes[0] << 24) |                 (bytes[1] << 16) |                 (bytes[2] << 8) |                 bytes[3]
return %21 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\Telepathy\\Utils.cs" :36 :12)

^1: // ExitBlock
cbde.unreachable

}
