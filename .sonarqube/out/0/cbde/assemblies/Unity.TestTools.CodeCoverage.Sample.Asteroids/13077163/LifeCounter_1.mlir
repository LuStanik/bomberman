func @_LifeCounter.OnEnable$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :13 :4) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :15 :12) // Not a variable of known type: instance
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :15 :24) // null (NullLiteralExpression)
%2 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :15 :12) // comparison of unknown type: instance == null
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :15 :12)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :16 :23) // this (ThisExpression)
br ^3

^2: // BinaryBranchBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :17 :17) // Not a variable of known type: instance
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :17 :29) // this (ThisExpression)
%6 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :17 :17) // comparison of unknown type: instance != this
cond_br %6, ^4, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :17 :17)

^4: // SimpleBlock
// Entity from another assembly: Destroy
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :18 :20) // Identifier from another assembly: gameObject
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :18 :12) // Destroy(gameObject) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_LifeCounter.Start$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :21 :4) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :23 :16) // Identifier from another assembly: transform
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :23 :16) // transform.GetComponentsInChildren<Animator>() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_LifeCounter.SetLives$int$(i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :26 :4) {
^entry (%_currentLives : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :26 :25)
cbde.store %_currentLives, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :26 :25)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :28 :12)
%2 = constant 3 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :28 :27)
%3 = cmpi "sgt", %1, %2 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :28 :12)
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :28 :12)

^1: // SimpleBlock
%4 = constant 3 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :29 :27)
cbde.store %4, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :29 :12)
br ^2

^2: // BinaryBranchBlock
%5 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :31 :11)
%6 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :31 :27) // Not a variable of known type: previousLifeCount
%7 = cmpi "sle", %5, %6 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :31 :11)
cond_br %7, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :31 :11)

^3: // ForInitializerBlock
%8 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :33 :25)
%9 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :33 :21) // i
cbde.store %8, %9 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :33 :21)
br ^5

^5: // BinaryBranchBlock
%10 = cbde.load %9 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :33 :28)
%11 = constant 3 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :33 :32)
%12 = cmpi "slt", %10, %11 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :33 :28)
cond_br %12, ^6, ^7 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :33 :28)

^6: // BinaryBranchBlock
%13 = cbde.load %9 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :35 :20)
%14 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :35 :25)
%15 = cmpi "sge", %13, %14 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :35 :20)
cond_br %15, ^8, ^9 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :35 :20)

^8: // BinaryBranchBlock
%16 = cbde.load %9 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :37 :24)
%17 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :37 :28) // Not a variable of known type: previousLifeCount
%18 = cmpi "slt", %16, %17 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :37 :24)
cond_br %18, ^10, ^9 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :37 :24)

^10: // SimpleBlock
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :39 :24) // Not a variable of known type: lives
%20 = cbde.load %9 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :39 :30)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :39 :24) // lives[i] (ElementAccessExpression)
%22 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :39 :24) // lives[i].enabled (SimpleMemberAccessExpression)
%23 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :39 :43) // true
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :40 :24) // Not a variable of known type: lives
%25 = cbde.load %9 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :40 :30)
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :40 :24) // lives[i] (ElementAccessExpression)
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :40 :44) // "Remove" (StringLiteralExpression)
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :40 :24) // lives[i].SetTrigger("Remove") (InvocationExpression)
br ^9

^9: // SimpleBlock
%29 = cbde.load %9 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :33 :35)
%30 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :33 :35)
%31 = addi %29, %30 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :33 :35)
cbde.store %31, %9 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :33 :35)
br ^5

^4: // ForInitializerBlock
%32 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :47 :25)
%33 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :47 :21) // i
cbde.store %32, %33 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :47 :21)
br ^11

^11: // BinaryBranchBlock
%34 = cbde.load %33 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :47 :28)
%35 = constant 3 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :47 :32)
%36 = cmpi "slt", %34, %35 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :47 :28)
cond_br %36, ^12, ^7 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :47 :28)

^12: // BinaryBranchBlock
%37 = cbde.load %33 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :49 :20)
%38 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :49 :25)
%39 = cmpi "sle", %37, %38 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :49 :20)
cond_br %39, ^13, ^14 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :49 :20)

^13: // BinaryBranchBlock
%40 = cbde.load %33 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :51 :24)
%41 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :51 :29) // Not a variable of known type: previousLifeCount
%42 = cmpi "sge", %40, %41 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :51 :24)
cond_br %42, ^15, ^14 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :51 :24)

^15: // SimpleBlock
%43 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :53 :24) // Not a variable of known type: lives
%44 = cbde.load %33 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :53 :30)
%45 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :53 :24) // lives[i] (ElementAccessExpression)
%46 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :53 :24) // lives[i].enabled (SimpleMemberAccessExpression)
%47 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :53 :43) // true
%48 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :54 :24) // Not a variable of known type: lives
%49 = cbde.load %33 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :54 :30)
%50 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :54 :24) // lives[i] (ElementAccessExpression)
%51 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :54 :44) // "Recover" (StringLiteralExpression)
%52 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :54 :24) // lives[i].SetTrigger("Recover") (InvocationExpression)
br ^14

^14: // SimpleBlock
%53 = cbde.load %33 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :47 :35)
%54 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :47 :35)
%55 = addi %53, %54 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :47 :35)
cbde.store %55, %33 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :47 :35)
br ^11

^7: // SimpleBlock
%56 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\LifeCounter.cs" :60 :28)
br ^16

^16: // ExitBlock
return

}
