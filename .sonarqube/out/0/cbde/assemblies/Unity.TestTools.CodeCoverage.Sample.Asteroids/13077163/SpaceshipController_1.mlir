func @_SpaceshipController.Update$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :19 :4) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GameManager
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :21 :13) // GameManager.IsPaused (SimpleMemberAccessExpression)
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :21 :12) // !GameManager.IsPaused (LogicalNotExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :21 :12)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Move
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :22 :12) // Move() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_SpaceshipController.Move$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :25 :4) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: Input
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :27 :45) // "Horizontal" (StringLiteralExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :27 :31) // Input.GetAxis("Horizontal") (InvocationExpression)
// Entity from another assembly: Input
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :28 :43) // "Vertical" (StringLiteralExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :28 :29) // Input.GetAxis("Vertical") (InvocationExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :30 :12) // Not a variable of known type: verticalAxis
%7 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :30 :27) // 0.0f (NumericLiteralExpression)
%8 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :30 :12) // comparison of unknown type: verticalAxis > 0.0f
cond_br %8, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :30 :12)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Thrust
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :31 :19) // Not a variable of known type: verticalAxis
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :31 :12) // Thrust(verticalAxis) (InvocationExpression)
br ^2

^2: // BinaryBranchBlock
// Entity from another assembly: Mathf
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :32 :22) // Not a variable of known type: horizontalAxis
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :32 :12) // Mathf.Abs(horizontalAxis) (InvocationExpression)
%13 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :32 :40) // 0.0f (NumericLiteralExpression)
%14 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :32 :12) // comparison of unknown type: Mathf.Abs(horizontalAxis) > 0.0f
cond_br %14, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :32 :12)

^3: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Turn
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :33 :17) // Not a variable of known type: horizontalAxis
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :33 :12) // Turn(horizontalAxis) (InvocationExpression)
br ^4

^4: // BinaryBranchBlock
// Entity from another assembly: Input
// Entity from another assembly: KeyCode
%17 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :34 :29) // KeyCode.Space (SimpleMemberAccessExpression)
%18 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :34 :12) // Input.GetKeyDown(KeyCode.Space) (InvocationExpression)
cond_br %18, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :34 :12)

^5: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Shoot
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :35 :12) // Shoot() (InvocationExpression)
br ^6

^6: // SimpleBlock
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :37 :8) // Identifier from another assembly: transform
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :37 :8) // transform.position (SimpleMemberAccessExpression)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :37 :39) // Not a variable of known type: direction
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :37 :30) // (Vector3)direction (CastExpression)
// Entity from another assembly: Time
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :37 :51) // Time.deltaTime (SimpleMemberAccessExpression)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :37 :30) // Binary expression on unsupported types (Vector3)direction * Time.deltaTime
%26 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :37 :68) // 4.0f (NumericLiteralExpression)
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :37 :30) // Binary expression on unsupported types (Vector3)direction * Time.deltaTime * 4.0f
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :37 :8) // Binary expression on unsupported types transform.position += (Vector3)direction * Time.deltaTime * 4.0f
// No identifier name for binary assignment expression
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CalculatePositionOnCamera
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :38 :8) // CalculatePositionOnCamera() (InvocationExpression)
br ^7

^7: // ExitBlock
return

}
func @_SpaceshipController.Thrust$float$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :41 :4) {
^entry (%_power : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :41 :23)
cbde.store %_power, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :41 :23)
br ^0

^0: // SimpleBlock
// Entity from another assembly: Vector2
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :43 :33) // Not a variable of known type: direction
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :43 :44) // Identifier from another assembly: transform
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :43 :44) // transform.up (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :43 :59) // Not a variable of known type: power
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :43 :44) // Binary expression on unsupported types transform.up * power
// Entity from another assembly: Time
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :43 :66) // Time.deltaTime (SimpleMemberAccessExpression)
%7 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :43 :83) // 4.0f (NumericLiteralExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :43 :66) // Binary expression on unsupported types Time.deltaTime * 4.0f
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :43 :20) // Vector2.Lerp(direction, transform.up * power, Time.deltaTime * 4.0f) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_SpaceshipController.Turn$float$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :46 :4) {
^entry (%_delta : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :46 :21)
cbde.store %_delta, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :46 :21)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :48 :8) // Identifier from another assembly: transform
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :48 :8) // transform.eulerAngles (SimpleMemberAccessExpression)
%3 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :48 :44) // 0.0f (NumericLiteralExpression)
%4 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :48 :50) // 0.0f (NumericLiteralExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :48 :56) // Identifier from another assembly: transform
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :48 :56) // transform.eulerAngles (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :48 :56) // transform.eulerAngles.z (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :48 :82) // Not a variable of known type: delta
%9 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :48 :90) // 150.0f (NumericLiteralExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :48 :82) // Binary expression on unsupported types delta * 150.0f
// Entity from another assembly: Time
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :48 :99) // Time.deltaTime (SimpleMemberAccessExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :48 :82) // Binary expression on unsupported types delta * 150.0f * Time.deltaTime
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :48 :56) // Binary expression on unsupported types transform.eulerAngles.z - delta * 150.0f * Time.deltaTime
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :48 :32) // new Vector3(0.0f, 0.0f, transform.eulerAngles.z - delta * 150.0f * Time.deltaTime) (ObjectCreationExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :49 :8) // Identifier from another assembly: transform
%16 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :49 :27)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :49 :8) // transform.GetChild(0) (InvocationExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :49 :8) // transform.GetChild(0).localEulerAngles (SimpleMemberAccessExpression)
%19 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :49 :61) // 0.0f (NumericLiteralExpression)
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :49 :68) // Not a variable of known type: delta
%21 = cbde.neg %20 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :49 :67)
%22 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :49 :76) // 30.0f (NumericLiteralExpression)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :49 :67) // Binary expression on unsupported types -delta * 30.0f
%24 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :49 :83) // 0.0f (NumericLiteralExpression)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :49 :49) // new Vector3(0.0f, -delta * 30.0f, 0.0f) (ObjectCreationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function Shoot(i1), it contains poisonous unsupported syntaxes

func @_SpaceshipController.OnTriggerEnter2D$UnityEngine.Collider2D$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :76 :4) {
^entry (%_collision : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :76 :34)
cbde.store %_collision, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :76 :34)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :79 :12) // Not a variable of known type: isColliding
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :79 :12)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :80 :12)

^2: // BinaryBranchBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :82 :48) // Not a variable of known type: collision
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :82 :48) // collision.gameObject (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :82 :48) // collision.gameObject.GetComponent<AsteroidController>() (InvocationExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :84 :12) // Not a variable of known type: asteroidController
%7 = cbde.unknown : i1 // Creating necessary bool for conversion
cond_br %7, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :84 :12)

^3: // BinaryBranchBlock
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :86 :12) // Not a variable of known type: asteroidController
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :86 :12) // asteroidController.Split() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GameManager
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :87 :15) // GameManager.instance (SimpleMemberAccessExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :87 :39) // null (NullLiteralExpression)
%12 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :87 :15) // comparison of unknown type: GameManager.instance != null
cond_br %12, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :87 :15)

^5: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GameManager
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :88 :16) // GameManager.instance (SimpleMemberAccessExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :88 :16) // GameManager.instance.RespawnShip() (InvocationExpression)
br ^6

^6: // SimpleBlock
// Entity from another assembly: Instantiate
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :89 :24) // Not a variable of known type: spaceshipDebris
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :89 :41) // Identifier from another assembly: transform
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :89 :41) // transform.position (SimpleMemberAccessExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :89 :61) // Identifier from another assembly: transform
%19 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :89 :80)
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :89 :61) // transform.GetChild(0) (InvocationExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :89 :61) // transform.GetChild(0).rotation (SimpleMemberAccessExpression)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :89 :12) // Instantiate(spaceshipDebris, transform.position, transform.GetChild(0).rotation) (InvocationExpression)
// Entity from another assembly: Destroy
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :90 :20) // Identifier from another assembly: gameObject
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :90 :12) // Destroy(gameObject) (InvocationExpression)
%25 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :91 :26) // true
br ^4

^4: // ExitBlock
return

}
func @_SpaceshipController.CalculatePositionOnCamera$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :95 :4) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: Camera
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :97 :12) // Camera.main (SimpleMemberAccessExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :97 :27) // null (NullLiteralExpression)
%2 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :97 :12) // comparison of unknown type: Camera.main != null
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :97 :12)

^1: // BinaryBranchBlock
%3 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :99 :26) // false
%4 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :99 :17) // warped
cbde.store %3, %4 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :99 :17)
// Entity from another assembly: Camera
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :101 :39) // Camera.main (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :101 :72) // Identifier from another assembly: transform
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :101 :72) // transform.position (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :101 :39) // Camera.main.WorldToViewportPoint(transform.position) (InvocationExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :102 :16) // Not a variable of known type: positionOnCamera
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :102 :16) // positionOnCamera.x (SimpleMemberAccessExpression)
%12 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :102 :37) // 1.05f (NumericLiteralExpression)
%13 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :102 :16) // comparison of unknown type: positionOnCamera.x > 1.05f
cond_br %13, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :102 :16)

^3: // SimpleBlock
%14 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :104 :25) // true
cbde.store %14, %4 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :104 :16)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :105 :16) // Identifier from another assembly: transform
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :105 :16) // transform.position (SimpleMemberAccessExpression)
// Entity from another assembly: Camera
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :105 :46) // Camera.main (SimpleMemberAccessExpression)
%18 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :105 :92) // 0.05f (NumericLiteralExpression)
%19 = cbde.neg %18 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :105 :91)
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :105 :99) // Not a variable of known type: positionOnCamera
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :105 :99) // positionOnCamera.y (SimpleMemberAccessExpression)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :105 :79) // new Vector2(-0.05f, positionOnCamera.y) (ObjectCreationExpression)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :105 :46) // Camera.main.ViewportToWorldPoint(new Vector2(-0.05f, positionOnCamera.y)) (InvocationExpression)
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :105 :37) // (Vector2)Camera.main.ViewportToWorldPoint(new Vector2(-0.05f, positionOnCamera.y)) (CastExpression)
br ^5

^4: // BinaryBranchBlock
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :107 :21) // Not a variable of known type: positionOnCamera
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :107 :21) // positionOnCamera.x (SimpleMemberAccessExpression)
%27 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :107 :43) // 0.05f (NumericLiteralExpression)
%28 = cbde.neg %27 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :107 :42)
%29 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :107 :21) // comparison of unknown type: positionOnCamera.x < -0.05f
cond_br %29, ^6, ^7 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :107 :21)

^6: // SimpleBlock
%30 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :109 :25) // true
cbde.store %30, %4 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :109 :16)
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :110 :16) // Identifier from another assembly: transform
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :110 :16) // transform.position (SimpleMemberAccessExpression)
// Entity from another assembly: Camera
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :110 :46) // Camera.main (SimpleMemberAccessExpression)
%34 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :110 :91) // 1.05f (NumericLiteralExpression)
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :110 :98) // Not a variable of known type: positionOnCamera
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :110 :98) // positionOnCamera.y (SimpleMemberAccessExpression)
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :110 :79) // new Vector2(1.05f, positionOnCamera.y) (ObjectCreationExpression)
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :110 :46) // Camera.main.ViewportToWorldPoint(new Vector2(1.05f, positionOnCamera.y)) (InvocationExpression)
%39 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :110 :37) // (Vector2)Camera.main.ViewportToWorldPoint(new Vector2(1.05f, positionOnCamera.y)) (CastExpression)
br ^5

^7: // BinaryBranchBlock
%40 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :112 :21) // Not a variable of known type: positionOnCamera
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :112 :21) // positionOnCamera.y (SimpleMemberAccessExpression)
%42 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :112 :42) // 1.05f (NumericLiteralExpression)
%43 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :112 :21) // comparison of unknown type: positionOnCamera.y > 1.05f
cond_br %43, ^8, ^9 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :112 :21)

^8: // SimpleBlock
%44 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :114 :25) // true
cbde.store %44, %4 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :114 :16)
%45 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :115 :16) // Identifier from another assembly: transform
%46 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :115 :16) // transform.position (SimpleMemberAccessExpression)
// Entity from another assembly: Camera
%47 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :115 :46) // Camera.main (SimpleMemberAccessExpression)
%48 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :115 :91) // Not a variable of known type: positionOnCamera
%49 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :115 :91) // positionOnCamera.x (SimpleMemberAccessExpression)
%50 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :115 :112) // 0.05f (NumericLiteralExpression)
%51 = cbde.neg %50 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :115 :111)
%52 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :115 :79) // new Vector2(positionOnCamera.x, -0.05f) (ObjectCreationExpression)
%53 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :115 :46) // Camera.main.ViewportToWorldPoint(new Vector2(positionOnCamera.x, -0.05f)) (InvocationExpression)
%54 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :115 :37) // (Vector2)Camera.main.ViewportToWorldPoint(new Vector2(positionOnCamera.x, -0.05f)) (CastExpression)
br ^5

^9: // BinaryBranchBlock
%55 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :117 :21) // Not a variable of known type: positionOnCamera
%56 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :117 :21) // positionOnCamera.y (SimpleMemberAccessExpression)
%57 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :117 :43) // 0.05f (NumericLiteralExpression)
%58 = cbde.neg %57 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :117 :42)
%59 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :117 :21) // comparison of unknown type: positionOnCamera.y < -0.05f
cond_br %59, ^10, ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :117 :21)

^10: // SimpleBlock
%60 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :119 :25) // true
cbde.store %60, %4 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :119 :16)
%61 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :120 :16) // Identifier from another assembly: transform
%62 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :120 :16) // transform.position (SimpleMemberAccessExpression)
// Entity from another assembly: Camera
%63 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :120 :46) // Camera.main (SimpleMemberAccessExpression)
%64 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :120 :91) // Not a variable of known type: positionOnCamera
%65 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :120 :91) // positionOnCamera.x (SimpleMemberAccessExpression)
%66 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :120 :111) // 1.05f (NumericLiteralExpression)
%67 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :120 :79) // new Vector2(positionOnCamera.x, 1.05f) (ObjectCreationExpression)
%68 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :120 :46) // Camera.main.ViewportToWorldPoint(new Vector2(positionOnCamera.x, 1.05f)) (InvocationExpression)
%69 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :120 :37) // (Vector2)Camera.main.ViewportToWorldPoint(new Vector2(positionOnCamera.x, 1.05f)) (CastExpression)
br ^5

^5: // BinaryBranchBlock
%70 = cbde.load %4 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :123 :16)
cond_br %70, ^11, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :123 :16)

^11: // SimpleBlock
%71 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :125 :16) // Identifier from another assembly: transform
%72 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :125 :35)
%73 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :125 :16) // transform.GetChild(0) (InvocationExpression)
%74 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :125 :47)
%75 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :125 :16) // transform.GetChild(0).GetChild(0) (InvocationExpression)
%76 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :125 :16) // transform.GetChild(0).GetChild(0).GetComponent<EngineTrail>() (InvocationExpression)
%77 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :125 :16) // transform.GetChild(0).GetChild(0).GetComponent<EngineTrail>().ClearParticles() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_SpaceshipController.UpdateWeapon$int$(i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :130 :4) {
^entry (%_score : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :130 :29)
cbde.store %_score, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :130 :29)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :132 :24) // Not a variable of known type: Weapon
%2 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :132 :24) // Weapon.Basic (SimpleMemberAccessExpression)
%4 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :134 :12)
%5 = constant 8000 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :134 :21)
%6 = cmpi "sge", %4, %5 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :134 :12)
cond_br %6, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :134 :12)

^1: // SimpleBlock
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :135 :21) // Not a variable of known type: Weapon
%8 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :135 :21) // Weapon.Laser (SimpleMemberAccessExpression)
br ^2

^2: // BinaryBranchBlock
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :137 :12) // Not a variable of known type: weapon
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :137 :22) // Not a variable of known type: currentWeapon
%11 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :137 :12) // comparison of unknown type: weapon == currentWeapon
cond_br %11, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :137 :12)

^3: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :138 :12)

^4: // SimpleBlock
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Controllers\\SpaceshipController.cs" :140 :24) // Not a variable of known type: weapon
br ^5

^5: // ExitBlock
return

}
