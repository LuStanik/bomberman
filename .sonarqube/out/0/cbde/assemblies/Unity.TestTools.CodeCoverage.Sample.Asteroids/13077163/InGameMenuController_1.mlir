func @_InGameMenuController.Start$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\InGameMenuController.cs" :7 :1) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\InGameMenuController.cs" :9 :20) // Identifier from another assembly: transform
%1 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\InGameMenuController.cs" :9 :39)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\InGameMenuController.cs" :9 :20) // transform.GetChild(0) (InvocationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\InGameMenuController.cs" :9 :20) // transform.GetChild(0).gameObject (SimpleMemberAccessExpression)
br ^1

^1: // ExitBlock
return

}
func @_InGameMenuController.Update$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\InGameMenuController.cs" :12 :4) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: Input
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\InGameMenuController.cs" :14 :32) // "Cancel" (StringLiteralExpression)
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\InGameMenuController.cs" :14 :12) // Input.GetButtonDown("Cancel") (InvocationExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\InGameMenuController.cs" :14 :12)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ChangeMenuState
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\InGameMenuController.cs" :15 :29) // Not a variable of known type: pauseMenuActive
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\InGameMenuController.cs" :15 :28) // !pauseMenuActive (LogicalNotExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\InGameMenuController.cs" :15 :12) // ChangeMenuState(!pauseMenuActive) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_InGameMenuController.ChangeMenuState$bool$(i1) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\InGameMenuController.cs" :18 :4) {
^entry (%_isPaused : i1):
%0 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\InGameMenuController.cs" :18 :32)
cbde.store %_isPaused, %0 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\InGameMenuController.cs" :18 :32)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\InGameMenuController.cs" :20 :12) // Not a variable of known type: pauseMenuActive
%2 = cbde.load %0 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\InGameMenuController.cs" :20 :31)
%3 = cmpi "eq", %1, %2 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\InGameMenuController.cs" :20 :12)
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\InGameMenuController.cs" :20 :12)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\InGameMenuController.cs" :21 :12)

^2: // SimpleBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\InGameMenuController.cs" :23 :8) // Not a variable of known type: pauseMenu
%5 = cbde.load %0 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\InGameMenuController.cs" :23 :28)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\InGameMenuController.cs" :23 :8) // pauseMenu.SetActive(isPaused) (InvocationExpression)
%7 = cbde.load %0 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\InGameMenuController.cs" :25 :26)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GameManager
%8 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\InGameMenuController.cs" :26 :8) // GameManager.IsPaused (SimpleMemberAccessExpression)
%9 = cbde.load %0 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\UI\\InGameMenuController.cs" :26 :31)
br ^3

^3: // ExitBlock
return

}
