func @_GameManager.InitializeTestingEnvironment$bool.bool.bool.bool.bool$(i1, i1, i1, i1, i1) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :12 :4) {
^entry (%_start : i1, %_update : i1, %_effects : i1, %_asteroids : i1, %_paused : i1):
%0 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :12 :52)
cbde.store %_start, %0 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :12 :52)
%1 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :12 :64)
cbde.store %_update, %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :12 :64)
%2 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :12 :77)
cbde.store %_effects, %2 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :12 :77)
%3 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :12 :91)
cbde.store %_asteroids, %3 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :12 :91)
%4 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :12 :107)
cbde.store %_paused, %4 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :12 :107)
br ^0

^0: // SimpleBlock
%5 = cbde.load %2 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :14 :25)
%6 = cbde.load %0 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :15 :23)
%7 = cbde.load %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :16 :24)
%8 = cbde.load %3 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :17 :27)
%9 = cbde.load %4 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :18 :19)
br ^1

^1: // ExitBlock
return

}
func @_GameManager.SpaceshipIsActive$$() -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :46 :4) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :46 :52) // Not a variable of known type: spaceship
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :46 :65) // null (NullLiteralExpression)
%2 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :46 :52) // comparison of unknown type: spaceship != null
return %2 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :46 :45)

^1: // ExitBlock
cbde.unreachable

}
func @_GameManager.OnEnable$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :48 :4) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :50 :12) // Not a variable of known type: instance
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :50 :24) // null (NullLiteralExpression)
%2 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :50 :12) // comparison of unknown type: instance == null
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :50 :12)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :51 :23) // this (ThisExpression)
br ^3

^2: // BinaryBranchBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :52 :17) // Not a variable of known type: instance
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :52 :29) // this (ThisExpression)
%6 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :52 :17) // comparison of unknown type: instance != this
cond_br %6, ^4, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :52 :17)

^4: // SimpleBlock
// Entity from another assembly: Destroy
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :53 :20) // Identifier from another assembly: gameObject
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :53 :12) // Destroy(gameObject) (InvocationExpression)
br ^3

^3: // SimpleBlock
// Entity from another assembly: Time
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :55 :8) // Time.timeScale (SimpleMemberAccessExpression)
%10 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :55 :25) // 1.0f (NumericLiteralExpression)
%11 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :56 :19) // false
%12 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :57 :16)
%13 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :58 :30)
br ^5

^5: // ExitBlock
return

}
func @_GameManager.Start$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :61 :4) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :63 :12) // Not a variable of known type: startEnabled
cond_br %0, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :63 :12)

^1: // SimpleBlock
// Entity from another assembly: Instantiate
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :65 :36) // Not a variable of known type: spaceshipPrefab
// Entity from another assembly: Vector3
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :65 :53) // Vector3.zero (SimpleMemberAccessExpression)
// Entity from another assembly: Quaternion
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :65 :67) // Quaternion.identity (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :65 :24) // Instantiate(spaceshipPrefab, Vector3.zero, Quaternion.identity) (InvocationExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :65 :24) // Instantiate(spaceshipPrefab, Vector3.zero, Quaternion.identity).GetComponent<SpaceshipController>() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
// Skipping function Update(), it contains poisonous unsupported syntaxes

func @_GameManager.UpdateTimers$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :79 :4) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :81 :12) // Not a variable of known type: asteroidSpawnTimer
%1 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :81 :33) // 0.0f (NumericLiteralExpression)
%2 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :81 :12) // comparison of unknown type: asteroidSpawnTimer > 0.0f
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :81 :12)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :82 :12) // Not a variable of known type: asteroidSpawnTimer
// Entity from another assembly: Time
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :82 :34) // Time.deltaTime (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :82 :12) // Binary expression on unsupported types asteroidSpawnTimer -= Time.deltaTime
br ^2

^2: // ExitBlock
return

}
func @_GameManager.RespawnShip$float$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :85 :4) {
^entry (%_delay : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :85 :28)
cbde.store %_delay, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :85 :28)
br ^0

^0: // SimpleBlock
// Entity from another assembly: StartCoroutine
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: RespawnShipCoroutine
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :87 :44) // Not a variable of known type: delay
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :87 :23) // RespawnShipCoroutine(delay) (InvocationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Samples\\Code Coverage\\0.4.0-preview\\Code Coverage Workshop\\Asteroids\\Scripts\\Managers\\GameManager.cs" :87 :8) // StartCoroutine(RespawnShipCoroutine(delay)) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function RespawnShipCoroutine(none), it contains poisonous unsupported syntaxes

// Skipping function SpawnAsteroids(), it contains poisonous unsupported syntaxes

// Skipping function ReloadScene(), it contains poisonous unsupported syntaxes

// Skipping function AddToScore(i32), it contains poisonous unsupported syntaxes

