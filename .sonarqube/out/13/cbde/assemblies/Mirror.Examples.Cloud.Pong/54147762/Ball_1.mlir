func @_Mirror.Cloud.Examples.Pong.Ball.OnStartServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Ball.cs" :9 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Ball.cs" :11 :12) // base (BaseExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Ball.cs" :11 :12) // base.OnStartServer() (InvocationExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Ball.cs" :14 :12) // Not a variable of known type: rigidbody2d
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Ball.cs" :14 :12) // rigidbody2d.simulated (SimpleMemberAccessExpression)
%4 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Ball.cs" :14 :36) // true
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Ball.cs" :17 :12) // Not a variable of known type: rigidbody2d
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Ball.cs" :17 :12) // rigidbody2d.velocity (SimpleMemberAccessExpression)
// Entity from another assembly: Vector2
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Ball.cs" :17 :35) // Vector2.right (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Ball.cs" :17 :51) // Not a variable of known type: speed
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Ball.cs" :17 :35) // Binary expression on unsupported types Vector2.right * speed
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Cloud.Examples.Pong.Ball.HitFactor$UnityEngine.Vector2.UnityEngine.Vector2.float$(none, none, none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Ball.cs" :20 :8) {
^entry (%_ballPos : none, %_racketPos : none, %_racketHeight : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Ball.cs" :20 :24)
cbde.store %_ballPos, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Ball.cs" :20 :24)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Ball.cs" :20 :41)
cbde.store %_racketPos, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Ball.cs" :20 :41)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Ball.cs" :20 :60)
cbde.store %_racketHeight, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Ball.cs" :20 :60)
br ^0

^0: // JumpBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Ball.cs" :28 :20) // Not a variable of known type: ballPos
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Ball.cs" :28 :20) // ballPos.y (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Ball.cs" :28 :32) // Not a variable of known type: racketPos
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Ball.cs" :28 :32) // racketPos.y (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Ball.cs" :28 :20) // Binary expression on unsupported types ballPos.y - racketPos.y
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Ball.cs" :28 :47) // Not a variable of known type: racketHeight
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Ball.cs" :28 :19) // Binary expression on unsupported types (ballPos.y - racketPos.y) / racketHeight
return %9 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Ball.cs" :28 :12)

^1: // ExitBlock
cbde.unreachable

}
// Skipping function OnCollisionEnter2D(none), it contains poisonous unsupported syntaxes

