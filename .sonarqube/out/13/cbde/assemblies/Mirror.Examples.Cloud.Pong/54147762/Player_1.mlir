func @_Mirror.Cloud.Examples.Pong.Player.FixedUpdate$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Player.cs" :10 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Player.cs" :14 :17) // Identifier from another assembly: isLocalPlayer
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Player.cs" :14 :16) // !isLocalPlayer (LogicalNotExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Player.cs" :14 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Player.cs" :15 :16)

^2: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Player.cs" :17 :12) // Not a variable of known type: rigidbody2d
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Player.cs" :17 :12) // rigidbody2d.velocity (SimpleMemberAccessExpression)
%4 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Player.cs" :17 :47)
// Entity from another assembly: Input
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Player.cs" :17 :67) // "Vertical" (StringLiteralExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Player.cs" :17 :50) // Input.GetAxisRaw("Vertical") (InvocationExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Player.cs" :17 :35) // new Vector2(0, Input.GetAxisRaw("Vertical")) (ObjectCreationExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Player.cs" :17 :82) // Not a variable of known type: speed
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Player.cs" :17 :35) // Binary expression on unsupported types new Vector2(0, Input.GetAxisRaw("Vertical")) * speed
// Entity from another assembly: Time
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Player.cs" :17 :90) // Time.fixedDeltaTime (SimpleMemberAccessExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\Player.cs" :17 :35) // Binary expression on unsupported types new Vector2(0, Input.GetAxisRaw("Vertical")) * speed * Time.fixedDeltaTime
br ^3

^3: // ExitBlock
return

}
