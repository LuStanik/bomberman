func @_Mirror.Cloud.Examples.Pong.BallManager.OnStartServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :11 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: NetworkManager
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :13 :23) // NetworkManager.singleton (SimpleMemberAccessExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :13 :23) // NetworkManager.singleton as NetworkManagerListServerPong (AsExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :14 :12) // Not a variable of known type: manager
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :14 :12) // manager.onPlayerListChanged (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: onPlayerListChanged
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :14 :12) // Binary expression on unsupported types manager.onPlayerListChanged += onPlayerListChanged
// No identifier name for binary assignment expression
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Cloud.Examples.Pong.BallManager.OnStopServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :16 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :18 :12) // Not a variable of known type: manager
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :18 :12) // manager.onPlayerListChanged (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: onPlayerListChanged
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :18 :12) // Binary expression on unsupported types manager.onPlayerListChanged -= onPlayerListChanged
// No identifier name for binary assignment expression
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Cloud.Examples.Pong.BallManager.onPlayerListChanged$int$(i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :21 :8) {
^entry (%_playerCount : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :21 :41)
cbde.store %_playerCount, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :21 :41)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :23 :16)
%2 = constant 2 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :23 :31)
%3 = cmpi "sge", %1, %2 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :23 :16)
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :23 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SpawnBall
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :25 :16) // SpawnBall() (InvocationExpression)
br ^2

^2: // BinaryBranchBlock
%5 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :27 :16)
%6 = constant 2 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :27 :30)
%7 = cmpi "slt", %5, %6 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :27 :16)
cond_br %7, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :27 :16)

^3: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: DestroyBall
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :29 :16) // DestroyBall() (InvocationExpression)
br ^4

^4: // ExitBlock
return

}
func @_Mirror.Cloud.Examples.Pong.BallManager.SpawnBall$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :33 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :35 :16) // Not a variable of known type: ball
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :35 :24) // null (NullLiteralExpression)
%2 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :35 :16) // comparison of unknown type: ball != null
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :35 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :36 :16)

^2: // SimpleBlock
// Entity from another assembly: Instantiate
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :38 :31) // Not a variable of known type: ballPrefab
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :38 :19) // Instantiate(ballPrefab) (InvocationExpression)
// Entity from another assembly: NetworkServer
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :39 :32) // Not a variable of known type: ball
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :39 :12) // NetworkServer.Spawn(ball) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Cloud.Examples.Pong.BallManager.DestroyBall$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :42 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :44 :16) // Not a variable of known type: ball
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :44 :24) // null (NullLiteralExpression)
%2 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :44 :16) // comparison of unknown type: ball == null
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :44 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :45 :16)

^2: // SimpleBlock
// Entity from another assembly: NetworkServer
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :48 :34) // Not a variable of known type: ball
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :48 :12) // NetworkServer.Destroy(ball) (InvocationExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\PongWithListServer\\Scripts\\BallManager.cs" :49 :19) // null (NullLiteralExpression)
br ^3

^3: // ExitBlock
return

}
