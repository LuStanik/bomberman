func @_Mirror.Experimental.NetworkLerpRigidbody.OnValidate$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :35 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :37 :16) // Not a variable of known type: target
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :37 :26) // null (NullLiteralExpression)
%2 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :37 :16) // comparison of unknown type: target == null
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :37 :16)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :39 :25) // GetComponent<Rigidbody>() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkLerpRigidbody.Update$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :43 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :45 :16) // Identifier from another assembly: isServer
cond_br %0, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :45 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SyncToClients
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :47 :16) // SyncToClients() (InvocationExpression)
br ^3

^2: // BinaryBranchBlock
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :49 :21) // Not a variable of known type: ClientWithAuthority
cond_br %2, ^4, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :49 :21)

^4: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SendToServer
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :51 :16) // SendToServer() (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkLerpRigidbody.SyncToClients$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :55 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :57 :29) // Not a variable of known type: target
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :57 :29) // target.velocity (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :58 :29) // Not a variable of known type: target
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :58 :29) // target.position (SimpleMemberAccessExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkLerpRigidbody.SendToServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :61 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: Time
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :63 :24) // Time.time (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :64 :16) // Not a variable of known type: now
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :64 :22) // Not a variable of known type: nextSyncTime
%4 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :64 :16) // comparison of unknown type: now > nextSyncTime
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :64 :16)

^1: // SimpleBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :66 :31) // Not a variable of known type: now
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :66 :37) // Identifier from another assembly: syncInterval
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :66 :31) // Binary expression on unsupported types now + syncInterval
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CmdSendState
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :67 :29) // Not a variable of known type: target
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :67 :29) // target.velocity (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :67 :46) // Not a variable of known type: target
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :67 :46) // target.position (SimpleMemberAccessExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :67 :16) // CmdSendState(target.velocity, target.position) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkLerpRigidbody.CmdSendState$UnityEngine.Vector3.UnityEngine.Vector3$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :71 :8) {
^entry (%_velocity : none, %_position : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :72 :34)
cbde.store %_velocity, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :72 :34)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :72 :52)
cbde.store %_position, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :72 :52)
br ^0

^0: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :74 :12) // Not a variable of known type: target
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :74 :12) // target.velocity (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :74 :30) // Not a variable of known type: velocity
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :75 :12) // Not a variable of known type: target
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :75 :12) // target.position (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :75 :30) // Not a variable of known type: position
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :76 :29) // Not a variable of known type: velocity
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :77 :29) // Not a variable of known type: position
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkLerpRigidbody.FixedUpdate$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :80 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :82 :16) // Not a variable of known type: IgnoreSync
cond_br %0, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :82 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :82 :30)

^2: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :84 :12) // Not a variable of known type: target
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :84 :12) // target.velocity (SimpleMemberAccessExpression)
// Entity from another assembly: Vector3
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :84 :43) // Not a variable of known type: target
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :84 :43) // target.velocity (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :84 :60) // Not a variable of known type: targetVelocity
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :84 :76) // Not a variable of known type: lerpVelocityAmount
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :84 :30) // Vector3.Lerp(target.velocity, targetVelocity, lerpVelocityAmount) (InvocationExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :85 :12) // Not a variable of known type: target
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :85 :12) // target.position (SimpleMemberAccessExpression)
// Entity from another assembly: Vector3
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :85 :43) // Not a variable of known type: target
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :85 :43) // target.position (SimpleMemberAccessExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :85 :60) // Not a variable of known type: targetPosition
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :85 :76) // Not a variable of known type: lerpPositionAmount
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :85 :30) // Vector3.Lerp(target.position, targetPosition, lerpPositionAmount) (InvocationExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :87 :12) // Not a variable of known type: targetPosition
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :87 :30) // Not a variable of known type: target
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :87 :30) // target.velocity (SimpleMemberAccessExpression)
// Entity from another assembly: Time
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :87 :48) // Time.fixedDeltaTime (SimpleMemberAccessExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :87 :30) // Binary expression on unsupported types target.velocity * Time.fixedDeltaTime
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkLerpRigidbody.cs" :87 :12) // Binary expression on unsupported types targetPosition += target.velocity * Time.fixedDeltaTime
br ^3

^3: // ExitBlock
return

}
