func @_Mirror.Discovery.NetworkDiscoveryHUD.OnValidate$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscoveryHUD.cs" :17 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscoveryHUD.cs" :19 :16) // Not a variable of known type: networkDiscovery
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscoveryHUD.cs" :19 :36) // null (NullLiteralExpression)
%2 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscoveryHUD.cs" :19 :16) // comparison of unknown type: networkDiscovery == null
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscoveryHUD.cs" :19 :16)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscoveryHUD.cs" :21 :35) // GetComponent<NetworkDiscovery>() (InvocationExpression)
// Entity from another assembly: UnityEditor
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscoveryHUD.cs" :22 :16) // UnityEditor.Events.UnityEventTools (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscoveryHUD.cs" :22 :73) // Not a variable of known type: networkDiscovery
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscoveryHUD.cs" :22 :73) // networkDiscovery.OnServerFound (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnDiscoveredServer
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscoveryHUD.cs" :22 :16) // UnityEditor.Events.UnityEventTools.AddPersistentListener(networkDiscovery.OnServerFound, OnDiscoveredServer) (InvocationExpression)
// Entity from another assembly: UnityEditor
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscoveryHUD.cs" :23 :16) // UnityEditor.Undo (SimpleMemberAccessExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscoveryHUD.cs" :23 :58) //  (OmittedArraySizeExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscoveryHUD.cs" :23 :51) // Object[] (ArrayType)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscoveryHUD.cs" :23 :47) // new Object[] { this, networkDiscovery } (ArrayCreationExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscoveryHUD.cs" :23 :62) // this (ThisExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscoveryHUD.cs" :23 :68) // Not a variable of known type: networkDiscovery
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscoveryHUD.cs" :23 :88) // "Set NetworkDiscovery" (StringLiteralExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscoveryHUD.cs" :23 :16) // UnityEditor.Undo.RecordObjects(new Object[] { this, networkDiscovery }, "Set NetworkDiscovery") (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
// Skipping function OnGUI(), it contains poisonous unsupported syntaxes

// Skipping function DrawGUI(), it contains poisonous unsupported syntaxes

func @_Mirror.Discovery.NetworkDiscoveryHUD.Connect$Mirror.Discovery.ServerResponse$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscoveryHUD.cs" :83 :8) {
^entry (%_info : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscoveryHUD.cs" :83 :21)
cbde.store %_info, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscoveryHUD.cs" :83 :21)
br ^0

^0: // SimpleBlock
// Entity from another assembly: NetworkManager
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscoveryHUD.cs" :85 :12) // NetworkManager.singleton (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscoveryHUD.cs" :85 :49) // Not a variable of known type: info
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscoveryHUD.cs" :85 :49) // info.uri (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscoveryHUD.cs" :85 :12) // NetworkManager.singleton.StartClient(info.uri) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Discovery.NetworkDiscoveryHUD.OnDiscoveredServer$Mirror.Discovery.ServerResponse$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscoveryHUD.cs" :88 :8) {
^entry (%_info : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscoveryHUD.cs" :88 :39)
cbde.store %_info, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscoveryHUD.cs" :88 :39)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscoveryHUD.cs" :91 :12) // Not a variable of known type: discoveredServers
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscoveryHUD.cs" :91 :30) // Not a variable of known type: info
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscoveryHUD.cs" :91 :30) // info.serverId (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscoveryHUD.cs" :91 :12) // discoveredServers[info.serverId] (ElementAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscoveryHUD.cs" :91 :47) // Not a variable of known type: info
br ^1

^1: // ExitBlock
return

}
