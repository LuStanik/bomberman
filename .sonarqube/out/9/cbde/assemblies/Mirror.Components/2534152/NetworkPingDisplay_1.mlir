func @_Mirror.NetworkPingDisplay.Awake$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :23 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :25 :20) // new GUIStyle() (ObjectCreationExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :26 :12) // Not a variable of known type: style
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :26 :12) // style.alignment (SimpleMemberAccessExpression)
// Entity from another assembly: TextAnchor
%3 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :26 :30) // TextAnchor.UpperLeft (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :27 :12) // Not a variable of known type: style
%5 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :27 :12) // style.fontSize (SimpleMemberAccessExpression)
%6 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :27 :29) // Not a variable of known type: fontSize
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :28 :12) // Not a variable of known type: style
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :28 :12) // style.normal (SimpleMemberAccessExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :28 :12) // style.normal.textColor (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :28 :37) // Not a variable of known type: textColor
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkPingDisplay.OnGUI$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :31 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :33 :17) // Not a variable of known type: showPing
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :33 :16) // !showPing (LogicalNotExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :33 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :33 :29)

^2: // BinaryBranchBlock
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :34 :16) // Not a variable of known type: showRoundTripTime
cond_br %2, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :34 :16)

^3: // SimpleBlock
%3 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :34 :53)
br ^5

^4: // SimpleBlock
%4 = constant 2 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :34 :81)
br ^5

^5: // SimpleBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :36 :26) // string (PredefinedType)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :36 :40) // Not a variable of known type: format
// Entity from another assembly: NetworkTime
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :36 :55) // NetworkTime.rtt (SimpleMemberAccessExpression)
%8 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :36 :73) // Not a variable of known type: rttMultiplier
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :36 :55) // Binary expression on unsupported types NetworkTime.rtt / rttMultiplier
%10 = constant 1000 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :36 :90)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :36 :54) // Binary expression on unsupported types (NetworkTime.rtt / rttMultiplier) * 1000
%12 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :36 :48) // (int)((NetworkTime.rtt / rttMultiplier) * 1000) (CastExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :36 :26) // string.Format(format, (int)((NetworkTime.rtt / rttMultiplier) * 1000)) (InvocationExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :39 :12) // Not a variable of known type: style
%16 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :39 :12) // style.fontSize (SimpleMemberAccessExpression)
%17 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :39 :29) // Not a variable of known type: fontSize
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :40 :12) // Not a variable of known type: style
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :40 :12) // style.normal (SimpleMemberAccessExpression)
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :40 :12) // style.normal.textColor (SimpleMemberAccessExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :40 :37) // Not a variable of known type: textColor
// Entity from another assembly: Screen
%22 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :42 :24) // Screen.width (SimpleMemberAccessExpression)
%23 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :42 :16) // width
cbde.store %22, %23 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :42 :16)
// Entity from another assembly: Screen
%24 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :43 :25) // Screen.height (SimpleMemberAccessExpression)
%25 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :43 :16) // height
cbde.store %24, %25 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :43 :16)
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :44 :33) // Not a variable of known type: position
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :44 :33) // position.x (SimpleMemberAccessExpression)
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :44 :45) // Not a variable of known type: position
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :44 :45) // position.y (SimpleMemberAccessExpression)
%30 = cbde.load %23 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :44 :57)
%31 = constant 200 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :44 :65)
%32 = subi %30, %31 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :44 :57)
%33 = cbde.load %25 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :44 :70)
%34 = constant 2 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :44 :79)
%35 = muli %33, %34 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :44 :70)
%36 = constant 100 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :44 :83)
%37 = divis %35, %36 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :44 :70)
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :44 :24) // new Rect(position.x, position.y, width - 200, height * 2 / 100) (ObjectCreationExpression)
// Entity from another assembly: GUI
%40 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :46 :22) // Not a variable of known type: rect
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :46 :28) // Not a variable of known type: text
%42 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :46 :34) // Not a variable of known type: style
%43 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkPingDisplay.cs" :46 :12) // GUI.Label(rect, text, style) (InvocationExpression)
br ^6

^6: // ExitBlock
return

}
