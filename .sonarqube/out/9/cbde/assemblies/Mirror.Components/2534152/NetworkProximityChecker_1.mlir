func @_Mirror.NetworkProximityChecker.OnStartServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkProximityChecker.cs" :34 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: InvokeRepeating
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkProximityChecker.cs" :36 :28) // nameof(RebuildObservers) (InvocationExpression)
%1 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkProximityChecker.cs" :36 :54)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkProximityChecker.cs" :36 :57) // Not a variable of known type: visUpdateInterval
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkProximityChecker.cs" :36 :12) // InvokeRepeating(nameof(RebuildObservers), 0, visUpdateInterval) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkProximityChecker.OnStopServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkProximityChecker.cs" :38 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: CancelInvoke
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkProximityChecker.cs" :40 :25) // nameof(RebuildObservers) (InvocationExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkProximityChecker.cs" :40 :12) // CancelInvoke(nameof(RebuildObservers)) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkProximityChecker.RebuildObservers$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkProximityChecker.cs" :43 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkProximityChecker.cs" :45 :12) // Identifier from another assembly: netIdentity
%1 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkProximityChecker.cs" :45 :41) // false
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkProximityChecker.cs" :45 :12) // netIdentity.RebuildObservers(false) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkProximityChecker.OnCheckObserver$Mirror.NetworkConnection$(none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkProximityChecker.cs" :54 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkProximityChecker.cs" :54 :45)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkProximityChecker.cs" :54 :45)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkProximityChecker.cs" :56 :16) // Not a variable of known type: forceHidden
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkProximityChecker.cs" :56 :16)

^1: // JumpBlock
%2 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkProximityChecker.cs" :57 :23) // false
return %2 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkProximityChecker.cs" :57 :16)

^2: // JumpBlock
// Entity from another assembly: Vector3
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkProximityChecker.cs" :59 :36) // Not a variable of known type: conn
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkProximityChecker.cs" :59 :36) // conn.identity (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkProximityChecker.cs" :59 :36) // conn.identity.transform (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkProximityChecker.cs" :59 :36) // conn.identity.transform.position (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkProximityChecker.cs" :59 :70) // Identifier from another assembly: transform
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkProximityChecker.cs" :59 :70) // transform.position (SimpleMemberAccessExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkProximityChecker.cs" :59 :19) // Vector3.Distance(conn.identity.transform.position, transform.position) (InvocationExpression)
%10 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkProximityChecker.cs" :59 :92) // Not a variable of known type: visRange
%11 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkProximityChecker.cs" :59 :19) // comparison of unknown type: Vector3.Distance(conn.identity.transform.position, transform.position) < visRange
return %11 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkProximityChecker.cs" :59 :12)

^3: // ExitBlock
cbde.unreachable

}
// Skipping function OnRebuildObservers(none, i1), it contains poisonous unsupported syntaxes

