func @_Mirror.NetworkRoomManager.OnValidate$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :103 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: Mathf
%0 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :106 :39) // Identifier from another assembly: maxConnections
%1 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :106 :55)
%2 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :106 :29) // Mathf.Max(maxConnections, 0) (InvocationExpression)
// Entity from another assembly: Mathf
%3 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :109 :35) // Not a variable of known type: minPlayers
%4 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :109 :47) // Identifier from another assembly: maxConnections
%5 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :109 :25) // Mathf.Min(minPlayers, maxConnections) (InvocationExpression)
// Entity from another assembly: Mathf
%6 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :112 :35) // Not a variable of known type: minPlayers
%7 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :112 :47)
%8 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :112 :25) // Mathf.Max(minPlayers, 0) (InvocationExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :114 :16) // Not a variable of known type: roomPlayerPrefab
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :114 :36) // null (NullLiteralExpression)
%11 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :114 :16) // comparison of unknown type: roomPlayerPrefab != null
cond_br %11, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :114 :16)

^1: // BinaryBranchBlock
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :116 :43) // Not a variable of known type: roomPlayerPrefab
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :116 :43) // roomPlayerPrefab.GetComponent<NetworkIdentity>() (InvocationExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :117 :20) // Not a variable of known type: identity
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :117 :32) // null (NullLiteralExpression)
%17 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :117 :20) // comparison of unknown type: identity == null
cond_br %17, ^3, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :117 :20)

^3: // SimpleBlock
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :119 :39) // null (NullLiteralExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :120 :20) // Not a variable of known type: logger
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :120 :36) // "RoomPlayer prefab must have a NetworkIdentity component." (StringLiteralExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :120 :20) // logger.LogError("RoomPlayer prefab must have a NetworkIdentity component.") (InvocationExpression)
br ^2

^2: // SimpleBlock
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :124 :12) // base (BaseExpression)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :124 :12) // base.OnValidate() (InvocationExpression)
br ^4

^4: // ExitBlock
return

}
// Skipping function ReadyStatusChanged(), it contains poisonous unsupported syntaxes

// Skipping function OnServerReady(none), it contains poisonous unsupported syntaxes

// Skipping function SceneLoadedForPlayer(none, none), it contains poisonous unsupported syntaxes

// Skipping function CheckReadyToBegin(), it contains poisonous unsupported syntaxes

// Skipping function CallOnClientEnterRoom(), it contains poisonous unsupported syntaxes

// Skipping function CallOnClientExitRoom(), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkRoomManager.OnServerConnect$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :248 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :248 :45)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :248 :45)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :250 :16) // Identifier from another assembly: numPlayers
%2 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :250 :30) // Identifier from another assembly: maxConnections
%3 = cmpi "sge", %1, %2 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :250 :16)
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :250 :16)

^1: // JumpBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :252 :16) // Not a variable of known type: conn
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :252 :16) // conn.Disconnect() (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :253 :16)

^2: // BinaryBranchBlock
// Entity from another assembly: IsSceneActive
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :257 :31) // Not a variable of known type: RoomScene
%7 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :257 :17) // IsSceneActive(RoomScene) (InvocationExpression)
%8 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :257 :16) // !IsSceneActive(RoomScene) (LogicalNotExpression)
cond_br %8, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :257 :16)

^3: // JumpBlock
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :259 :16) // Not a variable of known type: conn
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :259 :16) // conn.Disconnect() (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :260 :16)

^4: // SimpleBlock
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :263 :12) // base (BaseExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :263 :33) // Not a variable of known type: conn
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :263 :12) // base.OnServerConnect(conn) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnRoomServerConnect
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :264 :32) // Not a variable of known type: conn
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :264 :12) // OnRoomServerConnect(conn) (InvocationExpression)
br ^5

^5: // ExitBlock
return

}
// Skipping function OnServerDisconnect(none), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkRoomManager.OnServerAddPlayer$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :312 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :312 :47)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :312 :47)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :315 :12) // Not a variable of known type: clientIndex
%2 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :315 :12) // Inc/Decrement of field or property clientIndex
// Entity from another assembly: IsSceneActive
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :317 :30) // Not a variable of known type: RoomScene
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :317 :16) // IsSceneActive(RoomScene) (InvocationExpression)
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :317 :16)

^1: // BinaryBranchBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :319 :20) // Not a variable of known type: roomSlots
%6 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :319 :20) // roomSlots.Count (SimpleMemberAccessExpression)
%7 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :319 :39) // Identifier from another assembly: maxConnections
%8 = cmpi "eq", %6, %7 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :319 :20)
cond_br %8, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :319 :20)

^3: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :320 :20)

^4: // BinaryBranchBlock
%9 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :322 :34) // false
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :324 :20) // Not a variable of known type: logger
%11 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :324 :20) // logger.LogEnabled() (InvocationExpression)
cond_br %11, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :324 :20)

^5: // SimpleBlock
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :324 :41) // Not a variable of known type: logger
// Entity from another assembly: LogType
%13 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :324 :58) // LogType.Log (SimpleMemberAccessExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :324 :71) // "NetworkRoomManager.OnServerAddPlayer playerPrefab:{0}" (StringLiteralExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :324 :128) // Not a variable of known type: roomPlayerPrefab
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :324 :128) // roomPlayerPrefab.name (SimpleMemberAccessExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :324 :41) // logger.LogFormat(LogType.Log, "NetworkRoomManager.OnServerAddPlayer playerPrefab:{0}", roomPlayerPrefab.name) (InvocationExpression)
br ^6

^6: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnRoomServerCreateRoomPlayer
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :326 :76) // Not a variable of known type: conn
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :326 :47) // OnRoomServerCreateRoomPlayer(conn) (InvocationExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :327 :20) // Not a variable of known type: newRoomGameObject
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :327 :41) // null (NullLiteralExpression)
%23 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :327 :20) // comparison of unknown type: newRoomGameObject == null
cond_br %23, ^7, ^8 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :327 :20)

^7: // SimpleBlock
// Entity from another assembly: Instantiate
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :328 :52) // Not a variable of known type: roomPlayerPrefab
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :328 :52) // roomPlayerPrefab.gameObject (SimpleMemberAccessExpression)
// Entity from another assembly: Vector3
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :328 :81) // Vector3.zero (SimpleMemberAccessExpression)
// Entity from another assembly: Quaternion
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :328 :95) // Quaternion.identity (SimpleMemberAccessExpression)
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :328 :40) // Instantiate(roomPlayerPrefab.gameObject, Vector3.zero, Quaternion.identity) (InvocationExpression)
br ^8

^8: // SimpleBlock
// Entity from another assembly: NetworkServer
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :330 :53) // Not a variable of known type: conn
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :330 :59) // Not a variable of known type: newRoomGameObject
%31 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :330 :16) // NetworkServer.AddPlayerForConnection(conn, newRoomGameObject) (InvocationExpression)
br ^9

^2: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnRoomServerAddPlayer
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :333 :38) // Not a variable of known type: conn
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :333 :16) // OnRoomServerAddPlayer(conn) (InvocationExpression)
br ^9

^9: // ExitBlock
return

}
func @_Mirror.NetworkRoomManager.RecalculateRoomPlayerIndices$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :336 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :339 :16) // Not a variable of known type: roomSlots
%1 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :339 :16) // roomSlots.Count (SimpleMemberAccessExpression)
%2 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :339 :34)
%3 = cmpi "sgt", %1, %2 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :339 :16)
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :339 :16)

^1: // ForInitializerBlock
%4 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :341 :29)
%5 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :341 :25) // i
cbde.store %4, %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :341 :25)
br ^3

^3: // BinaryBranchBlock
%6 = cbde.load %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :341 :32)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :341 :36) // Not a variable of known type: roomSlots
%8 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :341 :36) // roomSlots.Count (SimpleMemberAccessExpression)
%9 = cmpi "slt", %6, %8 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :341 :32)
cond_br %9, ^4, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :341 :32)

^4: // SimpleBlock
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :343 :20) // Not a variable of known type: roomSlots
%11 = cbde.load %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :343 :30)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :343 :20) // roomSlots[i] (ElementAccessExpression)
%13 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :343 :20) // roomSlots[i].index (SimpleMemberAccessExpression)
%14 = cbde.load %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :343 :41)
br ^5

^5: // SimpleBlock
%15 = cbde.load %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :341 :53)
%16 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :341 :53)
%17 = addi %15, %16 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :341 :53)
cbde.store %17, %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :341 :53)
br ^3

^2: // ExitBlock
return

}
// Skipping function ServerChangeScene(none), it contains poisonous unsupported syntaxes

// Skipping function OnServerSceneChanged(none), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkRoomManager.OnStartServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :401 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :403 :16) // string (PredefinedType)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :403 :37) // Not a variable of known type: RoomScene
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :403 :16) // string.IsNullOrEmpty(RoomScene) (InvocationExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :403 :16)

^1: // JumpBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :405 :16) // Not a variable of known type: logger
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :405 :32) // "NetworkRoomManager RoomScene is empty. Set the RoomScene in the inspector for the NetworkRoomManager" (StringLiteralExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :405 :16) // logger.LogError("NetworkRoomManager RoomScene is empty. Set the RoomScene in the inspector for the NetworkRoomManager") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :406 :16)

^2: // BinaryBranchBlock
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :409 :16) // string (PredefinedType)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :409 :37) // Not a variable of known type: GameplayScene
%8 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :409 :16) // string.IsNullOrEmpty(GameplayScene) (InvocationExpression)
cond_br %8, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :409 :16)

^3: // JumpBlock
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :411 :16) // Not a variable of known type: logger
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :411 :32) // "NetworkRoomManager PlayScene is empty. Set the PlayScene in the inspector for the NetworkRoomManager" (StringLiteralExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :411 :16) // logger.LogError("NetworkRoomManager PlayScene is empty. Set the PlayScene in the inspector for the NetworkRoomManager") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :412 :16)

^4: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnRoomStartServer
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :415 :12) // OnRoomStartServer() (InvocationExpression)
br ^5

^5: // ExitBlock
return

}
func @_Mirror.NetworkRoomManager.OnStartHost$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :422 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnRoomStartHost
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :424 :12) // OnRoomStartHost() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkRoomManager.OnStopServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :430 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :432 :12) // Not a variable of known type: roomSlots
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :432 :12) // roomSlots.Clear() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnRoomStopServer
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :433 :12) // OnRoomStopServer() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkRoomManager.OnStopHost$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :439 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnRoomStopHost
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :441 :12) // OnRoomStopHost() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function OnStartClient(), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkRoomManager.OnClientConnect$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :469 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :469 :45)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :469 :45)
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnRoomClientConnect
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :471 :32) // Not a variable of known type: conn
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :471 :12) // OnRoomClientConnect(conn) (InvocationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :472 :12) // base (BaseExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :472 :33) // Not a variable of known type: conn
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :472 :12) // base.OnClientConnect(conn) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkRoomManager.OnClientDisconnect$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :480 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :480 :48)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :480 :48)
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnRoomClientDisconnect
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :482 :35) // Not a variable of known type: conn
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :482 :12) // OnRoomClientDisconnect(conn) (InvocationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :483 :12) // base (BaseExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :483 :36) // Not a variable of known type: conn
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :483 :12) // base.OnClientDisconnect(conn) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkRoomManager.OnStopClient$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :489 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnRoomStopClient
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :491 :12) // OnRoomStopClient() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CallOnClientExitRoom
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :492 :12) // CallOnClientExitRoom() (InvocationExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :493 :12) // Not a variable of known type: roomSlots
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :493 :12) // roomSlots.Clear() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkRoomManager.OnClientSceneChanged$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :501 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :501 :50)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :501 :50)
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: IsSceneActive
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :503 :30) // Not a variable of known type: RoomScene
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :503 :16) // IsSceneActive(RoomScene) (InvocationExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :503 :16)

^1: // BinaryBranchBlock
// Entity from another assembly: NetworkClient
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :505 :20) // NetworkClient.isConnected (SimpleMemberAccessExpression)
cond_br %3, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :505 :20)

^3: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CallOnClientEnterRoom
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :506 :20) // CallOnClientEnterRoom() (InvocationExpression)
br ^4

^2: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CallOnClientExitRoom
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :509 :16) // CallOnClientExitRoom() (InvocationExpression)
br ^4

^4: // SimpleBlock
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :511 :12) // base (BaseExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :511 :38) // Not a variable of known type: conn
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :511 :12) // base.OnClientSceneChanged(conn) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnRoomClientSceneChanged
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :512 :37) // Not a variable of known type: conn
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :512 :12) // OnRoomClientSceneChanged(conn) (InvocationExpression)
br ^5

^5: // ExitBlock
return

}
func @_Mirror.NetworkRoomManager.OnRoomStartHost$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :522 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkRoomManager.OnRoomStopHost$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :527 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkRoomManager.OnRoomStartServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :532 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkRoomManager.OnRoomStopServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :537 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkRoomManager.OnRoomServerConnect$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :543 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :543 :48)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :543 :48)
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkRoomManager.OnRoomServerDisconnect$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :549 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :549 :51)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :549 :51)
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkRoomManager.OnRoomServerSceneChanged$string$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :555 :8) {
^entry (%_sceneName : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :555 :53)
cbde.store %_sceneName, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :555 :53)
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkRoomManager.OnRoomServerCreateRoomPlayer$Mirror.NetworkConnection$(none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :563 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :563 :63)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :563 :63)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :565 :19) // null (NullLiteralExpression)
return %1 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :565 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.NetworkRoomManager.OnRoomServerCreateGamePlayer$Mirror.NetworkConnection.UnityEngine.GameObject$(none, none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :575 :8) {
^entry (%_conn : none, %_roomPlayer : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :575 :63)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :575 :63)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :575 :87)
cbde.store %_roomPlayer, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :575 :87)
br ^0

^0: // JumpBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :577 :19) // null (NullLiteralExpression)
return %2 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :577 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.NetworkRoomManager.OnRoomServerAddPlayer$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :586 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :586 :50)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :586 :50)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :588 :12) // base (BaseExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :588 :35) // Not a variable of known type: conn
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :588 :12) // base.OnServerAddPlayer(conn) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkRoomManager.OnRoomServerSceneLoadedForPlayer$Mirror.NetworkConnection.UnityEngine.GameObject.UnityEngine.GameObject$(none, none, none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :600 :8) {
^entry (%_conn : none, %_roomPlayer : none, %_gamePlayer : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :600 :61)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :600 :61)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :600 :85)
cbde.store %_roomPlayer, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :600 :85)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :600 :108)
cbde.store %_gamePlayer, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :600 :108)
br ^0

^0: // JumpBlock
%3 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :602 :19) // true
return %3 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :602 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.NetworkRoomManager.OnRoomServerPlayersReady$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :609 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ServerChangeScene
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :612 :30) // Not a variable of known type: GameplayScene
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :612 :12) // ServerChangeScene(GameplayScene) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkRoomManager.OnRoomServerPlayersNotReady$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :619 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkRoomManager.OnRoomClientEnter$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :628 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkRoomManager.OnRoomClientExit$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :633 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkRoomManager.OnRoomClientConnect$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :639 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :639 :48)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :639 :48)
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkRoomManager.OnRoomClientDisconnect$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :645 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :645 :51)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :645 :51)
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkRoomManager.OnRoomStartClient$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :651 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkRoomManager.OnRoomStopClient$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :656 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkRoomManager.OnRoomClientSceneChanged$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :662 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :662 :53)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :662 :53)
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkRoomManager.OnRoomClientAddPlayerFailed$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomManager.cs" :668 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
// Skipping function OnGUI(), it contains poisonous unsupported syntaxes

