func @_Mirror.NetworkMatchChecker.OnStartServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :72 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :74 :16) // Not a variable of known type: currentMatch
// Entity from another assembly: Guid
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :74 :32) // Guid.Empty (SimpleMemberAccessExpression)
%2 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :74 :16) // comparison of unknown type: currentMatch == Guid.Empty
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :74 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :74 :44)

^2: // BinaryBranchBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :76 :17) // Not a variable of known type: matchPlayers
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :76 :42) // Not a variable of known type: currentMatch
%5 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :76 :17) // matchPlayers.ContainsKey(currentMatch) (InvocationExpression)
%6 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :76 :16) // !matchPlayers.ContainsKey(currentMatch) (LogicalNotExpression)
cond_br %6, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :76 :16)

^3: // SimpleBlock
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :77 :16) // Not a variable of known type: matchPlayers
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :77 :33) // Not a variable of known type: currentMatch
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :77 :47) // new HashSet<NetworkIdentity>() (ObjectCreationExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :77 :16) // matchPlayers.Add(currentMatch, new HashSet<NetworkIdentity>()) (InvocationExpression)
br ^4

^4: // SimpleBlock
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :79 :12) // Not a variable of known type: matchPlayers
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :79 :25) // Not a variable of known type: currentMatch
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :79 :12) // matchPlayers[currentMatch] (ElementAccessExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :79 :43) // Identifier from another assembly: netIdentity
%15 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :79 :12) // matchPlayers[currentMatch].Add(netIdentity) (InvocationExpression)
br ^5

^5: // ExitBlock
return

}
// Skipping function OnStopServer(), it contains poisonous unsupported syntaxes

// Skipping function RebuildMatchObservers(none), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkMatchChecker.OnCheckObserver$Mirror.NetworkConnection$(none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :108 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :108 :45)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :108 :45)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :111 :16) // Not a variable of known type: matchId
// Entity from another assembly: Guid
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :111 :27) // Guid.Empty (SimpleMemberAccessExpression)
%3 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :111 :16) // comparison of unknown type: matchId == Guid.Empty
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :111 :16)

^1: // JumpBlock
%4 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :112 :23) // false
return %4 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :112 :16)

^2: // BinaryBranchBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :114 :54) // Not a variable of known type: conn
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :114 :54) // conn.identity (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :114 :54) // conn.identity.GetComponent<NetworkMatchChecker>() (InvocationExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :116 :16) // Not a variable of known type: networkMatchChecker
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :116 :39) // null (NullLiteralExpression)
%11 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :116 :16) // comparison of unknown type: networkMatchChecker == null
cond_br %11, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :116 :16)

^3: // JumpBlock
%12 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :117 :23) // false
return %12 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :117 :16)

^4: // JumpBlock
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :119 :19) // Not a variable of known type: networkMatchChecker
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :119 :19) // networkMatchChecker.matchId (SimpleMemberAccessExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :119 :50) // Not a variable of known type: matchId
%16 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :119 :19) // comparison of unknown type: networkMatchChecker.matchId == matchId
return %16 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkMatchChecker.cs" :119 :12)

^5: // ExitBlock
cbde.unreachable

}
// Skipping function OnRebuildObservers(none, i1), it contains poisonous unsupported syntaxes

