// Skipping function Start(), it contains poisonous unsupported syntaxes

// Skipping function OnDisable(), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkRoomPlayer.CmdChangeReadyState$bool$(i1) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :82 :8) {
^entry (%_readyState : i1):
%0 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :83 :40)
cbde.store %_readyState, %0 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :83 :40)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.load %0 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :85 :27)
// Entity from another assembly: NetworkManager
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :86 :38) // NetworkManager.singleton (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :86 :38) // NetworkManager.singleton as NetworkRoomManager (AsExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :87 :16) // Not a variable of known type: room
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :87 :24) // null (NullLiteralExpression)
%7 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :87 :16) // comparison of unknown type: room != null
cond_br %7, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :87 :16)

^1: // SimpleBlock
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :89 :16) // Not a variable of known type: room
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :89 :16) // room.ReadyStatusChanged() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.NetworkRoomPlayer.IndexChanged$int.int$(i32, i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :102 :8) {
^entry (%_oldIndex : i32, %_newIndex : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :102 :41)
cbde.store %_oldIndex, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :102 :41)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :102 :55)
cbde.store %_newIndex, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :102 :55)
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkRoomPlayer.ReadyStateChanged$bool.bool$(i1, i1) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :109 :8) {
^entry (%__ : i1, %_newReadyState : i1):
%0 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :109 :46)
cbde.store %__, %0 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :109 :46)
%1 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :109 :54)
cbde.store %_newReadyState, %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :109 :54)
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkRoomPlayer.OnClientEnterRoom$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :119 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkRoomPlayer.OnClientExitRoom$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :124 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkRoomPlayer.OnGUI$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :133 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :135 :17) // Not a variable of known type: showRoomGUI
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :135 :16) // !showRoomGUI (LogicalNotExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :135 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :136 :16)

^2: // BinaryBranchBlock
// Entity from another assembly: NetworkManager
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :138 :38) // NetworkManager.singleton (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :138 :38) // NetworkManager.singleton as NetworkRoomManager (AsExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :139 :16) // Not a variable of known type: room
%6 = cbde.unknown : i1 // Creating necessary bool for conversion
cond_br %6, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :139 :16)

^3: // BinaryBranchBlock
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :141 :21) // Not a variable of known type: room
%8 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :141 :21) // room.showRoomGUI (SimpleMemberAccessExpression)
%9 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :141 :20) // !room.showRoomGUI (LogicalNotExpression)
cond_br %9, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :141 :20)

^5: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :142 :20)

^6: // BinaryBranchBlock
// Entity from another assembly: NetworkManager
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :144 :50) // Not a variable of known type: room
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :144 :50) // room.RoomScene (SimpleMemberAccessExpression)
%12 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :144 :21) // NetworkManager.IsSceneActive(room.RoomScene) (InvocationExpression)
%13 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :144 :20) // !NetworkManager.IsSceneActive(room.RoomScene) (LogicalNotExpression)
cond_br %13, ^7, ^8 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :144 :20)

^7: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :145 :20)

^8: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: DrawPlayerReadyState
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :147 :16) // DrawPlayerReadyState() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: DrawPlayerReadyButton
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkRoomPlayer.cs" :148 :16) // DrawPlayerReadyButton() (InvocationExpression)
br ^4

^4: // ExitBlock
return

}
// Skipping function DrawPlayerReadyState(), it contains poisonous unsupported syntaxes

// Skipping function DrawPlayerReadyButton(), it contains poisonous unsupported syntaxes

