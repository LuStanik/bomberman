func @_Mirror.NetworkTransformBase.SerializeIntoWriter$Mirror.NetworkWriter.UnityEngine.Vector3.UnityEngine.Quaternion.UnityEngine.Vector3$(none, none, none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :75 :8) {
^entry (%_writer : none, %_position : none, %_rotation : none, %_scale : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :75 :47)
cbde.store %_writer, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :75 :47)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :75 :69)
cbde.store %_position, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :75 :69)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :75 :87)
cbde.store %_rotation, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :75 :87)
%3 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :75 :108)
cbde.store %_scale, %3 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :75 :108)
br ^0

^0: // SimpleBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :80 :12) // Not a variable of known type: writer
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :80 :32) // Not a variable of known type: position
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :80 :12) // writer.WriteVector3(position) (InvocationExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :81 :12) // Not a variable of known type: writer
// Entity from another assembly: Compression
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :81 :62) // Not a variable of known type: rotation
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :81 :31) // Compression.CompressQuaternion(rotation) (InvocationExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :81 :12) // writer.WriteUInt32(Compression.CompressQuaternion(rotation)) (InvocationExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :82 :12) // Not a variable of known type: writer
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :82 :32) // Not a variable of known type: scale
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :82 :12) // writer.WriteVector3(scale) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkTransformBase.OnSerialize$Mirror.NetworkWriter.bool$(none, i1) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :85 :8) {
^entry (%_writer : none, %_initialState : i1):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :85 :41)
cbde.store %_writer, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :85 :41)
%1 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :85 :63)
cbde.store %_initialState, %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :85 :63)
br ^0

^0: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SerializeIntoWriter
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :88 :32) // Not a variable of known type: writer
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :88 :40) // Not a variable of known type: targetComponent
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :88 :40) // targetComponent.transform (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :88 :40) // targetComponent.transform.localPosition (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :88 :81) // Not a variable of known type: targetComponent
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :88 :81) // targetComponent.transform (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :88 :81) // targetComponent.transform.localRotation (SimpleMemberAccessExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :88 :122) // Not a variable of known type: targetComponent
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :88 :122) // targetComponent.transform (SimpleMemberAccessExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :88 :122) // targetComponent.transform.localScale (SimpleMemberAccessExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :88 :12) // SerializeIntoWriter(writer, targetComponent.transform.localPosition, targetComponent.transform.localRotation, targetComponent.transform.localScale) (InvocationExpression)
%13 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :89 :19) // true
return %13 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :89 :12)

^1: // ExitBlock
cbde.unreachable

}
// Skipping function EstimateMovementSpeed(none, none, none, none), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkTransformBase.DeserializeFromReader$Mirror.NetworkReader$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :106 :8) {
^entry (%_reader : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :106 :35)
cbde.store %_reader, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :106 :35)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :109 :29) // new DataPoint             {                 // deserialize position, rotation, scale                 // (rotation is compressed)                 localPosition = reader.ReadVector3(),                 localRotation = Compression.DecompressQuaternion(reader.ReadUInt32()),                 localScale = reader.ReadVector3(),                 timeStamp = Time.time             } (ObjectCreationExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :113 :32) // Not a variable of known type: reader
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :113 :32) // reader.ReadVector3() (InvocationExpression)
// Entity from another assembly: Compression
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :114 :65) // Not a variable of known type: reader
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :114 :65) // reader.ReadUInt32() (InvocationExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :114 :32) // Compression.DecompressQuaternion(reader.ReadUInt32()) (InvocationExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :115 :29) // Not a variable of known type: reader
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :115 :29) // reader.ReadVector3() (InvocationExpression)
// Entity from another assembly: Time
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :116 :28) // Time.time (SimpleMemberAccessExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :121 :12) // Not a variable of known type: temp
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :121 :12) // temp.movementSpeed (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: EstimateMovementSpeed
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :121 :55) // Not a variable of known type: goal
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :121 :61) // Not a variable of known type: temp
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :121 :67) // Not a variable of known type: targetComponent
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :121 :67) // targetComponent.transform (SimpleMemberAccessExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :121 :94) // Identifier from another assembly: syncInterval
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :121 :33) // EstimateMovementSpeed(goal, temp, targetComponent.transform, syncInterval) (InvocationExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :126 :16) // Not a variable of known type: start
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :126 :25) // null (NullLiteralExpression)
%21 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :126 :16) // comparison of unknown type: start == null
cond_br %21, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :126 :16)

^1: // SimpleBlock
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :128 :24) // new DataPoint                 {                     timeStamp = Time.time - syncInterval,                     // local position/rotation for VR support                     localPosition = targetComponent.transform.localPosition,                     localRotation = targetComponent.transform.localRotation,                     localScale = targetComponent.transform.localScale,                     movementSpeed = temp.movementSpeed                 } (ObjectCreationExpression)
// Entity from another assembly: Time
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :130 :32) // Time.time (SimpleMemberAccessExpression)
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :130 :44) // Identifier from another assembly: syncInterval
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :130 :32) // Binary expression on unsupported types Time.time - syncInterval
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :132 :36) // Not a variable of known type: targetComponent
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :132 :36) // targetComponent.transform (SimpleMemberAccessExpression)
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :132 :36) // targetComponent.transform.localPosition (SimpleMemberAccessExpression)
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :133 :36) // Not a variable of known type: targetComponent
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :133 :36) // targetComponent.transform (SimpleMemberAccessExpression)
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :133 :36) // targetComponent.transform.localRotation (SimpleMemberAccessExpression)
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :134 :33) // Not a variable of known type: targetComponent
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :134 :33) // targetComponent.transform (SimpleMemberAccessExpression)
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :134 :33) // targetComponent.transform.localScale (SimpleMemberAccessExpression)
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :135 :36) // Not a variable of known type: temp
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :135 :36) // temp.movementSpeed (SimpleMemberAccessExpression)
br ^3

^2: // BinaryBranchBlock
// Entity from another assembly: Vector3
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :169 :53) // Not a variable of known type: start
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :169 :53) // start.localPosition (SimpleMemberAccessExpression)
%39 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :169 :74) // Not a variable of known type: goal
%40 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :169 :74) // goal.localPosition (SimpleMemberAccessExpression)
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :169 :36) // Vector3.Distance(start.localPosition, goal.localPosition) (InvocationExpression)
// Entity from another assembly: Vector3
%43 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :170 :53) // Not a variable of known type: goal
%44 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :170 :53) // goal.localPosition (SimpleMemberAccessExpression)
%45 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :170 :73) // Not a variable of known type: temp
%46 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :170 :73) // temp.localPosition (SimpleMemberAccessExpression)
%47 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :170 :36) // Vector3.Distance(goal.localPosition, temp.localPosition) (InvocationExpression)
%49 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :172 :24) // Not a variable of known type: goal
// Entity from another assembly: Vector3
%50 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :178 :37) // Not a variable of known type: targetComponent
%51 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :178 :37) // targetComponent.transform (SimpleMemberAccessExpression)
%52 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :178 :37) // targetComponent.transform.localPosition (SimpleMemberAccessExpression)
%53 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :178 :78) // Not a variable of known type: start
%54 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :178 :78) // start.localPosition (SimpleMemberAccessExpression)
%55 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :178 :20) // Vector3.Distance(targetComponent.transform.localPosition, start.localPosition) (InvocationExpression)
%56 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :178 :101) // Not a variable of known type: oldDistance
%57 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :178 :115) // Not a variable of known type: newDistance
%58 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :178 :101) // Binary expression on unsupported types oldDistance + newDistance
%59 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :178 :20) // comparison of unknown type: Vector3.Distance(targetComponent.transform.localPosition, start.localPosition) < oldDistance + newDistance
cond_br %59, ^4, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :178 :20)

^4: // SimpleBlock
%60 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :180 :20) // Not a variable of known type: start
%61 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :180 :20) // start.localPosition (SimpleMemberAccessExpression)
%62 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :180 :42) // Not a variable of known type: targetComponent
%63 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :180 :42) // targetComponent.transform (SimpleMemberAccessExpression)
%64 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :180 :42) // targetComponent.transform.localPosition (SimpleMemberAccessExpression)
%65 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :181 :20) // Not a variable of known type: start
%66 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :181 :20) // start.localRotation (SimpleMemberAccessExpression)
%67 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :181 :42) // Not a variable of known type: targetComponent
%68 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :181 :42) // targetComponent.transform (SimpleMemberAccessExpression)
%69 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :181 :42) // targetComponent.transform.localRotation (SimpleMemberAccessExpression)
%70 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :182 :20) // Not a variable of known type: start
%71 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :182 :20) // start.localScale (SimpleMemberAccessExpression)
%72 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :182 :39) // Not a variable of known type: targetComponent
%73 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :182 :39) // targetComponent.transform (SimpleMemberAccessExpression)
%74 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :182 :39) // targetComponent.transform.localScale (SimpleMemberAccessExpression)
br ^3

^3: // SimpleBlock
%75 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :187 :19) // Not a variable of known type: temp
br ^5

^5: // ExitBlock
return

}
func @_Mirror.NetworkTransformBase.OnDeserialize$Mirror.NetworkReader.bool$(none, i1) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :190 :8) {
^entry (%_reader : none, %_initialState : i1):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :190 :43)
cbde.store %_reader, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :190 :43)
%1 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :190 :65)
cbde.store %_initialState, %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :190 :65)
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: DeserializeFromReader
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :193 :34) // Not a variable of known type: reader
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :193 :12) // DeserializeFromReader(reader) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function CmdClientToServerSync(none), it contains poisonous unsupported syntaxes

// Skipping function CurrentInterpolationFactor(none, none), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkTransformBase.InterpolatePosition$Mirror.NetworkTransformBase.DataPoint.Mirror.NetworkTransformBase.DataPoint.UnityEngine.Vector3$(none, none, none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :233 :8) {
^entry (%_start : none, %_goal : none, %_currentPosition : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :233 :43)
cbde.store %_start, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :233 :43)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :233 :60)
cbde.store %_goal, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :233 :60)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :233 :76)
cbde.store %_currentPosition, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :233 :76)
br ^0

^0: // BinaryBranchBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :235 :16) // Not a variable of known type: start
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :235 :25) // null (NullLiteralExpression)
%5 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :235 :16) // comparison of unknown type: start != null
cond_br %5, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :235 :16)

^1: // JumpBlock
// Entity from another assembly: Mathf
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :246 :40) // Not a variable of known type: start
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :246 :40) // start.movementSpeed (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :246 :61) // Not a variable of known type: goal
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :246 :61) // goal.movementSpeed (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :246 :30) // Mathf.Max(start.movementSpeed, goal.movementSpeed) (InvocationExpression)
// Entity from another assembly: Vector3
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :247 :43) // Not a variable of known type: currentPosition
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :247 :60) // Not a variable of known type: goal
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :247 :60) // goal.localPosition (SimpleMemberAccessExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :247 :80) // Not a variable of known type: speed
// Entity from another assembly: Time
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :247 :88) // Time.deltaTime (SimpleMemberAccessExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :247 :80) // Binary expression on unsupported types speed * Time.deltaTime
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :247 :23) // Vector3.MoveTowards(currentPosition, goal.localPosition, speed * Time.deltaTime) (InvocationExpression)
return %18 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :247 :16)

^2: // JumpBlock
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :249 :19) // Not a variable of known type: currentPosition
return %19 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :249 :12)

^3: // ExitBlock
cbde.unreachable

}
func @_Mirror.NetworkTransformBase.InterpolateRotation$Mirror.NetworkTransformBase.DataPoint.Mirror.NetworkTransformBase.DataPoint.UnityEngine.Quaternion$(none, none, none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :252 :8) {
^entry (%_start : none, %_goal : none, %_defaultRotation : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :252 :46)
cbde.store %_start, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :252 :46)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :252 :63)
cbde.store %_goal, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :252 :63)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :252 :79)
cbde.store %_defaultRotation, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :252 :79)
br ^0

^0: // BinaryBranchBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :254 :16) // Not a variable of known type: start
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :254 :25) // null (NullLiteralExpression)
%5 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :254 :16) // comparison of unknown type: start != null
cond_br %5, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :254 :16)

^1: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CurrentInterpolationFactor
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :256 :53) // Not a variable of known type: start
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :256 :60) // Not a variable of known type: goal
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :256 :26) // CurrentInterpolationFactor(start, goal) (InvocationExpression)
// Entity from another assembly: Quaternion
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :257 :40) // Not a variable of known type: start
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :257 :40) // start.localRotation (SimpleMemberAccessExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :257 :61) // Not a variable of known type: goal
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :257 :61) // goal.localRotation (SimpleMemberAccessExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :257 :81) // Not a variable of known type: t
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :257 :23) // Quaternion.Slerp(start.localRotation, goal.localRotation, t) (InvocationExpression)
return %15 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :257 :16)

^2: // JumpBlock
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :259 :19) // Not a variable of known type: defaultRotation
return %16 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :259 :12)

^3: // ExitBlock
cbde.unreachable

}
func @_Mirror.NetworkTransformBase.InterpolateScale$Mirror.NetworkTransformBase.DataPoint.Mirror.NetworkTransformBase.DataPoint.UnityEngine.Vector3$(none, none, none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :262 :8) {
^entry (%_start : none, %_goal : none, %_currentScale : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :262 :40)
cbde.store %_start, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :262 :40)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :262 :57)
cbde.store %_goal, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :262 :57)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :262 :73)
cbde.store %_currentScale, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :262 :73)
br ^0

^0: // BinaryBranchBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :264 :16) // Not a variable of known type: start
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :264 :25) // null (NullLiteralExpression)
%5 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :264 :16) // comparison of unknown type: start != null
cond_br %5, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :264 :16)

^1: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CurrentInterpolationFactor
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :266 :53) // Not a variable of known type: start
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :266 :60) // Not a variable of known type: goal
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :266 :26) // CurrentInterpolationFactor(start, goal) (InvocationExpression)
// Entity from another assembly: Vector3
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :267 :36) // Not a variable of known type: start
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :267 :36) // start.localScale (SimpleMemberAccessExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :267 :54) // Not a variable of known type: goal
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :267 :54) // goal.localScale (SimpleMemberAccessExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :267 :71) // Not a variable of known type: t
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :267 :23) // Vector3.Lerp(start.localScale, goal.localScale, t) (InvocationExpression)
return %15 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :267 :16)

^2: // JumpBlock
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :269 :19) // Not a variable of known type: currentScale
return %16 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :269 :12)

^3: // ExitBlock
cbde.unreachable

}
// Skipping function NeedsTeleport(), it contains poisonous unsupported syntaxes

// Skipping function HasEitherMovedRotatedScaled(), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkTransformBase.ApplyPositionRotationScale$UnityEngine.Vector3.UnityEngine.Quaternion.UnityEngine.Vector3$(none, none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :312 :8) {
^entry (%_position : none, %_rotation : none, %_scale : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :312 :40)
cbde.store %_position, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :312 :40)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :312 :58)
cbde.store %_rotation, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :312 :58)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :312 :79)
cbde.store %_scale, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :312 :79)
br ^0

^0: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :315 :12) // Not a variable of known type: targetComponent
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :315 :12) // targetComponent.transform (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :315 :12) // targetComponent.transform.localPosition (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :315 :54) // Not a variable of known type: position
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :316 :12) // Not a variable of known type: targetComponent
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :316 :12) // targetComponent.transform (SimpleMemberAccessExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :316 :12) // targetComponent.transform.localRotation (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :316 :54) // Not a variable of known type: rotation
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :317 :12) // Not a variable of known type: targetComponent
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :317 :12) // targetComponent.transform (SimpleMemberAccessExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :317 :12) // targetComponent.transform.localScale (SimpleMemberAccessExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :317 :51) // Not a variable of known type: scale
br ^1

^1: // ExitBlock
return

}
// Skipping function Update(), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkTransformBase.ServerTeleport$UnityEngine.Vector3$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :393 :8) {
^entry (%_position : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :394 :35)
cbde.store %_position, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :394 :35)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :396 :34) // Identifier from another assembly: transform
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :396 :34) // transform.rotation (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ServerTeleport
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :397 :27) // Not a variable of known type: position
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :397 :37) // Not a variable of known type: rotation
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :397 :12) // ServerTeleport(position, rotation) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function ServerTeleport(none, none), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkTransformBase.DoTeleport$UnityEngine.Vector3.UnityEngine.Quaternion$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :424 :8) {
^entry (%_newPosition : none, %_newRotation : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :424 :24)
cbde.store %_newPosition, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :424 :24)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :424 :45)
cbde.store %_newRotation, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :424 :45)
br ^0

^0: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :426 :12) // Identifier from another assembly: transform
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :426 :12) // transform.position (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :426 :33) // Not a variable of known type: newPosition
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :427 :12) // Identifier from another assembly: transform
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :427 :12) // transform.rotation (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :427 :33) // Not a variable of known type: newRotation
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :431 :19) // null (NullLiteralExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :432 :20) // null (NullLiteralExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :433 :27) // Not a variable of known type: newPosition
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :434 :27) // Not a variable of known type: newRotation
br ^1

^1: // ExitBlock
return

}
// Skipping function RpcTeleport(none, none, i1), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkTransformBase.CmdTeleportFinished$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :451 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :454 :16) // Not a variable of known type: clientAuthorityBeforeTeleport
cond_br %0, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :454 :16)

^1: // SimpleBlock
%1 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :456 :34) // true
%2 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :459 :48) // false
br ^3

^2: // SimpleBlock
// Entity from another assembly: Debug
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :463 :33) // "Client called TeleportFinished when clientAuthority was false on server" (StringLiteralExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :463 :108) // this (ThisExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :463 :16) // Debug.LogWarning("Client called TeleportFinished when clientAuthority was false on server", this) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.NetworkTransformBase.DrawDataPointGizmo$Mirror.NetworkTransformBase.DataPoint.UnityEngine.Color$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :468 :8) {
^entry (%_data : none, %_color : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :468 :39)
cbde.store %_data, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :468 :39)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :468 :55)
cbde.store %_color, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :468 :55)
br ^0

^0: // SimpleBlock
// Entity from another assembly: Vector3
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :472 :29) // Vector3.up (SimpleMemberAccessExpression)
%3 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :472 :42) // 0.01f (NumericLiteralExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :472 :29) // Binary expression on unsupported types Vector3.up * 0.01f
// Entity from another assembly: Gizmos
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :475 :12) // Gizmos.color (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :475 :27) // Not a variable of known type: color
// Entity from another assembly: Gizmos
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :476 :30) // Not a variable of known type: data
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :476 :30) // data.localPosition (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :476 :51) // Not a variable of known type: offset
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :476 :30) // Binary expression on unsupported types data.localPosition + offset
%12 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :476 :59) // 0.5f (NumericLiteralExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :476 :12) // Gizmos.DrawSphere(data.localPosition + offset, 0.5f) (InvocationExpression)
// Entity from another assembly: Gizmos
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :480 :12) // Gizmos.color (SimpleMemberAccessExpression)
// Entity from another assembly: Color
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :480 :27) // Color.blue (SimpleMemberAccessExpression)
// Entity from another assembly: Gizmos
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :481 :27) // Not a variable of known type: data
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :481 :27) // data.localPosition (SimpleMemberAccessExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :481 :48) // Not a variable of known type: offset
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :481 :27) // Binary expression on unsupported types data.localPosition + offset
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :481 :56) // Not a variable of known type: data
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :481 :56) // data.localRotation (SimpleMemberAccessExpression)
// Entity from another assembly: Vector3
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :481 :77) // Vector3.forward (SimpleMemberAccessExpression)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :481 :56) // Binary expression on unsupported types data.localRotation * Vector3.forward
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :481 :12) // Gizmos.DrawRay(data.localPosition + offset, data.localRotation * Vector3.forward) (InvocationExpression)
// Entity from another assembly: Gizmos
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :484 :12) // Gizmos.color (SimpleMemberAccessExpression)
// Entity from another assembly: Color
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :484 :27) // Color.green (SimpleMemberAccessExpression)
// Entity from another assembly: Gizmos
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :485 :27) // Not a variable of known type: data
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :485 :27) // data.localPosition (SimpleMemberAccessExpression)
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :485 :48) // Not a variable of known type: offset
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :485 :27) // Binary expression on unsupported types data.localPosition + offset
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :485 :56) // Not a variable of known type: data
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :485 :56) // data.localRotation (SimpleMemberAccessExpression)
// Entity from another assembly: Vector3
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :485 :77) // Vector3.up (SimpleMemberAccessExpression)
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :485 :56) // Binary expression on unsupported types data.localRotation * Vector3.up
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :485 :12) // Gizmos.DrawRay(data.localPosition + offset, data.localRotation * Vector3.up) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkTransformBase.DrawLineBetweenDataPoints$Mirror.NetworkTransformBase.DataPoint.Mirror.NetworkTransformBase.DataPoint.UnityEngine.Color$(none, none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :488 :8) {
^entry (%_data1 : none, %_data2 : none, %_color : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :488 :46)
cbde.store %_data1, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :488 :46)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :488 :63)
cbde.store %_data2, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :488 :63)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :488 :80)
cbde.store %_color, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :488 :80)
br ^0

^0: // SimpleBlock
// Entity from another assembly: Gizmos
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :490 :12) // Gizmos.color (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :490 :27) // Not a variable of known type: color
// Entity from another assembly: Gizmos
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :491 :28) // Not a variable of known type: data1
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :491 :28) // data1.localPosition (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :491 :49) // Not a variable of known type: data2
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :491 :49) // data2.localPosition (SimpleMemberAccessExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkTransformBase.cs" :491 :12) // Gizmos.DrawLine(data1.localPosition, data2.localPosition) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function OnDrawGizmos(), it contains poisonous unsupported syntaxes

