func @_Mirror.NetworkSceneChecker.Awake$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :31 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :34 :27) // Identifier from another assembly: gameObject
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :34 :27) // gameObject.scene (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :35 :16) // Not a variable of known type: logger
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :35 :16) // logger.LogEnabled() (InvocationExpression)
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :35 :16)

^1: // SimpleBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :35 :37) // Not a variable of known type: logger
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :35 :91) // Not a variable of known type: currentScene
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :35 :48) // $"NetworkSceneChecker.Awake currentScene: {currentScene}" (InterpolatedStringExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :35 :37) // logger.Log($"NetworkSceneChecker.Awake currentScene: {currentScene}") (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.NetworkSceneChecker.OnStartServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :38 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :40 :17) // Not a variable of known type: sceneCheckerObjects
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :40 :49) // Not a variable of known type: currentScene
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :40 :17) // sceneCheckerObjects.ContainsKey(currentScene) (InvocationExpression)
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :40 :16) // !sceneCheckerObjects.ContainsKey(currentScene) (LogicalNotExpression)
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :40 :16)

^1: // SimpleBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :41 :16) // Not a variable of known type: sceneCheckerObjects
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :41 :40) // Not a variable of known type: currentScene
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :41 :54) // new HashSet<NetworkIdentity>() (ObjectCreationExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :41 :16) // sceneCheckerObjects.Add(currentScene, new HashSet<NetworkIdentity>()) (InvocationExpression)
br ^2

^2: // SimpleBlock
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :43 :12) // Not a variable of known type: sceneCheckerObjects
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :43 :32) // Not a variable of known type: currentScene
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :43 :12) // sceneCheckerObjects[currentScene] (ElementAccessExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :43 :50) // Identifier from another assembly: netIdentity
%12 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :43 :12) // sceneCheckerObjects[currentScene].Add(netIdentity) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function OnStopServer(), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkSceneChecker.Update$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :52 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :55 :16) // Not a variable of known type: currentScene
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :55 :32) // Identifier from another assembly: gameObject
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :55 :32) // gameObject.scene (SimpleMemberAccessExpression)
%3 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :55 :16) // comparison of unknown type: currentScene == gameObject.scene
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :55 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :56 :16)

^2: // BinaryBranchBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :62 :12) // Not a variable of known type: sceneCheckerObjects
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :62 :32) // Not a variable of known type: currentScene
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :62 :12) // sceneCheckerObjects[currentScene] (ElementAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :62 :53) // Identifier from another assembly: netIdentity
%8 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :62 :12) // sceneCheckerObjects[currentScene].Remove(netIdentity) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: RebuildSceneObservers
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :65 :12) // RebuildSceneObservers() (InvocationExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :68 :27) // Identifier from another assembly: gameObject
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :68 :27) // gameObject.scene (SimpleMemberAccessExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :71 :17) // Not a variable of known type: sceneCheckerObjects
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :71 :49) // Not a variable of known type: currentScene
%14 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :71 :17) // sceneCheckerObjects.ContainsKey(currentScene) (InvocationExpression)
%15 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :71 :16) // !sceneCheckerObjects.ContainsKey(currentScene) (LogicalNotExpression)
cond_br %15, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :71 :16)

^3: // SimpleBlock
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :72 :16) // Not a variable of known type: sceneCheckerObjects
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :72 :40) // Not a variable of known type: currentScene
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :72 :54) // new HashSet<NetworkIdentity>() (ObjectCreationExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :72 :16) // sceneCheckerObjects.Add(currentScene, new HashSet<NetworkIdentity>()) (InvocationExpression)
br ^4

^4: // SimpleBlock
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :75 :12) // Not a variable of known type: sceneCheckerObjects
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :75 :32) // Not a variable of known type: currentScene
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :75 :12) // sceneCheckerObjects[currentScene] (ElementAccessExpression)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :75 :50) // Identifier from another assembly: netIdentity
%24 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :75 :12) // sceneCheckerObjects[currentScene].Add(netIdentity) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: RebuildSceneObservers
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :78 :12) // RebuildSceneObservers() (InvocationExpression)
br ^5

^5: // ExitBlock
return

}
// Skipping function RebuildSceneObservers(), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkSceneChecker.OnCheckObserver$Mirror.NetworkConnection$(none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :94 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :94 :45)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :94 :45)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :96 :16) // Not a variable of known type: forceHidden
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :96 :16)

^1: // JumpBlock
%2 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :97 :23) // false
return %2 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :97 :16)

^2: // JumpBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :99 :19) // Not a variable of known type: conn
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :99 :19) // conn.identity (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :99 :19) // conn.identity.gameObject (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :99 :19) // conn.identity.gameObject.scene (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :99 :53) // Identifier from another assembly: gameObject
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :99 :53) // gameObject.scene (SimpleMemberAccessExpression)
%9 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :99 :19) // comparison of unknown type: conn.identity.gameObject.scene == gameObject.scene
return %9 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkSceneChecker.cs" :99 :12)

^3: // ExitBlock
cbde.unreachable

}
// Skipping function OnRebuildObservers(none, i1), it contains poisonous unsupported syntaxes

