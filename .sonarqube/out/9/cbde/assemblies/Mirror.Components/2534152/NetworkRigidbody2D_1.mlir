func @_Mirror.Experimental.NetworkRigidbody2D.OnValidate$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :43 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :45 :16) // Not a variable of known type: target
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :45 :26) // null (NullLiteralExpression)
%2 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :45 :16) // comparison of unknown type: target == null
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :45 :16)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :47 :25) // GetComponent<Rigidbody2D>() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkRigidbody2D.OnVelocityChanged$UnityEngine.Vector2.UnityEngine.Vector2$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :79 :8) {
^entry (%__ : none, %_newValue : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :79 :31)
cbde.store %__, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :79 :31)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :79 :42)
cbde.store %_newValue, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :79 :42)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :81 :16) // Not a variable of known type: IgnoreSync
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :81 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :82 :16)

^2: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :84 :12) // Not a variable of known type: target
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :84 :12) // target.velocity (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :84 :30) // Not a variable of known type: newValue
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkRigidbody2D.OnAngularVelocityChanged$float.float$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :88 :8) {
^entry (%__ : none, %_newValue : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :88 :38)
cbde.store %__, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :88 :38)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :88 :47)
cbde.store %_newValue, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :88 :47)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :90 :16) // Not a variable of known type: IgnoreSync
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :90 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :91 :16)

^2: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :93 :12) // Not a variable of known type: target
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :93 :12) // target.angularVelocity (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :93 :37) // Not a variable of known type: newValue
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkRigidbody2D.OnIsKinematicChanged$bool.bool$(i1, i1) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :96 :8) {
^entry (%__ : i1, %_newValue : i1):
%0 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :96 :34)
cbde.store %__, %0 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :96 :34)
%1 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :96 :42)
cbde.store %_newValue, %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :96 :42)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :98 :16) // Not a variable of known type: IgnoreSync
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :98 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :99 :16)

^2: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :101 :12) // Not a variable of known type: target
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :101 :12) // target.isKinematic (SimpleMemberAccessExpression)
%5 = cbde.load %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :101 :33)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkRigidbody2D.OnGravityScaleChanged$float.float$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :104 :8) {
^entry (%__ : none, %_newValue : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :104 :35)
cbde.store %__, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :104 :35)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :104 :44)
cbde.store %_newValue, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :104 :44)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :106 :16) // Not a variable of known type: IgnoreSync
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :106 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :107 :16)

^2: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :109 :12) // Not a variable of known type: target
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :109 :12) // target.gravityScale (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :109 :34) // Not a variable of known type: newValue
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkRigidbody2D.OnuDragChanged$float.float$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :112 :8) {
^entry (%__ : none, %_newValue : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :112 :28)
cbde.store %__, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :112 :28)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :112 :37)
cbde.store %_newValue, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :112 :37)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :114 :16) // Not a variable of known type: IgnoreSync
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :114 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :115 :16)

^2: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :117 :12) // Not a variable of known type: target
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :117 :12) // target.drag (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :117 :26) // Not a variable of known type: newValue
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkRigidbody2D.OnAngularDragChanged$float.float$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :120 :8) {
^entry (%__ : none, %_newValue : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :120 :34)
cbde.store %__, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :120 :34)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :120 :43)
cbde.store %_newValue, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :120 :43)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :122 :16) // Not a variable of known type: IgnoreSync
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :122 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :123 :16)

^2: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :125 :12) // Not a variable of known type: target
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :125 :12) // target.angularDrag (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :125 :33) // Not a variable of known type: newValue
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkRigidbody2D.Update$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :130 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :132 :16) // Identifier from another assembly: isServer
cond_br %0, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :132 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SyncToClients
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :134 :16) // SyncToClients() (InvocationExpression)
br ^3

^2: // BinaryBranchBlock
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :136 :21) // Not a variable of known type: ClientWithAuthority
cond_br %2, ^4, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :136 :21)

^4: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SendToServer
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :138 :16) // SendToServer() (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function FixedUpdate(), it contains poisonous unsupported syntaxes

// Skipping function SyncToClients(), it contains poisonous unsupported syntaxes

func @_Mirror.Experimental.NetworkRigidbody2D.SendToServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :191 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :194 :17) // Identifier from another assembly: hasAuthority
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :194 :16) // !hasAuthority (LogicalNotExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :194 :16)

^1: // JumpBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :196 :16) // Not a variable of known type: logger
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :196 :34) // "SendToServer called without authority" (StringLiteralExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :196 :16) // logger.LogWarning("SendToServer called without authority") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :197 :16)

^2: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SendVelocity
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :200 :12) // SendVelocity() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SendRigidBodySettings
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :201 :12) // SendRigidBodySettings() (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function SendVelocity(), it contains poisonous unsupported syntaxes

func @_Mirror.Experimental.NetworkRigidbody2D.SendRigidBodySettings$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :239 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :243 :16) // Not a variable of known type: previousValue
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :243 :16) // previousValue.isKinematic (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :243 :45) // Not a variable of known type: target
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :243 :45) // target.isKinematic (SimpleMemberAccessExpression)
%4 = cmpi "ne", %1, %3 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :243 :16)
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :243 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CmdSendIsKinematic
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :245 :35) // Not a variable of known type: target
%6 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :245 :35) // target.isKinematic (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :245 :16) // CmdSendIsKinematic(target.isKinematic) (InvocationExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :246 :16) // Not a variable of known type: previousValue
%9 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :246 :16) // previousValue.isKinematic (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :246 :44) // Not a variable of known type: target
%11 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :246 :44) // target.isKinematic (SimpleMemberAccessExpression)
br ^2

^2: // BinaryBranchBlock
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :248 :16) // Not a variable of known type: previousValue
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :248 :16) // previousValue.gravityScale (SimpleMemberAccessExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :248 :46) // Not a variable of known type: target
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :248 :46) // target.gravityScale (SimpleMemberAccessExpression)
%16 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :248 :16) // comparison of unknown type: previousValue.gravityScale != target.gravityScale
cond_br %16, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :248 :16)

^3: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CmdChangeGravityScale
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :250 :38) // Not a variable of known type: target
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :250 :38) // target.gravityScale (SimpleMemberAccessExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :250 :16) // CmdChangeGravityScale(target.gravityScale) (InvocationExpression)
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :251 :16) // Not a variable of known type: previousValue
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :251 :16) // previousValue.gravityScale (SimpleMemberAccessExpression)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :251 :45) // Not a variable of known type: target
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :251 :45) // target.gravityScale (SimpleMemberAccessExpression)
br ^4

^4: // BinaryBranchBlock
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :253 :16) // Not a variable of known type: previousValue
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :253 :16) // previousValue.drag (SimpleMemberAccessExpression)
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :253 :38) // Not a variable of known type: target
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :253 :38) // target.drag (SimpleMemberAccessExpression)
%28 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :253 :16) // comparison of unknown type: previousValue.drag != target.drag
cond_br %28, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :253 :16)

^5: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CmdSendDrag
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :255 :28) // Not a variable of known type: target
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :255 :28) // target.drag (SimpleMemberAccessExpression)
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :255 :16) // CmdSendDrag(target.drag) (InvocationExpression)
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :256 :16) // Not a variable of known type: previousValue
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :256 :16) // previousValue.drag (SimpleMemberAccessExpression)
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :256 :37) // Not a variable of known type: target
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :256 :37) // target.drag (SimpleMemberAccessExpression)
br ^6

^6: // BinaryBranchBlock
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :258 :16) // Not a variable of known type: previousValue
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :258 :16) // previousValue.angularDrag (SimpleMemberAccessExpression)
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :258 :45) // Not a variable of known type: target
%39 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :258 :45) // target.angularDrag (SimpleMemberAccessExpression)
%40 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :258 :16) // comparison of unknown type: previousValue.angularDrag != target.angularDrag
cond_br %40, ^7, ^8 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :258 :16)

^7: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CmdSendAngularDrag
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :260 :35) // Not a variable of known type: target
%42 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :260 :35) // target.angularDrag (SimpleMemberAccessExpression)
%43 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :260 :16) // CmdSendAngularDrag(target.angularDrag) (InvocationExpression)
%44 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :261 :16) // Not a variable of known type: previousValue
%45 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :261 :16) // previousValue.angularDrag (SimpleMemberAccessExpression)
%46 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :261 :44) // Not a variable of known type: target
%47 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :261 :44) // target.angularDrag (SimpleMemberAccessExpression)
br ^8

^8: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkRigidbody2D.CmdSendVelocity$UnityEngine.Vector2$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :268 :8) {
^entry (%_velocity : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :269 :29)
cbde.store %_velocity, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :269 :29)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :272 :17) // Not a variable of known type: clientAuthority
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :272 :16) // !clientAuthority (LogicalNotExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :272 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :273 :16)

^2: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :275 :12) // this (ThisExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :275 :12) // this.velocity (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :275 :28) // Not a variable of known type: velocity
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :276 :12) // Not a variable of known type: target
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :276 :12) // target.velocity (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :276 :30) // Not a variable of known type: velocity
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkRigidbody2D.CmdSendVelocityAndAngular$UnityEngine.Vector2.float$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :282 :8) {
^entry (%_velocity : none, %_angularVelocity : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :283 :39)
cbde.store %_velocity, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :283 :39)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :283 :57)
cbde.store %_angularVelocity, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :283 :57)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :286 :17) // Not a variable of known type: clientAuthority
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :286 :16) // !clientAuthority (LogicalNotExpression)
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :286 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :287 :16)

^2: // BinaryBranchBlock
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :289 :16) // Not a variable of known type: syncVelocity
cond_br %4, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :289 :16)

^3: // SimpleBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :291 :16) // this (ThisExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :291 :16) // this.velocity (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :291 :32) // Not a variable of known type: velocity
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :293 :16) // Not a variable of known type: target
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :293 :16) // target.velocity (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :293 :34) // Not a variable of known type: velocity
br ^4

^4: // SimpleBlock
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :296 :12) // this (ThisExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :296 :12) // this.angularVelocity (SimpleMemberAccessExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :296 :35) // Not a variable of known type: angularVelocity
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :297 :12) // Not a variable of known type: target
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :297 :12) // target.angularVelocity (SimpleMemberAccessExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :297 :37) // Not a variable of known type: angularVelocity
br ^5

^5: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkRigidbody2D.CmdSendIsKinematic$bool$(i1) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :300 :8) {
^entry (%_isKinematic : i1):
%0 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :301 :32)
cbde.store %_isKinematic, %0 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :301 :32)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :304 :17) // Not a variable of known type: clientAuthority
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :304 :16) // !clientAuthority (LogicalNotExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :304 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :305 :16)

^2: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :307 :12) // this (ThisExpression)
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :307 :12) // this.isKinematic (SimpleMemberAccessExpression)
%5 = cbde.load %0 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :307 :31)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :308 :12) // Not a variable of known type: target
%7 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :308 :12) // target.isKinematic (SimpleMemberAccessExpression)
%8 = cbde.load %0 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :308 :33)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkRigidbody2D.CmdChangeGravityScale$float$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :311 :8) {
^entry (%_gravityScale : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :312 :35)
cbde.store %_gravityScale, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :312 :35)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :315 :17) // Not a variable of known type: clientAuthority
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :315 :16) // !clientAuthority (LogicalNotExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :315 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :316 :16)

^2: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :318 :12) // this (ThisExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :318 :12) // this.gravityScale (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :318 :32) // Not a variable of known type: gravityScale
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :319 :12) // Not a variable of known type: target
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :319 :12) // target.gravityScale (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :319 :34) // Not a variable of known type: gravityScale
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkRigidbody2D.CmdSendDrag$float$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :322 :8) {
^entry (%_drag : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :323 :25)
cbde.store %_drag, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :323 :25)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :326 :17) // Not a variable of known type: clientAuthority
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :326 :16) // !clientAuthority (LogicalNotExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :326 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :327 :16)

^2: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :329 :12) // this (ThisExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :329 :12) // this.drag (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :329 :24) // Not a variable of known type: drag
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :330 :12) // Not a variable of known type: target
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :330 :12) // target.drag (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :330 :26) // Not a variable of known type: drag
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkRigidbody2D.CmdSendAngularDrag$float$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :333 :8) {
^entry (%_angularDrag : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :334 :32)
cbde.store %_angularDrag, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :334 :32)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :337 :17) // Not a variable of known type: clientAuthority
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :337 :16) // !clientAuthority (LogicalNotExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :337 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :338 :16)

^2: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :340 :12) // this (ThisExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :340 :12) // this.angularDrag (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :340 :31) // Not a variable of known type: angularDrag
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :341 :12) // Not a variable of known type: target
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :341 :12) // target.angularDrag (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody2D.cs" :341 :33) // Not a variable of known type: angularDrag
br ^3

^3: // ExitBlock
return

}
