// Skipping function FixedUpdate(), it contains poisonous unsupported syntaxes

func @_Mirror.Experimental.NetworkTransformBase.ServerUpdate$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :138 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: RpcMove
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :140 :20) // Not a variable of known type: targetTransform
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :140 :20) // targetTransform.localPosition (SimpleMemberAccessExpression)
// Entity from another assembly: Compression
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :140 :82) // Not a variable of known type: targetTransform
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :140 :82) // targetTransform.localRotation (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :140 :51) // Compression.CompressQuaternion(targetTransform.localRotation) (InvocationExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :140 :114) // Not a variable of known type: targetTransform
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :140 :114) // targetTransform.localScale (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :140 :12) // RpcMove(targetTransform.localPosition, Compression.CompressQuaternion(targetTransform.localRotation), targetTransform.localScale) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function ClientAuthorityUpdate(), it contains poisonous unsupported syntaxes

func @_Mirror.Experimental.NetworkTransformBase.ClientRemoteUpdate$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :154 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NeedsTeleport
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :157 :16) // NeedsTeleport() (InvocationExpression)
cond_br %0, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :157 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ApplyPositionRotationScale
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :160 :43) // Not a variable of known type: goal
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :160 :43) // goal.localPosition (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :160 :63) // Not a variable of known type: goal
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :160 :63) // goal.localRotation (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :160 :83) // Not a variable of known type: goal
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :160 :83) // goal.localScale (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :160 :16) // ApplyPositionRotationScale(goal.localPosition, goal.localRotation, goal.localScale) (InvocationExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :163 :24) // new DataPoint() (ObjectCreationExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :164 :23) // new DataPoint() (ObjectCreationExpression)
br ^3

^2: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ApplyPositionRotationScale
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: InterpolatePosition
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :169 :63) // Not a variable of known type: start
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :169 :70) // Not a variable of known type: goal
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :169 :76) // Not a variable of known type: targetTransform
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :169 :76) // targetTransform.localPosition (SimpleMemberAccessExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :169 :43) // InterpolatePosition(start, goal, targetTransform.localPosition) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: InterpolateRotation
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :170 :63) // Not a variable of known type: start
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :170 :70) // Not a variable of known type: goal
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :170 :76) // Not a variable of known type: targetTransform
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :170 :76) // targetTransform.localRotation (SimpleMemberAccessExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :170 :43) // InterpolateRotation(start, goal, targetTransform.localRotation) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: InterpolateScale
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :171 :60) // Not a variable of known type: start
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :171 :67) // Not a variable of known type: goal
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :171 :73) // Not a variable of known type: targetTransform
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :171 :73) // targetTransform.localScale (SimpleMemberAccessExpression)
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :171 :43) // InterpolateScale(start, goal, targetTransform.localScale) (InvocationExpression)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :169 :16) // ApplyPositionRotationScale(InterpolatePosition(start, goal, targetTransform.localPosition),                                            InterpolateRotation(start, goal, targetTransform.localRotation),                                            InterpolateScale(start, goal, targetTransform.localScale)) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function HasEitherMovedRotatedScaled(), it contains poisonous unsupported syntaxes

// Skipping function NeedsTeleport(), it contains poisonous unsupported syntaxes

// Skipping function CmdClientToServerSync(none, none, none), it contains poisonous unsupported syntaxes

// Skipping function RpcMove(none, none, none), it contains poisonous unsupported syntaxes

func @_Mirror.Experimental.NetworkTransformBase.SetGoal$UnityEngine.Vector3.UnityEngine.Quaternion.UnityEngine.Vector3$(none, none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :241 :8) {
^entry (%_position : none, %_rotation : none, %_scale : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :241 :21)
cbde.store %_position, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :241 :21)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :241 :39)
cbde.store %_rotation, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :241 :39)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :241 :60)
cbde.store %_scale, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :241 :60)
br ^0

^0: // BinaryBranchBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :244 :29) // new DataPoint             {                 // deserialize position                 localPosition = position,                 localRotation = rotation,                 localScale = scale,                 timeStamp = Time.time             } (ObjectCreationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :247 :32) // Not a variable of known type: position
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :248 :32) // Not a variable of known type: rotation
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :249 :29) // Not a variable of known type: scale
// Entity from another assembly: Time
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :250 :28) // Time.time (SimpleMemberAccessExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :254 :12) // Not a variable of known type: temp
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :254 :12) // temp.movementSpeed (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: EstimateMovementSpeed
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :254 :55) // Not a variable of known type: goal
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :254 :61) // Not a variable of known type: temp
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :254 :67) // Not a variable of known type: targetTransform
// Entity from another assembly: Time
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :254 :84) // Time.fixedDeltaTime (SimpleMemberAccessExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :254 :33) // EstimateMovementSpeed(goal, temp, targetTransform, Time.fixedDeltaTime) (InvocationExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :258 :16) // Not a variable of known type: start
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :258 :16) // start.timeStamp (SimpleMemberAccessExpression)
%18 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :258 :35)
%19 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :258 :16) // comparison of unknown type: start.timeStamp == 0
cond_br %19, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :258 :16)

^1: // SimpleBlock
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :260 :24) // new DataPoint                 {                     timeStamp = Time.time - Time.fixedDeltaTime,                     // local position/rotation for VR support                     localPosition = targetTransform.localPosition,                     localRotation = targetTransform.localRotation,                     localScale = targetTransform.localScale,                     movementSpeed = temp.movementSpeed                 } (ObjectCreationExpression)
// Entity from another assembly: Time
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :262 :32) // Time.time (SimpleMemberAccessExpression)
// Entity from another assembly: Time
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :262 :44) // Time.fixedDeltaTime (SimpleMemberAccessExpression)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :262 :32) // Binary expression on unsupported types Time.time - Time.fixedDeltaTime
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :264 :36) // Not a variable of known type: targetTransform
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :264 :36) // targetTransform.localPosition (SimpleMemberAccessExpression)
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :265 :36) // Not a variable of known type: targetTransform
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :265 :36) // targetTransform.localRotation (SimpleMemberAccessExpression)
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :266 :33) // Not a variable of known type: targetTransform
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :266 :33) // targetTransform.localScale (SimpleMemberAccessExpression)
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :267 :36) // Not a variable of known type: temp
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :267 :36) // temp.movementSpeed (SimpleMemberAccessExpression)
br ^3

^2: // BinaryBranchBlock
// Entity from another assembly: Vector3
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :300 :53) // Not a variable of known type: start
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :300 :53) // start.localPosition (SimpleMemberAccessExpression)
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :300 :74) // Not a variable of known type: goal
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :300 :74) // goal.localPosition (SimpleMemberAccessExpression)
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :300 :36) // Vector3.Distance(start.localPosition, goal.localPosition) (InvocationExpression)
// Entity from another assembly: Vector3
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :301 :53) // Not a variable of known type: goal
%39 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :301 :53) // goal.localPosition (SimpleMemberAccessExpression)
%40 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :301 :73) // Not a variable of known type: temp
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :301 :73) // temp.localPosition (SimpleMemberAccessExpression)
%42 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :301 :36) // Vector3.Distance(goal.localPosition, temp.localPosition) (InvocationExpression)
%44 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :303 :24) // Not a variable of known type: goal
// Entity from another assembly: Vector3
%45 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :308 :37) // Not a variable of known type: targetTransform
%46 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :308 :37) // targetTransform.localPosition (SimpleMemberAccessExpression)
%47 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :308 :68) // Not a variable of known type: start
%48 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :308 :68) // start.localPosition (SimpleMemberAccessExpression)
%49 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :308 :20) // Vector3.Distance(targetTransform.localPosition, start.localPosition) (InvocationExpression)
%50 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :308 :91) // Not a variable of known type: oldDistance
%51 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :308 :105) // Not a variable of known type: newDistance
%52 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :308 :91) // Binary expression on unsupported types oldDistance + newDistance
%53 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :308 :20) // comparison of unknown type: Vector3.Distance(targetTransform.localPosition, start.localPosition) < oldDistance + newDistance
cond_br %53, ^4, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :308 :20)

^4: // SimpleBlock
%54 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :310 :20) // Not a variable of known type: start
%55 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :310 :20) // start.localPosition (SimpleMemberAccessExpression)
%56 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :310 :42) // Not a variable of known type: targetTransform
%57 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :310 :42) // targetTransform.localPosition (SimpleMemberAccessExpression)
%58 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :311 :20) // Not a variable of known type: start
%59 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :311 :20) // start.localRotation (SimpleMemberAccessExpression)
%60 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :311 :42) // Not a variable of known type: targetTransform
%61 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :311 :42) // targetTransform.localRotation (SimpleMemberAccessExpression)
%62 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :312 :20) // Not a variable of known type: start
%63 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :312 :20) // start.localScale (SimpleMemberAccessExpression)
%64 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :312 :39) // Not a variable of known type: targetTransform
%65 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :312 :39) // targetTransform.localScale (SimpleMemberAccessExpression)
br ^3

^3: // SimpleBlock
%66 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :317 :19) // Not a variable of known type: temp
br ^5

^5: // ExitBlock
return

}
// Skipping function EstimateMovementSpeed(none, none, none, none), it contains poisonous unsupported syntaxes

func @_Mirror.Experimental.NetworkTransformBase.ApplyPositionRotationScale$UnityEngine.Vector3.UnityEngine.Quaternion.UnityEngine.Vector3$(none, none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :334 :8) {
^entry (%_position : none, %_rotation : none, %_scale : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :334 :40)
cbde.store %_position, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :334 :40)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :334 :58)
cbde.store %_rotation, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :334 :58)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :334 :79)
cbde.store %_scale, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :334 :79)
br ^0

^0: // BinaryBranchBlock
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :337 :16) // Not a variable of known type: syncPosition
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :337 :16)

^1: // SimpleBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :337 :30) // Not a variable of known type: targetTransform
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :337 :30) // targetTransform.localPosition (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :337 :62) // Not a variable of known type: position
br ^2

^2: // BinaryBranchBlock
%7 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :338 :16) // Not a variable of known type: syncRotation
cond_br %7, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :338 :16)

^3: // SimpleBlock
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :338 :30) // Not a variable of known type: targetTransform
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :338 :30) // targetTransform.localRotation (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :338 :62) // Not a variable of known type: rotation
br ^4

^4: // BinaryBranchBlock
%11 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :339 :16) // Not a variable of known type: syncScale
cond_br %11, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :339 :16)

^5: // SimpleBlock
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :339 :27) // Not a variable of known type: targetTransform
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :339 :27) // targetTransform.localScale (SimpleMemberAccessExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :339 :56) // Not a variable of known type: scale
br ^6

^6: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkTransformBase.InterpolatePosition$Mirror.Experimental.NetworkTransformBase.DataPoint.Mirror.Experimental.NetworkTransformBase.DataPoint.UnityEngine.Vector3$(none, none, none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :343 :8) {
^entry (%_start : none, %_goal : none, %_currentPosition : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :343 :36)
cbde.store %_start, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :343 :36)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :343 :53)
cbde.store %_goal, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :343 :53)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :343 :69)
cbde.store %_currentPosition, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :343 :69)
br ^0

^0: // BinaryBranchBlock
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :345 :17) // Not a variable of known type: interpolatePosition
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :345 :16) // !interpolatePosition (LogicalNotExpression)
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :345 :16)

^1: // JumpBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :346 :23) // Not a variable of known type: currentPosition
return %5 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :346 :16)

^2: // BinaryBranchBlock
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :348 :16) // Not a variable of known type: start
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :348 :16) // start.movementSpeed (SimpleMemberAccessExpression)
%8 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :348 :39)
%9 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :348 :16) // comparison of unknown type: start.movementSpeed != 0
cond_br %9, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :348 :16)

^3: // JumpBlock
// Entity from another assembly: Mathf
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :358 :40) // Not a variable of known type: start
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :358 :40) // start.movementSpeed (SimpleMemberAccessExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :358 :61) // Not a variable of known type: goal
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :358 :61) // goal.movementSpeed (SimpleMemberAccessExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :358 :30) // Mathf.Max(start.movementSpeed, goal.movementSpeed) (InvocationExpression)
// Entity from another assembly: Vector3
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :359 :43) // Not a variable of known type: currentPosition
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :359 :60) // Not a variable of known type: goal
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :359 :60) // goal.localPosition (SimpleMemberAccessExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :359 :80) // Not a variable of known type: speed
// Entity from another assembly: Time
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :359 :88) // Time.deltaTime (SimpleMemberAccessExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :359 :80) // Binary expression on unsupported types speed * Time.deltaTime
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :359 :23) // Vector3.MoveTowards(currentPosition, goal.localPosition, speed * Time.deltaTime) (InvocationExpression)
return %22 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :359 :16)

^4: // JumpBlock
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :362 :19) // Not a variable of known type: currentPosition
return %23 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :362 :12)

^5: // ExitBlock
cbde.unreachable

}
func @_Mirror.Experimental.NetworkTransformBase.InterpolateRotation$Mirror.Experimental.NetworkTransformBase.DataPoint.Mirror.Experimental.NetworkTransformBase.DataPoint.UnityEngine.Quaternion$(none, none, none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :365 :8) {
^entry (%_start : none, %_goal : none, %_defaultRotation : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :365 :39)
cbde.store %_start, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :365 :39)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :365 :56)
cbde.store %_goal, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :365 :56)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :365 :72)
cbde.store %_defaultRotation, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :365 :72)
br ^0

^0: // BinaryBranchBlock
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :367 :17) // Not a variable of known type: interpolateRotation
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :367 :16) // !interpolateRotation (LogicalNotExpression)
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :367 :16)

^1: // JumpBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :368 :23) // Not a variable of known type: defaultRotation
return %5 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :368 :16)

^2: // BinaryBranchBlock
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :370 :16) // Not a variable of known type: start
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :370 :16) // start.localRotation (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :370 :39) // Not a variable of known type: goal
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :370 :39) // goal.localRotation (SimpleMemberAccessExpression)
%10 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :370 :16) // comparison of unknown type: start.localRotation != goal.localRotation
cond_br %10, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :370 :16)

^3: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CurrentInterpolationFactor
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :372 :53) // Not a variable of known type: start
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :372 :60) // Not a variable of known type: goal
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :372 :26) // CurrentInterpolationFactor(start, goal) (InvocationExpression)
// Entity from another assembly: Quaternion
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :373 :40) // Not a variable of known type: start
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :373 :40) // start.localRotation (SimpleMemberAccessExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :373 :61) // Not a variable of known type: goal
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :373 :61) // goal.localRotation (SimpleMemberAccessExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :373 :81) // Not a variable of known type: t
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :373 :23) // Quaternion.Slerp(start.localRotation, goal.localRotation, t) (InvocationExpression)
return %20 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :373 :16)

^4: // JumpBlock
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :376 :19) // Not a variable of known type: defaultRotation
return %21 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :376 :12)

^5: // ExitBlock
cbde.unreachable

}
func @_Mirror.Experimental.NetworkTransformBase.InterpolateScale$Mirror.Experimental.NetworkTransformBase.DataPoint.Mirror.Experimental.NetworkTransformBase.DataPoint.UnityEngine.Vector3$(none, none, none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :379 :8) {
^entry (%_start : none, %_goal : none, %_currentScale : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :379 :33)
cbde.store %_start, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :379 :33)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :379 :50)
cbde.store %_goal, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :379 :50)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :379 :66)
cbde.store %_currentScale, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :379 :66)
br ^0

^0: // BinaryBranchBlock
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :381 :17) // Not a variable of known type: interpolateScale
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :381 :16) // !interpolateScale (LogicalNotExpression)
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :381 :16)

^1: // JumpBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :382 :23) // Not a variable of known type: currentScale
return %5 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :382 :16)

^2: // BinaryBranchBlock
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :384 :16) // Not a variable of known type: start
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :384 :16) // start.localScale (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :384 :36) // Not a variable of known type: goal
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :384 :36) // goal.localScale (SimpleMemberAccessExpression)
%10 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :384 :16) // comparison of unknown type: start.localScale != goal.localScale
cond_br %10, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :384 :16)

^3: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CurrentInterpolationFactor
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :386 :53) // Not a variable of known type: start
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :386 :60) // Not a variable of known type: goal
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :386 :26) // CurrentInterpolationFactor(start, goal) (InvocationExpression)
// Entity from another assembly: Vector3
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :387 :36) // Not a variable of known type: start
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :387 :36) // start.localScale (SimpleMemberAccessExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :387 :54) // Not a variable of known type: goal
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :387 :54) // goal.localScale (SimpleMemberAccessExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :387 :71) // Not a variable of known type: t
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :387 :23) // Vector3.Lerp(start.localScale, goal.localScale, t) (InvocationExpression)
return %20 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :387 :16)

^4: // JumpBlock
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :390 :19) // Not a variable of known type: currentScale
return %21 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :390 :12)

^5: // ExitBlock
cbde.unreachable

}
// Skipping function CurrentInterpolationFactor(none, none), it contains poisonous unsupported syntaxes

func @_Mirror.Experimental.NetworkTransformBase.ServerTeleport$UnityEngine.Vector3$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :415 :8) {
^entry (%_localPosition : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :416 :35)
cbde.store %_localPosition, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :416 :35)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :418 :39) // Not a variable of known type: targetTransform
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :418 :39) // targetTransform.localRotation (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ServerTeleport
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :419 :27) // Not a variable of known type: localPosition
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :419 :42) // Not a variable of known type: localRotation
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :419 :12) // ServerTeleport(localPosition, localRotation) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function ServerTeleport(none, none), it contains poisonous unsupported syntaxes

func @_Mirror.Experimental.NetworkTransformBase.DoTeleport$UnityEngine.Vector3.UnityEngine.Quaternion$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :445 :8) {
^entry (%_newLocalPosition : none, %_newLocalRotation : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :445 :24)
cbde.store %_newLocalPosition, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :445 :24)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :445 :50)
cbde.store %_newLocalRotation, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :445 :50)
br ^0

^0: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :447 :12) // Not a variable of known type: targetTransform
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :447 :12) // targetTransform.localPosition (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :447 :44) // Not a variable of known type: newLocalPosition
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :448 :12) // Not a variable of known type: targetTransform
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :448 :12) // targetTransform.localRotation (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :448 :44) // Not a variable of known type: newLocalRotation
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :452 :19) // new DataPoint() (ObjectCreationExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :453 :20) // new DataPoint() (ObjectCreationExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :454 :27) // Not a variable of known type: newLocalPosition
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :455 :27) // Not a variable of known type: newLocalRotation
br ^1

^1: // ExitBlock
return

}
// Skipping function RpcTeleport(none, none, i1), it contains poisonous unsupported syntaxes

func @_Mirror.Experimental.NetworkTransformBase.CmdTeleportFinished$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :472 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :475 :16) // Not a variable of known type: clientAuthorityBeforeTeleport
cond_br %0, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :475 :16)

^1: // SimpleBlock
%1 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :477 :34) // true
%2 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :480 :48) // false
br ^3

^2: // SimpleBlock
// Entity from another assembly: Debug
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :484 :33) // "Client called TeleportFinished when clientAuthority was false on server" (StringLiteralExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :484 :108) // this (ThisExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :484 :16) // Debug.LogWarning("Client called TeleportFinished when clientAuthority was false on server", this) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkTransformBase.OnDrawGizmos$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :493 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :496 :16) // Not a variable of known type: start
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :496 :16) // start.localPosition (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :496 :39) // Not a variable of known type: goal
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :496 :39) // goal.localPosition (SimpleMemberAccessExpression)
%4 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :496 :16) // comparison of unknown type: start.localPosition != goal.localPosition
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :496 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: DrawDataPointGizmo
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :498 :35) // Not a variable of known type: start
// Entity from another assembly: Color
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :498 :42) // Color.yellow (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :498 :16) // DrawDataPointGizmo(start, Color.yellow) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: DrawDataPointGizmo
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :499 :35) // Not a variable of known type: goal
// Entity from another assembly: Color
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :499 :41) // Color.green (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :499 :16) // DrawDataPointGizmo(goal, Color.green) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: DrawLineBetweenDataPoints
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :500 :42) // Not a variable of known type: start
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :500 :49) // Not a variable of known type: goal
// Entity from another assembly: Color
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :500 :55) // Color.cyan (SimpleMemberAccessExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :500 :16) // DrawLineBetweenDataPoints(start, goal, Color.cyan) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkTransformBase.DrawDataPointGizmo$Mirror.Experimental.NetworkTransformBase.DataPoint.UnityEngine.Color$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :504 :8) {
^entry (%_data : none, %_color : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :504 :39)
cbde.store %_data, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :504 :39)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :504 :55)
cbde.store %_color, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :504 :55)
br ^0

^0: // SimpleBlock
// Entity from another assembly: Vector3
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :507 :29) // Vector3.up (SimpleMemberAccessExpression)
%3 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :507 :42) // 0.01f (NumericLiteralExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :507 :29) // Binary expression on unsupported types Vector3.up * 0.01f
// Entity from another assembly: Gizmos
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :510 :12) // Gizmos.color (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :510 :27) // Not a variable of known type: color
// Entity from another assembly: Gizmos
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :511 :30) // Not a variable of known type: data
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :511 :30) // data.localPosition (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :511 :51) // Not a variable of known type: offset
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :511 :30) // Binary expression on unsupported types data.localPosition + offset
%12 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :511 :59) // 0.5f (NumericLiteralExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :511 :12) // Gizmos.DrawSphere(data.localPosition + offset, 0.5f) (InvocationExpression)
// Entity from another assembly: Gizmos
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :514 :12) // Gizmos.color (SimpleMemberAccessExpression)
// Entity from another assembly: Color
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :514 :27) // Color.blue (SimpleMemberAccessExpression)
// Entity from another assembly: Gizmos
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :515 :27) // Not a variable of known type: data
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :515 :27) // data.localPosition (SimpleMemberAccessExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :515 :48) // Not a variable of known type: offset
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :515 :27) // Binary expression on unsupported types data.localPosition + offset
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :515 :56) // Not a variable of known type: data
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :515 :56) // data.localRotation (SimpleMemberAccessExpression)
// Entity from another assembly: Vector3
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :515 :77) // Vector3.forward (SimpleMemberAccessExpression)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :515 :56) // Binary expression on unsupported types data.localRotation * Vector3.forward
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :515 :12) // Gizmos.DrawRay(data.localPosition + offset, data.localRotation * Vector3.forward) (InvocationExpression)
// Entity from another assembly: Gizmos
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :516 :12) // Gizmos.color (SimpleMemberAccessExpression)
// Entity from another assembly: Color
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :516 :27) // Color.green (SimpleMemberAccessExpression)
// Entity from another assembly: Gizmos
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :517 :27) // Not a variable of known type: data
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :517 :27) // data.localPosition (SimpleMemberAccessExpression)
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :517 :48) // Not a variable of known type: offset
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :517 :27) // Binary expression on unsupported types data.localPosition + offset
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :517 :56) // Not a variable of known type: data
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :517 :56) // data.localRotation (SimpleMemberAccessExpression)
// Entity from another assembly: Vector3
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :517 :77) // Vector3.up (SimpleMemberAccessExpression)
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :517 :56) // Binary expression on unsupported types data.localRotation * Vector3.up
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :517 :12) // Gizmos.DrawRay(data.localPosition + offset, data.localRotation * Vector3.up) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkTransformBase.DrawLineBetweenDataPoints$Mirror.Experimental.NetworkTransformBase.DataPoint.Mirror.Experimental.NetworkTransformBase.DataPoint.UnityEngine.Color$(none, none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :520 :8) {
^entry (%_data1 : none, %_data2 : none, %_color : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :520 :46)
cbde.store %_data1, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :520 :46)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :520 :63)
cbde.store %_data2, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :520 :63)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :520 :80)
cbde.store %_color, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :520 :80)
br ^0

^0: // SimpleBlock
// Entity from another assembly: Gizmos
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :522 :12) // Gizmos.color (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :522 :27) // Not a variable of known type: color
// Entity from another assembly: Gizmos
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :523 :28) // Not a variable of known type: data1
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :523 :28) // data1.localPosition (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :523 :49) // Not a variable of known type: data2
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :523 :49) // data2.localPosition (SimpleMemberAccessExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkTransformBase.cs" :523 :12) // Gizmos.DrawLine(data1.localPosition, data2.localPosition) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
