func @_Mirror.Experimental.NetworkRigidbody.OnValidate$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :44 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :46 :16) // Not a variable of known type: target
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :46 :26) // null (NullLiteralExpression)
%2 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :46 :16) // comparison of unknown type: target == null
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :46 :16)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :48 :25) // GetComponent<Rigidbody>() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkRigidbody.OnVelocityChanged$UnityEngine.Vector3.UnityEngine.Vector3$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :80 :8) {
^entry (%__ : none, %_newValue : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :80 :31)
cbde.store %__, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :80 :31)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :80 :42)
cbde.store %_newValue, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :80 :42)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :82 :16) // Not a variable of known type: IgnoreSync
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :82 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :83 :16)

^2: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :85 :12) // Not a variable of known type: target
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :85 :12) // target.velocity (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :85 :30) // Not a variable of known type: newValue
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkRigidbody.OnAngularVelocityChanged$UnityEngine.Vector3.UnityEngine.Vector3$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :89 :8) {
^entry (%__ : none, %_newValue : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :89 :38)
cbde.store %__, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :89 :38)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :89 :49)
cbde.store %_newValue, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :89 :49)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :91 :16) // Not a variable of known type: IgnoreSync
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :91 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :92 :16)

^2: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :94 :12) // Not a variable of known type: target
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :94 :12) // target.angularVelocity (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :94 :37) // Not a variable of known type: newValue
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkRigidbody.OnIsKinematicChanged$bool.bool$(i1, i1) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :97 :8) {
^entry (%__ : i1, %_newValue : i1):
%0 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :97 :34)
cbde.store %__, %0 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :97 :34)
%1 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :97 :42)
cbde.store %_newValue, %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :97 :42)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :99 :16) // Not a variable of known type: IgnoreSync
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :99 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :100 :16)

^2: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :102 :12) // Not a variable of known type: target
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :102 :12) // target.isKinematic (SimpleMemberAccessExpression)
%5 = cbde.load %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :102 :33)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkRigidbody.OnUseGravityChanged$bool.bool$(i1, i1) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :105 :8) {
^entry (%__ : i1, %_newValue : i1):
%0 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :105 :33)
cbde.store %__, %0 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :105 :33)
%1 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :105 :41)
cbde.store %_newValue, %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :105 :41)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :107 :16) // Not a variable of known type: IgnoreSync
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :107 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :108 :16)

^2: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :110 :12) // Not a variable of known type: target
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :110 :12) // target.useGravity (SimpleMemberAccessExpression)
%5 = cbde.load %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :110 :32)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkRigidbody.OnuDragChanged$float.float$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :113 :8) {
^entry (%__ : none, %_newValue : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :113 :28)
cbde.store %__, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :113 :28)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :113 :37)
cbde.store %_newValue, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :113 :37)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :115 :16) // Not a variable of known type: IgnoreSync
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :115 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :116 :16)

^2: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :118 :12) // Not a variable of known type: target
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :118 :12) // target.drag (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :118 :26) // Not a variable of known type: newValue
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkRigidbody.OnAngularDragChanged$float.float$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :121 :8) {
^entry (%__ : none, %_newValue : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :121 :34)
cbde.store %__, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :121 :34)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :121 :43)
cbde.store %_newValue, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :121 :43)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :123 :16) // Not a variable of known type: IgnoreSync
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :123 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :124 :16)

^2: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :126 :12) // Not a variable of known type: target
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :126 :12) // target.angularDrag (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :126 :33) // Not a variable of known type: newValue
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkRigidbody.Update$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :131 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :133 :16) // Identifier from another assembly: isServer
cond_br %0, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :133 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SyncToClients
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :135 :16) // SyncToClients() (InvocationExpression)
br ^3

^2: // BinaryBranchBlock
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :137 :21) // Not a variable of known type: ClientWithAuthority
cond_br %2, ^4, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :137 :21)

^4: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SendToServer
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :139 :16) // SendToServer() (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function FixedUpdate(), it contains poisonous unsupported syntaxes

// Skipping function SyncToClients(), it contains poisonous unsupported syntaxes

func @_Mirror.Experimental.NetworkRigidbody.SendToServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :192 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :195 :17) // Identifier from another assembly: hasAuthority
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :195 :16) // !hasAuthority (LogicalNotExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :195 :16)

^1: // JumpBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :197 :16) // Not a variable of known type: logger
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :197 :34) // "SendToServer called without authority" (StringLiteralExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :197 :16) // logger.LogWarning("SendToServer called without authority") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :198 :16)

^2: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SendVelocity
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :201 :12) // SendVelocity() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SendRigidBodySettings
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :202 :12) // SendRigidBodySettings() (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function SendVelocity(), it contains poisonous unsupported syntaxes

func @_Mirror.Experimental.NetworkRigidbody.SendRigidBodySettings$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :240 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :244 :16) // Not a variable of known type: previousValue
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :244 :16) // previousValue.isKinematic (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :244 :45) // Not a variable of known type: target
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :244 :45) // target.isKinematic (SimpleMemberAccessExpression)
%4 = cmpi "ne", %1, %3 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :244 :16)
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :244 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CmdSendIsKinematic
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :246 :35) // Not a variable of known type: target
%6 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :246 :35) // target.isKinematic (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :246 :16) // CmdSendIsKinematic(target.isKinematic) (InvocationExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :247 :16) // Not a variable of known type: previousValue
%9 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :247 :16) // previousValue.isKinematic (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :247 :44) // Not a variable of known type: target
%11 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :247 :44) // target.isKinematic (SimpleMemberAccessExpression)
br ^2

^2: // BinaryBranchBlock
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :249 :16) // Not a variable of known type: previousValue
%13 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :249 :16) // previousValue.useGravity (SimpleMemberAccessExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :249 :44) // Not a variable of known type: target
%15 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :249 :44) // target.useGravity (SimpleMemberAccessExpression)
%16 = cmpi "ne", %13, %15 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :249 :16)
cond_br %16, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :249 :16)

^3: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CmdSendUseGravity
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :251 :34) // Not a variable of known type: target
%18 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :251 :34) // target.useGravity (SimpleMemberAccessExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :251 :16) // CmdSendUseGravity(target.useGravity) (InvocationExpression)
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :252 :16) // Not a variable of known type: previousValue
%21 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :252 :16) // previousValue.useGravity (SimpleMemberAccessExpression)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :252 :43) // Not a variable of known type: target
%23 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :252 :43) // target.useGravity (SimpleMemberAccessExpression)
br ^4

^4: // BinaryBranchBlock
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :254 :16) // Not a variable of known type: previousValue
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :254 :16) // previousValue.drag (SimpleMemberAccessExpression)
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :254 :38) // Not a variable of known type: target
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :254 :38) // target.drag (SimpleMemberAccessExpression)
%28 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :254 :16) // comparison of unknown type: previousValue.drag != target.drag
cond_br %28, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :254 :16)

^5: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CmdSendDrag
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :256 :28) // Not a variable of known type: target
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :256 :28) // target.drag (SimpleMemberAccessExpression)
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :256 :16) // CmdSendDrag(target.drag) (InvocationExpression)
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :257 :16) // Not a variable of known type: previousValue
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :257 :16) // previousValue.drag (SimpleMemberAccessExpression)
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :257 :37) // Not a variable of known type: target
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :257 :37) // target.drag (SimpleMemberAccessExpression)
br ^6

^6: // BinaryBranchBlock
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :259 :16) // Not a variable of known type: previousValue
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :259 :16) // previousValue.angularDrag (SimpleMemberAccessExpression)
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :259 :45) // Not a variable of known type: target
%39 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :259 :45) // target.angularDrag (SimpleMemberAccessExpression)
%40 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :259 :16) // comparison of unknown type: previousValue.angularDrag != target.angularDrag
cond_br %40, ^7, ^8 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :259 :16)

^7: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CmdSendAngularDrag
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :261 :35) // Not a variable of known type: target
%42 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :261 :35) // target.angularDrag (SimpleMemberAccessExpression)
%43 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :261 :16) // CmdSendAngularDrag(target.angularDrag) (InvocationExpression)
%44 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :262 :16) // Not a variable of known type: previousValue
%45 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :262 :16) // previousValue.angularDrag (SimpleMemberAccessExpression)
%46 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :262 :44) // Not a variable of known type: target
%47 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :262 :44) // target.angularDrag (SimpleMemberAccessExpression)
br ^8

^8: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkRigidbody.CmdSendVelocity$UnityEngine.Vector3$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :269 :8) {
^entry (%_velocity : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :270 :29)
cbde.store %_velocity, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :270 :29)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :273 :17) // Not a variable of known type: clientAuthority
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :273 :16) // !clientAuthority (LogicalNotExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :273 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :274 :16)

^2: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :276 :12) // this (ThisExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :276 :12) // this.velocity (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :276 :28) // Not a variable of known type: velocity
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :277 :12) // Not a variable of known type: target
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :277 :12) // target.velocity (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :277 :30) // Not a variable of known type: velocity
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkRigidbody.CmdSendVelocityAndAngular$UnityEngine.Vector3.UnityEngine.Vector3$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :283 :8) {
^entry (%_velocity : none, %_angularVelocity : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :284 :39)
cbde.store %_velocity, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :284 :39)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :284 :57)
cbde.store %_angularVelocity, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :284 :57)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :287 :17) // Not a variable of known type: clientAuthority
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :287 :16) // !clientAuthority (LogicalNotExpression)
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :287 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :288 :16)

^2: // BinaryBranchBlock
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :290 :16) // Not a variable of known type: syncVelocity
cond_br %4, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :290 :16)

^3: // SimpleBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :292 :16) // this (ThisExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :292 :16) // this.velocity (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :292 :32) // Not a variable of known type: velocity
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :294 :16) // Not a variable of known type: target
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :294 :16) // target.velocity (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :294 :34) // Not a variable of known type: velocity
br ^4

^4: // SimpleBlock
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :297 :12) // this (ThisExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :297 :12) // this.angularVelocity (SimpleMemberAccessExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :297 :35) // Not a variable of known type: angularVelocity
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :298 :12) // Not a variable of known type: target
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :298 :12) // target.angularVelocity (SimpleMemberAccessExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :298 :37) // Not a variable of known type: angularVelocity
br ^5

^5: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkRigidbody.CmdSendIsKinematic$bool$(i1) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :301 :8) {
^entry (%_isKinematic : i1):
%0 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :302 :32)
cbde.store %_isKinematic, %0 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :302 :32)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :305 :17) // Not a variable of known type: clientAuthority
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :305 :16) // !clientAuthority (LogicalNotExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :305 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :306 :16)

^2: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :308 :12) // this (ThisExpression)
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :308 :12) // this.isKinematic (SimpleMemberAccessExpression)
%5 = cbde.load %0 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :308 :31)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :309 :12) // Not a variable of known type: target
%7 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :309 :12) // target.isKinematic (SimpleMemberAccessExpression)
%8 = cbde.load %0 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :309 :33)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkRigidbody.CmdSendUseGravity$bool$(i1) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :312 :8) {
^entry (%_useGravity : i1):
%0 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :313 :31)
cbde.store %_useGravity, %0 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :313 :31)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :316 :17) // Not a variable of known type: clientAuthority
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :316 :16) // !clientAuthority (LogicalNotExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :316 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :317 :16)

^2: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :319 :12) // this (ThisExpression)
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :319 :12) // this.useGravity (SimpleMemberAccessExpression)
%5 = cbde.load %0 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :319 :30)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :320 :12) // Not a variable of known type: target
%7 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :320 :12) // target.useGravity (SimpleMemberAccessExpression)
%8 = cbde.load %0 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :320 :32)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkRigidbody.CmdSendDrag$float$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :323 :8) {
^entry (%_drag : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :324 :25)
cbde.store %_drag, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :324 :25)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :327 :17) // Not a variable of known type: clientAuthority
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :327 :16) // !clientAuthority (LogicalNotExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :327 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :328 :16)

^2: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :330 :12) // this (ThisExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :330 :12) // this.drag (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :330 :24) // Not a variable of known type: drag
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :331 :12) // Not a variable of known type: target
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :331 :12) // target.drag (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :331 :26) // Not a variable of known type: drag
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Experimental.NetworkRigidbody.CmdSendAngularDrag$float$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :334 :8) {
^entry (%_angularDrag : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :335 :32)
cbde.store %_angularDrag, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :335 :32)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :338 :17) // Not a variable of known type: clientAuthority
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :338 :16) // !clientAuthority (LogicalNotExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :338 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :339 :16)

^2: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :341 :12) // this (ThisExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :341 :12) // this.angularDrag (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :341 :31) // Not a variable of known type: angularDrag
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :342 :12) // Not a variable of known type: target
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :342 :12) // target.angularDrag (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Experimental\\NetworkRigidbody.cs" :342 :33) // Not a variable of known type: angularDrag
br ^3

^3: // ExitBlock
return

}
