// Skipping function Awake(), it contains poisonous unsupported syntaxes

// Skipping function FixedUpdate(), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkAnimator.CheckSpeed$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :121 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :123 :29) // Not a variable of known type: animator
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :123 :29) // animator.speed (SimpleMemberAccessExpression)
// Entity from another assembly: Mathf
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :124 :26) // Not a variable of known type: previousSpeed
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :124 :42) // Not a variable of known type: newSpeed
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :124 :26) // Binary expression on unsupported types previousSpeed - newSpeed
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :124 :16) // Mathf.Abs(previousSpeed - newSpeed) (InvocationExpression)
%7 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :124 :54) // 0.001f (NumericLiteralExpression)
%8 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :124 :16) // comparison of unknown type: Mathf.Abs(previousSpeed - newSpeed) > 0.001f
cond_br %8, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :124 :16)

^1: // BinaryBranchBlock
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :126 :32) // Not a variable of known type: newSpeed
%10 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :127 :20) // Identifier from another assembly: isServer
cond_br %10, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :127 :20)

^3: // SimpleBlock
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :129 :36) // Not a variable of known type: newSpeed
br ^2

^4: // BinaryBranchBlock
// Entity from another assembly: ClientScene
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :131 :25) // ClientScene.readyConnection (SimpleMemberAccessExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :131 :56) // null (NullLiteralExpression)
%14 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :131 :25) // comparison of unknown type: ClientScene.readyConnection != null
cond_br %14, ^5, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :131 :25)

^5: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CmdSetAnimatorSpeed
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :133 :40) // Not a variable of known type: newSpeed
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :133 :20) // CmdSetAnimatorSpeed(newSpeed) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.NetworkAnimator.CmdSetAnimatorSpeed$float$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :138 :8) {
^entry (%_newSpeed : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :138 :33)
cbde.store %_newSpeed, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :138 :33)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :141 :12) // Not a variable of known type: animator
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :141 :12) // animator.speed (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :141 :29) // Not a variable of known type: newSpeed
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :142 :28) // Not a variable of known type: newSpeed
br ^1

^1: // ExitBlock
return

}
// Skipping function onAnimatorSpeedChanged(none, none), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkAnimator.CheckAnimStateChanged$outint.outfloat.int$(i32, none, i32) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :155 :8) {
^entry (%_stateHash : i32, %_normalizedTime : none, %_layerId : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :155 :35)
cbde.store %_stateHash, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :155 :35)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :155 :54)
cbde.store %_normalizedTime, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :155 :54)
%2 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :155 :80)
cbde.store %_layerId, %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :155 :80)
br ^0

^0: // BinaryBranchBlock
%3 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :157 :26) // false
%4 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :157 :17) // change
cbde.store %3, %4 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :157 :17)
%5 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :158 :24)
cbde.store %5, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :158 :12)
%6 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :159 :29)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :161 :23) // Not a variable of known type: animator
%8 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :161 :47)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :161 :23) // animator.GetLayerWeight(layerId) (InvocationExpression)
// Entity from another assembly: Mathf
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :162 :26) // Not a variable of known type: lw
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :162 :31) // Not a variable of known type: layerWeight
%13 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :162 :43)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :162 :31) // layerWeight[layerId] (ElementAccessExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :162 :26) // Binary expression on unsupported types lw - layerWeight[layerId]
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :162 :16) // Mathf.Abs(lw - layerWeight[layerId]) (InvocationExpression)
%17 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :162 :55) // 0.001f (NumericLiteralExpression)
%18 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :162 :16) // comparison of unknown type: Mathf.Abs(lw - layerWeight[layerId]) > 0.001f
cond_br %18, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :162 :16)

^1: // SimpleBlock
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :164 :16) // Not a variable of known type: layerWeight
%20 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :164 :28)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :164 :16) // layerWeight[layerId] (ElementAccessExpression)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :164 :39) // Not a variable of known type: lw
%23 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :165 :25) // true
cbde.store %23, %4 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :165 :16)
br ^2

^2: // BinaryBranchBlock
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :168 :16) // Not a variable of known type: animator
%25 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :168 :40)
%26 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :168 :16) // animator.IsInTransition(layerId) (InvocationExpression)
cond_br %26, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :168 :16)

^3: // BinaryBranchBlock
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :170 :44) // Not a variable of known type: animator
%28 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :170 :79)
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :170 :44) // animator.GetAnimatorTransitionInfo(layerId) (InvocationExpression)
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :171 :20) // Not a variable of known type: tt
%32 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :171 :20) // tt.fullPathHash (SimpleMemberAccessExpression)
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :171 :39) // Not a variable of known type: transitionHash
%34 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :171 :54)
%35 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :171 :39) // transitionHash[layerId] (ElementAccessExpression)
%36 = cmpi "ne", %32, %35 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :171 :20)
cond_br %36, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :171 :20)

^5: // JumpBlock
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :174 :20) // Not a variable of known type: transitionHash
%38 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :174 :35)
%39 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :174 :20) // transitionHash[layerId] (ElementAccessExpression)
%40 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :174 :46) // Not a variable of known type: tt
%41 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :174 :46) // tt.fullPathHash (SimpleMemberAccessExpression)
%42 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :175 :20) // Not a variable of known type: animationHash
%43 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :175 :34)
%44 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :175 :20) // animationHash[layerId] (ElementAccessExpression)
%45 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :175 :45)
%46 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :176 :27) // true
return %46 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :176 :20)

^6: // JumpBlock
%47 = cbde.load %4 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :178 :23)
return %47 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :178 :16)

^4: // BinaryBranchBlock
%48 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :181 :35) // Not a variable of known type: animator
%49 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :181 :72)
%50 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :181 :35) // animator.GetCurrentAnimatorStateInfo(layerId) (InvocationExpression)
%52 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :182 :16) // Not a variable of known type: st
%53 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :182 :16) // st.fullPathHash (SimpleMemberAccessExpression)
%54 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :182 :35) // Not a variable of known type: animationHash
%55 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :182 :49)
%56 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :182 :35) // animationHash[layerId] (ElementAccessExpression)
%57 = cmpi "ne", %53, %56 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :182 :16)
cond_br %57, ^7, ^8 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :182 :16)

^7: // BinaryBranchBlock
%58 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :185 :20) // Not a variable of known type: animationHash
%59 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :185 :34)
%60 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :185 :20) // animationHash[layerId] (ElementAccessExpression)
%61 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :185 :46)
%62 = cmpi "ne", %60, %61 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :185 :20)
cond_br %62, ^9, ^10 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :185 :20)

^9: // SimpleBlock
%63 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :188 :32) // Not a variable of known type: st
%64 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :188 :32) // st.fullPathHash (SimpleMemberAccessExpression)
cbde.store %64, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :188 :20)
%65 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :189 :37) // Not a variable of known type: st
%66 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :189 :37) // st.normalizedTime (SimpleMemberAccessExpression)
br ^10

^10: // JumpBlock
%67 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :191 :16) // Not a variable of known type: transitionHash
%68 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :191 :31)
%69 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :191 :16) // transitionHash[layerId] (ElementAccessExpression)
%70 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :191 :42)
%71 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :192 :16) // Not a variable of known type: animationHash
%72 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :192 :30)
%73 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :192 :16) // animationHash[layerId] (ElementAccessExpression)
%74 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :192 :41) // Not a variable of known type: st
%75 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :192 :41) // st.fullPathHash (SimpleMemberAccessExpression)
%76 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :193 :23) // true
return %76 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :193 :16)

^8: // JumpBlock
%77 = cbde.load %4 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :195 :19)
return %77 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :195 :12)

^11: // ExitBlock
cbde.unreachable

}
// Skipping function CheckSendRate(), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkAnimator.SendAnimationMessage$int.float.int.float.byte$$$(i32, none, i32, none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :213 :8) {
^entry (%_stateHash : i32, %_normalizedTime : none, %_layerId : i32, %_weight : none, %_parameters : none):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :213 :34)
cbde.store %_stateHash, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :213 :34)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :213 :49)
cbde.store %_normalizedTime, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :213 :49)
%2 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :213 :71)
cbde.store %_layerId, %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :213 :71)
%3 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :213 :84)
cbde.store %_weight, %3 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :213 :84)
%4 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :213 :98)
cbde.store %_parameters, %4 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :213 :98)
br ^0

^0: // BinaryBranchBlock
%5 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :215 :16) // Identifier from another assembly: isServer
cond_br %5, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :215 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: RpcOnAnimationClientMessage
%6 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :217 :44)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :217 :55) // Not a variable of known type: normalizedTime
%8 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :217 :71)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :217 :80) // Not a variable of known type: weight
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :217 :88) // Not a variable of known type: parameters
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :217 :16) // RpcOnAnimationClientMessage(stateHash, normalizedTime, layerId, weight, parameters) (InvocationExpression)
br ^3

^2: // BinaryBranchBlock
// Entity from another assembly: ClientScene
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :219 :21) // ClientScene.readyConnection (SimpleMemberAccessExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :219 :52) // null (NullLiteralExpression)
%14 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :219 :21) // comparison of unknown type: ClientScene.readyConnection != null
cond_br %14, ^4, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :219 :21)

^4: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CmdOnAnimationServerMessage
%15 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :221 :44)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :221 :55) // Not a variable of known type: normalizedTime
%17 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :221 :71)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :221 :80) // Not a variable of known type: weight
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :221 :88) // Not a variable of known type: parameters
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :221 :16) // CmdOnAnimationServerMessage(stateHash, normalizedTime, layerId, weight, parameters) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.NetworkAnimator.SendAnimationParametersMessage$byte$$$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :225 :8) {
^entry (%_parameters : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :225 :44)
cbde.store %_parameters, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :225 :44)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :227 :16) // Identifier from another assembly: isServer
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :227 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: RpcOnAnimationParametersClientMessage
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :229 :54) // Not a variable of known type: parameters
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :229 :16) // RpcOnAnimationParametersClientMessage(parameters) (InvocationExpression)
br ^3

^2: // BinaryBranchBlock
// Entity from another assembly: ClientScene
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :231 :21) // ClientScene.readyConnection (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :231 :52) // null (NullLiteralExpression)
%6 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :231 :21) // comparison of unknown type: ClientScene.readyConnection != null
cond_br %6, ^4, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :231 :21)

^4: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CmdOnAnimationParametersServerMessage
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :233 :54) // Not a variable of known type: parameters
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :233 :16) // CmdOnAnimationParametersServerMessage(parameters) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function HandleAnimMsg(i32, none, i32, none, none), it contains poisonous unsupported syntaxes

// Skipping function HandleAnimParamsMsg(none), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkAnimator.HandleAnimTriggerMsg$int$(i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :263 :8) {
^entry (%_hash : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :263 :34)
cbde.store %_hash, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :263 :34)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :265 :16) // Not a variable of known type: animator
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :265 :16) // animator.enabled (SimpleMemberAccessExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :265 :16)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :266 :16) // Not a variable of known type: animator
%4 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :266 :36)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :266 :16) // animator.SetTrigger(hash) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.NetworkAnimator.HandleAnimResetTriggerMsg$int$(i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :269 :8) {
^entry (%_hash : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :269 :39)
cbde.store %_hash, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :269 :39)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :271 :16) // Not a variable of known type: animator
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :271 :16) // animator.enabled (SimpleMemberAccessExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :271 :16)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :272 :16) // Not a variable of known type: animator
%4 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :272 :38)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :272 :16) // animator.ResetTrigger(hash) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.NetworkAnimator.NextDirtyBits$$() -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :275 :8) {
^entry :
br ^0

^0: // ForInitializerBlock
%0 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :277 :30)
%2 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :278 :25)
%3 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :278 :21) // i
cbde.store %2, %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :278 :21)
br ^1

^1: // BinaryBranchBlock
%4 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :278 :28)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :278 :32) // Not a variable of known type: parameters
%6 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :278 :32) // parameters.Length (SimpleMemberAccessExpression)
%7 = cmpi "slt", %4, %6 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :278 :28)
cond_br %7, ^2, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :278 :28)

^2: // BinaryBranchBlock
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :280 :50) // Not a variable of known type: parameters
%9 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :280 :61)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :280 :50) // parameters[i] (ElementAccessExpression)
%12 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :281 :31) // false
%13 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :281 :21) // changed
cbde.store %12, %13 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :281 :21)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :282 :20) // Not a variable of known type: par
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :282 :20) // par.type (SimpleMemberAccessExpression)
// Entity from another assembly: AnimatorControllerParameterType
%16 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :282 :32) // AnimatorControllerParameterType.Int (SimpleMemberAccessExpression)
%17 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :282 :20) // comparison of unknown type: par.type == AnimatorControllerParameterType.Int
cond_br %17, ^4, ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :282 :20)

^4: // BinaryBranchBlock
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :284 :38) // Not a variable of known type: animator
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :284 :58) // Not a variable of known type: par
%20 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :284 :58) // par.nameHash (SimpleMemberAccessExpression)
%21 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :284 :38) // animator.GetInteger(par.nameHash) (InvocationExpression)
%22 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :284 :24) // newIntValue
cbde.store %21, %22 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :284 :24)
%23 = cbde.load %22 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :285 :30)
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :285 :45) // Not a variable of known type: lastIntParameters
%25 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :285 :63)
%26 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :285 :45) // lastIntParameters[i] (ElementAccessExpression)
%27 = cmpi "ne", %23, %26 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :285 :30)
cbde.store %27, %13 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :285 :20)
%28 = cbde.load %13 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :286 :24)
cond_br %28, ^6, ^7 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :286 :24)

^6: // SimpleBlock
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :287 :24) // Not a variable of known type: lastIntParameters
%30 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :287 :42)
%31 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :287 :24) // lastIntParameters[i] (ElementAccessExpression)
%32 = cbde.load %22 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :287 :47)
br ^7

^5: // BinaryBranchBlock
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :289 :25) // Not a variable of known type: par
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :289 :25) // par.type (SimpleMemberAccessExpression)
// Entity from another assembly: AnimatorControllerParameterType
%35 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :289 :37) // AnimatorControllerParameterType.Float (SimpleMemberAccessExpression)
%36 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :289 :25) // comparison of unknown type: par.type == AnimatorControllerParameterType.Float
cond_br %36, ^8, ^9 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :289 :25)

^8: // BinaryBranchBlock
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :291 :42) // Not a variable of known type: animator
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :291 :60) // Not a variable of known type: par
%39 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :291 :60) // par.nameHash (SimpleMemberAccessExpression)
%40 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :291 :42) // animator.GetFloat(par.nameHash) (InvocationExpression)
// Entity from another assembly: Mathf
%42 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :292 :40) // Not a variable of known type: newFloatValue
%43 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :292 :56) // Not a variable of known type: lastFloatParameters
%44 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :292 :76)
%45 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :292 :56) // lastFloatParameters[i] (ElementAccessExpression)
%46 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :292 :40) // Binary expression on unsupported types newFloatValue - lastFloatParameters[i]
%47 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :292 :30) // Mathf.Abs(newFloatValue - lastFloatParameters[i]) (InvocationExpression)
%48 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :292 :82) // 0.001f (NumericLiteralExpression)
%49 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :292 :30) // comparison of unknown type: Mathf.Abs(newFloatValue - lastFloatParameters[i]) > 0.001f
cbde.store %49, %13 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :292 :20)
%50 = cbde.load %13 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :294 :24)
cond_br %50, ^10, ^7 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :294 :24)

^10: // SimpleBlock
%51 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :295 :24) // Not a variable of known type: lastFloatParameters
%52 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :295 :44)
%53 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :295 :24) // lastFloatParameters[i] (ElementAccessExpression)
%54 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :295 :49) // Not a variable of known type: newFloatValue
br ^7

^9: // BinaryBranchBlock
%55 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :297 :25) // Not a variable of known type: par
%56 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :297 :25) // par.type (SimpleMemberAccessExpression)
// Entity from another assembly: AnimatorControllerParameterType
%57 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :297 :37) // AnimatorControllerParameterType.Bool (SimpleMemberAccessExpression)
%58 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :297 :25) // comparison of unknown type: par.type == AnimatorControllerParameterType.Bool
cond_br %58, ^11, ^7 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :297 :25)

^11: // BinaryBranchBlock
%59 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :299 :40) // Not a variable of known type: animator
%60 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :299 :57) // Not a variable of known type: par
%61 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :299 :57) // par.nameHash (SimpleMemberAccessExpression)
%62 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :299 :40) // animator.GetBool(par.nameHash) (InvocationExpression)
%63 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :299 :25) // newBoolValue
cbde.store %62, %63 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :299 :25)
%64 = cbde.load %63 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :300 :30)
%65 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :300 :46) // Not a variable of known type: lastBoolParameters
%66 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :300 :65)
%67 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :300 :46) // lastBoolParameters[i] (ElementAccessExpression)
%68 = cmpi "ne", %64, %67 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :300 :30)
cbde.store %68, %13 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :300 :20)
%69 = cbde.load %13 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :301 :24)
cond_br %69, ^12, ^7 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :301 :24)

^12: // SimpleBlock
%70 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :302 :24) // Not a variable of known type: lastBoolParameters
%71 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :302 :43)
%72 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :302 :24) // lastBoolParameters[i] (ElementAccessExpression)
%73 = cbde.load %63 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :302 :48)
br ^7

^7: // BinaryBranchBlock
%74 = cbde.load %13 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :304 :20)
cond_br %74, ^13, ^14 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :304 :20)

^13: // SimpleBlock
%75 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :306 :20) // Not a variable of known type: dirtyBits
%76 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :306 :33) // 1ul (NumericLiteralExpression)
%77 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :306 :40)
%78 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :306 :33) // Binary expression on unsupported types 1ul << i
%79 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :306 :20) // Binary expression on unsupported types dirtyBits |= 1ul << i
br ^14

^14: // SimpleBlock
%80 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :278 :51)
%81 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :278 :51)
%82 = addi %80, %81 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :278 :51)
cbde.store %82, %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :278 :51)
br ^1

^3: // JumpBlock
%83 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :309 :19) // Not a variable of known type: dirtyBits
return %83 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :309 :12)

^15: // ExitBlock
cbde.unreachable

}
// Skipping function WriteParameters(none, i1), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkAnimator.ReadParameters$Mirror.NetworkReader$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :341 :8) {
^entry (%_reader : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :341 :28)
cbde.store %_reader, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :341 :28)
br ^0

^0: // ForInitializerBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :343 :35) // Not a variable of known type: animator
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :343 :35) // animator.enabled (SimpleMemberAccessExpression)
%3 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :343 :17) // animatorEnabled
cbde.store %2, %3 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :343 :17)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :346 :30) // Not a variable of known type: reader
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :346 :30) // reader.ReadUInt64() (InvocationExpression)
%7 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :347 :25)
%8 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :347 :21) // i
cbde.store %7, %8 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :347 :21)
br ^1

^1: // BinaryBranchBlock
%9 = cbde.load %8 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :347 :28)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :347 :32) // Not a variable of known type: parameters
%11 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :347 :32) // parameters.Length (SimpleMemberAccessExpression)
%12 = cmpi "slt", %9, %11 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :347 :28)
cond_br %12, ^2, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :347 :28)

^2: // BinaryBranchBlock
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :349 :21) // Not a variable of known type: dirtyBits
%14 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :349 :34) // 1ul (NumericLiteralExpression)
%15 = cbde.load %8 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :349 :41)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :349 :34) // Binary expression on unsupported types 1ul << i
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :349 :21) // Binary expression on unsupported types dirtyBits & (1ul << i)
%18 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :349 :48)
%19 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :349 :20) // comparison of unknown type: (dirtyBits & (1ul << i)) == 0
cond_br %19, ^4, ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :349 :20)

^4: // JumpBlock
br ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :350 :20) // continue

^5: // BinaryBranchBlock
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :352 :50) // Not a variable of known type: parameters
%21 = cbde.load %8 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :352 :61)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :352 :50) // parameters[i] (ElementAccessExpression)
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :353 :20) // Not a variable of known type: par
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :353 :20) // par.type (SimpleMemberAccessExpression)
// Entity from another assembly: AnimatorControllerParameterType
%26 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :353 :32) // AnimatorControllerParameterType.Int (SimpleMemberAccessExpression)
%27 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :353 :20) // comparison of unknown type: par.type == AnimatorControllerParameterType.Int
cond_br %27, ^7, ^8 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :353 :20)

^7: // BinaryBranchBlock
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :355 :38) // Not a variable of known type: reader
%29 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :355 :38) // reader.ReadInt32() (InvocationExpression)
%30 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :355 :24) // newIntValue
cbde.store %29, %30 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :355 :24)
%31 = cbde.load %3 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :356 :24)
cond_br %31, ^9, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :356 :24)

^9: // SimpleBlock
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :357 :24) // Not a variable of known type: animator
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :357 :44) // Not a variable of known type: par
%34 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :357 :44) // par.nameHash (SimpleMemberAccessExpression)
%35 = cbde.load %30 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :357 :58)
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :357 :24) // animator.SetInteger(par.nameHash, newIntValue) (InvocationExpression)
br ^6

^8: // BinaryBranchBlock
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :359 :25) // Not a variable of known type: par
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :359 :25) // par.type (SimpleMemberAccessExpression)
// Entity from another assembly: AnimatorControllerParameterType
%39 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :359 :37) // AnimatorControllerParameterType.Float (SimpleMemberAccessExpression)
%40 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :359 :25) // comparison of unknown type: par.type == AnimatorControllerParameterType.Float
cond_br %40, ^10, ^11 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :359 :25)

^10: // BinaryBranchBlock
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :361 :42) // Not a variable of known type: reader
%42 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :361 :42) // reader.ReadSingle() (InvocationExpression)
%44 = cbde.load %3 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :362 :24)
cond_br %44, ^12, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :362 :24)

^12: // SimpleBlock
%45 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :363 :24) // Not a variable of known type: animator
%46 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :363 :42) // Not a variable of known type: par
%47 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :363 :42) // par.nameHash (SimpleMemberAccessExpression)
%48 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :363 :56) // Not a variable of known type: newFloatValue
%49 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :363 :24) // animator.SetFloat(par.nameHash, newFloatValue) (InvocationExpression)
br ^6

^11: // BinaryBranchBlock
%50 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :365 :25) // Not a variable of known type: par
%51 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :365 :25) // par.type (SimpleMemberAccessExpression)
// Entity from another assembly: AnimatorControllerParameterType
%52 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :365 :37) // AnimatorControllerParameterType.Bool (SimpleMemberAccessExpression)
%53 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :365 :25) // comparison of unknown type: par.type == AnimatorControllerParameterType.Bool
cond_br %53, ^13, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :365 :25)

^13: // BinaryBranchBlock
%54 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :367 :40) // Not a variable of known type: reader
%55 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :367 :40) // reader.ReadBoolean() (InvocationExpression)
%56 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :367 :25) // newBoolValue
cbde.store %55, %56 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :367 :25)
%57 = cbde.load %3 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :368 :24)
cond_br %57, ^14, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :368 :24)

^14: // SimpleBlock
%58 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :369 :24) // Not a variable of known type: animator
%59 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :369 :41) // Not a variable of known type: par
%60 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :369 :41) // par.nameHash (SimpleMemberAccessExpression)
%61 = cbde.load %56 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :369 :55)
%62 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :369 :24) // animator.SetBool(par.nameHash, newBoolValue) (InvocationExpression)
br ^6

^6: // SimpleBlock
%63 = cbde.load %8 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :347 :51)
%64 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :347 :51)
%65 = addi %63, %64 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :347 :51)
cbde.store %65, %8 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :347 :51)
br ^1

^3: // ExitBlock
return

}
func @_Mirror.NetworkAnimator.OnSerialize$Mirror.NetworkWriter.bool$(none, i1) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :380 :8) {
^entry (%_writer : none, %_initialState : i1):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :380 :41)
cbde.store %_writer, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :380 :41)
%1 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :380 :63)
cbde.store %_initialState, %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :380 :63)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :382 :27) // base (BaseExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :382 :44) // Not a variable of known type: writer
%4 = cbde.load %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :382 :52)
%5 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :382 :27) // base.OnSerialize(writer, initialState) (InvocationExpression)
%6 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :382 :17) // changed
cbde.store %5, %6 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :382 :17)
%7 = cbde.load %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :383 :16)
cond_br %7, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :383 :16)

^1: // ForInitializerBlock
%8 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :385 :29)
%9 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :385 :25) // i
cbde.store %8, %9 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :385 :25)
br ^3

^3: // BinaryBranchBlock
%10 = cbde.load %9 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :385 :32)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :385 :36) // Not a variable of known type: animator
%12 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :385 :36) // animator.layerCount (SimpleMemberAccessExpression)
%13 = cmpi "slt", %10, %12 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :385 :32)
cond_br %13, ^4, ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :385 :32)

^4: // BinaryBranchBlock
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :387 :24) // Not a variable of known type: animator
%15 = cbde.load %9 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :387 :48)
%16 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :387 :24) // animator.IsInTransition(i) (InvocationExpression)
cond_br %16, ^6, ^7 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :387 :24)

^6: // SimpleBlock
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :389 :47) // Not a variable of known type: animator
%18 = cbde.load %9 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :389 :81)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :389 :47) // animator.GetNextAnimatorStateInfo(i) (InvocationExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :390 :24) // Not a variable of known type: writer
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :390 :42) // Not a variable of known type: st
%23 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :390 :42) // st.fullPathHash (SimpleMemberAccessExpression)
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :390 :24) // writer.WriteInt32(st.fullPathHash) (InvocationExpression)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :391 :24) // Not a variable of known type: writer
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :391 :43) // Not a variable of known type: st
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :391 :43) // st.normalizedTime (SimpleMemberAccessExpression)
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :391 :24) // writer.WriteSingle(st.normalizedTime) (InvocationExpression)
br ^8

^7: // SimpleBlock
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :395 :47) // Not a variable of known type: animator
%30 = cbde.load %9 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :395 :84)
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :395 :47) // animator.GetCurrentAnimatorStateInfo(i) (InvocationExpression)
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :396 :24) // Not a variable of known type: writer
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :396 :42) // Not a variable of known type: st
%35 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :396 :42) // st.fullPathHash (SimpleMemberAccessExpression)
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :396 :24) // writer.WriteInt32(st.fullPathHash) (InvocationExpression)
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :397 :24) // Not a variable of known type: writer
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :397 :43) // Not a variable of known type: st
%39 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :397 :43) // st.normalizedTime (SimpleMemberAccessExpression)
%40 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :397 :24) // writer.WriteSingle(st.normalizedTime) (InvocationExpression)
br ^8

^8: // SimpleBlock
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :399 :20) // Not a variable of known type: writer
%42 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :399 :39) // Not a variable of known type: animator
%43 = cbde.load %9 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :399 :63)
%44 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :399 :39) // animator.GetLayerWeight(i) (InvocationExpression)
%45 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :399 :20) // writer.WriteSingle(animator.GetLayerWeight(i)) (InvocationExpression)
br ^9

^9: // SimpleBlock
%46 = cbde.load %9 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :385 :57)
%47 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :385 :57)
%48 = addi %46, %47 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :385 :57)
cbde.store %48, %9 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :385 :57)
br ^3

^5: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WriteParameters
%49 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :401 :32) // Not a variable of known type: writer
%50 = cbde.load %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :401 :40)
%51 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :401 :16) // WriteParameters(writer, initialState) (InvocationExpression)
%52 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :402 :23) // true
return %52 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :402 :16)

^2: // JumpBlock
%53 = cbde.load %6 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :404 :19)
return %53 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :404 :12)

^10: // ExitBlock
cbde.unreachable

}
func @_Mirror.NetworkAnimator.OnDeserialize$Mirror.NetworkReader.bool$(none, i1) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :412 :8) {
^entry (%_reader : none, %_initialState : i1):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :412 :43)
cbde.store %_reader, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :412 :43)
%1 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :412 :65)
cbde.store %_initialState, %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :412 :65)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :414 :12) // base (BaseExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :414 :31) // Not a variable of known type: reader
%4 = cbde.load %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :414 :39)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :414 :12) // base.OnDeserialize(reader, initialState) (InvocationExpression)
%6 = cbde.load %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :415 :16)
cond_br %6, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :415 :16)

^1: // ForInitializerBlock
%7 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :417 :29)
%8 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :417 :25) // i
cbde.store %7, %8 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :417 :25)
br ^3

^3: // BinaryBranchBlock
%9 = cbde.load %8 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :417 :32)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :417 :36) // Not a variable of known type: animator
%11 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :417 :36) // animator.layerCount (SimpleMemberAccessExpression)
%12 = cmpi "slt", %9, %11 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :417 :32)
cond_br %12, ^4, ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :417 :32)

^4: // SimpleBlock
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :419 :36) // Not a variable of known type: reader
%14 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :419 :36) // reader.ReadInt32() (InvocationExpression)
%15 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :419 :24) // stateHash
cbde.store %14, %15 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :419 :24)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :420 :43) // Not a variable of known type: reader
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :420 :43) // reader.ReadSingle() (InvocationExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :421 :20) // Not a variable of known type: animator
%20 = cbde.load %8 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :421 :44)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :421 :47) // Not a variable of known type: reader
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :421 :47) // reader.ReadSingle() (InvocationExpression)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :421 :20) // animator.SetLayerWeight(i, reader.ReadSingle()) (InvocationExpression)
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :422 :20) // Not a variable of known type: animator
%25 = cbde.load %15 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :422 :34)
%26 = cbde.load %8 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :422 :45)
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :422 :48) // Not a variable of known type: normalizedTime
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :422 :20) // animator.Play(stateHash, i, normalizedTime) (InvocationExpression)
br ^6

^6: // SimpleBlock
%29 = cbde.load %8 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :417 :57)
%30 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :417 :57)
%31 = addi %29, %30 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :417 :57)
cbde.store %31, %8 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :417 :57)
br ^3

^5: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ReadParameters
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :425 :31) // Not a variable of known type: reader
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :425 :16) // ReadParameters(reader) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.NetworkAnimator.SetTrigger$string$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :434 :8) {
^entry (%_triggerName : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :434 :31)
cbde.store %_triggerName, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :434 :31)
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SetTrigger
// Entity from another assembly: Animator
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :436 :45) // Not a variable of known type: triggerName
%2 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :436 :23) // Animator.StringToHash(triggerName) (InvocationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :436 :12) // SetTrigger(Animator.StringToHash(triggerName)) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkAnimator.SetTrigger$int$(i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :443 :8) {
^entry (%_hash : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :443 :31)
cbde.store %_hash, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :443 :31)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :445 :16) // Not a variable of known type: clientAuthority
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :445 :16)

^1: // BinaryBranchBlock
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :447 :21) // Identifier from another assembly: isClient
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :447 :20) // !isClient (LogicalNotExpression)
cond_br %3, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :447 :20)

^3: // JumpBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :449 :20) // Not a variable of known type: logger
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :449 :38) // "Tried to set animation in the server for a client-controlled animator" (StringLiteralExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :449 :20) // logger.LogWarning("Tried to set animation in the server for a client-controlled animator") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :450 :20)

^4: // BinaryBranchBlock
%7 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :453 :21) // Identifier from another assembly: hasAuthority
%8 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :453 :20) // !hasAuthority (LogicalNotExpression)
cond_br %8, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :453 :20)

^5: // JumpBlock
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :455 :20) // Not a variable of known type: logger
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :455 :38) // "Only the client with authority can set animations" (StringLiteralExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :455 :20) // logger.LogWarning("Only the client with authority can set animations") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :456 :20)

^6: // BinaryBranchBlock
// Entity from another assembly: ClientScene
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :459 :20) // ClientScene.readyConnection (SimpleMemberAccessExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :459 :51) // null (NullLiteralExpression)
%14 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :459 :20) // comparison of unknown type: ClientScene.readyConnection != null
cond_br %14, ^7, ^8 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :459 :20)

^7: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CmdOnAnimationTriggerServerMessage
%15 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :460 :55)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :460 :20) // CmdOnAnimationTriggerServerMessage(hash) (InvocationExpression)
br ^8

^8: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: HandleAnimTriggerMsg
%17 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :463 :37)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :463 :16) // HandleAnimTriggerMsg(hash) (InvocationExpression)
br ^9

^2: // BinaryBranchBlock
%19 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :467 :21) // Identifier from another assembly: isServer
%20 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :467 :20) // !isServer (LogicalNotExpression)
cond_br %20, ^10, ^11 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :467 :20)

^10: // JumpBlock
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :469 :20) // Not a variable of known type: logger
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :469 :38) // "Tried to set animation in the client for a server-controlled animator" (StringLiteralExpression)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :469 :20) // logger.LogWarning("Tried to set animation in the client for a server-controlled animator") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :470 :20)

^11: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: HandleAnimTriggerMsg
%24 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :473 :37)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :473 :16) // HandleAnimTriggerMsg(hash) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: RpcOnAnimationTriggerClientMessage
%26 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :474 :51)
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :474 :16) // RpcOnAnimationTriggerClientMessage(hash) (InvocationExpression)
br ^9

^9: // ExitBlock
return

}
func @_Mirror.NetworkAnimator.ResetTrigger$string$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :483 :8) {
^entry (%_triggerName : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :483 :33)
cbde.store %_triggerName, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :483 :33)
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ResetTrigger
// Entity from another assembly: Animator
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :485 :47) // Not a variable of known type: triggerName
%2 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :485 :25) // Animator.StringToHash(triggerName) (InvocationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :485 :12) // ResetTrigger(Animator.StringToHash(triggerName)) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkAnimator.ResetTrigger$int$(i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :492 :8) {
^entry (%_hash : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :492 :33)
cbde.store %_hash, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :492 :33)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :494 :16) // Not a variable of known type: clientAuthority
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :494 :16)

^1: // BinaryBranchBlock
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :496 :21) // Identifier from another assembly: isClient
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :496 :20) // !isClient (LogicalNotExpression)
cond_br %3, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :496 :20)

^3: // JumpBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :498 :20) // Not a variable of known type: logger
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :498 :38) // "Tried to reset animation in the server for a client-controlled animator" (StringLiteralExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :498 :20) // logger.LogWarning("Tried to reset animation in the server for a client-controlled animator") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :499 :20)

^4: // BinaryBranchBlock
%7 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :502 :21) // Identifier from another assembly: hasAuthority
%8 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :502 :20) // !hasAuthority (LogicalNotExpression)
cond_br %8, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :502 :20)

^5: // JumpBlock
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :504 :20) // Not a variable of known type: logger
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :504 :38) // "Only the client with authority can reset animations" (StringLiteralExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :504 :20) // logger.LogWarning("Only the client with authority can reset animations") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :505 :20)

^6: // BinaryBranchBlock
// Entity from another assembly: ClientScene
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :508 :20) // ClientScene.readyConnection (SimpleMemberAccessExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :508 :51) // null (NullLiteralExpression)
%14 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :508 :20) // comparison of unknown type: ClientScene.readyConnection != null
cond_br %14, ^7, ^8 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :508 :20)

^7: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CmdOnAnimationResetTriggerServerMessage
%15 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :509 :60)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :509 :20) // CmdOnAnimationResetTriggerServerMessage(hash) (InvocationExpression)
br ^8

^8: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: HandleAnimResetTriggerMsg
%17 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :512 :42)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :512 :16) // HandleAnimResetTriggerMsg(hash) (InvocationExpression)
br ^9

^2: // BinaryBranchBlock
%19 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :516 :21) // Identifier from another assembly: isServer
%20 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :516 :20) // !isServer (LogicalNotExpression)
cond_br %20, ^10, ^11 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :516 :20)

^10: // JumpBlock
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :518 :20) // Not a variable of known type: logger
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :518 :38) // "Tried to reset animation in the client for a server-controlled animator" (StringLiteralExpression)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :518 :20) // logger.LogWarning("Tried to reset animation in the client for a server-controlled animator") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :519 :20)

^11: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: HandleAnimResetTriggerMsg
%24 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :522 :42)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :522 :16) // HandleAnimResetTriggerMsg(hash) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: RpcOnAnimationResetTriggerClientMessage
%26 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :523 :56)
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\NetworkAnimator.cs" :523 :16) // RpcOnAnimationResetTriggerClientMessage(hash) (InvocationExpression)
br ^9

^9: // ExitBlock
return

}
// Skipping function CmdOnAnimationServerMessage(i32, none, i32, none, none), it contains poisonous unsupported syntaxes

// Skipping function CmdOnAnimationParametersServerMessage(none), it contains poisonous unsupported syntaxes

// Skipping function CmdOnAnimationTriggerServerMessage(i32), it contains poisonous unsupported syntaxes

// Skipping function CmdOnAnimationResetTriggerServerMessage(i32), it contains poisonous unsupported syntaxes

// Skipping function RpcOnAnimationClientMessage(i32, none, i32, none, none), it contains poisonous unsupported syntaxes

// Skipping function RpcOnAnimationParametersClientMessage(none), it contains poisonous unsupported syntaxes

// Skipping function RpcOnAnimationTriggerClientMessage(i32), it contains poisonous unsupported syntaxes

// Skipping function RpcOnAnimationResetTriggerClientMessage(i32), it contains poisonous unsupported syntaxes

