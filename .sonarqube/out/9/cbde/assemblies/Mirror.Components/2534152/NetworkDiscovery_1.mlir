func @_Mirror.Discovery.NetworkDiscovery.Start$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscovery.cs" :24 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: RandomLong
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscovery.cs" :26 :23) // RandomLong() (InvocationExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscovery.cs" :31 :16) // Not a variable of known type: transport
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscovery.cs" :31 :29) // null (NullLiteralExpression)
%3 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscovery.cs" :31 :16) // comparison of unknown type: transport == null
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscovery.cs" :31 :16)

^1: // SimpleBlock
// Entity from another assembly: Transport
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscovery.cs" :32 :28) // Transport.activeTransport (SimpleMemberAccessExpression)
br ^2

^2: // SimpleBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscovery.cs" :34 :12) // base (BaseExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscovery.cs" :34 :12) // base.Start() (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function ProcessRequest(none, none), it contains poisonous unsupported syntaxes

func @_Mirror.Discovery.NetworkDiscovery.ProcessResponse$Mirror.Discovery.ServerResponse.System.Net.IPEndPoint$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscovery.cs" :93 :8) {
^entry (%_response : none, %_endpoint : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscovery.cs" :93 :48)
cbde.store %_response, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscovery.cs" :93 :48)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscovery.cs" :93 :73)
cbde.store %_endpoint, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscovery.cs" :93 :73)
br ^0

^0: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscovery.cs" :96 :12) // Not a variable of known type: response
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscovery.cs" :96 :12) // response.EndPoint (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscovery.cs" :96 :32) // Not a variable of known type: endpoint
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscovery.cs" :102 :48) // Not a variable of known type: response
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscovery.cs" :102 :48) // response.uri (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscovery.cs" :102 :33) // new UriBuilder(response.uri)             {                 Host = response.EndPoint.Address.ToString()             } (ObjectCreationExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscovery.cs" :104 :23) // Not a variable of known type: response
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscovery.cs" :104 :23) // response.EndPoint (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscovery.cs" :104 :23) // response.EndPoint.Address (SimpleMemberAccessExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscovery.cs" :104 :23) // response.EndPoint.Address.ToString() (InvocationExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscovery.cs" :106 :12) // Not a variable of known type: response
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscovery.cs" :106 :12) // response.uri (SimpleMemberAccessExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscovery.cs" :106 :27) // Not a variable of known type: realUri
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscovery.cs" :106 :27) // realUri.Uri (SimpleMemberAccessExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscovery.cs" :108 :12) // Not a variable of known type: OnServerFound
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscovery.cs" :108 :33) // Not a variable of known type: response
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Components\\Discovery\\NetworkDiscovery.cs" :108 :12) // OnServerFound.Invoke(response) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
