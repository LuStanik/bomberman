// Skipping function GetSyncVarStart(none), it contains poisonous unsupported syntaxes

func @_Mirror.Weaver.WeaverLists.SetNumSyncVars$string.int$(none, i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :27 :8) {
^entry (%_className : none, %_num : i32):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :27 :35)
cbde.store %_className, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :27 :35)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :27 :53)
cbde.store %_num, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :27 :53)
br ^0

^0: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :29 :12) // Not a variable of known type: numSyncVars
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :29 :24) // Not a variable of known type: className
%4 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :29 :12) // numSyncVars[className] (ElementAccessExpression)
%5 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :29 :37)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Weaver.Weaver.DLog$Mono.CecilX.TypeDefinition.string.paramsobject$$$(none, none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :52 :8) {
^entry (%_td : none, %_fmt : none, %_args : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :52 :32)
cbde.store %_td, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :52 :32)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :52 :51)
cbde.store %_fmt, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :52 :51)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :52 :63)
cbde.store %_args, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :52 :63)
br ^0

^0: // BinaryBranchBlock
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :54 :17) // Not a variable of known type: DebugLogEnabled
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :54 :16) // !DebugLogEnabled (LogicalNotExpression)
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :54 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :55 :16)

^2: // SimpleBlock
// Entity from another assembly: Console
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :57 :30) // "[" (StringLiteralExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :57 :36) // Not a variable of known type: td
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :57 :36) // td.Name (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :57 :30) // Binary expression on unsupported types "[" + td.Name
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :57 :46) // "] " (StringLiteralExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :57 :30) // Binary expression on unsupported types "[" + td.Name + "] "
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :57 :53) // string (PredefinedType)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :57 :67) // Not a variable of known type: fmt
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :57 :72) // Not a variable of known type: args
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :57 :53) // string.Format(fmt, args) (InvocationExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :57 :30) // Binary expression on unsupported types "[" + td.Name + "] " + string.Format(fmt, args)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :57 :12) // Console.WriteLine("[" + td.Name + "] " + string.Format(fmt, args)) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Weaver.Weaver.Error$string$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :62 :8) {
^entry (%_message : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :62 :33)
cbde.store %_message, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :62 :33)
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Log
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :64 :22) // Not a variable of known type: message
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :64 :12) // Log.Error(message) (InvocationExpression)
%3 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :65 :28) // true
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Weaver.Weaver.Error$string.Mono.CecilX.MemberReference$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :68 :8) {
^entry (%_message : none, %_mr : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :68 :33)
cbde.store %_message, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :68 :33)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :68 :49)
cbde.store %_mr, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :68 :49)
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Log
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :70 :25) // Not a variable of known type: message
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :70 :39) // Not a variable of known type: mr
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :70 :22) // $"{message} (at {mr})" (InterpolatedStringExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :70 :12) // Log.Error($"{message} (at {mr})") (InvocationExpression)
%6 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :71 :28) // true
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Weaver.Weaver.Warning$string.Mono.CecilX.MemberReference$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :74 :8) {
^entry (%_message : none, %_mr : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :74 :35)
cbde.store %_message, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :74 :35)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :74 :51)
cbde.store %_mr, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :74 :51)
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Log
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :76 :27) // Not a variable of known type: message
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :76 :41) // Not a variable of known type: mr
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :76 :24) // $"{message} (at {mr})" (InterpolatedStringExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :76 :12) // Log.Warning($"{message} (at {mr})") (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Weaver.Weaver.CheckMonoBehaviour$Mono.CecilX.TypeDefinition$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :80 :8) {
^entry (%_td : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :80 :39)
cbde.store %_td, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :80 :39)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :82 :16) // Not a variable of known type: td
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :82 :16) // td.IsDerivedFrom<UnityEngine.MonoBehaviour>() (InvocationExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :82 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: MonoBehaviourProcessor
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :84 :47) // Not a variable of known type: td
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Weaver.cs" :84 :16) // MonoBehaviourProcessor.Process(td) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
// Skipping function WeaveNetworkBehavior(none), it contains poisonous unsupported syntaxes

// Skipping function WeaveModule(none), it contains poisonous unsupported syntaxes

// Skipping function Weave(none, none), it contains poisonous unsupported syntaxes

// Skipping function WeaveAssembly(none, none), it contains poisonous unsupported syntaxes

