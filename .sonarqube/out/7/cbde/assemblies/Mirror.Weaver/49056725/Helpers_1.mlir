// Skipping function UnityEngineDllDirectoryName(), it contains poisonous unsupported syntaxes

func @_Mirror.Weaver.Helpers.DestinationFileFor$string.string$(none, none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Helpers.cs" :16 :8) {
^entry (%_outputDir : none, %_assemblyPath : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Helpers.cs" :16 :48)
cbde.store %_outputDir, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Helpers.cs" :16 :48)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Helpers.cs" :16 :66)
cbde.store %_assemblyPath, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Helpers.cs" :16 :66)
br ^0

^0: // JumpBlock
// Entity from another assembly: Path
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Helpers.cs" :18 :47) // Not a variable of known type: assemblyPath
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Helpers.cs" :18 :30) // Path.GetFileName(assemblyPath) (InvocationExpression)
// Entity from another assembly: Debug
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Helpers.cs" :19 :25) // Not a variable of known type: fileName
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Helpers.cs" :19 :37) // null (NullLiteralExpression)
%7 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Helpers.cs" :19 :25) // comparison of unknown type: fileName != null
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Helpers.cs" :19 :43) // "fileName != null" (StringLiteralExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Helpers.cs" :19 :12) // Debug.Assert(fileName != null, "fileName != null") (InvocationExpression)
// Entity from another assembly: Path
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Helpers.cs" :21 :32) // Not a variable of known type: outputDir
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Helpers.cs" :21 :43) // Not a variable of known type: fileName
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Helpers.cs" :21 :19) // Path.Combine(outputDir, fileName) (InvocationExpression)
return %12 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Helpers.cs" :21 :12)

^1: // ExitBlock
cbde.unreachable

}
