func @_Mirror.Weaver.Writers.Init$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :12 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :14 :72) // new TypeReferenceComparer() (ObjectCreationExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :14 :25) // new Dictionary<TypeReference, MethodReference>(new TypeReferenceComparer()) (ObjectCreationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Weaver.Writers.Register$Mono.CecilX.TypeReference.Mono.CecilX.MethodReference$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :17 :8) {
^entry (%_dataType : none, %_methodReference : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :17 :36)
cbde.store %_dataType, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :17 :36)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :17 :60)
cbde.store %_methodReference, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :17 :60)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :19 :16) // Not a variable of known type: writeFuncs
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :19 :39) // Not a variable of known type: dataType
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :19 :16) // writeFuncs.ContainsKey(dataType) (InvocationExpression)
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :19 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Weaver
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :21 :65) // Not a variable of known type: dataType
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :21 :65) // dataType.FullName (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :21 :31) // $"Registering a Write method for {dataType.FullName} when one already exists" (InterpolatedStringExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :21 :110) // Not a variable of known type: methodReference
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :21 :16) // Weaver.Warning($"Registering a Write method for {dataType.FullName} when one already exists", methodReference) (InvocationExpression)
br ^2

^2: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Weaver
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :25 :37) // Weaver.CurrentAssembly (SimpleMemberAccessExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :25 :37) // Weaver.CurrentAssembly.MainModule (SimpleMemberAccessExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :25 :87) // Not a variable of known type: dataType
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :25 :37) // Weaver.CurrentAssembly.MainModule.ImportReference(dataType) (InvocationExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :26 :12) // Not a variable of known type: writeFuncs
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :26 :23) // Not a variable of known type: imported
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :26 :12) // writeFuncs[imported] (ElementAccessExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :26 :35) // Not a variable of known type: methodReference
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Weaver.Writers.RegisterWriteFunc$Mono.CecilX.TypeReference.Mono.CecilX.MethodDefinition$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :29 :8) {
^entry (%_typeReference : none, %_newWriterFunc : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :29 :38)
cbde.store %_typeReference, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :29 :38)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :29 :67)
cbde.store %_newWriterFunc, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :29 :67)
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Register
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :31 :21) // Not a variable of known type: typeReference
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :31 :36) // Not a variable of known type: newWriterFunc
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :31 :12) // Register(typeReference, newWriterFunc) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Weaver
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :33 :12) // Weaver.WeaveLists (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :33 :12) // Weaver.WeaveLists.generateContainerClass (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :33 :12) // Weaver.WeaveLists.generateContainerClass.Methods (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :33 :65) // Not a variable of known type: newWriterFunc
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :33 :12) // Weaver.WeaveLists.generateContainerClass.Methods.Add(newWriterFunc) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function GetWriteFunc(none), it contains poisonous unsupported syntaxes

// Skipping function GenerateWriter(none), it contains poisonous unsupported syntaxes

// Skipping function GetNetworkBehaviourWriter(none), it contains poisonous unsupported syntaxes

func @_Mirror.Weaver.Writers.GenerateEnumWriteFunc$Mono.CecilX.TypeReference$(none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :165 :8) {
^entry (%_variable : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :165 :62)
cbde.store %_variable, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :165 :62)
br ^0

^0: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GenerateWriterFunc
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :167 :61) // Not a variable of known type: variable
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :167 :42) // GenerateWriterFunc(variable) (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :169 :33) // Not a variable of known type: writerFunc
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :169 :33) // writerFunc.Body (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :169 :33) // writerFunc.Body.GetILProcessor() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetWriteFunc
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :171 :60) // Not a variable of known type: variable
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :171 :60) // variable.Resolve() (InvocationExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :171 :60) // variable.Resolve().GetEnumUnderlyingType() (InvocationExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :171 :47) // GetWriteFunc(variable.Resolve().GetEnumUnderlyingType()) (InvocationExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :173 :12) // Not a variable of known type: worker
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :173 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :173 :40) // OpCodes.Ldarg_0 (SimpleMemberAccessExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :173 :26) // worker.Create(OpCodes.Ldarg_0) (InvocationExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :173 :12) // worker.Append(worker.Create(OpCodes.Ldarg_0)) (InvocationExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :174 :12) // Not a variable of known type: worker
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :174 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :174 :40) // OpCodes.Ldarg_1 (SimpleMemberAccessExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :174 :26) // worker.Create(OpCodes.Ldarg_1) (InvocationExpression)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :174 :12) // worker.Append(worker.Create(OpCodes.Ldarg_1)) (InvocationExpression)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :175 :12) // Not a variable of known type: worker
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :175 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :175 :40) // OpCodes.Call (SimpleMemberAccessExpression)
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :175 :54) // Not a variable of known type: underlyingWriter
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :175 :26) // worker.Create(OpCodes.Call, underlyingWriter) (InvocationExpression)
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :175 :12) // worker.Append(worker.Create(OpCodes.Call, underlyingWriter)) (InvocationExpression)
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :177 :12) // Not a variable of known type: worker
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :177 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :177 :40) // OpCodes.Ret (SimpleMemberAccessExpression)
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :177 :26) // worker.Create(OpCodes.Ret) (InvocationExpression)
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :177 :12) // worker.Append(worker.Create(OpCodes.Ret)) (InvocationExpression)
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :178 :19) // Not a variable of known type: writerFunc
return %34 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :178 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.Weaver.Writers.GenerateWriterFunc$Mono.CecilX.TypeReference$(none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :181 :8) {
^entry (%_variable : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :181 :59)
cbde.store %_variable, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :181 :59)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :183 :34) // "_Write_" (StringLiteralExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :183 :46) // Not a variable of known type: variable
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :183 :46) // variable.FullName (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :183 :34) // Binary expression on unsupported types "_Write_" + variable.FullName
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :185 :63) // Not a variable of known type: functionName
// Entity from another assembly: MethodAttributes
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :186 :20) // MethodAttributes.Public (SimpleMemberAccessExpression)
// Entity from another assembly: MethodAttributes
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :187 :20) // MethodAttributes.Static (SimpleMemberAccessExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :186 :20) // Binary expression on unsupported types MethodAttributes.Public |                     MethodAttributes.Static
// Entity from another assembly: MethodAttributes
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :188 :20) // MethodAttributes.HideBySig (SimpleMemberAccessExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :186 :20) // Binary expression on unsupported types MethodAttributes.Public |                     MethodAttributes.Static |                     MethodAttributes.HideBySig
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WeaverTypes
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :189 :39) // typeof(void) (TypeOfExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :189 :20) // WeaverTypes.Import(typeof(void)) (InvocationExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :185 :42) // new MethodDefinition(functionName,                     MethodAttributes.Public |                     MethodAttributes.Static |                     MethodAttributes.HideBySig,                     WeaverTypes.Import(typeof(void))) (ObjectCreationExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :191 :12) // Not a variable of known type: writerFunc
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :191 :12) // writerFunc.Parameters (SimpleMemberAccessExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :191 :62) // "writer" (StringLiteralExpression)
// Entity from another assembly: ParameterAttributes
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :191 :72) // ParameterAttributes.None (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WeaverTypes
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :191 :98) // WeaverTypes.Import<NetworkWriter>() (InvocationExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :191 :38) // new ParameterDefinition("writer", ParameterAttributes.None, WeaverTypes.Import<NetworkWriter>()) (ObjectCreationExpression)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :191 :12) // writerFunc.Parameters.Add(new ParameterDefinition("writer", ParameterAttributes.None, WeaverTypes.Import<NetworkWriter>())) (InvocationExpression)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :192 :12) // Not a variable of known type: writerFunc
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :192 :12) // writerFunc.Parameters (SimpleMemberAccessExpression)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :192 :62) // "value" (StringLiteralExpression)
// Entity from another assembly: ParameterAttributes
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :192 :71) // ParameterAttributes.None (SimpleMemberAccessExpression)
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :192 :97) // Not a variable of known type: variable
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :192 :38) // new ParameterDefinition("value", ParameterAttributes.None, variable) (ObjectCreationExpression)
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :192 :12) // writerFunc.Parameters.Add(new ParameterDefinition("value", ParameterAttributes.None, variable)) (InvocationExpression)
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :193 :12) // Not a variable of known type: writerFunc
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :193 :12) // writerFunc.Body (SimpleMemberAccessExpression)
%32 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :193 :12) // writerFunc.Body.InitLocals (SimpleMemberAccessExpression)
%33 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :193 :41) // true
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: RegisterWriteFunc
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :195 :30) // Not a variable of known type: variable
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :195 :40) // Not a variable of known type: writerFunc
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :195 :12) // RegisterWriteFunc(variable, writerFunc) (InvocationExpression)
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :196 :19) // Not a variable of known type: writerFunc
return %37 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :196 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.Weaver.Writers.GenerateClassOrStructWriterFunction$Mono.CecilX.TypeReference$(none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :199 :8) {
^entry (%_variable : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :199 :68)
cbde.store %_variable, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :199 :68)
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GenerateWriterFunc
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :201 :61) // Not a variable of known type: variable
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :201 :42) // GenerateWriterFunc(variable) (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :203 :33) // Not a variable of known type: writerFunc
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :203 :33) // writerFunc.Body (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :203 :33) // writerFunc.Body.GetILProcessor() (InvocationExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :205 :17) // Not a variable of known type: variable
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :205 :17) // variable.Resolve() (InvocationExpression)
%10 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :205 :17) // variable.Resolve().IsValueType (SimpleMemberAccessExpression)
%11 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :205 :16) // !variable.Resolve().IsValueType (LogicalNotExpression)
cond_br %11, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :205 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WriteNullCheck
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :206 :31) // Not a variable of known type: worker
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :206 :16) // WriteNullCheck(worker) (InvocationExpression)
br ^2

^2: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WriteAllFields
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :208 :32) // Not a variable of known type: variable
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :208 :42) // Not a variable of known type: worker
%16 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :208 :17) // WriteAllFields(variable, worker) (InvocationExpression)
%17 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :208 :16) // !WriteAllFields(variable, worker) (LogicalNotExpression)
cond_br %17, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :208 :16)

^3: // JumpBlock
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :209 :23) // null (NullLiteralExpression)
return %18 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :209 :16)

^4: // JumpBlock
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :211 :12) // Not a variable of known type: worker
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :211 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :211 :40) // OpCodes.Ret (SimpleMemberAccessExpression)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :211 :26) // worker.Create(OpCodes.Ret) (InvocationExpression)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :211 :12) // worker.Append(worker.Create(OpCodes.Ret)) (InvocationExpression)
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :212 :19) // Not a variable of known type: writerFunc
return %24 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :212 :12)

^5: // ExitBlock
cbde.unreachable

}
func @_Mirror.Weaver.Writers.WriteNullCheck$Mono.CecilX.Cil.ILProcessor$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :215 :8) {
^entry (%_worker : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :215 :43)
cbde.store %_worker, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :215 :43)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :224 :39) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :224 :53) // OpCodes.Nop (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :224 :39) // worker.Create(OpCodes.Nop) (InvocationExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :225 :12) // Not a variable of known type: worker
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :225 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :225 :40) // OpCodes.Ldarg_1 (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :225 :26) // worker.Create(OpCodes.Ldarg_1) (InvocationExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :225 :12) // worker.Append(worker.Create(OpCodes.Ldarg_1)) (InvocationExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :226 :12) // Not a variable of known type: worker
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :226 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :226 :40) // OpCodes.Brtrue (SimpleMemberAccessExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :226 :56) // Not a variable of known type: labelNotNull
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :226 :26) // worker.Create(OpCodes.Brtrue, labelNotNull) (InvocationExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :226 :12) // worker.Append(worker.Create(OpCodes.Brtrue, labelNotNull)) (InvocationExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :227 :12) // Not a variable of known type: worker
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :227 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :227 :40) // OpCodes.Ldarg_0 (SimpleMemberAccessExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :227 :26) // worker.Create(OpCodes.Ldarg_0) (InvocationExpression)
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :227 :12) // worker.Append(worker.Create(OpCodes.Ldarg_0)) (InvocationExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :228 :12) // Not a variable of known type: worker
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :228 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :228 :40) // OpCodes.Ldc_I4_0 (SimpleMemberAccessExpression)
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :228 :26) // worker.Create(OpCodes.Ldc_I4_0) (InvocationExpression)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :228 :12) // worker.Append(worker.Create(OpCodes.Ldc_I4_0)) (InvocationExpression)
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :229 :12) // Not a variable of known type: worker
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :229 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :229 :40) // OpCodes.Call (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetWriteFunc
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WeaverTypes
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :229 :67) // WeaverTypes.Import<bool>() (InvocationExpression)
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :229 :54) // GetWriteFunc(WeaverTypes.Import<bool>()) (InvocationExpression)
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :229 :26) // worker.Create(OpCodes.Call, GetWriteFunc(WeaverTypes.Import<bool>())) (InvocationExpression)
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :229 :12) // worker.Append(worker.Create(OpCodes.Call, GetWriteFunc(WeaverTypes.Import<bool>()))) (InvocationExpression)
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :230 :12) // Not a variable of known type: worker
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :230 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :230 :40) // OpCodes.Ret (SimpleMemberAccessExpression)
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :230 :26) // worker.Create(OpCodes.Ret) (InvocationExpression)
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :230 :12) // worker.Append(worker.Create(OpCodes.Ret)) (InvocationExpression)
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :231 :12) // Not a variable of known type: worker
%39 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :231 :26) // Not a variable of known type: labelNotNull
%40 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :231 :12) // worker.Append(labelNotNull) (InvocationExpression)
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :234 :12) // Not a variable of known type: worker
%42 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :234 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%43 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :234 :40) // OpCodes.Ldarg_0 (SimpleMemberAccessExpression)
%44 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :234 :26) // worker.Create(OpCodes.Ldarg_0) (InvocationExpression)
%45 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :234 :12) // worker.Append(worker.Create(OpCodes.Ldarg_0)) (InvocationExpression)
%46 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :235 :12) // Not a variable of known type: worker
%47 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :235 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%48 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :235 :40) // OpCodes.Ldc_I4_1 (SimpleMemberAccessExpression)
%49 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :235 :26) // worker.Create(OpCodes.Ldc_I4_1) (InvocationExpression)
%50 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :235 :12) // worker.Append(worker.Create(OpCodes.Ldc_I4_1)) (InvocationExpression)
%51 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :236 :12) // Not a variable of known type: worker
%52 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :236 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%53 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :236 :40) // OpCodes.Call (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetWriteFunc
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WeaverTypes
%54 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :236 :67) // WeaverTypes.Import<bool>() (InvocationExpression)
%55 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :236 :54) // GetWriteFunc(WeaverTypes.Import<bool>()) (InvocationExpression)
%56 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :236 :26) // worker.Create(OpCodes.Call, GetWriteFunc(WeaverTypes.Import<bool>())) (InvocationExpression)
%57 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :236 :12) // worker.Append(worker.Create(OpCodes.Call, GetWriteFunc(WeaverTypes.Import<bool>()))) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function WriteAllFields(none, none), it contains poisonous unsupported syntaxes

func @_Mirror.Weaver.Writers.GenerateCollectionWriter$Mono.CecilX.TypeReference.Mono.CecilX.TypeReference.string$(none, none, none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :266 :8) {
^entry (%_variable : none, %_elementType : none, %_writerFunction : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :266 :57)
cbde.store %_variable, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :266 :57)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :266 :81)
cbde.store %_elementType, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :266 :81)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :266 :108)
cbde.store %_writerFunction, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :266 :108)
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GenerateWriterFunc
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :269 :61) // Not a variable of known type: variable
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :269 :42) // GenerateWriterFunc(variable) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetWriteFunc
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :271 :60) // Not a variable of known type: elementType
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :271 :47) // GetWriteFunc(elementType) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetWriteFunc
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WeaverTypes
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :272 :57) // WeaverTypes.Import<int>() (InvocationExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :272 :44) // GetWriteFunc(WeaverTypes.Import<int>()) (InvocationExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :275 :16) // Not a variable of known type: elementWriteFunc
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :275 :36) // null (NullLiteralExpression)
%14 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :275 :16) // comparison of unknown type: elementWriteFunc == null
cond_br %14, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :275 :16)

^1: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Weaver
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :277 :59) // Not a variable of known type: variable
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :277 :29) // $"Cannot generate writer for {variable}. Use a supported type or provide a custom writer" (InterpolatedStringExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :277 :120) // Not a variable of known type: variable
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :277 :16) // Weaver.Error($"Cannot generate writer for {variable}. Use a supported type or provide a custom writer", variable) (InvocationExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :278 :23) // Not a variable of known type: writerFunc
return %19 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :278 :16)

^2: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Weaver
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :281 :38) // Weaver.CurrentAssembly (SimpleMemberAccessExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :281 :38) // Weaver.CurrentAssembly.MainModule (SimpleMemberAccessExpression)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :282 :45) // Not a variable of known type: module
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :282 :68) // typeof(NetworkWriterExtensions) (TypeOfExpression)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :282 :45) // module.ImportReference(typeof(NetworkWriterExtensions)) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Resolvers
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :283 :71) // Not a variable of known type: readerExtensions
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Weaver
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :283 :89) // Weaver.CurrentAssembly (SimpleMemberAccessExpression)
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :283 :113) // Not a variable of known type: writerFunction
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :283 :47) // Resolvers.ResolveMethod(readerExtensions, Weaver.CurrentAssembly, writerFunction) (InvocationExpression)
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :285 :72) // Not a variable of known type: collectionWriter
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :285 :46) // new GenericInstanceMethod(collectionWriter) (ObjectCreationExpression)
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :286 :12) // Not a variable of known type: methodRef
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :286 :12) // methodRef.GenericArguments (SimpleMemberAccessExpression)
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :286 :43) // Not a variable of known type: elementType
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :286 :12) // methodRef.GenericArguments.Add(elementType) (InvocationExpression)
%39 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :291 :33) // Not a variable of known type: writerFunc
%40 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :291 :33) // writerFunc.Body (SimpleMemberAccessExpression)
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :291 :33) // writerFunc.Body.GetILProcessor() (InvocationExpression)
%43 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :292 :12) // Not a variable of known type: worker
%44 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :292 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%45 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :292 :40) // OpCodes.Ldarg_0 (SimpleMemberAccessExpression)
%46 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :292 :26) // worker.Create(OpCodes.Ldarg_0) (InvocationExpression)
%47 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :292 :12) // worker.Append(worker.Create(OpCodes.Ldarg_0)) (InvocationExpression)
%48 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :293 :12) // Not a variable of known type: worker
%49 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :293 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%50 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :293 :40) // OpCodes.Ldarg_1 (SimpleMemberAccessExpression)
%51 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :293 :26) // worker.Create(OpCodes.Ldarg_1) (InvocationExpression)
%52 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :293 :12) // worker.Append(worker.Create(OpCodes.Ldarg_1)) (InvocationExpression)
%53 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :295 :12) // Not a variable of known type: worker
%54 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :295 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%55 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :295 :40) // OpCodes.Call (SimpleMemberAccessExpression)
%56 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :295 :54) // Not a variable of known type: methodRef
%57 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :295 :26) // worker.Create(OpCodes.Call, methodRef) (InvocationExpression)
%58 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :295 :12) // worker.Append(worker.Create(OpCodes.Call, methodRef)) (InvocationExpression)
%59 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :297 :12) // Not a variable of known type: worker
%60 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :297 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%61 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :297 :40) // OpCodes.Ret (SimpleMemberAccessExpression)
%62 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :297 :26) // worker.Create(OpCodes.Ret) (InvocationExpression)
%63 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :297 :12) // worker.Append(worker.Create(OpCodes.Ret)) (InvocationExpression)
%64 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :299 :19) // Not a variable of known type: writerFunc
return %64 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Writers.cs" :299 :12)

^3: // ExitBlock
cbde.unreachable

}
// Skipping function InitializeWriters(none), it contains poisonous unsupported syntaxes

