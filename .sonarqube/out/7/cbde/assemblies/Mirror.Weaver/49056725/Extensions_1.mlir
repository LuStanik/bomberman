func @_Mirror.Weaver.Extensions.Is$Mono.CecilX.TypeReference.System.Type$(none, none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :9 :8) {
^entry (%_td : none, %_t : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :9 :30)
cbde.store %_td, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :9 :30)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :9 :53)
cbde.store %_t, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :9 :53)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :11 :16) // Not a variable of known type: t
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :11 :16) // t.IsGenericType (SimpleMemberAccessExpression)
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :11 :16)

^1: // JumpBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :13 :23) // Not a variable of known type: td
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :13 :23) // td.GetElementType() (InvocationExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :13 :23) // td.GetElementType().FullName (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :13 :55) // Not a variable of known type: t
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :13 :55) // t.FullName (SimpleMemberAccessExpression)
%9 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :13 :23) // comparison of unknown type: td.GetElementType().FullName == t.FullName
return %9 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :13 :16)

^2: // JumpBlock
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :15 :19) // Not a variable of known type: td
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :15 :19) // td.FullName (SimpleMemberAccessExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :15 :34) // Not a variable of known type: t
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :15 :34) // t.FullName (SimpleMemberAccessExpression)
%14 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :15 :19) // comparison of unknown type: td.FullName == t.FullName
return %14 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :15 :12)

^3: // ExitBlock
cbde.unreachable

}
func @_Mirror.Weaver.Extensions.IsDerivedFrom$Mono.CecilX.TypeReference.System.Type$(none, none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :22 :8) {
^entry (%_tr : none, %_baseClass : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :22 :41)
cbde.store %_tr, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :22 :41)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :22 :64)
cbde.store %_baseClass, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :22 :64)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :24 :32) // Not a variable of known type: tr
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :24 :32) // tr.Resolve() (InvocationExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :25 :17) // Not a variable of known type: td
%6 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :25 :17) // td.IsClass (SimpleMemberAccessExpression)
%7 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :25 :16) // !td.IsClass (LogicalNotExpression)
cond_br %7, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :25 :16)

^1: // JumpBlock
%8 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :26 :23) // false
return %8 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :26 :16)

^2: // BinaryBranchBlock
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :29 :35) // Not a variable of known type: td
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :29 :35) // td.BaseType (SimpleMemberAccessExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :31 :16) // Not a variable of known type: parent
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :31 :26) // null (NullLiteralExpression)
%14 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :31 :16) // comparison of unknown type: parent == null
cond_br %14, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :31 :16)

^3: // JumpBlock
%15 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :32 :23) // false
return %15 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :32 :16)

^4: // BinaryBranchBlock
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :34 :16) // Not a variable of known type: parent
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :34 :26) // Not a variable of known type: baseClass
%18 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :34 :16) // parent.Is(baseClass) (InvocationExpression)
cond_br %18, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :34 :16)

^5: // JumpBlock
%19 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :35 :23) // true
return %19 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :35 :16)

^6: // BinaryBranchBlock
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :37 :16) // Not a variable of known type: parent
%21 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :37 :16) // parent.CanBeResolved() (InvocationExpression)
cond_br %21, ^7, ^8 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :37 :16)

^7: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: IsDerivedFrom
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :38 :37) // Not a variable of known type: parent
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :38 :37) // parent.Resolve() (InvocationExpression)
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :38 :55) // Not a variable of known type: baseClass
%25 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :38 :23) // IsDerivedFrom(parent.Resolve(), baseClass) (InvocationExpression)
return %25 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :38 :16)

^8: // JumpBlock
%26 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :40 :19) // false
return %26 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :40 :12)

^9: // ExitBlock
cbde.unreachable

}
// Skipping function GetEnumUnderlyingType(none), it contains poisonous unsupported syntaxes

// Skipping function ImplementsInterface(none), it contains poisonous unsupported syntaxes

// Skipping function IsMultidimensionalArray(none), it contains poisonous unsupported syntaxes

// Skipping function IsNetworkIdentityField(none), it contains poisonous unsupported syntaxes

// Skipping function CanBeResolved(none), it contains poisonous unsupported syntaxes

func @_Mirror.Weaver.Extensions.MakeGeneric$Mono.CecilX.MethodReference.Mono.CecilX.TypeReference$(none, none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :129 :8) {
^entry (%_generic : none, %_variableReference : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :129 :50)
cbde.store %_generic, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :129 :50)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :129 :80)
cbde.store %_variableReference, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :129 :80)
br ^0

^0: // JumpBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :131 :71) // Not a variable of known type: generic
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :131 :45) // new GenericInstanceMethod(generic) (ObjectCreationExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :132 :12) // Not a variable of known type: instance
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :132 :12) // instance.GenericArguments (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :132 :42) // Not a variable of known type: variableReference
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :132 :12) // instance.GenericArguments.Add(variableReference) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Weaver
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :134 :39) // Weaver.CurrentAssembly (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :134 :39) // Weaver.CurrentAssembly.MainModule (SimpleMemberAccessExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :134 :89) // Not a variable of known type: instance
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :134 :39) // Weaver.CurrentAssembly.MainModule.ImportReference(instance) (InvocationExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :135 :19) // Not a variable of known type: readFunc
return %14 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :135 :12)

^1: // ExitBlock
cbde.unreachable

}
// Skipping function MakeHostInstanceGeneric(none, none), it contains poisonous unsupported syntaxes

func @_Mirror.Weaver.Extensions.SpecializeField$Mono.CecilX.FieldReference.Mono.CecilX.GenericInstanceType$(none, none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :174 :8) {
^entry (%_self : none, %_instanceType : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :174 :53)
cbde.store %_self, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :174 :53)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :174 :79)
cbde.store %_instanceType, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :174 :79)
br ^0

^0: // JumpBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :176 :58) // Not a variable of known type: self
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :176 :58) // self.Name (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :176 :69) // Not a variable of known type: self
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :176 :69) // self.FieldType (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :176 :85) // Not a variable of known type: instanceType
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :176 :39) // new FieldReference(self.Name, self.FieldType, instanceType) (ObjectCreationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Weaver
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :178 :19) // Weaver.CurrentAssembly (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :178 :19) // Weaver.CurrentAssembly.MainModule (SimpleMemberAccessExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :178 :69) // Not a variable of known type: reference
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :178 :19) // Weaver.CurrentAssembly.MainModule.ImportReference(reference) (InvocationExpression)
return %12 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :178 :12)

^1: // ExitBlock
cbde.unreachable

}
// Skipping function GetCustomAttribute(none), it contains poisonous unsupported syntaxes

// Skipping function HasCustomAttribute(none), it contains poisonous unsupported syntaxes

// Skipping function GetField(none, none, none), it contains poisonous unsupported syntaxes

// Skipping function GetMethod(none, none), it contains poisonous unsupported syntaxes

// Skipping function GetMethods(none, none), it contains poisonous unsupported syntaxes

// Skipping function GetMethodInBaseType(none, none), it contains poisonous unsupported syntaxes

func @_Mirror.Weaver.Extensions.FindAllPublicFields$Mono.CecilX.TypeReference$(none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :253 :8) {
^entry (%_variable : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :253 :71)
cbde.store %_variable, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :253 :71)
br ^0

^0: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: FindAllPublicFields
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :255 :39) // Not a variable of known type: variable
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :255 :39) // variable.Resolve() (InvocationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :255 :19) // FindAllPublicFields(variable.Resolve()) (InvocationExpression)
return %3 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Extensions.cs" :255 :12)

^1: // ExitBlock
cbde.unreachable

}
// Skipping function FindAllPublicFields(none), it contains poisonous unsupported syntaxes

