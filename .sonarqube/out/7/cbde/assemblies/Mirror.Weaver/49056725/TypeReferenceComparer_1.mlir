func @_Mirror.Weaver.TypeReferenceComparer.Equals$Mono.CecilX.TypeReference.Mono.CecilX.TypeReference$(none, none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\TypeReferenceComparer.cs" :10 :8) {
^entry (%_x : none, %_y : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\TypeReferenceComparer.cs" :10 :27)
cbde.store %_x, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\TypeReferenceComparer.cs" :10 :27)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\TypeReferenceComparer.cs" :10 :44)
cbde.store %_y, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\TypeReferenceComparer.cs" :10 :44)
br ^0

^0: // JumpBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\TypeReferenceComparer.cs" :12 :19) // Not a variable of known type: x
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\TypeReferenceComparer.cs" :12 :19) // x.FullName (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\TypeReferenceComparer.cs" :12 :33) // Not a variable of known type: y
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\TypeReferenceComparer.cs" :12 :33) // y.FullName (SimpleMemberAccessExpression)
%6 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\TypeReferenceComparer.cs" :12 :19) // comparison of unknown type: x.FullName == y.FullName
return %6 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\TypeReferenceComparer.cs" :12 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.Weaver.TypeReferenceComparer.GetHashCode$Mono.CecilX.TypeReference$(none) -> i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\TypeReferenceComparer.cs" :15 :8) {
^entry (%_obj : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\TypeReferenceComparer.cs" :15 :31)
cbde.store %_obj, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\TypeReferenceComparer.cs" :15 :31)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\TypeReferenceComparer.cs" :17 :19) // Not a variable of known type: obj
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\TypeReferenceComparer.cs" :17 :19) // obj.FullName (SimpleMemberAccessExpression)
%3 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\TypeReferenceComparer.cs" :17 :19) // obj.FullName.GetHashCode() (InvocationExpression)
return %3 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\TypeReferenceComparer.cs" :17 :12)

^1: // ExitBlock
cbde.unreachable

}
