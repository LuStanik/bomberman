func @_Mirror.Weaver.SyncVarProcessor.GetHookMethod$Mono.CecilX.TypeDefinition.Mono.CecilX.FieldDefinition$(none, none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :20 :8) {
^entry (%_td : none, %_syncVar : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :20 :53)
cbde.store %_td, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :20 :53)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :20 :72)
cbde.store %_syncVar, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :20 :72)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :22 :42) // Not a variable of known type: syncVar
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :22 :42) // syncVar.GetCustomAttribute<SyncVarAttribute>() (InvocationExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :24 :16) // Not a variable of known type: syncVarAttr
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :24 :31) // null (NullLiteralExpression)
%7 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :24 :16) // comparison of unknown type: syncVarAttr == null
cond_br %7, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :24 :16)

^1: // JumpBlock
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :25 :23) // null (NullLiteralExpression)
return %8 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :25 :16)

^2: // BinaryBranchBlock
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :27 :38) // Not a variable of known type: syncVarAttr
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :27 :67) // "hook" (StringLiteralExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :27 :75) // null (NullLiteralExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :27 :38) // syncVarAttr.GetField<string>("hook", null) (InvocationExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :29 :16) // Not a variable of known type: hookFunctionName
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :29 :36) // null (NullLiteralExpression)
%16 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :29 :16) // comparison of unknown type: hookFunctionName == null
cond_br %16, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :29 :16)

^3: // JumpBlock
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :30 :23) // null (NullLiteralExpression)
return %17 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :30 :16)

^4: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: FindHookMethod
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :32 :34) // Not a variable of known type: td
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :32 :38) // Not a variable of known type: syncVar
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :32 :47) // Not a variable of known type: hookFunctionName
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :32 :19) // FindHookMethod(td, syncVar, hookFunctionName) (InvocationExpression)
return %21 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :32 :12)

^5: // ExitBlock
cbde.unreachable

}
// Skipping function FindHookMethod(none, none, none), it contains poisonous unsupported syntaxes

// Skipping function MatchesParameters(none, none), it contains poisonous unsupported syntaxes

func @_Mirror.Weaver.SyncVarProcessor.GenerateSyncVarGetter$Mono.CecilX.FieldDefinition.string.Mono.CecilX.FieldDefinition$(none, none, none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :72 :8) {
^entry (%_fd : none, %_originalName : none, %_netFieldId : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :72 :61)
cbde.store %_fd, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :72 :61)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :72 :81)
cbde.store %_originalName, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :72 :81)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :72 :102)
cbde.store %_netFieldId, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :72 :102)
br ^0

^0: // BinaryBranchBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :76 :20) // "get_Network" (StringLiteralExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :76 :36) // Not a variable of known type: originalName
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :76 :20) // Binary expression on unsupported types "get_Network" + originalName
// Entity from another assembly: MethodAttributes
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :76 :50) // MethodAttributes.Public (SimpleMemberAccessExpression)
// Entity from another assembly: MethodAttributes
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :77 :20) // MethodAttributes.SpecialName (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :76 :50) // Binary expression on unsupported types MethodAttributes.Public |                     MethodAttributes.SpecialName
// Entity from another assembly: MethodAttributes
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :78 :20) // MethodAttributes.HideBySig (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :76 :50) // Binary expression on unsupported types MethodAttributes.Public |                     MethodAttributes.SpecialName |                     MethodAttributes.HideBySig
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :79 :20) // Not a variable of known type: fd
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :79 :20) // fd.FieldType (SimpleMemberAccessExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :75 :35) // new MethodDefinition(                     "get_Network" + originalName, MethodAttributes.Public |                     MethodAttributes.SpecialName |                     MethodAttributes.HideBySig,                     fd.FieldType) (ObjectCreationExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :81 :33) // Not a variable of known type: get
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :81 :33) // get.Body (SimpleMemberAccessExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :81 :33) // get.Body.GetILProcessor() (InvocationExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :84 :16) // Not a variable of known type: fd
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :84 :16) // fd.FieldType (SimpleMemberAccessExpression)
%21 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :84 :16) // fd.FieldType.Is<UnityEngine.GameObject>() (InvocationExpression)
cond_br %21, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :84 :16)

^1: // SimpleBlock
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :88 :16) // Not a variable of known type: worker
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :88 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :88 :44) // OpCodes.Ldarg_0 (SimpleMemberAccessExpression)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :88 :30) // worker.Create(OpCodes.Ldarg_0) (InvocationExpression)
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :88 :16) // worker.Append(worker.Create(OpCodes.Ldarg_0)) (InvocationExpression)
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :89 :16) // Not a variable of known type: worker
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :89 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :89 :44) // OpCodes.Ldarg_0 (SimpleMemberAccessExpression)
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :89 :30) // worker.Create(OpCodes.Ldarg_0) (InvocationExpression)
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :89 :16) // worker.Append(worker.Create(OpCodes.Ldarg_0)) (InvocationExpression)
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :90 :16) // Not a variable of known type: worker
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :90 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :90 :44) // OpCodes.Ldfld (SimpleMemberAccessExpression)
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :90 :59) // Not a variable of known type: netFieldId
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :90 :30) // worker.Create(OpCodes.Ldfld, netFieldId) (InvocationExpression)
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :90 :16) // worker.Append(worker.Create(OpCodes.Ldfld, netFieldId)) (InvocationExpression)
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :91 :16) // Not a variable of known type: worker
%39 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :91 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%40 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :91 :44) // OpCodes.Ldarg_0 (SimpleMemberAccessExpression)
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :91 :30) // worker.Create(OpCodes.Ldarg_0) (InvocationExpression)
%42 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :91 :16) // worker.Append(worker.Create(OpCodes.Ldarg_0)) (InvocationExpression)
%43 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :92 :16) // Not a variable of known type: worker
%44 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :92 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%45 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :92 :44) // OpCodes.Ldflda (SimpleMemberAccessExpression)
%46 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :92 :60) // Not a variable of known type: fd
%47 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :92 :30) // worker.Create(OpCodes.Ldflda, fd) (InvocationExpression)
%48 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :92 :16) // worker.Append(worker.Create(OpCodes.Ldflda, fd)) (InvocationExpression)
%49 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :93 :16) // Not a variable of known type: worker
%50 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :93 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%51 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :93 :44) // OpCodes.Call (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WeaverTypes
%52 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :93 :58) // WeaverTypes.getSyncVarGameObjectReference (SimpleMemberAccessExpression)
%53 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :93 :30) // worker.Create(OpCodes.Call, WeaverTypes.getSyncVarGameObjectReference) (InvocationExpression)
%54 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :93 :16) // worker.Append(worker.Create(OpCodes.Call, WeaverTypes.getSyncVarGameObjectReference)) (InvocationExpression)
%55 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :94 :16) // Not a variable of known type: worker
%56 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :94 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%57 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :94 :44) // OpCodes.Ret (SimpleMemberAccessExpression)
%58 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :94 :30) // worker.Create(OpCodes.Ret) (InvocationExpression)
%59 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :94 :16) // worker.Append(worker.Create(OpCodes.Ret)) (InvocationExpression)
br ^3

^2: // BinaryBranchBlock
%60 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :97 :21) // Not a variable of known type: fd
%61 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :97 :21) // fd.FieldType (SimpleMemberAccessExpression)
%62 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :97 :21) // fd.FieldType.Is<NetworkIdentity>() (InvocationExpression)
cond_br %62, ^4, ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :97 :21)

^4: // SimpleBlock
%63 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :101 :16) // Not a variable of known type: worker
%64 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :101 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%65 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :101 :44) // OpCodes.Ldarg_0 (SimpleMemberAccessExpression)
%66 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :101 :30) // worker.Create(OpCodes.Ldarg_0) (InvocationExpression)
%67 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :101 :16) // worker.Append(worker.Create(OpCodes.Ldarg_0)) (InvocationExpression)
%68 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :102 :16) // Not a variable of known type: worker
%69 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :102 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%70 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :102 :44) // OpCodes.Ldarg_0 (SimpleMemberAccessExpression)
%71 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :102 :30) // worker.Create(OpCodes.Ldarg_0) (InvocationExpression)
%72 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :102 :16) // worker.Append(worker.Create(OpCodes.Ldarg_0)) (InvocationExpression)
%73 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :103 :16) // Not a variable of known type: worker
%74 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :103 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%75 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :103 :44) // OpCodes.Ldfld (SimpleMemberAccessExpression)
%76 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :103 :59) // Not a variable of known type: netFieldId
%77 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :103 :30) // worker.Create(OpCodes.Ldfld, netFieldId) (InvocationExpression)
%78 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :103 :16) // worker.Append(worker.Create(OpCodes.Ldfld, netFieldId)) (InvocationExpression)
%79 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :104 :16) // Not a variable of known type: worker
%80 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :104 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%81 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :104 :44) // OpCodes.Ldarg_0 (SimpleMemberAccessExpression)
%82 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :104 :30) // worker.Create(OpCodes.Ldarg_0) (InvocationExpression)
%83 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :104 :16) // worker.Append(worker.Create(OpCodes.Ldarg_0)) (InvocationExpression)
%84 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :105 :16) // Not a variable of known type: worker
%85 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :105 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%86 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :105 :44) // OpCodes.Ldflda (SimpleMemberAccessExpression)
%87 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :105 :60) // Not a variable of known type: fd
%88 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :105 :30) // worker.Create(OpCodes.Ldflda, fd) (InvocationExpression)
%89 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :105 :16) // worker.Append(worker.Create(OpCodes.Ldflda, fd)) (InvocationExpression)
%90 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :106 :16) // Not a variable of known type: worker
%91 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :106 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%92 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :106 :44) // OpCodes.Call (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WeaverTypes
%93 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :106 :58) // WeaverTypes.getSyncVarNetworkIdentityReference (SimpleMemberAccessExpression)
%94 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :106 :30) // worker.Create(OpCodes.Call, WeaverTypes.getSyncVarNetworkIdentityReference) (InvocationExpression)
%95 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :106 :16) // worker.Append(worker.Create(OpCodes.Call, WeaverTypes.getSyncVarNetworkIdentityReference)) (InvocationExpression)
%96 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :107 :16) // Not a variable of known type: worker
%97 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :107 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%98 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :107 :44) // OpCodes.Ret (SimpleMemberAccessExpression)
%99 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :107 :30) // worker.Create(OpCodes.Ret) (InvocationExpression)
%100 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :107 :16) // worker.Append(worker.Create(OpCodes.Ret)) (InvocationExpression)
br ^3

^5: // BinaryBranchBlock
%101 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :109 :21) // Not a variable of known type: fd
%102 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :109 :21) // fd.FieldType (SimpleMemberAccessExpression)
%103 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :109 :21) // fd.FieldType.IsDerivedFrom<NetworkBehaviour>() (InvocationExpression)
cond_br %103, ^6, ^7 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :109 :21)

^6: // SimpleBlock
%104 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :113 :16) // Not a variable of known type: worker
%105 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :113 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%106 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :113 :44) // OpCodes.Ldarg_0 (SimpleMemberAccessExpression)
%107 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :113 :30) // worker.Create(OpCodes.Ldarg_0) (InvocationExpression)
%108 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :113 :16) // worker.Append(worker.Create(OpCodes.Ldarg_0)) (InvocationExpression)
%109 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :114 :16) // Not a variable of known type: worker
%110 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :114 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%111 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :114 :44) // OpCodes.Ldarg_0 (SimpleMemberAccessExpression)
%112 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :114 :30) // worker.Create(OpCodes.Ldarg_0) (InvocationExpression)
%113 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :114 :16) // worker.Append(worker.Create(OpCodes.Ldarg_0)) (InvocationExpression)
%114 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :115 :16) // Not a variable of known type: worker
%115 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :115 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%116 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :115 :44) // OpCodes.Ldfld (SimpleMemberAccessExpression)
%117 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :115 :59) // Not a variable of known type: netFieldId
%118 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :115 :30) // worker.Create(OpCodes.Ldfld, netFieldId) (InvocationExpression)
%119 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :115 :16) // worker.Append(worker.Create(OpCodes.Ldfld, netFieldId)) (InvocationExpression)
%120 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :116 :16) // Not a variable of known type: worker
%121 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :116 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%122 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :116 :44) // OpCodes.Ldarg_0 (SimpleMemberAccessExpression)
%123 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :116 :30) // worker.Create(OpCodes.Ldarg_0) (InvocationExpression)
%124 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :116 :16) // worker.Append(worker.Create(OpCodes.Ldarg_0)) (InvocationExpression)
%125 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :117 :16) // Not a variable of known type: worker
%126 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :117 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%127 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :117 :44) // OpCodes.Ldflda (SimpleMemberAccessExpression)
%128 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :117 :60) // Not a variable of known type: fd
%129 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :117 :30) // worker.Create(OpCodes.Ldflda, fd) (InvocationExpression)
%130 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :117 :16) // worker.Append(worker.Create(OpCodes.Ldflda, fd)) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WeaverTypes
%131 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :118 :42) // WeaverTypes.getSyncVarNetworkBehaviourReference (SimpleMemberAccessExpression)
%132 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :118 :102) // Not a variable of known type: fd
%133 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :118 :102) // fd.FieldType (SimpleMemberAccessExpression)
%134 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :118 :42) // WeaverTypes.getSyncVarNetworkBehaviourReference.MakeGeneric(fd.FieldType) (InvocationExpression)
%136 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :119 :16) // Not a variable of known type: worker
%137 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :119 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%138 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :119 :44) // OpCodes.Call (SimpleMemberAccessExpression)
%139 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :119 :58) // Not a variable of known type: getFunc
%140 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :119 :30) // worker.Create(OpCodes.Call, getFunc) (InvocationExpression)
%141 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :119 :16) // worker.Append(worker.Create(OpCodes.Call, getFunc)) (InvocationExpression)
%142 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :120 :16) // Not a variable of known type: worker
%143 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :120 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%144 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :120 :44) // OpCodes.Ret (SimpleMemberAccessExpression)
%145 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :120 :30) // worker.Create(OpCodes.Ret) (InvocationExpression)
%146 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :120 :16) // worker.Append(worker.Create(OpCodes.Ret)) (InvocationExpression)
br ^3

^7: // SimpleBlock
%147 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :125 :16) // Not a variable of known type: worker
%148 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :125 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%149 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :125 :44) // OpCodes.Ldarg_0 (SimpleMemberAccessExpression)
%150 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :125 :30) // worker.Create(OpCodes.Ldarg_0) (InvocationExpression)
%151 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :125 :16) // worker.Append(worker.Create(OpCodes.Ldarg_0)) (InvocationExpression)
%152 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :126 :16) // Not a variable of known type: worker
%153 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :126 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%154 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :126 :44) // OpCodes.Ldfld (SimpleMemberAccessExpression)
%155 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :126 :59) // Not a variable of known type: fd
%156 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :126 :30) // worker.Create(OpCodes.Ldfld, fd) (InvocationExpression)
%157 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :126 :16) // worker.Append(worker.Create(OpCodes.Ldfld, fd)) (InvocationExpression)
%158 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :127 :16) // Not a variable of known type: worker
%159 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :127 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%160 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :127 :44) // OpCodes.Ret (SimpleMemberAccessExpression)
%161 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :127 :30) // worker.Create(OpCodes.Ret) (InvocationExpression)
%162 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :127 :16) // worker.Append(worker.Create(OpCodes.Ret)) (InvocationExpression)
br ^3

^3: // JumpBlock
%163 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :130 :12) // Not a variable of known type: get
%164 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :130 :12) // get.Body (SimpleMemberAccessExpression)
%165 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :130 :12) // get.Body.Variables (SimpleMemberAccessExpression)
%166 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :130 :58) // Not a variable of known type: fd
%167 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :130 :58) // fd.FieldType (SimpleMemberAccessExpression)
%168 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :130 :35) // new VariableDefinition(fd.FieldType) (ObjectCreationExpression)
%169 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :130 :12) // get.Body.Variables.Add(new VariableDefinition(fd.FieldType)) (InvocationExpression)
%170 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :131 :12) // Not a variable of known type: get
%171 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :131 :12) // get.Body (SimpleMemberAccessExpression)
%172 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :131 :12) // get.Body.InitLocals (SimpleMemberAccessExpression)
%173 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :131 :34) // true
%174 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :132 :12) // Not a variable of known type: get
%175 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :132 :12) // get.SemanticsAttributes (SimpleMemberAccessExpression)
// Entity from another assembly: MethodSemanticsAttributes
%176 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :132 :38) // MethodSemanticsAttributes.Getter (SimpleMemberAccessExpression)
%177 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :134 :19) // Not a variable of known type: get
return %177 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :134 :12)

^8: // ExitBlock
cbde.unreachable

}
func @_Mirror.Weaver.SyncVarProcessor.GenerateSyncVarSetter$Mono.CecilX.TypeDefinition.Mono.CecilX.FieldDefinition.string.long.Mono.CecilX.FieldDefinition$(none, none, none, none, none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :137 :8) {
^entry (%_td : none, %_fd : none, %_originalName : none, %_dirtyBit : none, %_netFieldId : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :137 :61)
cbde.store %_td, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :137 :61)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :137 :80)
cbde.store %_fd, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :137 :80)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :137 :100)
cbde.store %_originalName, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :137 :100)
%3 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :137 :121)
cbde.store %_dirtyBit, %3 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :137 :121)
%4 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :137 :136)
cbde.store %_netFieldId, %4 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :137 :136)
br ^0

^0: // BinaryBranchBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :140 :56) // "set_Network" (StringLiteralExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :140 :72) // Not a variable of known type: originalName
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :140 :56) // Binary expression on unsupported types "set_Network" + originalName
// Entity from another assembly: MethodAttributes
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :140 :86) // MethodAttributes.Public (SimpleMemberAccessExpression)
// Entity from another assembly: MethodAttributes
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :141 :20) // MethodAttributes.SpecialName (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :140 :86) // Binary expression on unsupported types MethodAttributes.Public |                     MethodAttributes.SpecialName
// Entity from another assembly: MethodAttributes
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :142 :20) // MethodAttributes.HideBySig (SimpleMemberAccessExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :140 :86) // Binary expression on unsupported types MethodAttributes.Public |                     MethodAttributes.SpecialName |                     MethodAttributes.HideBySig
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WeaverTypes
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :143 :39) // typeof(void) (TypeOfExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :143 :20) // WeaverTypes.Import(typeof(void)) (InvocationExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :140 :35) // new MethodDefinition("set_Network" + originalName, MethodAttributes.Public |                     MethodAttributes.SpecialName |                     MethodAttributes.HideBySig,                     WeaverTypes.Import(typeof(void))) (ObjectCreationExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :145 :33) // Not a variable of known type: set
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :145 :33) // set.Body (SimpleMemberAccessExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :145 :33) // set.Body.GetILProcessor() (InvocationExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :148 :38) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :148 :52) // OpCodes.Nop (SimpleMemberAccessExpression)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :148 :38) // worker.Create(OpCodes.Nop) (InvocationExpression)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :151 :12) // Not a variable of known type: worker
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :151 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :151 :40) // OpCodes.Ldarg_0 (SimpleMemberAccessExpression)
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :151 :26) // worker.Create(OpCodes.Ldarg_0) (InvocationExpression)
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :151 :12) // worker.Append(worker.Create(OpCodes.Ldarg_0)) (InvocationExpression)
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :153 :12) // Not a variable of known type: worker
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :153 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :153 :40) // OpCodes.Ldarg_1 (SimpleMemberAccessExpression)
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :153 :26) // worker.Create(OpCodes.Ldarg_1) (InvocationExpression)
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :153 :12) // worker.Append(worker.Create(OpCodes.Ldarg_1)) (InvocationExpression)
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :156 :16) // Not a variable of known type: fd
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :156 :16) // fd.FieldType (SimpleMemberAccessExpression)
%37 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :156 :16) // fd.FieldType.Is<UnityEngine.GameObject>() (InvocationExpression)
cond_br %37, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :156 :16)

^1: // SimpleBlock
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :159 :16) // Not a variable of known type: worker
%39 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :159 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%40 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :159 :44) // OpCodes.Ldarg_0 (SimpleMemberAccessExpression)
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :159 :30) // worker.Create(OpCodes.Ldarg_0) (InvocationExpression)
%42 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :159 :16) // worker.Append(worker.Create(OpCodes.Ldarg_0)) (InvocationExpression)
%43 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :160 :16) // Not a variable of known type: worker
%44 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :160 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%45 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :160 :44) // OpCodes.Ldfld (SimpleMemberAccessExpression)
%46 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :160 :59) // Not a variable of known type: netFieldId
%47 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :160 :30) // worker.Create(OpCodes.Ldfld, netFieldId) (InvocationExpression)
%48 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :160 :16) // worker.Append(worker.Create(OpCodes.Ldfld, netFieldId)) (InvocationExpression)
%49 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :162 :16) // Not a variable of known type: worker
%50 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :162 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%51 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :162 :44) // OpCodes.Call (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WeaverTypes
%52 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :162 :58) // WeaverTypes.syncVarGameObjectEqualReference (SimpleMemberAccessExpression)
%53 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :162 :30) // worker.Create(OpCodes.Call, WeaverTypes.syncVarGameObjectEqualReference) (InvocationExpression)
%54 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :162 :16) // worker.Append(worker.Create(OpCodes.Call, WeaverTypes.syncVarGameObjectEqualReference)) (InvocationExpression)
br ^3

^2: // BinaryBranchBlock
%55 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :164 :21) // Not a variable of known type: fd
%56 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :164 :21) // fd.FieldType (SimpleMemberAccessExpression)
%57 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :164 :21) // fd.FieldType.Is<NetworkIdentity>() (InvocationExpression)
cond_br %57, ^4, ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :164 :21)

^4: // SimpleBlock
%58 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :167 :16) // Not a variable of known type: worker
%59 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :167 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%60 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :167 :44) // OpCodes.Ldarg_0 (SimpleMemberAccessExpression)
%61 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :167 :30) // worker.Create(OpCodes.Ldarg_0) (InvocationExpression)
%62 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :167 :16) // worker.Append(worker.Create(OpCodes.Ldarg_0)) (InvocationExpression)
%63 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :168 :16) // Not a variable of known type: worker
%64 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :168 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%65 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :168 :44) // OpCodes.Ldfld (SimpleMemberAccessExpression)
%66 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :168 :59) // Not a variable of known type: netFieldId
%67 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :168 :30) // worker.Create(OpCodes.Ldfld, netFieldId) (InvocationExpression)
%68 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :168 :16) // worker.Append(worker.Create(OpCodes.Ldfld, netFieldId)) (InvocationExpression)
%69 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :170 :16) // Not a variable of known type: worker
%70 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :170 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%71 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :170 :44) // OpCodes.Call (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WeaverTypes
%72 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :170 :58) // WeaverTypes.syncVarNetworkIdentityEqualReference (SimpleMemberAccessExpression)
%73 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :170 :30) // worker.Create(OpCodes.Call, WeaverTypes.syncVarNetworkIdentityEqualReference) (InvocationExpression)
%74 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :170 :16) // worker.Append(worker.Create(OpCodes.Call, WeaverTypes.syncVarNetworkIdentityEqualReference)) (InvocationExpression)
br ^3

^5: // BinaryBranchBlock
%75 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :172 :21) // Not a variable of known type: fd
%76 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :172 :21) // fd.FieldType (SimpleMemberAccessExpression)
%77 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :172 :21) // fd.FieldType.IsDerivedFrom<NetworkBehaviour>() (InvocationExpression)
cond_br %77, ^6, ^7 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :172 :21)

^6: // SimpleBlock
%78 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :175 :16) // Not a variable of known type: worker
%79 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :175 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%80 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :175 :44) // OpCodes.Ldarg_0 (SimpleMemberAccessExpression)
%81 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :175 :30) // worker.Create(OpCodes.Ldarg_0) (InvocationExpression)
%82 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :175 :16) // worker.Append(worker.Create(OpCodes.Ldarg_0)) (InvocationExpression)
%83 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :176 :16) // Not a variable of known type: worker
%84 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :176 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%85 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :176 :44) // OpCodes.Ldfld (SimpleMemberAccessExpression)
%86 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :176 :59) // Not a variable of known type: netFieldId
%87 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :176 :30) // worker.Create(OpCodes.Ldfld, netFieldId) (InvocationExpression)
%88 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :176 :16) // worker.Append(worker.Create(OpCodes.Ldfld, netFieldId)) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WeaverTypes
%89 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :178 :42) // WeaverTypes.syncVarNetworkBehaviourEqualReference (SimpleMemberAccessExpression)
%90 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :178 :104) // Not a variable of known type: fd
%91 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :178 :104) // fd.FieldType (SimpleMemberAccessExpression)
%92 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :178 :42) // WeaverTypes.syncVarNetworkBehaviourEqualReference.MakeGeneric(fd.FieldType) (InvocationExpression)
%94 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :179 :16) // Not a variable of known type: worker
%95 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :179 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%96 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :179 :44) // OpCodes.Call (SimpleMemberAccessExpression)
%97 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :179 :58) // Not a variable of known type: getFunc
%98 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :179 :30) // worker.Create(OpCodes.Call, getFunc) (InvocationExpression)
%99 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :179 :16) // worker.Append(worker.Create(OpCodes.Call, getFunc)) (InvocationExpression)
br ^3

^7: // SimpleBlock
%100 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :183 :16) // Not a variable of known type: worker
%101 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :183 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%102 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :183 :44) // OpCodes.Ldarg_0 (SimpleMemberAccessExpression)
%103 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :183 :30) // worker.Create(OpCodes.Ldarg_0) (InvocationExpression)
%104 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :183 :16) // worker.Append(worker.Create(OpCodes.Ldarg_0)) (InvocationExpression)
%105 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :184 :16) // Not a variable of known type: worker
%106 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :184 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%107 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :184 :44) // OpCodes.Ldflda (SimpleMemberAccessExpression)
%108 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :184 :60) // Not a variable of known type: fd
%109 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :184 :30) // worker.Create(OpCodes.Ldflda, fd) (InvocationExpression)
%110 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :184 :16) // worker.Append(worker.Create(OpCodes.Ldflda, fd)) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WeaverTypes
%111 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :186 :81) // WeaverTypes.syncVarEqualReference (SimpleMemberAccessExpression)
%112 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :186 :55) // new GenericInstanceMethod(WeaverTypes.syncVarEqualReference) (ObjectCreationExpression)
%114 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :187 :16) // Not a variable of known type: syncVarEqualGm
%115 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :187 :16) // syncVarEqualGm.GenericArguments (SimpleMemberAccessExpression)
%116 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :187 :52) // Not a variable of known type: fd
%117 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :187 :52) // fd.FieldType (SimpleMemberAccessExpression)
%118 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :187 :16) // syncVarEqualGm.GenericArguments.Add(fd.FieldType) (InvocationExpression)
%119 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :188 :16) // Not a variable of known type: worker
%120 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :188 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%121 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :188 :44) // OpCodes.Call (SimpleMemberAccessExpression)
%122 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :188 :58) // Not a variable of known type: syncVarEqualGm
%123 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :188 :30) // worker.Create(OpCodes.Call, syncVarEqualGm) (InvocationExpression)
%124 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :188 :16) // worker.Append(worker.Create(OpCodes.Call, syncVarEqualGm)) (InvocationExpression)
br ^3

^3: // BinaryBranchBlock
%125 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :191 :12) // Not a variable of known type: worker
%126 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :191 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%127 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :191 :40) // OpCodes.Brtrue (SimpleMemberAccessExpression)
%128 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :191 :56) // Not a variable of known type: endOfMethod
%129 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :191 :26) // worker.Create(OpCodes.Brtrue, endOfMethod) (InvocationExpression)
%130 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :191 :12) // worker.Append(worker.Create(OpCodes.Brtrue, endOfMethod)) (InvocationExpression)
%131 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :195 :65) // Not a variable of known type: fd
%132 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :195 :65) // fd.FieldType (SimpleMemberAccessExpression)
%133 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :195 :42) // new VariableDefinition(fd.FieldType) (ObjectCreationExpression)
%135 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :196 :12) // Not a variable of known type: set
%136 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :196 :12) // set.Body (SimpleMemberAccessExpression)
%137 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :196 :12) // set.Body.Variables (SimpleMemberAccessExpression)
%138 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :196 :35) // Not a variable of known type: oldValue
%139 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :196 :12) // set.Body.Variables.Add(oldValue) (InvocationExpression)
%140 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :197 :12) // Not a variable of known type: worker
%141 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :197 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%142 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :197 :40) // OpCodes.Ldarg_0 (SimpleMemberAccessExpression)
%143 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :197 :26) // worker.Create(OpCodes.Ldarg_0) (InvocationExpression)
%144 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :197 :12) // worker.Append(worker.Create(OpCodes.Ldarg_0)) (InvocationExpression)
%145 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :198 :12) // Not a variable of known type: worker
%146 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :198 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%147 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :198 :40) // OpCodes.Ldfld (SimpleMemberAccessExpression)
%148 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :198 :55) // Not a variable of known type: fd
%149 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :198 :26) // worker.Create(OpCodes.Ldfld, fd) (InvocationExpression)
%150 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :198 :12) // worker.Append(worker.Create(OpCodes.Ldfld, fd)) (InvocationExpression)
%151 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :199 :12) // Not a variable of known type: worker
%152 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :199 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%153 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :199 :40) // OpCodes.Stloc (SimpleMemberAccessExpression)
%154 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :199 :55) // Not a variable of known type: oldValue
%155 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :199 :26) // worker.Create(OpCodes.Stloc, oldValue) (InvocationExpression)
%156 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :199 :12) // worker.Append(worker.Create(OpCodes.Stloc, oldValue)) (InvocationExpression)
%157 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :202 :12) // Not a variable of known type: worker
%158 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :202 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%159 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :202 :40) // OpCodes.Ldarg_0 (SimpleMemberAccessExpression)
%160 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :202 :26) // worker.Create(OpCodes.Ldarg_0) (InvocationExpression)
%161 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :202 :12) // worker.Append(worker.Create(OpCodes.Ldarg_0)) (InvocationExpression)
%162 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :205 :12) // Not a variable of known type: worker
%163 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :205 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%164 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :205 :40) // OpCodes.Ldarg_1 (SimpleMemberAccessExpression)
%165 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :205 :26) // worker.Create(OpCodes.Ldarg_1) (InvocationExpression)
%166 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :205 :12) // worker.Append(worker.Create(OpCodes.Ldarg_1)) (InvocationExpression)
%167 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :208 :12) // Not a variable of known type: worker
%168 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :208 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%169 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :208 :40) // OpCodes.Ldarg_0 (SimpleMemberAccessExpression)
%170 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :208 :26) // worker.Create(OpCodes.Ldarg_0) (InvocationExpression)
%171 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :208 :12) // worker.Append(worker.Create(OpCodes.Ldarg_0)) (InvocationExpression)
%172 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :209 :12) // Not a variable of known type: worker
%173 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :209 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%174 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :209 :40) // OpCodes.Ldflda (SimpleMemberAccessExpression)
%175 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :209 :56) // Not a variable of known type: fd
%176 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :209 :26) // worker.Create(OpCodes.Ldflda, fd) (InvocationExpression)
%177 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :209 :12) // worker.Append(worker.Create(OpCodes.Ldflda, fd)) (InvocationExpression)
%178 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :213 :12) // Not a variable of known type: worker
%179 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :213 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%180 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :213 :40) // OpCodes.Ldc_I8 (SimpleMemberAccessExpression)
%181 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :213 :56) // Not a variable of known type: dirtyBit
%182 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :213 :26) // worker.Create(OpCodes.Ldc_I8, dirtyBit) (InvocationExpression)
%183 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :213 :12) // worker.Append(worker.Create(OpCodes.Ldc_I8, dirtyBit)) (InvocationExpression)
%184 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :215 :16) // Not a variable of known type: fd
%185 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :215 :16) // fd.FieldType (SimpleMemberAccessExpression)
%186 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :215 :16) // fd.FieldType.Is<UnityEngine.GameObject>() (InvocationExpression)
cond_br %186, ^8, ^9 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :215 :16)

^8: // SimpleBlock
%187 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :218 :16) // Not a variable of known type: worker
%188 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :218 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%189 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :218 :44) // OpCodes.Ldarg_0 (SimpleMemberAccessExpression)
%190 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :218 :30) // worker.Create(OpCodes.Ldarg_0) (InvocationExpression)
%191 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :218 :16) // worker.Append(worker.Create(OpCodes.Ldarg_0)) (InvocationExpression)
%192 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :219 :16) // Not a variable of known type: worker
%193 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :219 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%194 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :219 :44) // OpCodes.Ldflda (SimpleMemberAccessExpression)
%195 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :219 :60) // Not a variable of known type: netFieldId
%196 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :219 :30) // worker.Create(OpCodes.Ldflda, netFieldId) (InvocationExpression)
%197 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :219 :16) // worker.Append(worker.Create(OpCodes.Ldflda, netFieldId)) (InvocationExpression)
%198 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :221 :16) // Not a variable of known type: worker
%199 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :221 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%200 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :221 :44) // OpCodes.Call (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WeaverTypes
%201 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :221 :58) // WeaverTypes.setSyncVarGameObjectReference (SimpleMemberAccessExpression)
%202 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :221 :30) // worker.Create(OpCodes.Call, WeaverTypes.setSyncVarGameObjectReference) (InvocationExpression)
%203 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :221 :16) // worker.Append(worker.Create(OpCodes.Call, WeaverTypes.setSyncVarGameObjectReference)) (InvocationExpression)
br ^10

^9: // BinaryBranchBlock
%204 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :223 :21) // Not a variable of known type: fd
%205 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :223 :21) // fd.FieldType (SimpleMemberAccessExpression)
%206 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :223 :21) // fd.FieldType.Is<NetworkIdentity>() (InvocationExpression)
cond_br %206, ^11, ^12 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :223 :21)

^11: // SimpleBlock
%207 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :226 :16) // Not a variable of known type: worker
%208 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :226 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%209 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :226 :44) // OpCodes.Ldarg_0 (SimpleMemberAccessExpression)
%210 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :226 :30) // worker.Create(OpCodes.Ldarg_0) (InvocationExpression)
%211 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :226 :16) // worker.Append(worker.Create(OpCodes.Ldarg_0)) (InvocationExpression)
%212 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :227 :16) // Not a variable of known type: worker
%213 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :227 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%214 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :227 :44) // OpCodes.Ldflda (SimpleMemberAccessExpression)
%215 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :227 :60) // Not a variable of known type: netFieldId
%216 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :227 :30) // worker.Create(OpCodes.Ldflda, netFieldId) (InvocationExpression)
%217 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :227 :16) // worker.Append(worker.Create(OpCodes.Ldflda, netFieldId)) (InvocationExpression)
%218 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :229 :16) // Not a variable of known type: worker
%219 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :229 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%220 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :229 :44) // OpCodes.Call (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WeaverTypes
%221 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :229 :58) // WeaverTypes.setSyncVarNetworkIdentityReference (SimpleMemberAccessExpression)
%222 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :229 :30) // worker.Create(OpCodes.Call, WeaverTypes.setSyncVarNetworkIdentityReference) (InvocationExpression)
%223 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :229 :16) // worker.Append(worker.Create(OpCodes.Call, WeaverTypes.setSyncVarNetworkIdentityReference)) (InvocationExpression)
br ^10

^12: // BinaryBranchBlock
%224 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :231 :21) // Not a variable of known type: fd
%225 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :231 :21) // fd.FieldType (SimpleMemberAccessExpression)
%226 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :231 :21) // fd.FieldType.IsDerivedFrom<NetworkBehaviour>() (InvocationExpression)
cond_br %226, ^13, ^14 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :231 :21)

^13: // SimpleBlock
%227 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :234 :16) // Not a variable of known type: worker
%228 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :234 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%229 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :234 :44) // OpCodes.Ldarg_0 (SimpleMemberAccessExpression)
%230 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :234 :30) // worker.Create(OpCodes.Ldarg_0) (InvocationExpression)
%231 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :234 :16) // worker.Append(worker.Create(OpCodes.Ldarg_0)) (InvocationExpression)
%232 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :235 :16) // Not a variable of known type: worker
%233 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :235 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%234 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :235 :44) // OpCodes.Ldflda (SimpleMemberAccessExpression)
%235 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :235 :60) // Not a variable of known type: netFieldId
%236 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :235 :30) // worker.Create(OpCodes.Ldflda, netFieldId) (InvocationExpression)
%237 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :235 :16) // worker.Append(worker.Create(OpCodes.Ldflda, netFieldId)) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WeaverTypes
%238 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :237 :42) // WeaverTypes.setSyncVarNetworkBehaviourReference (SimpleMemberAccessExpression)
%239 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :237 :102) // Not a variable of known type: fd
%240 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :237 :102) // fd.FieldType (SimpleMemberAccessExpression)
%241 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :237 :42) // WeaverTypes.setSyncVarNetworkBehaviourReference.MakeGeneric(fd.FieldType) (InvocationExpression)
%243 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :238 :16) // Not a variable of known type: worker
%244 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :238 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%245 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :238 :44) // OpCodes.Call (SimpleMemberAccessExpression)
%246 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :238 :58) // Not a variable of known type: getFunc
%247 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :238 :30) // worker.Create(OpCodes.Call, getFunc) (InvocationExpression)
%248 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :238 :16) // worker.Append(worker.Create(OpCodes.Call, getFunc)) (InvocationExpression)
br ^10

^14: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WeaverTypes
%249 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :243 :69) // WeaverTypes.setSyncVarReference (SimpleMemberAccessExpression)
%250 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :243 :43) // new GenericInstanceMethod(WeaverTypes.setSyncVarReference) (ObjectCreationExpression)
%252 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :244 :16) // Not a variable of known type: gm
%253 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :244 :16) // gm.GenericArguments (SimpleMemberAccessExpression)
%254 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :244 :40) // Not a variable of known type: fd
%255 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :244 :40) // fd.FieldType (SimpleMemberAccessExpression)
%256 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :244 :16) // gm.GenericArguments.Add(fd.FieldType) (InvocationExpression)
%257 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :247 :16) // Not a variable of known type: worker
%258 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :247 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%259 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :247 :44) // OpCodes.Call (SimpleMemberAccessExpression)
%260 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :247 :58) // Not a variable of known type: gm
%261 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :247 :30) // worker.Create(OpCodes.Call, gm) (InvocationExpression)
%262 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :247 :16) // worker.Append(worker.Create(OpCodes.Call, gm)) (InvocationExpression)
br ^10

^10: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetHookMethod
%263 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :250 :56) // Not a variable of known type: td
%264 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :250 :60) // Not a variable of known type: fd
%265 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :250 :42) // GetHookMethod(td, fd) (InvocationExpression)
%267 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :252 :16) // Not a variable of known type: hookMethod
%268 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :252 :30) // null (NullLiteralExpression)
%269 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :252 :16) // comparison of unknown type: hookMethod != null
cond_br %269, ^15, ^16 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :252 :16)

^15: // SimpleBlock
%270 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :255 :36) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%271 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :255 :50) // OpCodes.Nop (SimpleMemberAccessExpression)
%272 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :255 :36) // worker.Create(OpCodes.Nop) (InvocationExpression)
%274 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :256 :16) // Not a variable of known type: worker
%275 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :256 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%276 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :256 :44) // OpCodes.Call (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WeaverTypes
%277 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :256 :58) // WeaverTypes.NetworkServerGetLocalClientActive (SimpleMemberAccessExpression)
%278 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :256 :30) // worker.Create(OpCodes.Call, WeaverTypes.NetworkServerGetLocalClientActive) (InvocationExpression)
%279 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :256 :16) // worker.Append(worker.Create(OpCodes.Call, WeaverTypes.NetworkServerGetLocalClientActive)) (InvocationExpression)
%280 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :257 :16) // Not a variable of known type: worker
%281 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :257 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%282 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :257 :44) // OpCodes.Brfalse (SimpleMemberAccessExpression)
%283 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :257 :61) // Not a variable of known type: label
%284 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :257 :30) // worker.Create(OpCodes.Brfalse, label) (InvocationExpression)
%285 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :257 :16) // worker.Append(worker.Create(OpCodes.Brfalse, label)) (InvocationExpression)
%286 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :258 :16) // Not a variable of known type: worker
%287 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :258 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%288 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :258 :44) // OpCodes.Ldarg_0 (SimpleMemberAccessExpression)
%289 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :258 :30) // worker.Create(OpCodes.Ldarg_0) (InvocationExpression)
%290 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :258 :16) // worker.Append(worker.Create(OpCodes.Ldarg_0)) (InvocationExpression)
%291 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :259 :16) // Not a variable of known type: worker
%292 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :259 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%293 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :259 :44) // OpCodes.Ldc_I8 (SimpleMemberAccessExpression)
%294 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :259 :60) // Not a variable of known type: dirtyBit
%295 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :259 :30) // worker.Create(OpCodes.Ldc_I8, dirtyBit) (InvocationExpression)
%296 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :259 :16) // worker.Append(worker.Create(OpCodes.Ldc_I8, dirtyBit)) (InvocationExpression)
%297 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :260 :16) // Not a variable of known type: worker
%298 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :260 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%299 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :260 :44) // OpCodes.Call (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WeaverTypes
%300 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :260 :58) // WeaverTypes.getSyncVarHookGuard (SimpleMemberAccessExpression)
%301 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :260 :30) // worker.Create(OpCodes.Call, WeaverTypes.getSyncVarHookGuard) (InvocationExpression)
%302 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :260 :16) // worker.Append(worker.Create(OpCodes.Call, WeaverTypes.getSyncVarHookGuard)) (InvocationExpression)
%303 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :261 :16) // Not a variable of known type: worker
%304 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :261 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%305 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :261 :44) // OpCodes.Brtrue (SimpleMemberAccessExpression)
%306 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :261 :60) // Not a variable of known type: label
%307 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :261 :30) // worker.Create(OpCodes.Brtrue, label) (InvocationExpression)
%308 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :261 :16) // worker.Append(worker.Create(OpCodes.Brtrue, label)) (InvocationExpression)
%309 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :264 :16) // Not a variable of known type: worker
%310 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :264 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%311 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :264 :44) // OpCodes.Ldarg_0 (SimpleMemberAccessExpression)
%312 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :264 :30) // worker.Create(OpCodes.Ldarg_0) (InvocationExpression)
%313 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :264 :16) // worker.Append(worker.Create(OpCodes.Ldarg_0)) (InvocationExpression)
%314 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :265 :16) // Not a variable of known type: worker
%315 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :265 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%316 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :265 :44) // OpCodes.Ldc_I8 (SimpleMemberAccessExpression)
%317 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :265 :60) // Not a variable of known type: dirtyBit
%318 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :265 :30) // worker.Create(OpCodes.Ldc_I8, dirtyBit) (InvocationExpression)
%319 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :265 :16) // worker.Append(worker.Create(OpCodes.Ldc_I8, dirtyBit)) (InvocationExpression)
%320 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :266 :16) // Not a variable of known type: worker
%321 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :266 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%322 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :266 :44) // OpCodes.Ldc_I4_1 (SimpleMemberAccessExpression)
%323 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :266 :30) // worker.Create(OpCodes.Ldc_I4_1) (InvocationExpression)
%324 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :266 :16) // worker.Append(worker.Create(OpCodes.Ldc_I4_1)) (InvocationExpression)
%325 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :267 :16) // Not a variable of known type: worker
%326 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :267 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%327 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :267 :44) // OpCodes.Call (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WeaverTypes
%328 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :267 :58) // WeaverTypes.setSyncVarHookGuard (SimpleMemberAccessExpression)
%329 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :267 :30) // worker.Create(OpCodes.Call, WeaverTypes.setSyncVarHookGuard) (InvocationExpression)
%330 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :267 :16) // worker.Append(worker.Create(OpCodes.Call, WeaverTypes.setSyncVarHookGuard)) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WriteCallHookMethodUsingArgument
%331 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :271 :49) // Not a variable of known type: worker
%332 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :271 :57) // Not a variable of known type: hookMethod
%333 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :271 :69) // Not a variable of known type: oldValue
%334 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :271 :16) // WriteCallHookMethodUsingArgument(worker, hookMethod, oldValue) (InvocationExpression)
%335 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :274 :16) // Not a variable of known type: worker
%336 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :274 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%337 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :274 :44) // OpCodes.Ldarg_0 (SimpleMemberAccessExpression)
%338 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :274 :30) // worker.Create(OpCodes.Ldarg_0) (InvocationExpression)
%339 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :274 :16) // worker.Append(worker.Create(OpCodes.Ldarg_0)) (InvocationExpression)
%340 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :275 :16) // Not a variable of known type: worker
%341 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :275 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%342 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :275 :44) // OpCodes.Ldc_I8 (SimpleMemberAccessExpression)
%343 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :275 :60) // Not a variable of known type: dirtyBit
%344 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :275 :30) // worker.Create(OpCodes.Ldc_I8, dirtyBit) (InvocationExpression)
%345 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :275 :16) // worker.Append(worker.Create(OpCodes.Ldc_I8, dirtyBit)) (InvocationExpression)
%346 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :276 :16) // Not a variable of known type: worker
%347 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :276 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%348 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :276 :44) // OpCodes.Ldc_I4_0 (SimpleMemberAccessExpression)
%349 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :276 :30) // worker.Create(OpCodes.Ldc_I4_0) (InvocationExpression)
%350 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :276 :16) // worker.Append(worker.Create(OpCodes.Ldc_I4_0)) (InvocationExpression)
%351 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :277 :16) // Not a variable of known type: worker
%352 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :277 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%353 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :277 :44) // OpCodes.Call (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WeaverTypes
%354 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :277 :58) // WeaverTypes.setSyncVarHookGuard (SimpleMemberAccessExpression)
%355 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :277 :30) // worker.Create(OpCodes.Call, WeaverTypes.setSyncVarHookGuard) (InvocationExpression)
%356 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :277 :16) // worker.Append(worker.Create(OpCodes.Call, WeaverTypes.setSyncVarHookGuard)) (InvocationExpression)
%357 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :279 :16) // Not a variable of known type: worker
%358 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :279 :30) // Not a variable of known type: label
%359 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :279 :16) // worker.Append(label) (InvocationExpression)
br ^16

^16: // JumpBlock
%360 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :282 :12) // Not a variable of known type: worker
%361 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :282 :26) // Not a variable of known type: endOfMethod
%362 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :282 :12) // worker.Append(endOfMethod) (InvocationExpression)
%363 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :284 :12) // Not a variable of known type: worker
%364 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :284 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%365 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :284 :40) // OpCodes.Ret (SimpleMemberAccessExpression)
%366 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :284 :26) // worker.Create(OpCodes.Ret) (InvocationExpression)
%367 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :284 :12) // worker.Append(worker.Create(OpCodes.Ret)) (InvocationExpression)
%368 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :286 :12) // Not a variable of known type: set
%369 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :286 :12) // set.Parameters (SimpleMemberAccessExpression)
%370 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :286 :55) // "value" (StringLiteralExpression)
// Entity from another assembly: ParameterAttributes
%371 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :286 :64) // ParameterAttributes.In (SimpleMemberAccessExpression)
%372 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :286 :88) // Not a variable of known type: fd
%373 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :286 :88) // fd.FieldType (SimpleMemberAccessExpression)
%374 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :286 :31) // new ParameterDefinition("value", ParameterAttributes.In, fd.FieldType) (ObjectCreationExpression)
%375 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :286 :12) // set.Parameters.Add(new ParameterDefinition("value", ParameterAttributes.In, fd.FieldType)) (InvocationExpression)
%376 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :287 :12) // Not a variable of known type: set
%377 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :287 :12) // set.SemanticsAttributes (SimpleMemberAccessExpression)
// Entity from another assembly: MethodSemanticsAttributes
%378 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :287 :38) // MethodSemanticsAttributes.Setter (SimpleMemberAccessExpression)
%379 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :289 :19) // Not a variable of known type: set
return %379 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :289 :12)

^17: // ExitBlock
cbde.unreachable

}
func @_Mirror.Weaver.SyncVarProcessor.ProcessSyncVar$Mono.CecilX.TypeDefinition.Mono.CecilX.FieldDefinition.System.Collections.Generic.Dictionary$Mono.CecilX.FieldDefinition.Mono.CecilX.FieldDefinition$.long$(none, none, none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :292 :8) {
^entry (%_td : none, %_fd : none, %_syncVarNetIds : none, %_dirtyBit : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :292 :42)
cbde.store %_td, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :292 :42)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :292 :61)
cbde.store %_fd, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :292 :61)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :292 :81)
cbde.store %_syncVarNetIds, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :292 :81)
%3 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :292 :141)
cbde.store %_dirtyBit, %3 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :292 :141)
br ^0

^0: // BinaryBranchBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :294 :34) // Not a variable of known type: fd
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :294 :34) // fd.Name (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Weaver
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :295 :24) // Not a variable of known type: td
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :295 :28) // "Sync Var " (StringLiteralExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :295 :42) // Not a variable of known type: fd
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :295 :42) // fd.Name (SimpleMemberAccessExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :295 :28) // Binary expression on unsupported types "Sync Var " + fd.Name
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :295 :52) // " " (StringLiteralExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :295 :28) // Binary expression on unsupported types "Sync Var " + fd.Name + " "
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :295 :58) // Not a variable of known type: fd
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :295 :58) // fd.FieldType (SimpleMemberAccessExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :295 :28) // Binary expression on unsupported types "Sync Var " + fd.Name + " " + fd.FieldType
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :295 :12) // Weaver.DLog(td, "Sync Var " + fd.Name + " " + fd.FieldType) (InvocationExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :298 :41) // null (NullLiteralExpression)
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :300 :16) // Not a variable of known type: fd
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :300 :16) // fd.FieldType (SimpleMemberAccessExpression)
%22 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :300 :16) // fd.FieldType.IsDerivedFrom<NetworkBehaviour>() (InvocationExpression)
cond_br %22, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :300 :16)

^1: // SimpleBlock
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :302 :49) // "___" (StringLiteralExpression)
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :302 :57) // Not a variable of known type: fd
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :302 :57) // fd.Name (SimpleMemberAccessExpression)
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :302 :49) // Binary expression on unsupported types "___" + fd.Name
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :302 :67) // "NetId" (StringLiteralExpression)
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :302 :49) // Binary expression on unsupported types "___" + fd.Name + "NetId"
// Entity from another assembly: FieldAttributes
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :303 :19) // FieldAttributes.Private (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WeaverTypes
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :304 :19) // WeaverTypes.Import<NetworkBehaviour.NetworkBehaviourSyncVar>() (InvocationExpression)
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :302 :29) // new FieldDefinition("___" + fd.Name + "NetId",                    FieldAttributes.Private,                    WeaverTypes.Import<NetworkBehaviour.NetworkBehaviourSyncVar>()) (ObjectCreationExpression)
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :306 :16) // Not a variable of known type: syncVarNetIds
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :306 :30) // Not a variable of known type: fd
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :306 :16) // syncVarNetIds[fd] (ElementAccessExpression)
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :306 :36) // Not a variable of known type: netIdField
br ^3

^2: // BinaryBranchBlock
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :308 :21) // Not a variable of known type: fd
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :308 :21) // fd.FieldType (SimpleMemberAccessExpression)
%38 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :308 :21) // fd.FieldType.IsNetworkIdentityField() (InvocationExpression)
cond_br %38, ^4, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :308 :21)

^4: // SimpleBlock
%39 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :310 :49) // "___" (StringLiteralExpression)
%40 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :310 :57) // Not a variable of known type: fd
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :310 :57) // fd.Name (SimpleMemberAccessExpression)
%42 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :310 :49) // Binary expression on unsupported types "___" + fd.Name
%43 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :310 :67) // "NetId" (StringLiteralExpression)
%44 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :310 :49) // Binary expression on unsupported types "___" + fd.Name + "NetId"
// Entity from another assembly: FieldAttributes
%45 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :311 :20) // FieldAttributes.Private (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WeaverTypes
%46 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :312 :20) // WeaverTypes.Import<uint>() (InvocationExpression)
%47 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :310 :29) // new FieldDefinition("___" + fd.Name + "NetId",                     FieldAttributes.Private,                     WeaverTypes.Import<uint>()) (ObjectCreationExpression)
%48 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :314 :16) // Not a variable of known type: syncVarNetIds
%49 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :314 :30) // Not a variable of known type: fd
%50 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :314 :16) // syncVarNetIds[fd] (ElementAccessExpression)
%51 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :314 :36) // Not a variable of known type: netIdField
br ^3

^3: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GenerateSyncVarGetter
%52 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :317 :57) // Not a variable of known type: fd
%53 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :317 :61) // Not a variable of known type: originalName
%54 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :317 :75) // Not a variable of known type: netIdField
%55 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :317 :35) // GenerateSyncVarGetter(fd, originalName, netIdField) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GenerateSyncVarSetter
%57 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :318 :57) // Not a variable of known type: td
%58 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :318 :61) // Not a variable of known type: fd
%59 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :318 :65) // Not a variable of known type: originalName
%60 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :318 :79) // Not a variable of known type: dirtyBit
%61 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :318 :89) // Not a variable of known type: netIdField
%62 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :318 :35) // GenerateSyncVarSetter(td, fd, originalName, dirtyBit, netIdField) (InvocationExpression)
%64 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :322 :75) // "Network" (StringLiteralExpression)
%65 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :322 :87) // Not a variable of known type: originalName
%66 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :322 :75) // Binary expression on unsupported types "Network" + originalName
// Entity from another assembly: PropertyAttributes
%67 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :322 :101) // PropertyAttributes.None (SimpleMemberAccessExpression)
%68 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :322 :126) // Not a variable of known type: fd
%69 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :322 :126) // fd.FieldType (SimpleMemberAccessExpression)
%70 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :322 :52) // new PropertyDefinition("Network" + originalName, PropertyAttributes.None, fd.FieldType)             {                 GetMethod = get,                 SetMethod = set             } (ObjectCreationExpression)
%71 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :324 :28) // Not a variable of known type: get
%72 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :325 :28) // Not a variable of known type: set
%74 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :329 :12) // Not a variable of known type: td
%75 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :329 :12) // td.Methods (SimpleMemberAccessExpression)
%76 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :329 :27) // Not a variable of known type: get
%77 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :329 :12) // td.Methods.Add(get) (InvocationExpression)
%78 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :330 :12) // Not a variable of known type: td
%79 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :330 :12) // td.Methods (SimpleMemberAccessExpression)
%80 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :330 :27) // Not a variable of known type: set
%81 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :330 :12) // td.Methods.Add(set) (InvocationExpression)
%82 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :331 :12) // Not a variable of known type: td
%83 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :331 :12) // td.Properties (SimpleMemberAccessExpression)
%84 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :331 :30) // Not a variable of known type: propertyDefinition
%85 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :331 :12) // td.Properties.Add(propertyDefinition) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Weaver
%86 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :332 :12) // Weaver.WeaveLists (SimpleMemberAccessExpression)
%87 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :332 :12) // Weaver.WeaveLists.replacementSetterProperties (SimpleMemberAccessExpression)
%88 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :332 :58) // Not a variable of known type: fd
%89 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :332 :12) // Weaver.WeaveLists.replacementSetterProperties[fd] (ElementAccessExpression)
%90 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :332 :64) // Not a variable of known type: set
%91 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :338 :16) // Not a variable of known type: fd
%92 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :338 :16) // fd.FieldType (SimpleMemberAccessExpression)
%93 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :338 :16) // fd.FieldType.IsNetworkIdentityField() (InvocationExpression)
cond_br %93, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :338 :16)

^5: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Weaver
%94 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :340 :16) // Weaver.WeaveLists (SimpleMemberAccessExpression)
%95 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :340 :16) // Weaver.WeaveLists.replacementGetterProperties (SimpleMemberAccessExpression)
%96 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :340 :62) // Not a variable of known type: fd
%97 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :340 :16) // Weaver.WeaveLists.replacementGetterProperties[fd] (ElementAccessExpression)
%98 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :340 :68) // Not a variable of known type: get
br ^6

^6: // ExitBlock
return

}
// Skipping function ProcessSyncVars(none), it contains poisonous unsupported syntaxes

func @_Mirror.Weaver.SyncVarProcessor.WriteCallHookMethodUsingArgument$Mono.CecilX.Cil.ILProcessor.Mono.CecilX.MethodDefinition.Mono.CecilX.Cil.VariableDefinition$(none, none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :400 :8) {
^entry (%_worker : none, %_hookMethod : none, %_oldValue : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :400 :60)
cbde.store %_worker, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :400 :60)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :400 :80)
cbde.store %_hookMethod, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :400 :80)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :400 :109)
cbde.store %_oldValue, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :400 :109)
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WriteCallHookMethod
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :402 :32) // Not a variable of known type: worker
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :402 :40) // Not a variable of known type: hookMethod
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :402 :52) // Not a variable of known type: oldValue
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :402 :62) // null (NullLiteralExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :402 :12) // WriteCallHookMethod(worker, hookMethod, oldValue, null) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Weaver.SyncVarProcessor.WriteCallHookMethodUsingField$Mono.CecilX.Cil.ILProcessor.Mono.CecilX.MethodDefinition.Mono.CecilX.Cil.VariableDefinition.Mono.CecilX.FieldDefinition$(none, none, none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :405 :8) {
^entry (%_worker : none, %_hookMethod : none, %_oldValue : none, %_newValue : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :405 :57)
cbde.store %_worker, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :405 :57)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :405 :77)
cbde.store %_hookMethod, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :405 :77)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :405 :106)
cbde.store %_oldValue, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :405 :106)
%3 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :405 :135)
cbde.store %_newValue, %3 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :405 :135)
br ^0

^0: // BinaryBranchBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :407 :16) // Not a variable of known type: newValue
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :407 :28) // null (NullLiteralExpression)
%6 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :407 :16) // comparison of unknown type: newValue == null
cond_br %6, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :407 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Weaver
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :409 :29) // "NewValue field was null when writing SyncVar hook" (StringLiteralExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :409 :16) // Weaver.Error("NewValue field was null when writing SyncVar hook") (InvocationExpression)
br ^2

^2: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WriteCallHookMethod
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :412 :32) // Not a variable of known type: worker
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :412 :40) // Not a variable of known type: hookMethod
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :412 :52) // Not a variable of known type: oldValue
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :412 :62) // Not a variable of known type: newValue
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Processors\\SyncVarProcessor.cs" :412 :12) // WriteCallHookMethod(worker, hookMethod, oldValue, newValue) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function WriteCallHookMethod(none, none, none, none), it contains poisonous unsupported syntaxes

