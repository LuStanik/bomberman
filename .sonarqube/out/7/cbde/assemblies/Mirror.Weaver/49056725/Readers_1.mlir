func @_Mirror.Weaver.Readers.Init$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :12 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :14 :71) // new TypeReferenceComparer() (ObjectCreationExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :14 :24) // new Dictionary<TypeReference, MethodReference>(new TypeReferenceComparer()) (ObjectCreationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Weaver.Readers.Register$Mono.CecilX.TypeReference.Mono.CecilX.MethodReference$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :17 :8) {
^entry (%_dataType : none, %_methodReference : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :17 :38)
cbde.store %_dataType, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :17 :38)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :17 :62)
cbde.store %_methodReference, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :17 :62)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :19 :16) // Not a variable of known type: readFuncs
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :19 :38) // Not a variable of known type: dataType
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :19 :16) // readFuncs.ContainsKey(dataType) (InvocationExpression)
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :19 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Weaver
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :21 :64) // Not a variable of known type: dataType
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :21 :64) // dataType.FullName (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :21 :31) // $"Registering a Read method for {dataType.FullName} when one already exists" (InterpolatedStringExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :21 :109) // Not a variable of known type: methodReference
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :21 :16) // Weaver.Warning($"Registering a Read method for {dataType.FullName} when one already exists", methodReference) (InvocationExpression)
br ^2

^2: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Weaver
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :25 :37) // Weaver.CurrentAssembly (SimpleMemberAccessExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :25 :37) // Weaver.CurrentAssembly.MainModule (SimpleMemberAccessExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :25 :87) // Not a variable of known type: dataType
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :25 :37) // Weaver.CurrentAssembly.MainModule.ImportReference(dataType) (InvocationExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :26 :12) // Not a variable of known type: readFuncs
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :26 :22) // Not a variable of known type: imported
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :26 :12) // readFuncs[imported] (ElementAccessExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :26 :34) // Not a variable of known type: methodReference
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Weaver.Readers.RegisterReadFunc$Mono.CecilX.TypeReference.Mono.CecilX.MethodDefinition$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :29 :8) {
^entry (%_typeReference : none, %_newReaderFunc : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :29 :37)
cbde.store %_typeReference, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :29 :37)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :29 :66)
cbde.store %_newReaderFunc, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :29 :66)
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Register
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :31 :21) // Not a variable of known type: typeReference
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :31 :36) // Not a variable of known type: newReaderFunc
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :31 :12) // Register(typeReference, newReaderFunc) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Weaver
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :33 :12) // Weaver.WeaveLists (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :33 :12) // Weaver.WeaveLists.generateContainerClass (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :33 :12) // Weaver.WeaveLists.generateContainerClass.Methods (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :33 :65) // Not a variable of known type: newReaderFunc
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :33 :12) // Weaver.WeaveLists.generateContainerClass.Methods.Add(newReaderFunc) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function GetReadFunc(none), it contains poisonous unsupported syntaxes

// Skipping function GenerateReader(none), it contains poisonous unsupported syntaxes

func @_Mirror.Weaver.Readers.GetNetworkBehaviourReader$Mono.CecilX.TypeReference$(none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :139 :8) {
^entry (%_variableReference : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :139 :65)
cbde.store %_variableReference, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :139 :65)
br ^0

^0: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WeaverTypes
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :142 :38) // WeaverTypes.readNetworkBehaviourGeneric (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :144 :39) // Not a variable of known type: generic
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :144 :59) // Not a variable of known type: variableReference
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :144 :39) // generic.MakeGeneric(variableReference) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Register
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :148 :21) // Not a variable of known type: variableReference
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :148 :40) // Not a variable of known type: readFunc
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :148 :12) // Register(variableReference, readFunc) (InvocationExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :150 :19) // Not a variable of known type: readFunc
return %10 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :150 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.Weaver.Readers.GenerateEnumReadFunc$Mono.CecilX.TypeReference$(none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :153 :8) {
^entry (%_variable : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :153 :53)
cbde.store %_variable, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :153 :53)
br ^0

^0: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GenerateReaderFunction
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :155 :65) // Not a variable of known type: variable
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :155 :42) // GenerateReaderFunction(variable) (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :157 :33) // Not a variable of known type: readerFunc
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :157 :33) // readerFunc.Body (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :157 :33) // readerFunc.Body.GetILProcessor() (InvocationExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :159 :12) // Not a variable of known type: worker
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :159 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :159 :40) // OpCodes.Ldarg_0 (SimpleMemberAccessExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :159 :26) // worker.Create(OpCodes.Ldarg_0) (InvocationExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :159 :12) // worker.Append(worker.Create(OpCodes.Ldarg_0)) (InvocationExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :161 :43) // Not a variable of known type: variable
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :161 :43) // variable.Resolve() (InvocationExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :161 :43) // variable.Resolve().GetEnumUnderlyingType() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetReadFunc
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :162 :57) // Not a variable of known type: underlyingType
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :162 :45) // GetReadFunc(underlyingType) (InvocationExpression)
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :164 :12) // Not a variable of known type: worker
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :164 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :164 :40) // OpCodes.Call (SimpleMemberAccessExpression)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :164 :54) // Not a variable of known type: underlyingFunc
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :164 :26) // worker.Create(OpCodes.Call, underlyingFunc) (InvocationExpression)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :164 :12) // worker.Append(worker.Create(OpCodes.Call, underlyingFunc)) (InvocationExpression)
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :165 :12) // Not a variable of known type: worker
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :165 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :165 :40) // OpCodes.Ret (SimpleMemberAccessExpression)
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :165 :26) // worker.Create(OpCodes.Ret) (InvocationExpression)
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :165 :12) // worker.Append(worker.Create(OpCodes.Ret)) (InvocationExpression)
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :166 :19) // Not a variable of known type: readerFunc
return %31 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :166 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.Weaver.Readers.GenerateArraySegmentReadFunc$Mono.CecilX.TypeReference$(none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :169 :8) {
^entry (%_variable : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :169 :61)
cbde.store %_variable, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :169 :61)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :171 :71) // Not a variable of known type: variable
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :171 :50) // (GenericInstanceType)variable (CastExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :172 :40) // Not a variable of known type: genericInstance
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :172 :40) // genericInstance.GenericArguments (SimpleMemberAccessExpression)
%6 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :172 :73)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :172 :40) // genericInstance.GenericArguments[0] (ElementAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GenerateReaderFunction
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :174 :65) // Not a variable of known type: variable
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :174 :42) // GenerateReaderFunction(variable) (InvocationExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :176 :33) // Not a variable of known type: readerFunc
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :176 :33) // readerFunc.Body (SimpleMemberAccessExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :176 :33) // readerFunc.Body.GetILProcessor() (InvocationExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :179 :34) // Not a variable of known type: elementType
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :179 :34) // elementType.MakeArrayType() (InvocationExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :180 :12) // Not a variable of known type: worker
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :180 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :180 :40) // OpCodes.Ldarg_0 (SimpleMemberAccessExpression)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :180 :26) // worker.Create(OpCodes.Ldarg_0) (InvocationExpression)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :180 :12) // worker.Append(worker.Create(OpCodes.Ldarg_0)) (InvocationExpression)
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :181 :12) // Not a variable of known type: worker
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :181 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :181 :40) // OpCodes.Call (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetReadFunc
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :181 :66) // Not a variable of known type: arrayType
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :181 :54) // GetReadFunc(arrayType) (InvocationExpression)
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :181 :26) // worker.Create(OpCodes.Call, GetReadFunc(arrayType)) (InvocationExpression)
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :181 :12) // worker.Append(worker.Create(OpCodes.Call, GetReadFunc(arrayType))) (InvocationExpression)
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :184 :12) // Not a variable of known type: worker
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :184 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :184 :40) // OpCodes.Newobj (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WeaverTypes
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :184 :56) // WeaverTypes.ArraySegmentConstructorReference (SimpleMemberAccessExpression)
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :184 :125) // Not a variable of known type: genericInstance
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :184 :56) // WeaverTypes.ArraySegmentConstructorReference.MakeHostInstanceGeneric(genericInstance) (InvocationExpression)
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :184 :26) // worker.Create(OpCodes.Newobj, WeaverTypes.ArraySegmentConstructorReference.MakeHostInstanceGeneric(genericInstance)) (InvocationExpression)
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :184 :12) // worker.Append(worker.Create(OpCodes.Newobj, WeaverTypes.ArraySegmentConstructorReference.MakeHostInstanceGeneric(genericInstance))) (InvocationExpression)
%39 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :185 :12) // Not a variable of known type: worker
%40 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :185 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :185 :40) // OpCodes.Ret (SimpleMemberAccessExpression)
%42 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :185 :26) // worker.Create(OpCodes.Ret) (InvocationExpression)
%43 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :185 :12) // worker.Append(worker.Create(OpCodes.Ret)) (InvocationExpression)
%44 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :186 :19) // Not a variable of known type: readerFunc
return %44 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :186 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.Weaver.Readers.GenerateReaderFunction$Mono.CecilX.TypeReference$(none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :189 :8) {
^entry (%_variable : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :189 :63)
cbde.store %_variable, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :189 :63)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :191 :34) // "_Read_" (StringLiteralExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :191 :45) // Not a variable of known type: variable
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :191 :45) // variable.FullName (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :191 :34) // Binary expression on unsupported types "_Read_" + variable.FullName
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :194 :63) // Not a variable of known type: functionName
// Entity from another assembly: MethodAttributes
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :195 :20) // MethodAttributes.Public (SimpleMemberAccessExpression)
// Entity from another assembly: MethodAttributes
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :196 :20) // MethodAttributes.Static (SimpleMemberAccessExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :195 :20) // Binary expression on unsupported types MethodAttributes.Public |                     MethodAttributes.Static
// Entity from another assembly: MethodAttributes
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :197 :20) // MethodAttributes.HideBySig (SimpleMemberAccessExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :195 :20) // Binary expression on unsupported types MethodAttributes.Public |                     MethodAttributes.Static |                     MethodAttributes.HideBySig
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :198 :20) // Not a variable of known type: variable
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :194 :42) // new MethodDefinition(functionName,                     MethodAttributes.Public |                     MethodAttributes.Static |                     MethodAttributes.HideBySig,                     variable) (ObjectCreationExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :200 :12) // Not a variable of known type: readerFunc
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :200 :12) // readerFunc.Parameters (SimpleMemberAccessExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :200 :62) // "reader" (StringLiteralExpression)
// Entity from another assembly: ParameterAttributes
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :200 :72) // ParameterAttributes.None (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WeaverTypes
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :200 :98) // WeaverTypes.Import<NetworkReader>() (InvocationExpression)
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :200 :38) // new ParameterDefinition("reader", ParameterAttributes.None, WeaverTypes.Import<NetworkReader>()) (ObjectCreationExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :200 :12) // readerFunc.Parameters.Add(new ParameterDefinition("reader", ParameterAttributes.None, WeaverTypes.Import<NetworkReader>())) (InvocationExpression)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :201 :12) // Not a variable of known type: readerFunc
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :201 :12) // readerFunc.Body (SimpleMemberAccessExpression)
%24 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :201 :12) // readerFunc.Body.InitLocals (SimpleMemberAccessExpression)
%25 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :201 :41) // true
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: RegisterReadFunc
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :202 :29) // Not a variable of known type: variable
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :202 :39) // Not a variable of known type: readerFunc
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :202 :12) // RegisterReadFunc(variable, readerFunc) (InvocationExpression)
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :204 :19) // Not a variable of known type: readerFunc
return %29 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :204 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.Weaver.Readers.GenerateReadCollection$Mono.CecilX.TypeReference.Mono.CecilX.TypeReference.string$(none, none, none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :207 :8) {
^entry (%_variable : none, %_elementType : none, %_readerFunction : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :207 :55)
cbde.store %_variable, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :207 :55)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :207 :79)
cbde.store %_elementType, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :207 :79)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :207 :106)
cbde.store %_readerFunction, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :207 :106)
br ^0

^0: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GenerateReaderFunction
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :209 :65) // Not a variable of known type: variable
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :209 :42) // GenerateReaderFunction(variable) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetReadFunc
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :211 :24) // Not a variable of known type: elementType
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :211 :12) // GetReadFunc(elementType) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Weaver
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :213 :38) // Weaver.CurrentAssembly (SimpleMemberAccessExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :213 :38) // Weaver.CurrentAssembly.MainModule (SimpleMemberAccessExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :214 :45) // Not a variable of known type: module
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :214 :68) // typeof(NetworkReaderExtensions) (TypeOfExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :214 :45) // module.ImportReference(typeof(NetworkReaderExtensions)) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Resolvers
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :215 :65) // Not a variable of known type: readerExtensions
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Weaver
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :215 :83) // Weaver.CurrentAssembly (SimpleMemberAccessExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :215 :107) // Not a variable of known type: readerFunction
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :215 :41) // Resolvers.ResolveMethod(readerExtensions, Weaver.CurrentAssembly, readerFunction) (InvocationExpression)
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :217 :72) // Not a variable of known type: listReader
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :217 :46) // new GenericInstanceMethod(listReader) (ObjectCreationExpression)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :218 :12) // Not a variable of known type: methodRef
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :218 :12) // methodRef.GenericArguments (SimpleMemberAccessExpression)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :218 :43) // Not a variable of known type: elementType
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :218 :12) // methodRef.GenericArguments.Add(elementType) (InvocationExpression)
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :223 :33) // Not a variable of known type: readerFunc
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :223 :33) // readerFunc.Body (SimpleMemberAccessExpression)
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :223 :33) // readerFunc.Body.GetILProcessor() (InvocationExpression)
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :224 :12) // Not a variable of known type: worker
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :224 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :224 :40) // OpCodes.Ldarg_0 (SimpleMemberAccessExpression)
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :224 :26) // worker.Create(OpCodes.Ldarg_0) (InvocationExpression)
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :224 :12) // worker.Append(worker.Create(OpCodes.Ldarg_0)) (InvocationExpression)
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :225 :12) // Not a variable of known type: worker
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :225 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :225 :40) // OpCodes.Call (SimpleMemberAccessExpression)
%39 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :225 :54) // Not a variable of known type: methodRef
%40 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :225 :26) // worker.Create(OpCodes.Call, methodRef) (InvocationExpression)
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :225 :12) // worker.Append(worker.Create(OpCodes.Call, methodRef)) (InvocationExpression)
%42 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :227 :12) // Not a variable of known type: worker
%43 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :227 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%44 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :227 :40) // OpCodes.Ret (SimpleMemberAccessExpression)
%45 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :227 :26) // worker.Create(OpCodes.Ret) (InvocationExpression)
%46 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :227 :12) // worker.Append(worker.Create(OpCodes.Ret)) (InvocationExpression)
%47 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :229 :19) // Not a variable of known type: readerFunc
return %47 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :229 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.Weaver.Readers.GenerateClassOrStructReadFunction$Mono.CecilX.TypeReference$(none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :232 :8) {
^entry (%_variable : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :232 :66)
cbde.store %_variable, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :232 :66)
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GenerateReaderFunction
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :234 :65) // Not a variable of known type: variable
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :234 :42) // GenerateReaderFunction(variable) (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :237 :12) // Not a variable of known type: readerFunc
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :237 :12) // readerFunc.Body (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :237 :12) // readerFunc.Body.Variables (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :237 :65) // Not a variable of known type: variable
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :237 :42) // new VariableDefinition(variable) (ObjectCreationExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :237 :12) // readerFunc.Body.Variables.Add(new VariableDefinition(variable)) (InvocationExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :239 :33) // Not a variable of known type: readerFunc
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :239 :33) // readerFunc.Body (SimpleMemberAccessExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :239 :33) // readerFunc.Body.GetILProcessor() (InvocationExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :241 :32) // Not a variable of known type: variable
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :241 :32) // variable.Resolve() (InvocationExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :243 :17) // Not a variable of known type: td
%18 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :243 :17) // td.IsValueType (SimpleMemberAccessExpression)
%19 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :243 :16) // !td.IsValueType (LogicalNotExpression)
cond_br %19, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :243 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GenerateNullCheck
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :244 :34) // Not a variable of known type: worker
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :244 :16) // GenerateNullCheck(worker) (InvocationExpression)
br ^2

^2: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CreateNew
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :246 :22) // Not a variable of known type: variable
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :246 :32) // Not a variable of known type: worker
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :246 :40) // Not a variable of known type: td
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :246 :12) // CreateNew(variable, worker, td) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ReadAllFields
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :247 :26) // Not a variable of known type: variable
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :247 :36) // Not a variable of known type: worker
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :247 :12) // ReadAllFields(variable, worker) (InvocationExpression)
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :249 :12) // Not a variable of known type: worker
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :249 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :249 :40) // OpCodes.Ldloc_0 (SimpleMemberAccessExpression)
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :249 :26) // worker.Create(OpCodes.Ldloc_0) (InvocationExpression)
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :249 :12) // worker.Append(worker.Create(OpCodes.Ldloc_0)) (InvocationExpression)
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :250 :12) // Not a variable of known type: worker
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :250 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :250 :40) // OpCodes.Ret (SimpleMemberAccessExpression)
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :250 :26) // worker.Create(OpCodes.Ret) (InvocationExpression)
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :250 :12) // worker.Append(worker.Create(OpCodes.Ret)) (InvocationExpression)
%39 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :251 :19) // Not a variable of known type: readerFunc
return %39 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :251 :12)

^3: // ExitBlock
cbde.unreachable

}
func @_Mirror.Weaver.Readers.GenerateNullCheck$Mono.CecilX.Cil.ILProcessor$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :254 :8) {
^entry (%_worker : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :254 :46)
cbde.store %_worker, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :254 :46)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :259 :12) // Not a variable of known type: worker
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :259 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :259 :40) // OpCodes.Ldarg_0 (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :259 :26) // worker.Create(OpCodes.Ldarg_0) (InvocationExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :259 :12) // worker.Append(worker.Create(OpCodes.Ldarg_0)) (InvocationExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :260 :12) // Not a variable of known type: worker
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :260 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :260 :40) // OpCodes.Call (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetReadFunc
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WeaverTypes
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :260 :66) // WeaverTypes.Import<bool>() (InvocationExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :260 :54) // GetReadFunc(WeaverTypes.Import<bool>()) (InvocationExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :260 :26) // worker.Create(OpCodes.Call, GetReadFunc(WeaverTypes.Import<bool>())) (InvocationExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :260 :12) // worker.Append(worker.Create(OpCodes.Call, GetReadFunc(WeaverTypes.Import<bool>()))) (InvocationExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :262 :42) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :262 :56) // OpCodes.Nop (SimpleMemberAccessExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :262 :42) // worker.Create(OpCodes.Nop) (InvocationExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :263 :12) // Not a variable of known type: worker
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :263 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :263 :40) // OpCodes.Brtrue (SimpleMemberAccessExpression)
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :263 :56) // Not a variable of known type: labelEmptyArray
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :263 :26) // worker.Create(OpCodes.Brtrue, labelEmptyArray) (InvocationExpression)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :263 :12) // worker.Append(worker.Create(OpCodes.Brtrue, labelEmptyArray)) (InvocationExpression)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :265 :12) // Not a variable of known type: worker
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :265 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :265 :40) // OpCodes.Ldnull (SimpleMemberAccessExpression)
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :265 :26) // worker.Create(OpCodes.Ldnull) (InvocationExpression)
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :265 :12) // worker.Append(worker.Create(OpCodes.Ldnull)) (InvocationExpression)
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :266 :12) // Not a variable of known type: worker
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :266 :26) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :266 :40) // OpCodes.Ret (SimpleMemberAccessExpression)
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :266 :26) // worker.Create(OpCodes.Ret) (InvocationExpression)
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :266 :12) // worker.Append(worker.Create(OpCodes.Ret)) (InvocationExpression)
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :267 :12) // Not a variable of known type: worker
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :267 :26) // Not a variable of known type: labelEmptyArray
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :267 :12) // worker.Append(labelEmptyArray) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Weaver.Readers.CreateNew$Mono.CecilX.TypeReference.Mono.CecilX.Cil.ILProcessor.Mono.CecilX.TypeDefinition$(none, none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :271 :8) {
^entry (%_variable : none, %_worker : none, %_td : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :271 :30)
cbde.store %_variable, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :271 :30)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :271 :54)
cbde.store %_worker, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :271 :54)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :271 :74)
cbde.store %_td, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :271 :74)
br ^0

^0: // BinaryBranchBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :273 :16) // Not a variable of known type: variable
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :273 :16) // variable.IsValueType (SimpleMemberAccessExpression)
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :273 :16)

^1: // SimpleBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :276 :16) // Not a variable of known type: worker
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :276 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :276 :44) // OpCodes.Ldloca (SimpleMemberAccessExpression)
%8 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :276 :60)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :276 :30) // worker.Create(OpCodes.Ldloca, 0) (InvocationExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :276 :16) // worker.Append(worker.Create(OpCodes.Ldloca, 0)) (InvocationExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :277 :16) // Not a variable of known type: worker
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :277 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :277 :44) // OpCodes.Initobj (SimpleMemberAccessExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :277 :61) // Not a variable of known type: variable
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :277 :30) // worker.Create(OpCodes.Initobj, variable) (InvocationExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :277 :16) // worker.Append(worker.Create(OpCodes.Initobj, variable)) (InvocationExpression)
br ^3

^2: // BinaryBranchBlock
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :279 :21) // Not a variable of known type: td
%18 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :279 :21) // td.IsDerivedFrom<UnityEngine.ScriptableObject>() (InvocationExpression)
cond_br %18, ^4, ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :279 :21)

^4: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WeaverTypes
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :281 :88) // WeaverTypes.ScriptableObjectCreateInstanceMethod (SimpleMemberAccessExpression)
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :281 :62) // new GenericInstanceMethod(WeaverTypes.ScriptableObjectCreateInstanceMethod) (ObjectCreationExpression)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :282 :16) // Not a variable of known type: genericInstanceMethod
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :282 :16) // genericInstanceMethod.GenericArguments (SimpleMemberAccessExpression)
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :282 :59) // Not a variable of known type: variable
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :282 :16) // genericInstanceMethod.GenericArguments.Add(variable) (InvocationExpression)
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :283 :16) // Not a variable of known type: worker
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :283 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :283 :44) // OpCodes.Call (SimpleMemberAccessExpression)
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :283 :58) // Not a variable of known type: genericInstanceMethod
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :283 :30) // worker.Create(OpCodes.Call, genericInstanceMethod) (InvocationExpression)
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :283 :16) // worker.Append(worker.Create(OpCodes.Call, genericInstanceMethod)) (InvocationExpression)
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :284 :16) // Not a variable of known type: worker
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :284 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :284 :44) // OpCodes.Stloc_0 (SimpleMemberAccessExpression)
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :284 :30) // worker.Create(OpCodes.Stloc_0) (InvocationExpression)
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :284 :16) // worker.Append(worker.Create(OpCodes.Stloc_0)) (InvocationExpression)
br ^3

^5: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Resolvers
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :289 :75) // Not a variable of known type: variable
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :289 :40) // Resolvers.ResolveDefaultPublicCtor(variable) (InvocationExpression)
%40 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :290 :20) // Not a variable of known type: ctor
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :290 :28) // null (NullLiteralExpression)
%42 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :290 :20) // comparison of unknown type: ctor == null
cond_br %42, ^6, ^7 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :290 :20)

^6: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Weaver
%43 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :292 :36) // Not a variable of known type: variable
%44 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :292 :36) // variable.Name (SimpleMemberAccessExpression)
%45 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :292 :33) // $"{variable.Name} can't be deserialized because it has no default constructor" (InterpolatedStringExpression)
%46 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :292 :113) // Not a variable of known type: variable
%47 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :292 :20) // Weaver.Error($"{variable.Name} can't be deserialized because it has no default constructor", variable) (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :293 :20)

^7: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Weaver
%48 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :296 :42) // Weaver.CurrentAssembly (SimpleMemberAccessExpression)
%49 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :296 :42) // Weaver.CurrentAssembly.MainModule (SimpleMemberAccessExpression)
%50 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :296 :92) // Not a variable of known type: ctor
%51 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :296 :42) // Weaver.CurrentAssembly.MainModule.ImportReference(ctor) (InvocationExpression)
%53 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :298 :16) // Not a variable of known type: worker
%54 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :298 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%55 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :298 :44) // OpCodes.Newobj (SimpleMemberAccessExpression)
%56 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :298 :60) // Not a variable of known type: ctorRef
%57 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :298 :30) // worker.Create(OpCodes.Newobj, ctorRef) (InvocationExpression)
%58 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :298 :16) // worker.Append(worker.Create(OpCodes.Newobj, ctorRef)) (InvocationExpression)
%59 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :299 :16) // Not a variable of known type: worker
%60 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :299 :30) // Not a variable of known type: worker
// Entity from another assembly: OpCodes
%61 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :299 :44) // OpCodes.Stloc_0 (SimpleMemberAccessExpression)
%62 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :299 :30) // worker.Create(OpCodes.Stloc_0) (InvocationExpression)
%63 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Weaver\\Readers.cs" :299 :16) // worker.Append(worker.Create(OpCodes.Stloc_0)) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function ReadAllFields(none, none), it contains poisonous unsupported syntaxes

// Skipping function InitializeReaders(none), it contains poisonous unsupported syntaxes

