func @_CollectibleMulti.SpawnCol$int.int$(i32, i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :23 :4) {
^entry (%_oldVersion : i32, %_i : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :23 :26)
cbde.store %_oldVersion, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :23 :26)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :23 :42)
cbde.store %_i, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :23 :42)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :25 :18)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :26 :13) // Not a variable of known type: spriteRenderer
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :26 :12) // !spriteRenderer (LogicalNotExpression)
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :26 :12)

^1: // SimpleBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :26 :48) // GetComponent<SpriteRenderer>() (InvocationExpression)
br ^2

^2: // SimpleBlock
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :28 :8) // Not a variable of known type: spriteRenderer
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :28 :8) // spriteRenderer.sprite (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :28 :32) // Not a variable of known type: graphicsArr
%9 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :28 :44) // Not a variable of known type: version
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :28 :32) // graphicsArr[version] (ElementAccessExpression)
// Entity from another assembly: Debug
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :29 :18) // "Power spawned: " (StringLiteralExpression)
%12 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :29 :38) // Not a variable of known type: version
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :29 :18) // Binary expression on unsupported types "Power spawned: " + version
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :29 :8) // Debug.Log("Power spawned: " + version) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_CollectibleMulti.OnTriggerEnter2D$UnityEngine.Collider2D$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :31 :4) {
^entry (%_other : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :31 :34)
cbde.store %_other, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :31 :34)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :33 :13) // Not a variable of known type: other
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :33 :30) // "Player" (StringLiteralExpression)
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :33 :13) // other.CompareTag("Player") (InvocationExpression)
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :33 :12) // !other.CompareTag("Player") (LogicalNotExpression)
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :33 :12)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :33 :41)

^2: // SimpleBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :35 :8) // Not a variable of known type: other
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :35 :26) // "GetBonus" (StringLiteralExpression)
%7 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :35 :38) // Not a variable of known type: version
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :35 :8) // other.SendMessage("GetBonus", version) (InvocationExpression)
// Entity from another assembly: Destroy
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :36 :16) // Identifier from another assembly: gameObject
%10 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :36 :28) // 0.1f (NumericLiteralExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\CollectibleMulti.cs" :36 :8) // Destroy(gameObject, 0.1f) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
