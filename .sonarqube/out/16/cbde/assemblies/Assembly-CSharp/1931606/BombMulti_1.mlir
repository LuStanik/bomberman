func @_BombMulti.Start$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :13 :4) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :15 :19) // Not a variable of known type: bomb
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :15 :19) // bomb.transform (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :15 :19) // bomb.transform.position (SimpleMemberAccessExpression)
// Entity from another assembly: Invoke
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :16 :15) // nameof(Collision) (InvocationExpression)
%4 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :16 :34) // 0.5f (NumericLiteralExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :16 :8) // Invoke(nameof(Collision), 0.5f) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_BombMulti.Explode$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :20 :4) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :24 :13) // Identifier from another assembly: isServer
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :24 :12) // !isServer (LogicalNotExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :24 :12)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :24 :25)

^2: // SimpleBlock
// Entity from another assembly: Debug
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :25 :18) // "Wszedlem" (StringLiteralExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :25 :8) // Debug.Log("Wszedlem") (InvocationExpression)
%4 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :26 :19) // true
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Collision
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :27 :8) // Collision() (InvocationExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :28 :8) // Identifier from another assembly: gameObject
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :28 :8) // gameObject.GetComponent<SpriteRenderer>() (InvocationExpression)
%8 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :28 :8) // gameObject.GetComponent<SpriteRenderer>().enabled (SimpleMemberAccessExpression)
%9 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :28 :60) // false
// Entity from another assembly: Destroy
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :29 :16) // Identifier from another assembly: gameObject
%11 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :29 :28) // .2f (NumericLiteralExpression)
%12 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :29 :34) // Not a variable of known type: power
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :29 :28) // Binary expression on unsupported types .2f * power
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :29 :8) // Destroy(gameObject, .2f * power) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CmdExplode
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :30 :19) // Not a variable of known type: position
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :30 :8) // CmdExplode(position) (InvocationExpression)
// Entity from another assembly: StartCoroutine
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CreateExplosion
// Entity from another assembly: Vector2
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :31 :39) // Vector2.right (SimpleMemberAccessExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :31 :23) // CreateExplosion(Vector2.right) (InvocationExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :31 :8) // StartCoroutine(CreateExplosion(Vector2.right)) (InvocationExpression)
// Entity from another assembly: StartCoroutine
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CreateExplosion
// Entity from another assembly: Vector2
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :32 :39) // Vector2.left (SimpleMemberAccessExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :32 :23) // CreateExplosion(Vector2.left) (InvocationExpression)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :32 :8) // StartCoroutine(CreateExplosion(Vector2.left)) (InvocationExpression)
// Entity from another assembly: StartCoroutine
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CreateExplosion
// Entity from another assembly: Vector2
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :33 :39) // Vector2.up (SimpleMemberAccessExpression)
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :33 :23) // CreateExplosion(Vector2.up) (InvocationExpression)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :33 :8) // StartCoroutine(CreateExplosion(Vector2.up)) (InvocationExpression)
// Entity from another assembly: StartCoroutine
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CreateExplosion
// Entity from another assembly: Vector2
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :34 :39) // Vector2.down (SimpleMemberAccessExpression)
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :34 :23) // CreateExplosion(Vector2.down) (InvocationExpression)
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :34 :8) // StartCoroutine(CreateExplosion(Vector2.down)) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_BombMulti.CmdExplode$UnityEngine.Vector3$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :38 :4) {
^entry (%_pos : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :38 :27)
cbde.store %_pos, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :38 :27)
br ^0

^0: // SimpleBlock
// Entity from another assembly: Instantiate
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :40 :45) // Not a variable of known type: explosion
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :40 :56) // Not a variable of known type: pos
// Entity from another assembly: Quaternion
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :40 :61) // Quaternion.identity (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :40 :33) // Instantiate(explosion, pos, Quaternion.identity) (InvocationExpression)
// Entity from another assembly: NetworkServer
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :41 :28) // Not a variable of known type: newExplode
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :41 :40) // Identifier from another assembly: connectionToClient
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :41 :8) // NetworkServer.Spawn(newExplode, connectionToClient) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function CreateExplosion(none), it contains poisonous unsupported syntaxes

func @_BombMulti.CmdExplosion$UnityEngine.Vector3$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :68 :4) {
^entry (%_pos : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :68 :29)
cbde.store %_pos, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :68 :29)
br ^0

^0: // SimpleBlock
// Entity from another assembly: Instantiate
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :70 :47) // Not a variable of known type: explosion
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :70 :58) // Not a variable of known type: pos
// Entity from another assembly: Quaternion
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :70 :63) // Quaternion.identity (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :70 :35) // Instantiate(explosion, pos, Quaternion.identity) (InvocationExpression)
// Entity from another assembly: NetworkServer
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :71 :28) // Not a variable of known type: newExplosion
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :71 :42) // Identifier from another assembly: connectionToClient
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :71 :8) // NetworkServer.Spawn(newExplosion, connectionToClient) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_BombMulti.Collision$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :74 :4) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :76 :8) // Identifier from another assembly: gameObject
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :76 :8) // gameObject.GetComponent<BoxCollider2D>() (InvocationExpression)
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :76 :8) // gameObject.GetComponent<BoxCollider2D>().enabled (SimpleMemberAccessExpression)
%3 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :76 :59) // true
br ^1

^1: // ExitBlock
return

}
func @_BombMulti.Boom$int$(i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :79 :4) {
^entry (%_pow : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :79 :21)
cbde.store %_pow, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :79 :21)
br ^0

^0: // SimpleBlock
%1 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :81 :16)
// Entity from another assembly: Debug
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :82 :18) // "Power: " (StringLiteralExpression)
%3 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :82 :30) // Not a variable of known type: power
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :82 :18) // Binary expression on unsupported types "Power: " + power
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :82 :8) // Debug.Log("Power: " + power) (InvocationExpression)
// Entity from another assembly: Invoke
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :84 :15) // nameof(Explode) (InvocationExpression)
%7 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :84 :32) // 3f (NumericLiteralExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :84 :8) // Invoke(nameof(Explode), 3f) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function OnTriggerEnter2D(none), it contains poisonous unsupported syntaxes

func @_BombMulti.GetPower$$() -> i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :97 :4) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :99 :15) // Not a variable of known type: power
return %0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\BombMulti.cs" :99 :8)

^1: // ExitBlock
cbde.unreachable

}
