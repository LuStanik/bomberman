func @_TutorialInfo.Awake$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :31 :1) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :34 :5) // Not a variable of known type: alreadyShownThisSession
cond_br %0, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :34 :5)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: StartGame
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :36 :3) // StartGame() (InvocationExpression)
br ^3

^2: // BinaryBranchBlock
%2 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :40 :29) // true
// Entity from another assembly: PlayerPrefs
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :43 :26) // Not a variable of known type: showAtStartPrefsKey
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :43 :7) // PlayerPrefs.HasKey(showAtStartPrefsKey) (InvocationExpression)
cond_br %4, ^4, ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :43 :7)

^4: // SimpleBlock
// Entity from another assembly: PlayerPrefs
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :45 :37) // Not a variable of known type: showAtStartPrefsKey
%6 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :45 :18) // PlayerPrefs.GetInt(showAtStartPrefsKey) (InvocationExpression)
%7 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :45 :61)
%8 = cmpi "eq", %6, %7 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :45 :18)
br ^5

^5: // BinaryBranchBlock
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :49 :3) // Not a variable of known type: showAtStartToggle
%10 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :49 :3) // showAtStartToggle.isOn (SimpleMemberAccessExpression)
%11 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :49 :28) // Not a variable of known type: showAtStart
%12 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :52 :7) // Not a variable of known type: showAtStart
cond_br %12, ^6, ^7 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :52 :7)

^6: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ShowLaunchScreen
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :54 :4) // ShowLaunchScreen() (InvocationExpression)
br ^3

^7: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: StartGame
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :58 :4) // StartGame () (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_TutorialInfo.ShowLaunchScreen$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :65 :1) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: Time
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :67 :2) // Time.timeScale (SimpleMemberAccessExpression)
%1 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :67 :19) // 0f (NumericLiteralExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :68 :2) // Not a variable of known type: mainListener
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :68 :2) // mainListener.enabled (SimpleMemberAccessExpression)
%4 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :68 :25) // false
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :69 :2) // Not a variable of known type: overlay
%6 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :69 :21) // true
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :69 :2) // overlay.SetActive (true) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_TutorialInfo.LaunchTutorial$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :73 :1) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: Application
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :75 :23) // Not a variable of known type: url
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :75 :2) // Application.OpenURL (url) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_TutorialInfo.StartGame$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :80 :1) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :82 :2) // Not a variable of known type: overlay
%1 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :82 :21) // false
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :82 :2) // overlay.SetActive (false) (InvocationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :83 :2) // Not a variable of known type: mainListener
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :83 :2) // mainListener.enabled (SimpleMemberAccessExpression)
%5 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :83 :25) // true
// Entity from another assembly: Time
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :84 :2) // Time.timeScale (SimpleMemberAccessExpression)
%7 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\TutorialInfo\\Scripts\\TutorialInfo.cs" :84 :19) // 1f (NumericLiteralExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function ToggleShowAtLaunch(), it contains poisonous unsupported syntaxes

