func @_Volume.SetVolume$float$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Volume.cs" :9 :4) {
^entry (%_value : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Volume.cs" :9 :26)
cbde.store %_value, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Volume.cs" :9 :26)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Volume.cs" :11 :8) // Not a variable of known type: audioMixer
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Volume.cs" :11 :28) // "Volume" (StringLiteralExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Volume.cs" :11 :38) // Not a variable of known type: value
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Volume.cs" :11 :8) // audioMixer.SetFloat("Volume", value) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
