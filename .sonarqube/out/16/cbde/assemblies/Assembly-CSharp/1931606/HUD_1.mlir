func @_HUD.Awake$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :25 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :27 :22) // GetComponent<Network>() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function OnGUI(), it contains poisonous unsupported syntaxes

func @_HUD.StartButtons$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :64 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: NetworkClient
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :66 :17) // NetworkClient.active (SimpleMemberAccessExpression)
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :66 :16) // !NetworkClient.active (LogicalNotExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :66 :16)

^1: // BinaryBranchBlock
// Entity from another assembly: GUILayout
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :68 :16) // GUILayout.BeginHorizontal() (InvocationExpression)
// Entity from another assembly: GUILayout
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :69 :16) // GUILayout.BeginVertical() (InvocationExpression)
// Entity from another assembly: Application
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :71 :20) // Application.platform (SimpleMemberAccessExpression)
// Entity from another assembly: RuntimePlatform
%5 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :71 :44) // RuntimePlatform.WebGLPlayer (SimpleMemberAccessExpression)
%6 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :71 :20) // comparison of unknown type: Application.platform != RuntimePlatform.WebGLPlayer
cond_br %6, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :71 :20)

^3: // BinaryBranchBlock
// Entity from another assembly: GUILayout
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :73 :41) // "Host (Server + Client)" (StringLiteralExpression)
// Entity from another assembly: GUILayout
%8 = constant 200 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :73 :83)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :73 :67) // GUILayout.Width(200) (InvocationExpression)
// Entity from another assembly: GUILayout
%10 = constant 30 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :73 :106)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :73 :89) // GUILayout.Height(30) (InvocationExpression)
%12 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :73 :24) // GUILayout.Button("Host (Server + Client)", GUILayout.Width(200), GUILayout.Height(30)) (InvocationExpression)
cond_br %12, ^5, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :73 :24)

^5: // SimpleBlock
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :75 :24) // Not a variable of known type: manager
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :75 :24) // manager.StartHost() (InvocationExpression)
br ^4

^4: // BinaryBranchBlock
// Entity from another assembly: Application
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :80 :20) // Application.platform (SimpleMemberAccessExpression)
// Entity from another assembly: RuntimePlatform
%16 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :80 :44) // RuntimePlatform.WebGLPlayer (SimpleMemberAccessExpression)
%17 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :80 :20) // comparison of unknown type: Application.platform == RuntimePlatform.WebGLPlayer
cond_br %17, ^6, ^7 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :80 :20)

^6: // SimpleBlock
// Entity from another assembly: GUILayout
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :83 :34) // "(  WebGL cannot be server  )" (StringLiteralExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :83 :20) // GUILayout.Box("(  WebGL cannot be server  )") (InvocationExpression)
br ^8

^7: // BinaryBranchBlock
// Entity from another assembly: GUILayout
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :87 :41) // "Server Only" (StringLiteralExpression)
// Entity from another assembly: GUILayout
%21 = constant 200 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :87 :72)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :87 :56) // GUILayout.Width(200) (InvocationExpression)
// Entity from another assembly: GUILayout
%23 = constant 30 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :87 :95)
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :87 :78) // GUILayout.Height(30) (InvocationExpression)
%25 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :87 :24) // GUILayout.Button("Server Only", GUILayout.Width(200), GUILayout.Height(30)) (InvocationExpression)
cond_br %25, ^9, ^8 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :87 :24)

^9: // SimpleBlock
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :87 :101) // Not a variable of known type: manager
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :87 :101) // manager.StartServer() (InvocationExpression)
br ^8

^8: // BinaryBranchBlock
// Entity from another assembly: GUILayout
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :89 :16) // GUILayout.EndVertical() (InvocationExpression)
// Entity from another assembly: GUILayout
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :92 :16) // GUILayout.BeginVertical() (InvocationExpression)
// Entity from another assembly: GUILayout
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :93 :37) // "Client" (StringLiteralExpression)
// Entity from another assembly: GUILayout
%31 = constant 200 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :93 :63)
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :93 :47) // GUILayout.Width(200) (InvocationExpression)
// Entity from another assembly: GUILayout
%33 = constant 30 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :93 :86)
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :93 :69) // GUILayout.Height(30) (InvocationExpression)
%35 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :93 :20) // GUILayout.Button("Client", GUILayout.Width(200), GUILayout.Height(30)) (InvocationExpression)
cond_br %35, ^10, ^11 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :93 :20)

^10: // SimpleBlock
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :95 :20) // Not a variable of known type: manager
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :95 :20) // manager.StartClient() (InvocationExpression)
br ^11

^11: // BinaryBranchBlock
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :97 :16) // Not a variable of known type: manager
%39 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :97 :16) // manager.networkAddress (SimpleMemberAccessExpression)
// Entity from another assembly: GUILayout
%40 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :97 :61) // Not a variable of known type: manager
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :97 :61) // manager.networkAddress (SimpleMemberAccessExpression)
// Entity from another assembly: GUILayout
%42 = constant 200 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :97 :101)
%43 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :97 :85) // GUILayout.Width(200) (InvocationExpression)
// Entity from another assembly: GUILayout
%44 = constant 30 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :97 :124)
%45 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :97 :107) // GUILayout.Height(30) (InvocationExpression)
%46 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :97 :41) // GUILayout.TextField(manager.networkAddress, GUILayout.Width(200), GUILayout.Height(30)) (InvocationExpression)
// Entity from another assembly: GUILayout
%47 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :98 :16) // GUILayout.EndVertical() (InvocationExpression)
// Entity from another assembly: GUILayout
%48 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :100 :37) // "Exit" (StringLiteralExpression)
// Entity from another assembly: GUILayout
%49 = constant 200 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :100 :61)
%50 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :100 :45) // GUILayout.Width(200) (InvocationExpression)
// Entity from another assembly: GUILayout
%51 = constant 60 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :100 :84)
%52 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :100 :67) // GUILayout.Height(60) (InvocationExpression)
%53 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :100 :20) // GUILayout.Button("Exit", GUILayout.Width(200), GUILayout.Height(60)) (InvocationExpression)
cond_br %53, ^12, ^13 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :100 :20)

^12: // SimpleBlock
// Entity from another assembly: SceneManager
%54 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :100 :113) // "Menu" (StringLiteralExpression)
%55 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :100 :90) // SceneManager.LoadScene("Menu") (InvocationExpression)
br ^13

^13: // SimpleBlock
// Entity from another assembly: GUILayout
%56 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :101 :16) // GUILayout.EndHorizontal() (InvocationExpression)
br ^14

^2: // BinaryBranchBlock
// Entity from another assembly: GUILayout
%57 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :106 :32) // "Connecting to " (StringLiteralExpression)
%58 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :106 :51) // Not a variable of known type: manager
%59 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :106 :51) // manager.networkAddress (SimpleMemberAccessExpression)
%60 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :106 :32) // Binary expression on unsupported types "Connecting to " + manager.networkAddress
%61 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :106 :76) // ".." (StringLiteralExpression)
%62 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :106 :32) // Binary expression on unsupported types "Connecting to " + manager.networkAddress + ".."
%63 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :106 :16) // GUILayout.Label("Connecting to " + manager.networkAddress + "..") (InvocationExpression)
// Entity from another assembly: GUILayout
%64 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :107 :37) // "Cancel Connection Attempt" (StringLiteralExpression)
%65 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :107 :20) // GUILayout.Button("Cancel Connection Attempt") (InvocationExpression)
cond_br %65, ^15, ^14 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :107 :20)

^15: // SimpleBlock
%66 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :109 :20) // Not a variable of known type: manager
%67 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :109 :20) // manager.StopClient() (InvocationExpression)
// Entity from another assembly: SceneManager
%68 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :110 :43) // "MultiGame" (StringLiteralExpression)
%69 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :110 :20) // SceneManager.LoadScene("MultiGame") (InvocationExpression)
br ^14

^14: // ExitBlock
return

}
func @_HUD.StatusLabels$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :115 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: GUILayout
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :117 :12) // GUILayout.BeginHorizontal() (InvocationExpression)
// Entity from another assembly: NetworkServer
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :119 :16) // NetworkServer.active (SimpleMemberAccessExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :119 :16)

^1: // SimpleBlock
// Entity from another assembly: GUILayout
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :121 :32) // "Server: active. Transport: " (StringLiteralExpression)
// Entity from another assembly: Transport
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :121 :64) // Transport.activeTransport (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :121 :32) // Binary expression on unsupported types "Server: active. Transport: " + Transport.activeTransport
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :121 :16) // GUILayout.Label("Server: active. Transport: " + Transport.activeTransport) (InvocationExpression)
br ^2

^2: // BinaryBranchBlock
// Entity from another assembly: NetworkClient
%6 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :123 :16) // NetworkClient.isConnected (SimpleMemberAccessExpression)
cond_br %6, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :123 :16)

^3: // SimpleBlock
// Entity from another assembly: GUILayout
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :125 :32) // "Client: address=" (StringLiteralExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :125 :53) // Not a variable of known type: manager
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :125 :53) // manager.networkAddress (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :125 :32) // Binary expression on unsupported types "Client: address=" + manager.networkAddress
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :125 :16) // GUILayout.Label("Client: address=" + manager.networkAddress) (InvocationExpression)
br ^4

^4: // SimpleBlock
// Entity from another assembly: GUILayout
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\HUD.cs" :127 :12) // GUILayout.EndHorizontal() (InvocationExpression)
br ^5

^5: // ExitBlock
return

}
// Skipping function StopButtons(), it contains poisonous unsupported syntaxes

