func @_Mirror.Authenticators.BasicAuthenticator.OnStartServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :40 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: NetworkServer
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnAuthRequestMessage
%0 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :43 :84) // false
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :43 :12) // NetworkServer.RegisterHandler<AuthRequestMessage>(OnAuthRequestMessage, false) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Authenticators.BasicAuthenticator.OnStopServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :50 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: NetworkServer
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :53 :12) // NetworkServer.UnregisterHandler<AuthRequestMessage>() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Authenticators.BasicAuthenticator.OnServerAuthenticate$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :60 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :60 :50)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :60 :50)
br ^0

^0: // ExitBlock
return

}
// Skipping function OnAuthRequestMessage(none, none), it contains poisonous unsupported syntaxes

// Skipping function DelayedDisconnect(none, none), it contains poisonous unsupported syntaxes

func @_Mirror.Authenticators.BasicAuthenticator.OnStartClient$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :124 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: NetworkClient
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnAuthResponseMessage
%0 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :127 :86) // false
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :127 :12) // NetworkClient.RegisterHandler<AuthResponseMessage>(OnAuthResponseMessage, false) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Authenticators.BasicAuthenticator.OnStopClient$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :134 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: NetworkClient
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :137 :12) // NetworkClient.UnregisterHandler<AuthResponseMessage>() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Authenticators.BasicAuthenticator.OnClientAuthenticate$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :144 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :144 :50)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :144 :50)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :146 :52) // new AuthRequestMessage             {                 authUsername = username,                 authPassword = password             } (ObjectCreationExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :148 :31) // Not a variable of known type: username
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :149 :31) // Not a variable of known type: password
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :152 :12) // Not a variable of known type: conn
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :152 :22) // Not a variable of known type: authRequestMessage
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :152 :12) // conn.Send(authRequestMessage) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Authenticators.BasicAuthenticator.OnAuthResponseMessage$Mirror.NetworkConnection.Mirror.Authenticators.BasicAuthenticator.AuthResponseMessage$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :160 :8) {
^entry (%_conn : none, %_msg : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :160 :42)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :160 :42)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :160 :66)
cbde.store %_msg, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :160 :66)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :162 :16) // Not a variable of known type: msg
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :162 :16) // msg.code (SimpleMemberAccessExpression)
%4 = constant 100 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :162 :28)
%5 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :162 :16) // comparison of unknown type: msg.code == 100
cond_br %5, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :162 :16)

^1: // BinaryBranchBlock
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :164 :20) // Not a variable of known type: logger
%7 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :164 :20) // logger.LogEnabled() (InvocationExpression)
cond_br %7, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :164 :20)

^3: // SimpleBlock
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :164 :41) // Not a variable of known type: logger
// Entity from another assembly: LogType
%9 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :164 :58) // LogType.Log (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :164 :71) // "Authentication Response: {0}" (StringLiteralExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :164 :103) // Not a variable of known type: msg
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :164 :103) // msg.message (SimpleMemberAccessExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :164 :41) // logger.LogFormat(LogType.Log, "Authentication Response: {0}", msg.message) (InvocationExpression)
br ^4

^4: // SimpleBlock
// Entity from another assembly: ClientAccept
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :167 :29) // Not a variable of known type: conn
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :167 :16) // ClientAccept(conn) (InvocationExpression)
br ^5

^2: // SimpleBlock
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :171 :16) // Not a variable of known type: logger
// Entity from another assembly: LogType
%17 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :171 :33) // LogType.Error (SimpleMemberAccessExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :171 :48) // "Authentication Response: {0}" (StringLiteralExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :171 :80) // Not a variable of known type: msg
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :171 :80) // msg.message (SimpleMemberAccessExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :171 :16) // logger.LogFormat(LogType.Error, "Authentication Response: {0}", msg.message) (InvocationExpression)
// Entity from another assembly: ClientReject
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :174 :29) // Not a variable of known type: conn
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\BasicAuthenticator.cs" :174 :16) // ClientReject(conn) (InvocationExpression)
br ^5

^5: // ExitBlock
return

}
