// Skipping function Awake(), it contains poisonous unsupported syntaxes

func @_Mirror.Authenticators.TimeoutAuthenticator.OnStartServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :25 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :27 :12) // Not a variable of known type: authenticator
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :27 :12) // authenticator.OnStartServer() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Authenticators.TimeoutAuthenticator.OnStopServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :30 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :32 :12) // Not a variable of known type: authenticator
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :32 :12) // authenticator.OnStopServer() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Authenticators.TimeoutAuthenticator.OnStartClient$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :35 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :37 :12) // Not a variable of known type: authenticator
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :37 :12) // authenticator.OnStartClient() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Authenticators.TimeoutAuthenticator.OnStopClient$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :40 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :42 :12) // Not a variable of known type: authenticator
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :42 :12) // authenticator.OnStopClient() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Authenticators.TimeoutAuthenticator.OnServerAuthenticate$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :45 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :45 :50)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :45 :50)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :47 :12) // Not a variable of known type: authenticator
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :47 :47) // Not a variable of known type: conn
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :47 :12) // authenticator.OnServerAuthenticate(conn) (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :48 :16) // Not a variable of known type: timeout
%5 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :48 :26)
%6 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :48 :16) // comparison of unknown type: timeout > 0
cond_br %6, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :48 :16)

^1: // SimpleBlock
// Entity from another assembly: StartCoroutine
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: BeginAuthentication
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :49 :51) // Not a variable of known type: conn
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :49 :31) // BeginAuthentication(conn) (InvocationExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :49 :16) // StartCoroutine(BeginAuthentication(conn)) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.Authenticators.TimeoutAuthenticator.OnClientAuthenticate$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :52 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :52 :50)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :52 :50)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :54 :12) // Not a variable of known type: authenticator
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :54 :47) // Not a variable of known type: conn
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :54 :12) // authenticator.OnClientAuthenticate(conn) (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :55 :16) // Not a variable of known type: timeout
%5 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :55 :26)
%6 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :55 :16) // comparison of unknown type: timeout > 0
cond_br %6, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :55 :16)

^1: // SimpleBlock
// Entity from another assembly: StartCoroutine
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: BeginAuthentication
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :56 :51) // Not a variable of known type: conn
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :56 :31) // BeginAuthentication(conn) (InvocationExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Authenticators\\TimeoutAuthenticator.cs" :56 :16) // StartCoroutine(BeginAuthentication(conn)) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
// Skipping function BeginAuthentication(none), it contains poisonous unsupported syntaxes

