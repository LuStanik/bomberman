func @_Mirror.EnterPlayModeSettingsCheck.OnInitializeOnLoad$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :11 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CheckPlayModeOptions
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :17 :12) // CheckPlayModeOptions() (InvocationExpression)
// Entity from another assembly: EditorApplication
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :22 :12) // EditorApplication.playModeStateChanged (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnPlayModeStateChanged
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :22 :12) // Binary expression on unsupported types EditorApplication.playModeStateChanged += OnPlayModeStateChanged
// No identifier name for binary assignment expression
br ^1

^1: // ExitBlock
return

}
func @_Mirror.EnterPlayModeSettingsCheck.OnPlayModeStateChanged$UnityEditor.PlayModeStateChange$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :25 :8) {
^entry (%_state : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :25 :43)
cbde.store %_state, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :25 :43)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :29 :16) // Not a variable of known type: state
// Entity from another assembly: PlayModeStateChange
%2 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :29 :25) // PlayModeStateChange.ExitingEditMode (SimpleMemberAccessExpression)
%3 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :29 :16) // comparison of unknown type: state == PlayModeStateChange.ExitingEditMode
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :29 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CheckSuccessfulWeave
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :31 :16) // CheckSuccessfulWeave() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CheckPlayModeOptions
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :36 :16) // CheckPlayModeOptions() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.EnterPlayModeSettingsCheck.CheckSuccessfulWeave$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :41 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: SessionState
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :44 :38) // "MIRROR_WEAVE_SUCCESS" (StringLiteralExpression)
%1 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :44 :62) // false
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :44 :17) // SessionState.GetBool("MIRROR_WEAVE_SUCCESS", false) (InvocationExpression)
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :44 :16) // !SessionState.GetBool("MIRROR_WEAVE_SUCCESS", false) (LogicalNotExpression)
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :44 :16)

^1: // BinaryBranchBlock
// Entity from another assembly: SessionState
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :48 :37) // "MIRROR_WEAVE_SUCCESS" (StringLiteralExpression)
%5 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :48 :61) // true
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :48 :16) // SessionState.SetBool("MIRROR_WEAVE_SUCCESS", true) (InvocationExpression)
// Entity from another assembly: Weaver
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :49 :16) // Weaver.CompilationFinishedHook (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :49 :16) // Weaver.CompilationFinishedHook.WeaveExistingAssemblies() (InvocationExpression)
// Entity from another assembly: SessionState
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :52 :42) // "MIRROR_WEAVE_SUCCESS" (StringLiteralExpression)
%10 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :52 :66) // false
%11 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :52 :21) // SessionState.GetBool("MIRROR_WEAVE_SUCCESS", false) (InvocationExpression)
%12 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :52 :20) // !SessionState.GetBool("MIRROR_WEAVE_SUCCESS", false) (LogicalNotExpression)
cond_br %12, ^3, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :52 :20)

^3: // SimpleBlock
// Entity from another assembly: Debug
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :55 :35) // "Can't enter play mode until weaver issues are resolved." (StringLiteralExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :55 :20) // Debug.LogError("Can't enter play mode until weaver issues are resolved.") (InvocationExpression)
// Entity from another assembly: EditorApplication
%15 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :56 :20) // EditorApplication.isPlaying (SimpleMemberAccessExpression)
%16 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :56 :50) // false
br ^2

^2: // ExitBlock
return

}
func @_Mirror.EnterPlayModeSettingsCheck.CheckPlayModeOptions$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :62 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: EditorSettings
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :65 :16) // EditorSettings.enterPlayModeOptionsEnabled (SimpleMemberAccessExpression)
cond_br %0, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :65 :16)

^1: // SimpleBlock
// Entity from another assembly: Debug
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :67 :31) // "Enter Play Mode Options are not supported by Mirror. Please disable 'ProjectSettings -> Editor -> Enter Play Mode Settings (Experimental)'." (StringLiteralExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :67 :16) // Debug.LogError("Enter Play Mode Options are not supported by Mirror. Please disable 'ProjectSettings -> Editor -> Enter Play Mode Settings (Experimental)'.") (InvocationExpression)
// Entity from another assembly: EditorApplication
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :68 :16) // EditorApplication.isPlaying (SimpleMemberAccessExpression)
%4 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\EnterPlayModeSettingsCheck.cs" :68 :46) // false
br ^2

^2: // ExitBlock
return

}
