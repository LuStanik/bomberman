func @_Mirror.EditorScripts.ScriptableObjectUtility.CreateAsset$T$$string$(none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :10 :8) {
^entry (%_defaultName : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :10 :39)
cbde.store %_defaultName, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :10 :39)
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SavePanel
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :12 :36) // Not a variable of known type: defaultName
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :12 :26) // SavePanel(defaultName) (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :14 :16) // string (PredefinedType)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :14 :37) // Not a variable of known type: path
%6 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :14 :16) // string.IsNullOrEmpty(path) (InvocationExpression)
cond_br %6, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :14 :16)

^1: // JumpBlock
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :14 :53) // null (NullLiteralExpression)
return %7 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :14 :46)

^2: // JumpBlock
// Entity from another assembly: ScriptableObject
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :16 :22) // ScriptableObject.CreateInstance<T>() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SaveAsset
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :18 :22) // Not a variable of known type: path
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :18 :28) // Not a variable of known type: asset
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :18 :12) // SaveAsset(path, asset) (InvocationExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :20 :19) // Not a variable of known type: asset
return %13 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :20 :12)

^3: // ExitBlock
cbde.unreachable

}
func @_Mirror.EditorScripts.ScriptableObjectUtility.SavePanel$string$(none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :23 :8) {
^entry (%_name : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :23 :32)
cbde.store %_name, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :23 :32)
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: EditorUtility
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :26 :27) // "Save ScriptableObject" (StringLiteralExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :27 :27) // "Assets/Mirror/" (StringLiteralExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :28 :27) // Not a variable of known type: name
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :28 :34) // ".asset" (StringLiteralExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :28 :27) // Binary expression on unsupported types name + ".asset"
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :29 :27) // "asset" (StringLiteralExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :25 :26) // EditorUtility.SaveFilePanel(                            "Save ScriptableObject",                            "Assets/Mirror/",                            name + ".asset",                            "asset") (InvocationExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :32 :16) // string (PredefinedType)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :32 :37) // Not a variable of known type: path
%11 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :32 :16) // string.IsNullOrEmpty(path) (InvocationExpression)
cond_br %11, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :32 :16)

^1: // JumpBlock
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :32 :53) // Not a variable of known type: path
return %12 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :32 :46)

^2: // BinaryBranchBlock
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :35 :16) // Not a variable of known type: path
// Entity from another assembly: Application
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :35 :32) // Application.dataPath (SimpleMemberAccessExpression)
%15 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :35 :16) // path.StartsWith(Application.dataPath) (InvocationExpression)
cond_br %15, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :35 :16)

^3: // SimpleBlock
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :37 :23) // "Assets" (StringLiteralExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :37 :34) // Not a variable of known type: path
// Entity from another assembly: Application
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :37 :49) // Application.dataPath (SimpleMemberAccessExpression)
%19 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :37 :49) // Application.dataPath.Length (SimpleMemberAccessExpression)
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :37 :34) // path.Substring(Application.dataPath.Length) (InvocationExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :37 :23) // Binary expression on unsupported types "Assets" + path.Substring(Application.dataPath.Length)
br ^4

^4: // JumpBlock
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :40 :19) // Not a variable of known type: path
return %22 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :40 :12)

^5: // ExitBlock
cbde.unreachable

}
func @_Mirror.EditorScripts.ScriptableObjectUtility.SaveAsset$string.UnityEngine.ScriptableObject$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :43 :8) {
^entry (%_path : none, %_asset : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :43 :30)
cbde.store %_path, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :43 :30)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :43 :43)
cbde.store %_asset, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :43 :43)
br ^0

^0: // SimpleBlock
// Entity from another assembly: AssetDatabase
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :45 :76) // Not a variable of known type: path
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :45 :38) // AssetDatabase.GenerateUniqueAssetPath(path) (InvocationExpression)
// Entity from another assembly: AssetDatabase
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :47 :38) // Not a variable of known type: asset
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :47 :45) // Not a variable of known type: assetPathAndName
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :47 :12) // AssetDatabase.CreateAsset(asset, assetPathAndName) (InvocationExpression)
// Entity from another assembly: AssetDatabase
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :49 :12) // AssetDatabase.SaveAssets() (InvocationExpression)
// Entity from another assembly: AssetDatabase
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\ScriptableObjectUtility.cs" :50 :12) // AssetDatabase.Refresh() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
