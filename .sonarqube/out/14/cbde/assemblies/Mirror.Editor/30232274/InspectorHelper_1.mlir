// Skipping function GetAllFields(none, none), it contains poisonous unsupported syntaxes

func @_Mirror.InspectorHelper.IsSyncVar$System.Reflection.FieldInfo$(none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :46 :8) {
^entry (%_field : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :46 :37)
cbde.store %_field, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :46 :37)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :48 :36) // Not a variable of known type: field
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :48 :62) // typeof(SyncVarAttribute) (TypeOfExpression)
%3 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :48 :88) // true
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :48 :36) // field.GetCustomAttributes(typeof(SyncVarAttribute), true) (InvocationExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :49 :19) // Not a variable of known type: fieldMarkers
%7 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :49 :19) // fieldMarkers.Length (SimpleMemberAccessExpression)
%8 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :49 :41)
%9 = cmpi "sgt", %7, %8 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :49 :19)
return %9 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :49 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.InspectorHelper.IsSerializeField$System.Reflection.FieldInfo$(none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :51 :8) {
^entry (%_field : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :51 :44)
cbde.store %_field, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :51 :44)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :53 :36) // Not a variable of known type: field
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :53 :62) // typeof(SerializeField) (TypeOfExpression)
%3 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :53 :86) // true
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :53 :36) // field.GetCustomAttributes(typeof(SerializeField), true) (InvocationExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :54 :19) // Not a variable of known type: fieldMarkers
%7 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :54 :19) // fieldMarkers.Length (SimpleMemberAccessExpression)
%8 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :54 :41)
%9 = cmpi "sgt", %7, %8 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :54 :19)
return %9 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :54 :12)

^1: // ExitBlock
cbde.unreachable

}
// Skipping function IsVisibleField(none), it contains poisonous unsupported syntaxes

func @_Mirror.InspectorHelper.IsSyncObject$System.Reflection.FieldInfo$(none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :61 :8) {
^entry (%_field : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :61 :40)
cbde.store %_field, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :61 :40)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :63 :19) // typeof(SyncObject) (TypeOfExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :63 :55) // Not a variable of known type: field
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :63 :55) // field.FieldType (SimpleMemberAccessExpression)
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :63 :19) // typeof(SyncObject).IsAssignableFrom(field.FieldType) (InvocationExpression)
return %4 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :63 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.InspectorHelper.HasShowInInspector$System.Reflection.FieldInfo$(none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :65 :8) {
^entry (%_field : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :65 :46)
cbde.store %_field, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :65 :46)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :67 :36) // Not a variable of known type: field
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :67 :62) // typeof(ShowInInspectorAttribute) (TypeOfExpression)
%3 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :67 :96) // true
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :67 :36) // field.GetCustomAttributes(typeof(ShowInInspectorAttribute), true) (InvocationExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :68 :19) // Not a variable of known type: fieldMarkers
%7 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :68 :19) // fieldMarkers.Length (SimpleMemberAccessExpression)
%8 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :68 :41)
%9 = cmpi "sgt", %7, %8 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :68 :19)
return %9 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\InspectorHelper.cs" :68 :12)

^1: // ExitBlock
cbde.unreachable

}
// Skipping function IsVisibleSyncObject(none), it contains poisonous unsupported syntaxes

