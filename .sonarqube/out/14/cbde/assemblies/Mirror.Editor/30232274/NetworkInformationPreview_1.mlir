func @_Mirror.NetworkInformationPreview.GetPreviewTitle$$() -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :64 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :66 :16) // Not a variable of known type: title
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :66 :25) // null (NullLiteralExpression)
%2 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :66 :16) // comparison of unknown type: title == null
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :66 :16)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :68 :39) // "Network Information" (StringLiteralExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :68 :24) // new GUIContent("Network Information") (ObjectCreationExpression)
br ^2

^2: // JumpBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :70 :19) // Not a variable of known type: title
return %5 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :70 :12)

^3: // ExitBlock
cbde.unreachable

}
// Skipping function HasPreviewGUI(), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkInformationPreview.OnPreviewGUI$UnityEngine.Rect.UnityEngine.GUIStyle$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :79 :8) {
^entry (%_r : none, %_background : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :79 :42)
cbde.store %_r, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :79 :42)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :79 :50)
cbde.store %_background, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :79 :50)
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: Event
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :81 :16) // Event.current (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :81 :16) // Event.current.type (SimpleMemberAccessExpression)
// Entity from another assembly: EventType
%4 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :81 :38) // EventType.Repaint (SimpleMemberAccessExpression)
%5 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :81 :16) // comparison of unknown type: Event.current.type != EventType.Repaint
cond_br %5, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :81 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :82 :16)

^2: // BinaryBranchBlock
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :84 :16) // Identifier from another assembly: target
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :84 :26) // null (NullLiteralExpression)
%8 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :84 :16) // comparison of unknown type: target == null
cond_br %8, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :84 :16)

^3: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :85 :16)

^4: // BinaryBranchBlock
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :87 :42) // Identifier from another assembly: target
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :87 :42) // target as GameObject (AsExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :89 :16) // Not a variable of known type: targetGameObject
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :89 :36) // null (NullLiteralExpression)
%14 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :89 :16) // comparison of unknown type: targetGameObject == null
cond_br %14, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :89 :16)

^5: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :90 :16)

^6: // BinaryBranchBlock
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :92 :39) // Not a variable of known type: targetGameObject
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :92 :39) // targetGameObject.GetComponent<NetworkIdentity>() (InvocationExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :94 :16) // Not a variable of known type: identity
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :94 :28) // null (NullLiteralExpression)
%20 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :94 :16) // comparison of unknown type: identity == null
cond_br %20, ^7, ^8 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :94 :16)

^7: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :95 :16)

^8: // BinaryBranchBlock
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :97 :16) // Not a variable of known type: styles
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :97 :26) // null (NullLiteralExpression)
%23 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :97 :16) // comparison of unknown type: styles == null
cond_br %23, ^9, ^10 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :97 :16)

^9: // SimpleBlock
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :98 :25) // new Styles() (ObjectCreationExpression)
br ^10

^10: // SimpleBlock
%25 = constant 5 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :102 :56)
%26 = cbde.neg %25 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :102 :55)
%27 = constant 5 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :102 :60)
%28 = cbde.neg %27 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :102 :59)
%29 = constant 5 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :102 :64)
%30 = cbde.neg %29 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :102 :63)
%31 = constant 5 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :102 :68)
%32 = cbde.neg %31 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :102 :67)
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :102 :40) // new RectOffset(-5, -5, -5, -5) (ObjectCreationExpression)
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :103 :27) // Not a variable of known type: previewPadding
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :103 :46) // Not a variable of known type: r
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :103 :27) // previewPadding.Add(r) (InvocationExpression)
%39 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :106 :29) // Not a variable of known type: paddedr
%40 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :106 :29) // paddedr.x (SimpleMemberAccessExpression)
%41 = constant 10 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :106 :41)
%42 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :106 :29) // Binary expression on unsupported types paddedr.x + 10
%44 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :107 :22) // Not a variable of known type: paddedr
%45 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :107 :22) // paddedr.y (SimpleMemberAccessExpression)
%46 = constant 10 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :107 :34)
%47 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :107 :22) // Binary expression on unsupported types paddedr.y + 10
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: DrawNetworkIdentityInfo
%49 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :109 :40) // Not a variable of known type: identity
%50 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :109 :50) // Not a variable of known type: initialX
%51 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :109 :60) // Not a variable of known type: Y
%52 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :109 :16) // DrawNetworkIdentityInfo(identity, initialX, Y) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: DrawNetworkBehaviors
%53 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :111 :37) // Not a variable of known type: identity
%54 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :111 :47) // Not a variable of known type: initialX
%55 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :111 :57) // Not a variable of known type: Y
%56 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :111 :16) // DrawNetworkBehaviors(identity, initialX, Y) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: DrawObservers
%57 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :113 :30) // Not a variable of known type: identity
%58 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :113 :40) // Not a variable of known type: initialX
%59 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :113 :50) // Not a variable of known type: Y
%60 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :113 :16) // DrawObservers(identity, initialX, Y) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: DrawOwner
%61 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :115 :26) // Not a variable of known type: identity
%62 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :115 :36) // Not a variable of known type: initialX
%63 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :115 :46) // Not a variable of known type: Y
%64 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :115 :16) // DrawOwner(identity, initialX, Y) (InvocationExpression)
br ^11

^11: // ExitBlock
return

}
// Skipping function DrawNetworkIdentityInfo(none, none, none), it contains poisonous unsupported syntaxes

// Skipping function DrawNetworkBehaviors(none, none, none), it contains poisonous unsupported syntaxes

// Skipping function DrawObservers(none, none, none), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkInformationPreview.DrawOwner$Mirror.NetworkIdentity.float.float$(none, none, none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :193 :8) {
^entry (%_identity : none, %_initialX : none, %_Y : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :193 :24)
cbde.store %_identity, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :193 :24)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :193 :50)
cbde.store %_initialX, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :193 :50)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :193 :66)
cbde.store %_Y, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :193 :66)
br ^0

^0: // BinaryBranchBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :195 :16) // Not a variable of known type: identity
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :195 :16) // identity.connectionToClient (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :195 :47) // null (NullLiteralExpression)
%6 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :195 :16) // comparison of unknown type: identity.connectionToClient != null
cond_br %6, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :195 :16)

^1: // SimpleBlock
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :197 :42) // Not a variable of known type: initialX
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :197 :52) // Not a variable of known type: Y
%9 = constant 10 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :197 :56)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :197 :52) // Binary expression on unsupported types Y + 10
%11 = constant 400 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :197 :60)
%12 = constant 20 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :197 :65)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :197 :33) // new Rect(initialX, Y + 10, 400, 20) (ObjectCreationExpression)
// Entity from another assembly: GUI
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :198 :26) // Not a variable of known type: ownerRect
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :198 :52) // "Client Authority: " (StringLiteralExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :198 :75) // Not a variable of known type: identity
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :198 :75) // identity.connectionToClient (SimpleMemberAccessExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :198 :52) // Binary expression on unsupported types "Client Authority: " + identity.connectionToClient
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :198 :37) // new GUIContent("Client Authority: " + identity.connectionToClient) (ObjectCreationExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :198 :105) // Not a variable of known type: styles
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :198 :105) // styles.labelStyle (SimpleMemberAccessExpression)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :198 :16) // GUI.Label(ownerRect, new GUIContent("Client Authority: " + identity.connectionToClient), styles.labelStyle) (InvocationExpression)
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :199 :16) // Not a variable of known type: Y
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :199 :21) // Not a variable of known type: ownerRect
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :199 :21) // ownerRect.height (SimpleMemberAccessExpression)
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :199 :16) // Binary expression on unsupported types Y += ownerRect.height
br ^2

^2: // JumpBlock
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :201 :19) // Not a variable of known type: Y
return %28 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :201 :12)

^3: // ExitBlock
cbde.unreachable

}
// Skipping function GetMaxNameLabelSize(none), it contains poisonous unsupported syntaxes

// Skipping function GetMaxBehaviourLabelSize(none), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkInformationPreview.GetNetworkIdentityInfo$Mirror.NetworkIdentity$(none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :241 :8) {
^entry (%_identity : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :241 :64)
cbde.store %_identity, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :241 :64)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :243 :46) // new List<NetworkIdentityInfo>             {                 GetAssetId(identity),                 GetString("Scene ID", identity.sceneId.ToString("X"))             } (ObjectCreationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetAssetId
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :245 :27) // Not a variable of known type: identity
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :245 :16) // GetAssetId(identity) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetString
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :246 :26) // "Scene ID" (StringLiteralExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :246 :38) // Not a variable of known type: identity
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :246 :38) // identity.sceneId (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :246 :64) // "X" (StringLiteralExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :246 :38) // identity.sceneId.ToString("X") (InvocationExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :246 :16) // GetString("Scene ID", identity.sceneId.ToString("X")) (InvocationExpression)
// Entity from another assembly: Application
%11 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :249 :16) // Application.isPlaying (SimpleMemberAccessExpression)
cond_br %11, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :249 :16)

^1: // SimpleBlock
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :251 :16) // Not a variable of known type: infos
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetString
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :251 :36) // "Network ID" (StringLiteralExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :251 :50) // Not a variable of known type: identity
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :251 :50) // identity.netId (SimpleMemberAccessExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :251 :50) // identity.netId.ToString() (InvocationExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :251 :26) // GetString("Network ID", identity.netId.ToString()) (InvocationExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :251 :16) // infos.Add(GetString("Network ID", identity.netId.ToString())) (InvocationExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :252 :16) // Not a variable of known type: infos
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetBoolean
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :252 :37) // "Is Client" (StringLiteralExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :252 :50) // Not a variable of known type: identity
%22 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :252 :50) // identity.isClient (SimpleMemberAccessExpression)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :252 :26) // GetBoolean("Is Client", identity.isClient) (InvocationExpression)
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :252 :16) // infos.Add(GetBoolean("Is Client", identity.isClient)) (InvocationExpression)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :253 :16) // Not a variable of known type: infos
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetBoolean
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :253 :37) // "Is Server" (StringLiteralExpression)
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :253 :50) // Not a variable of known type: identity
%28 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :253 :50) // identity.isServer (SimpleMemberAccessExpression)
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :253 :26) // GetBoolean("Is Server", identity.isServer) (InvocationExpression)
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :253 :16) // infos.Add(GetBoolean("Is Server", identity.isServer)) (InvocationExpression)
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :254 :16) // Not a variable of known type: infos
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetBoolean
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :254 :37) // "Has Authority" (StringLiteralExpression)
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :254 :54) // Not a variable of known type: identity
%34 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :254 :54) // identity.hasAuthority (SimpleMemberAccessExpression)
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :254 :26) // GetBoolean("Has Authority", identity.hasAuthority) (InvocationExpression)
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :254 :16) // infos.Add(GetBoolean("Has Authority", identity.hasAuthority)) (InvocationExpression)
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :255 :16) // Not a variable of known type: infos
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetBoolean
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :255 :37) // "Is Local Player" (StringLiteralExpression)
%39 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :255 :56) // Not a variable of known type: identity
%40 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :255 :56) // identity.isLocalPlayer (SimpleMemberAccessExpression)
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :255 :26) // GetBoolean("Is Local Player", identity.isLocalPlayer) (InvocationExpression)
%42 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :255 :16) // infos.Add(GetBoolean("Is Local Player", identity.isLocalPlayer)) (InvocationExpression)
br ^2

^2: // JumpBlock
%43 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :257 :19) // Not a variable of known type: infos
return %43 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :257 :12)

^3: // ExitBlock
cbde.unreachable

}
// Skipping function GetNetworkBehaviorInfo(none), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkInformationPreview.GetAssetId$Mirror.NetworkIdentity$(none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :276 :8) {
^entry (%_identity : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :276 :39)
cbde.store %_identity, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :276 :39)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :278 :29) // Not a variable of known type: identity
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :278 :29) // identity.assetId (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :278 :29) // identity.assetId.ToString() (InvocationExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :279 :16) // string (PredefinedType)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :279 :37) // Not a variable of known type: assetId
%7 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :279 :16) // string.IsNullOrEmpty(assetId) (InvocationExpression)
cond_br %7, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :279 :16)

^1: // SimpleBlock
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :281 :26) // "<object has no prefab>" (StringLiteralExpression)
br ^2

^2: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetString
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :283 :29) // "Asset ID" (StringLiteralExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :283 :41) // Not a variable of known type: assetId
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :283 :19) // GetString("Asset ID", assetId) (InvocationExpression)
return %11 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :283 :12)

^3: // ExitBlock
cbde.unreachable

}
func @_Mirror.NetworkInformationPreview.GetString$string.string$(none, none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :286 :8) {
^entry (%_name : none, %_value : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :286 :45)
cbde.store %_name, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :286 :45)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :286 :58)
cbde.store %_value, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :286 :58)
br ^0

^0: // JumpBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :288 :19) // new NetworkIdentityInfo             {                 name = new GUIContent(name),                 value = new GUIContent(value)             } (ObjectCreationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :290 :38) // Not a variable of known type: name
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :290 :23) // new GUIContent(name) (ObjectCreationExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :291 :39) // Not a variable of known type: value
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :291 :24) // new GUIContent(value) (ObjectCreationExpression)
return %2 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\NetworkInformationPreview.cs" :288 :12)

^1: // ExitBlock
cbde.unreachable

}
// Skipping function GetBoolean(none, i1), it contains poisonous unsupported syntaxes

