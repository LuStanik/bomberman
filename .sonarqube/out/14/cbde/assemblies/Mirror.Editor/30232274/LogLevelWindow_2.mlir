func @_Mirror.EditorScripts.Logging.LogLevelWindow.OnEnable$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Logging\\LogLevelWindow.cs" :15 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Logging\\LogLevelWindow.cs" :17 :52) // this (ThisExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Logging\\LogLevelWindow.cs" :17 :31) // new SerializedObject(this) (ObjectCreationExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Logging\\LogLevelWindow.cs" :18 :27) // Not a variable of known type: serializedObject
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Logging\\LogLevelWindow.cs" :18 :57) // nameof(settings) (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Logging\\LogLevelWindow.cs" :18 :27) // serializedObject.FindProperty(nameof(settings)) (InvocationExpression)
// Entity from another assembly: EditorLogSettingsLoader
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Logging\\LogLevelWindow.cs" :20 :43) // EditorLogSettingsLoader.FindLogSettings() (InvocationExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Logging\\LogLevelWindow.cs" :21 :16) // Not a variable of known type: existingSettings
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Logging\\LogLevelWindow.cs" :21 :36) // null (NullLiteralExpression)
%9 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Logging\\LogLevelWindow.cs" :21 :16) // comparison of unknown type: existingSettings != null
cond_br %9, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Logging\\LogLevelWindow.cs" :21 :16)

^1: // SimpleBlock
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Logging\\LogLevelWindow.cs" :23 :16) // Not a variable of known type: settingsProp
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Logging\\LogLevelWindow.cs" :23 :16) // settingsProp.objectReferenceValue (SimpleMemberAccessExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Logging\\LogLevelWindow.cs" :23 :52) // Not a variable of known type: existingSettings
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Logging\\LogLevelWindow.cs" :24 :16) // Not a variable of known type: serializedObject
%14 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Logging\\LogLevelWindow.cs" :24 :16) // serializedObject.ApplyModifiedProperties() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
// Skipping function OnGUI(), it contains poisonous unsupported syntaxes

func @_Mirror.EditorScripts.Logging.LogLevelWindow.ShowWindow$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Logging\\LogLevelWindow.cs" :60 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Logging\\LogLevelWindow.cs" :63 :36) // GetWindow<LogLevelWindow>() (InvocationExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Logging\\LogLevelWindow.cs" :64 :12) // Not a variable of known type: window
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Logging\\LogLevelWindow.cs" :64 :12) // window.minSize (SimpleMemberAccessExpression)
%4 = constant 200 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Logging\\LogLevelWindow.cs" :64 :41)
%5 = constant 100 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Logging\\LogLevelWindow.cs" :64 :46)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Logging\\LogLevelWindow.cs" :64 :29) // new Vector2(200, 100) (ObjectCreationExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Logging\\LogLevelWindow.cs" :65 :12) // Not a variable of known type: window
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Logging\\LogLevelWindow.cs" :65 :12) // window.titleContent (SimpleMemberAccessExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Logging\\LogLevelWindow.cs" :65 :49) // "Mirror Log Levels" (StringLiteralExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Logging\\LogLevelWindow.cs" :65 :34) // new GUIContent("Mirror Log Levels") (ObjectCreationExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Logging\\LogLevelWindow.cs" :66 :12) // Not a variable of known type: window
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Editor\\Logging\\LogLevelWindow.cs" :66 :12) // window.Show() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
