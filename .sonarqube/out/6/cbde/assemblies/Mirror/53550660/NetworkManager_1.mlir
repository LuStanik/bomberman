// Skipping function OnValidate(), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkManager.Awake$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :228 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: InitializeSingleton
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :231 :17) // InitializeSingleton() (InvocationExpression)
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :231 :16) // !InitializeSingleton() (LogicalNotExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :231 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :231 :40)

^2: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :233 :12) // Not a variable of known type: logger
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :233 :23) // "Thank you for using Mirror! https://mirror-networking.com" (StringLiteralExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :233 :12) // logger.Log("Thank you for using Mirror! https://mirror-networking.com") (InvocationExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :237 :31) // Not a variable of known type: offlineScene
// Entity from another assembly: SceneManager
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :240 :12) // SceneManager.sceneLoaded (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnSceneLoaded
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :240 :12) // Binary expression on unsupported types SceneManager.sceneLoaded += OnSceneLoaded
// No identifier name for binary assignment expression
br ^3

^3: // ExitBlock
return

}
func @_Mirror.NetworkManager.Start$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :246 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkManager.LateUpdate$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :266 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkServer
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :271 :12) // NetworkServer.Update() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkClient
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :272 :12) // NetworkClient.Update() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: UpdateScene
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :273 :12) // UpdateScene() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function IsServerOnlineSceneChangeNeeded(), it contains poisonous unsupported syntaxes

// Skipping function IsSceneActive(none), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkManager.SetupServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :294 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :296 :16) // Not a variable of known type: logger
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :296 :16) // logger.LogEnabled() (InvocationExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :296 :16)

^1: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :296 :37) // Not a variable of known type: logger
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :296 :48) // "NetworkManager SetupServer" (StringLiteralExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :296 :37) // logger.Log("NetworkManager SetupServer") (InvocationExpression)
br ^2

^2: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: InitializeSingleton
%5 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :297 :12) // InitializeSingleton() (InvocationExpression)
%6 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :299 :16) // Not a variable of known type: runInBackground
cond_br %6, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :299 :16)

^3: // SimpleBlock
// Entity from another assembly: Application
%7 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :300 :16) // Application.runInBackground (SimpleMemberAccessExpression)
%8 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :300 :46) // true
br ^4

^4: // BinaryBranchBlock
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :302 :16) // Not a variable of known type: authenticator
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :302 :33) // null (NullLiteralExpression)
%11 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :302 :16) // comparison of unknown type: authenticator != null
cond_br %11, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :302 :16)

^5: // SimpleBlock
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :304 :16) // Not a variable of known type: authenticator
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :304 :16) // authenticator.OnStartServer() (InvocationExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :305 :16) // Not a variable of known type: authenticator
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :305 :16) // authenticator.OnServerAuthenticated (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnServerAuthenticated
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :305 :16) // authenticator.OnServerAuthenticated.AddListener(OnServerAuthenticated) (InvocationExpression)
br ^6

^6: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ConfigureServerFrameRate
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :308 :12) // ConfigureServerFrameRate() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkServer
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :311 :12) // NetworkServer.disconnectInactiveTimeout (SimpleMemberAccessExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :311 :54) // Not a variable of known type: disconnectInactiveTimeout
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkServer
%20 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :312 :12) // NetworkServer.disconnectInactiveConnections (SimpleMemberAccessExpression)
%21 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :312 :58) // Not a variable of known type: disconnectInactiveConnections
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkServer
%22 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :315 :33) // Not a variable of known type: maxConnections
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :315 :12) // NetworkServer.Listen(maxConnections) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnStartServer
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :325 :12) // OnStartServer() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: RegisterServerMessages
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :328 :12) // RegisterServerMessages() (InvocationExpression)
%26 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :330 :30) // true
br ^7

^7: // ExitBlock
return

}
func @_Mirror.NetworkManager.StartServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :336 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkServer
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :338 :16) // NetworkServer.active (SimpleMemberAccessExpression)
cond_br %0, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :338 :16)

^1: // JumpBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :340 :16) // Not a variable of known type: logger
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :340 :34) // "Server already started." (StringLiteralExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :340 :16) // logger.LogWarning("Server already started.") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :341 :16)

^2: // BinaryBranchBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :344 :19) // Not a variable of known type: NetworkManagerMode
%5 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :344 :19) // NetworkManagerMode.ServerOnly (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SetupServer
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :362 :12) // SetupServer() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: IsServerOnlineSceneChangeNeeded
%7 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :365 :16) // IsServerOnlineSceneChangeNeeded() (InvocationExpression)
cond_br %7, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :365 :16)

^3: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ServerChangeScene
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :367 :34) // Not a variable of known type: onlineScene
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :367 :16) // ServerChangeScene(onlineScene) (InvocationExpression)
br ^5

^4: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkServer
%10 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :372 :16) // NetworkServer.SpawnObjects() (InvocationExpression)
br ^5

^5: // ExitBlock
return

}
func @_Mirror.NetworkManager.StartClient$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :380 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkClient
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :382 :16) // NetworkClient.active (SimpleMemberAccessExpression)
cond_br %0, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :382 :16)

^1: // JumpBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :384 :16) // Not a variable of known type: logger
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :384 :34) // "Client already started." (StringLiteralExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :384 :16) // logger.LogWarning("Client already started.") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :385 :16)

^2: // BinaryBranchBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :388 :19) // Not a variable of known type: NetworkManagerMode
%5 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :388 :19) // NetworkManagerMode.ClientOnly (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: InitializeSingleton
%6 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :390 :12) // InitializeSingleton() (InvocationExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :392 :16) // Not a variable of known type: authenticator
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :392 :33) // null (NullLiteralExpression)
%9 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :392 :16) // comparison of unknown type: authenticator != null
cond_br %9, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :392 :16)

^3: // SimpleBlock
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :394 :16) // Not a variable of known type: authenticator
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :394 :16) // authenticator.OnStartClient() (InvocationExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :395 :16) // Not a variable of known type: authenticator
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :395 :16) // authenticator.OnClientAuthenticated (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnClientAuthenticated
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :395 :16) // authenticator.OnClientAuthenticated.AddListener(OnClientAuthenticated) (InvocationExpression)
br ^4

^4: // BinaryBranchBlock
%15 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :398 :16) // Not a variable of known type: runInBackground
cond_br %15, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :398 :16)

^5: // SimpleBlock
// Entity from another assembly: Application
%16 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :399 :16) // Application.runInBackground (SimpleMemberAccessExpression)
%17 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :399 :46) // true
br ^6

^6: // BinaryBranchBlock
%18 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :401 :30) // true
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: RegisterClientMessages
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :403 :12) // RegisterClientMessages() (InvocationExpression)
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :405 :16) // string (PredefinedType)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :405 :37) // Not a variable of known type: networkAddress
%22 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :405 :16) // string.IsNullOrEmpty(networkAddress) (InvocationExpression)
cond_br %22, ^7, ^8 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :405 :16)

^7: // JumpBlock
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :407 :16) // Not a variable of known type: logger
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :407 :32) // "Must set the Network Address field in the manager" (StringLiteralExpression)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :407 :16) // logger.LogError("Must set the Network Address field in the manager") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :408 :16)

^8: // BinaryBranchBlock
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :410 :16) // Not a variable of known type: logger
%27 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :410 :16) // logger.LogEnabled() (InvocationExpression)
cond_br %27, ^9, ^10 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :410 :16)

^9: // SimpleBlock
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :410 :37) // Not a variable of known type: logger
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :410 :48) // "NetworkManager StartClient address:" (StringLiteralExpression)
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :410 :88) // Not a variable of known type: networkAddress
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :410 :48) // Binary expression on unsupported types "NetworkManager StartClient address:" + networkAddress
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :410 :37) // logger.Log("NetworkManager StartClient address:" + networkAddress) (InvocationExpression)
br ^10

^10: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkClient
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :412 :34) // Not a variable of known type: networkAddress
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :412 :12) // NetworkClient.Connect(networkAddress) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnStartClient
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :414 :12) // OnStartClient() (InvocationExpression)
br ^11

^11: // ExitBlock
return

}
func @_Mirror.NetworkManager.StartClient$System.Uri$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :422 :8) {
^entry (%_uri : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :422 :32)
cbde.store %_uri, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :422 :32)
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkClient
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :424 :16) // NetworkClient.active (SimpleMemberAccessExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :424 :16)

^1: // JumpBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :426 :16) // Not a variable of known type: logger
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :426 :34) // "Client already started." (StringLiteralExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :426 :16) // logger.LogWarning("Client already started.") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :427 :16)

^2: // BinaryBranchBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :430 :19) // Not a variable of known type: NetworkManagerMode
%6 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :430 :19) // NetworkManagerMode.ClientOnly (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: InitializeSingleton
%7 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :432 :12) // InitializeSingleton() (InvocationExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :434 :16) // Not a variable of known type: authenticator
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :434 :33) // null (NullLiteralExpression)
%10 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :434 :16) // comparison of unknown type: authenticator != null
cond_br %10, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :434 :16)

^3: // SimpleBlock
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :436 :16) // Not a variable of known type: authenticator
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :436 :16) // authenticator.OnStartClient() (InvocationExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :437 :16) // Not a variable of known type: authenticator
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :437 :16) // authenticator.OnClientAuthenticated (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnClientAuthenticated
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :437 :16) // authenticator.OnClientAuthenticated.AddListener(OnClientAuthenticated) (InvocationExpression)
br ^4

^4: // BinaryBranchBlock
%16 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :440 :16) // Not a variable of known type: runInBackground
cond_br %16, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :440 :16)

^5: // SimpleBlock
// Entity from another assembly: Application
%17 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :442 :16) // Application.runInBackground (SimpleMemberAccessExpression)
%18 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :442 :46) // true
br ^6

^6: // BinaryBranchBlock
%19 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :445 :30) // true
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: RegisterClientMessages
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :447 :12) // RegisterClientMessages() (InvocationExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :449 :16) // Not a variable of known type: logger
%22 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :449 :16) // logger.LogEnabled() (InvocationExpression)
cond_br %22, ^7, ^8 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :449 :16)

^7: // SimpleBlock
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :449 :37) // Not a variable of known type: logger
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :449 :48) // "NetworkManager StartClient address:" (StringLiteralExpression)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :449 :88) // Not a variable of known type: uri
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :449 :48) // Binary expression on unsupported types "NetworkManager StartClient address:" + uri
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :449 :37) // logger.Log("NetworkManager StartClient address:" + uri) (InvocationExpression)
br ^8

^8: // SimpleBlock
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :450 :29) // Not a variable of known type: uri
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :450 :29) // uri.Host (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkClient
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :452 :34) // Not a variable of known type: uri
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :452 :12) // NetworkClient.Connect(uri) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnStartClient
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :454 :12) // OnStartClient() (InvocationExpression)
br ^9

^9: // ExitBlock
return

}
// Skipping function StartHost(), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkManager.FinishStartHost$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :526 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkClient
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :554 :12) // NetworkClient.ConnectHost() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkServer
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :557 :12) // NetworkServer.SpawnObjects() (InvocationExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :563 :12) // Not a variable of known type: logger
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :563 :23) // "StartHostClient called" (StringLiteralExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :563 :12) // logger.Log("StartHostClient called") (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: StartHostClient
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :564 :12) // StartHostClient() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkManager.StartHostClient$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :567 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :569 :12) // Not a variable of known type: logger
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :569 :23) // "NetworkManager ConnectLocalClient" (StringLiteralExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :569 :12) // logger.Log("NetworkManager ConnectLocalClient") (InvocationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :571 :16) // Not a variable of known type: authenticator
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :571 :33) // null (NullLiteralExpression)
%5 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :571 :16) // comparison of unknown type: authenticator != null
cond_br %5, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :571 :16)

^1: // SimpleBlock
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :573 :16) // Not a variable of known type: authenticator
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :573 :16) // authenticator.OnStartClient() (InvocationExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :574 :16) // Not a variable of known type: authenticator
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :574 :16) // authenticator.OnClientAuthenticated (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnClientAuthenticated
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :574 :16) // authenticator.OnClientAuthenticated.AddListener(OnClientAuthenticated) (InvocationExpression)
br ^2

^2: // SimpleBlock
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :577 :29) // "localhost" (StringLiteralExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkServer
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :578 :12) // NetworkServer.ActivateHostScene() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: RegisterClientMessages
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :579 :12) // RegisterClientMessages() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkClient
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :583 :12) // NetworkClient.ConnectLocalServer() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnStartClient
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :585 :12) // OnStartClient() (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.NetworkManager.StopHost$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :591 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnStopHost
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :593 :12) // OnStopHost() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkClient
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :601 :12) // NetworkClient.DisconnectLocalServer() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: StopClient
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :603 :12) // StopClient() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: StopServer
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :604 :12) // StopServer() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkManager.StopServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :610 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkServer
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :612 :17) // NetworkServer.active (SimpleMemberAccessExpression)
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :612 :16) // !NetworkServer.active (LogicalNotExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :612 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :613 :16)

^2: // BinaryBranchBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :615 :16) // Not a variable of known type: authenticator
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :615 :33) // null (NullLiteralExpression)
%4 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :615 :16) // comparison of unknown type: authenticator != null
cond_br %4, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :615 :16)

^3: // SimpleBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :617 :16) // Not a variable of known type: authenticator
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :617 :16) // authenticator.OnServerAuthenticated (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnServerAuthenticated
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :617 :16) // authenticator.OnServerAuthenticated.RemoveListener(OnServerAuthenticated) (InvocationExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :618 :16) // Not a variable of known type: authenticator
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :618 :16) // authenticator.OnStopServer() (InvocationExpression)
br ^4

^4: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnStopServer
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :621 :12) // OnStopServer() (InvocationExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :623 :12) // Not a variable of known type: logger
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :623 :23) // "NetworkManager StopServer" (StringLiteralExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :623 :12) // logger.Log("NetworkManager StopServer") (InvocationExpression)
%14 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :624 :30) // false
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkServer
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :625 :12) // NetworkServer.Shutdown() (InvocationExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :629 :19) // Not a variable of known type: NetworkManagerMode
%17 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :629 :19) // NetworkManagerMode.Offline (SimpleMemberAccessExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :631 :17) // string (PredefinedType)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :631 :38) // Not a variable of known type: offlineScene
%20 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :631 :17) // string.IsNullOrEmpty(offlineScene) (InvocationExpression)
%21 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :631 :16) // !string.IsNullOrEmpty(offlineScene) (LogicalNotExpression)
cond_br %21, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :631 :16)

^5: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ServerChangeScene
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :633 :34) // Not a variable of known type: offlineScene
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :633 :16) // ServerChangeScene(offlineScene) (InvocationExpression)
br ^6

^6: // SimpleBlock
%24 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :636 :33)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :638 :31) // "" (StringLiteralExpression)
br ^7

^7: // ExitBlock
return

}
// Skipping function StopClient(), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkManager.OnApplicationQuit$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :680 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkClient
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :685 :16) // NetworkClient.isConnected (SimpleMemberAccessExpression)
cond_br %0, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :685 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: StopClient
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :687 :16) // StopClient() (InvocationExpression)
// Entity from another assembly: print
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :688 :22) // "OnApplicationQuit: stopped client" (StringLiteralExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :688 :16) // print("OnApplicationQuit: stopped client") (InvocationExpression)
br ^2

^2: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkServer
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :692 :16) // NetworkServer.active (SimpleMemberAccessExpression)
cond_br %4, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :692 :16)

^3: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: StopServer
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :694 :16) // StopServer() (InvocationExpression)
// Entity from another assembly: print
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :695 :22) // "OnApplicationQuit: stopped server" (StringLiteralExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :695 :16) // print("OnApplicationQuit: stopped server") (InvocationExpression)
br ^4

^4: // ExitBlock
return

}
func @_Mirror.NetworkManager.ConfigureServerFrameRate$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :703 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
// Skipping function InitializeSingleton(), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkManager.RegisterServerMessages$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :750 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkServer
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnServerConnectInternal
%0 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :752 :83) // false
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :752 :12) // NetworkServer.RegisterHandler<ConnectMessage>(OnServerConnectInternal, false) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkServer
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnServerDisconnectInternal
%2 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :753 :89) // false
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :753 :12) // NetworkServer.RegisterHandler<DisconnectMessage>(OnServerDisconnectInternal, false) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkServer
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnServerAddPlayerInternal
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :754 :12) // NetworkServer.RegisterHandler<AddPlayerMessage>(OnServerAddPlayerInternal) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkServer
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnServerErrorInternal
%5 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :755 :79) // false
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :755 :12) // NetworkServer.RegisterHandler<ErrorMessage>(OnServerErrorInternal, false) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkServer
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnServerReadyMessageInternal
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :758 :12) // NetworkServer.ReplaceHandler<ReadyMessage>(OnServerReadyMessageInternal) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkManager.RegisterClientMessages$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :761 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkClient
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnClientConnectInternal
%0 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :763 :83) // false
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :763 :12) // NetworkClient.RegisterHandler<ConnectMessage>(OnClientConnectInternal, false) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkClient
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnClientDisconnectInternal
%2 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :764 :89) // false
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :764 :12) // NetworkClient.RegisterHandler<DisconnectMessage>(OnClientDisconnectInternal, false) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkClient
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnClientNotReadyMessageInternal
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :765 :12) // NetworkClient.RegisterHandler<NotReadyMessage>(OnClientNotReadyMessageInternal) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkClient
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnClientErrorInternal
%5 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :766 :79) // false
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :766 :12) // NetworkClient.RegisterHandler<ErrorMessage>(OnClientErrorInternal, false) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkClient
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnClientSceneInternal
%7 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :767 :79) // false
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :767 :12) // NetworkClient.RegisterHandler<SceneMessage>(OnClientSceneInternal, false) (InvocationExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :769 :16) // Not a variable of known type: playerPrefab
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :769 :32) // null (NullLiteralExpression)
%11 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :769 :16) // comparison of unknown type: playerPrefab != null
cond_br %11, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :769 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ClientScene
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :771 :43) // Not a variable of known type: playerPrefab
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :771 :16) // ClientScene.RegisterPrefab(playerPrefab) (InvocationExpression)
br ^2

^2: // ForInitializerBlock
%14 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :773 :25)
%15 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :773 :21) // i
cbde.store %14, %15 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :773 :21)
br ^3

^3: // BinaryBranchBlock
%16 = cbde.load %15 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :773 :28)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :773 :32) // Not a variable of known type: spawnPrefabs
%18 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :773 :32) // spawnPrefabs.Count (SimpleMemberAccessExpression)
%19 = cmpi "slt", %16, %18 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :773 :28)
cond_br %19, ^4, ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :773 :28)

^4: // BinaryBranchBlock
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :775 :36) // Not a variable of known type: spawnPrefabs
%21 = cbde.load %15 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :775 :49)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :775 :36) // spawnPrefabs[i] (ElementAccessExpression)
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :776 :20) // Not a variable of known type: prefab
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :776 :30) // null (NullLiteralExpression)
%26 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :776 :20) // comparison of unknown type: prefab != null
cond_br %26, ^6, ^7 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :776 :20)

^6: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ClientScene
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :778 :47) // Not a variable of known type: prefab
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :778 :20) // ClientScene.RegisterPrefab(prefab) (InvocationExpression)
br ^7

^7: // SimpleBlock
%29 = cbde.load %15 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :773 :52)
%30 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :773 :52)
%31 = addi %29, %30 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :773 :52)
cbde.store %31, %15 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :773 :52)
br ^3

^5: // ExitBlock
return

}
func @_Mirror.NetworkManager.Shutdown$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :786 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :788 :16) // Not a variable of known type: singleton
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :788 :29) // null (NullLiteralExpression)
%2 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :788 :16) // comparison of unknown type: singleton == null
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :788 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :789 :16)

^2: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :791 :12) // Not a variable of known type: startPositions
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :791 :12) // startPositions.Clear() (InvocationExpression)
%5 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :792 :33)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :793 :36) // null (NullLiteralExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :795 :12) // Not a variable of known type: singleton
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :795 :12) // singleton.StopHost() (InvocationExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :796 :24) // null (NullLiteralExpression)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.NetworkManager.OnDestroy$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :802 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :804 :12) // Not a variable of known type: logger
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :804 :23) // "NetworkManager destroyed" (StringLiteralExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :804 :12) // logger.Log("NetworkManager destroyed") (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkManager.ServerChangeScene$string$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :828 :8) {
^entry (%_newSceneName : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :828 :46)
cbde.store %_newSceneName, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :828 :46)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :830 :16) // string (PredefinedType)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :830 :37) // Not a variable of known type: newSceneName
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :830 :16) // string.IsNullOrEmpty(newSceneName) (InvocationExpression)
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :830 :16)

^1: // JumpBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :832 :16) // Not a variable of known type: logger
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :832 :32) // "ServerChangeScene empty scene name" (StringLiteralExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :832 :16) // logger.LogError("ServerChangeScene empty scene name") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :833 :16)

^2: // BinaryBranchBlock
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :836 :16) // Not a variable of known type: logger
%8 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :836 :16) // logger.logEnabled (SimpleMemberAccessExpression)
cond_br %8, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :836 :16)

^3: // SimpleBlock
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :836 :35) // Not a variable of known type: logger
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :836 :46) // "ServerChangeScene " (StringLiteralExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :836 :69) // Not a variable of known type: newSceneName
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :836 :46) // Binary expression on unsupported types "ServerChangeScene " + newSceneName
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :836 :35) // logger.Log("ServerChangeScene " + newSceneName) (InvocationExpression)
br ^4

^4: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkServer
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :837 :12) // NetworkServer.SetAllClientsNotReady() (InvocationExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :838 :31) // Not a variable of known type: newSceneName
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnServerChangeScene
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :841 :32) // Not a variable of known type: newSceneName
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :841 :12) // OnServerChangeScene(newSceneName) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Transport
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :845 :12) // Transport.activeTransport (SimpleMemberAccessExpression)
%19 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :845 :12) // Transport.activeTransport.enabled (SimpleMemberAccessExpression)
%20 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :845 :48) // false
// Entity from another assembly: SceneManager
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :847 :60) // Not a variable of known type: newSceneName
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :847 :32) // SceneManager.LoadSceneAsync(newSceneName) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkServer
%23 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :851 :16) // NetworkServer.active (SimpleMemberAccessExpression)
cond_br %23, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :851 :16)

^5: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkServer
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :854 :40) // new SceneMessage { sceneName = newSceneName } (ObjectCreationExpression)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :854 :71) // Not a variable of known type: newSceneName
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :854 :16) // NetworkServer.SendToAll(new SceneMessage { sceneName = newSceneName }) (InvocationExpression)
br ^6

^6: // SimpleBlock
%27 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :857 :33)
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :858 :12) // Not a variable of known type: startPositions
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :858 :12) // startPositions.Clear() (InvocationExpression)
br ^7

^7: // ExitBlock
return

}
// Skipping function ClientChangeScene(none, none, i1), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkManager.OnSceneLoaded$UnityEngine.SceneManagement.Scene.UnityEngine.SceneManagement.LoadSceneMode$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :942 :8) {
^entry (%_scene : none, %_mode : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :942 :27)
cbde.store %_scene, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :942 :27)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :942 :40)
cbde.store %_mode, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :942 :40)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :944 :16) // Not a variable of known type: mode
// Entity from another assembly: LoadSceneMode
%3 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :944 :24) // LoadSceneMode.Additive (SimpleMemberAccessExpression)
%4 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :944 :16) // comparison of unknown type: mode == LoadSceneMode.Additive
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :944 :16)

^1: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkServer
%5 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :946 :20) // NetworkServer.active (SimpleMemberAccessExpression)
cond_br %5, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :946 :20)

^3: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkServer
%6 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :949 :20) // NetworkServer.SpawnObjects() (InvocationExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :950 :24) // Not a variable of known type: logger
%8 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :950 :24) // logger.LogEnabled() (InvocationExpression)
cond_br %8, ^5, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :950 :24)

^5: // SimpleBlock
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :950 :45) // Not a variable of known type: logger
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :950 :56) // "Respawned Server objects after additive scene load: " (StringLiteralExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :950 :113) // Not a variable of known type: scene
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :950 :113) // scene.name (SimpleMemberAccessExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :950 :56) // Binary expression on unsupported types "Respawned Server objects after additive scene load: " + scene.name
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :950 :45) // logger.Log("Respawned Server objects after additive scene load: " + scene.name) (InvocationExpression)
br ^4

^4: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkClient
%15 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :952 :20) // NetworkClient.active (SimpleMemberAccessExpression)
cond_br %15, ^6, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :952 :20)

^6: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ClientScene
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :954 :20) // ClientScene.PrepareToSpawnSceneObjects() (InvocationExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :955 :24) // Not a variable of known type: logger
%18 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :955 :24) // logger.LogEnabled() (InvocationExpression)
cond_br %18, ^7, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :955 :24)

^7: // SimpleBlock
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :955 :45) // Not a variable of known type: logger
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :955 :56) // "Rebuild Client spawnableObjects after additive scene load: " (StringLiteralExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :955 :120) // Not a variable of known type: scene
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :955 :120) // scene.name (SimpleMemberAccessExpression)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :955 :56) // Binary expression on unsupported types "Rebuild Client spawnableObjects after additive scene load: " + scene.name
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :955 :45) // logger.Log("Rebuild Client spawnableObjects after additive scene load: " + scene.name) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
// Skipping function UpdateScene(), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkManager.FinishLoadScene$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :971 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :976 :12) // Not a variable of known type: logger
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :976 :23) // "FinishLoadScene: resuming handlers after scene was loading." (StringLiteralExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :976 :12) // logger.Log("FinishLoadScene: resuming handlers after scene was loading.") (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Transport
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :977 :12) // Transport.activeTransport (SimpleMemberAccessExpression)
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :977 :12) // Transport.activeTransport.enabled (SimpleMemberAccessExpression)
%5 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :977 :48) // true
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :980 :16) // Not a variable of known type: mode
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :980 :24) // Not a variable of known type: NetworkManagerMode
%8 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :980 :24) // NetworkManagerMode.Host (SimpleMemberAccessExpression)
%9 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :980 :16) // comparison of unknown type: mode == NetworkManagerMode.Host
cond_br %9, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :980 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: FinishLoadSceneHost
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :982 :16) // FinishLoadSceneHost() (InvocationExpression)
br ^3

^2: // BinaryBranchBlock
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :985 :21) // Not a variable of known type: mode
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :985 :29) // Not a variable of known type: NetworkManagerMode
%13 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :985 :29) // NetworkManagerMode.ServerOnly (SimpleMemberAccessExpression)
%14 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :985 :21) // comparison of unknown type: mode == NetworkManagerMode.ServerOnly
cond_br %14, ^4, ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :985 :21)

^4: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: FinishLoadSceneServerOnly
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :987 :16) // FinishLoadSceneServerOnly() (InvocationExpression)
br ^3

^5: // BinaryBranchBlock
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :990 :21) // Not a variable of known type: mode
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :990 :29) // Not a variable of known type: NetworkManagerMode
%18 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :990 :29) // NetworkManagerMode.ClientOnly (SimpleMemberAccessExpression)
%19 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :990 :21) // comparison of unknown type: mode == NetworkManagerMode.ClientOnly
cond_br %19, ^6, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :990 :21)

^6: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: FinishLoadSceneClientOnly
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :992 :16) // FinishLoadSceneClientOnly() (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.NetworkManager.FinishLoadSceneHost$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1001 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1005 :12) // Not a variable of known type: logger
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1005 :23) // "Finished loading scene in host mode." (StringLiteralExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1005 :12) // logger.Log("Finished loading scene in host mode.") (InvocationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1007 :16) // Not a variable of known type: clientReadyConnection
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1007 :41) // null (NullLiteralExpression)
%5 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1007 :16) // comparison of unknown type: clientReadyConnection != null
cond_br %5, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1007 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnClientConnect
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1009 :32) // Not a variable of known type: clientReadyConnection
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1009 :16) // OnClientConnect(clientReadyConnection) (InvocationExpression)
%8 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1010 :36) // true
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1011 :40) // null (NullLiteralExpression)
br ^2

^2: // BinaryBranchBlock
%10 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1016 :16) // Not a variable of known type: finishStartHostPending
cond_br %10, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1016 :16)

^3: // SimpleBlock
%11 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1018 :41) // false
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: FinishStartHost
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1019 :16) // FinishStartHost() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnServerSceneChanged
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1022 :37) // Not a variable of known type: networkSceneName
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1022 :16) // OnServerSceneChanged(networkSceneName) (InvocationExpression)
br ^5

^4: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkServer
%15 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1037 :16) // NetworkServer.SpawnObjects() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnServerSceneChanged
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1040 :37) // Not a variable of known type: networkSceneName
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1040 :16) // OnServerSceneChanged(networkSceneName) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkClient
%18 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1042 :20) // NetworkClient.isConnected (SimpleMemberAccessExpression)
cond_br %18, ^6, ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1042 :20)

^6: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnClientSceneChanged
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkClient
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1045 :41) // NetworkClient.connection (SimpleMemberAccessExpression)
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1045 :20) // OnClientSceneChanged(NetworkClient.connection) (InvocationExpression)
br ^5

^5: // ExitBlock
return

}
func @_Mirror.NetworkManager.FinishLoadSceneServerOnly$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1052 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1056 :12) // Not a variable of known type: logger
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1056 :23) // "Finished loading scene in server-only mode." (StringLiteralExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1056 :12) // logger.Log("Finished loading scene in server-only mode.") (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkServer
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1058 :12) // NetworkServer.SpawnObjects() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnServerSceneChanged
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1059 :33) // Not a variable of known type: networkSceneName
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1059 :12) // OnServerSceneChanged(networkSceneName) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkManager.FinishLoadSceneClientOnly$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1064 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1068 :12) // Not a variable of known type: logger
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1068 :23) // "Finished loading scene in client-only mode." (StringLiteralExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1068 :12) // logger.Log("Finished loading scene in client-only mode.") (InvocationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1070 :16) // Not a variable of known type: clientReadyConnection
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1070 :41) // null (NullLiteralExpression)
%5 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1070 :16) // comparison of unknown type: clientReadyConnection != null
cond_br %5, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1070 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnClientConnect
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1072 :32) // Not a variable of known type: clientReadyConnection
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1072 :16) // OnClientConnect(clientReadyConnection) (InvocationExpression)
%8 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1073 :36) // true
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1074 :40) // null (NullLiteralExpression)
br ^2

^2: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkClient
%10 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1077 :16) // NetworkClient.isConnected (SimpleMemberAccessExpression)
cond_br %10, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1077 :16)

^3: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnClientSceneChanged
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkClient
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1079 :37) // NetworkClient.connection (SimpleMemberAccessExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1079 :16) // OnClientSceneChanged(NetworkClient.connection) (InvocationExpression)
br ^4

^4: // ExitBlock
return

}
// Skipping function RegisterStartPosition(none), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkManager.UnRegisterStartPosition$UnityEngine.Transform$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1116 :8) {
^entry (%_start : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1116 :51)
cbde.store %_start, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1116 :51)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1118 :16) // Not a variable of known type: logger
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1118 :16) // logger.LogEnabled() (InvocationExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1118 :16)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1118 :37) // Not a variable of known type: logger
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1118 :48) // "UnRegisterStartPosition: (" (StringLiteralExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1118 :79) // Not a variable of known type: start
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1118 :79) // start.gameObject (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1118 :79) // start.gameObject.name (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1118 :48) // Binary expression on unsupported types "UnRegisterStartPosition: (" + start.gameObject.name
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1118 :103) // ") " (StringLiteralExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1118 :48) // Binary expression on unsupported types "UnRegisterStartPosition: (" + start.gameObject.name + ") "
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1118 :110) // Not a variable of known type: start
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1118 :110) // start.position (SimpleMemberAccessExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1118 :48) // Binary expression on unsupported types "UnRegisterStartPosition: (" + start.gameObject.name + ") " + start.position
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1118 :37) // logger.Log("UnRegisterStartPosition: (" + start.gameObject.name + ") " + start.position) (InvocationExpression)
br ^2

^2: // SimpleBlock
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1119 :12) // Not a variable of known type: startPositions
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1119 :34) // Not a variable of known type: start
%17 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1119 :12) // startPositions.Remove(start) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function GetStartPosition(), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkManager.OnServerConnectInternal$Mirror.NetworkConnection.Mirror.ConnectMessage$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1151 :8) {
^entry (%_conn : none, %_connectMsg : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1151 :37)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1151 :37)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1151 :61)
cbde.store %_connectMsg, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1151 :61)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1153 :12) // Not a variable of known type: logger
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1153 :23) // "NetworkManager.OnServerConnectInternal" (StringLiteralExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1153 :12) // logger.Log("NetworkManager.OnServerConnectInternal") (InvocationExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1155 :16) // Not a variable of known type: authenticator
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1155 :33) // null (NullLiteralExpression)
%7 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1155 :16) // comparison of unknown type: authenticator != null
cond_br %7, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1155 :16)

^1: // SimpleBlock
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1158 :16) // Not a variable of known type: authenticator
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1158 :51) // Not a variable of known type: conn
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1158 :16) // authenticator.OnServerAuthenticate(conn) (InvocationExpression)
br ^3

^2: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnServerAuthenticated
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1163 :38) // Not a variable of known type: conn
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1163 :16) // OnServerAuthenticated(conn) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function OnServerAuthenticated(none), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkManager.OnServerDisconnectInternal$Mirror.NetworkConnection.Mirror.DisconnectMessage$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1185 :8) {
^entry (%_conn : none, %_msg : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1185 :40)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1185 :40)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1185 :64)
cbde.store %_msg, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1185 :64)
br ^0

^0: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1187 :12) // Not a variable of known type: logger
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1187 :23) // "NetworkManager.OnServerDisconnectInternal" (StringLiteralExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1187 :12) // logger.Log("NetworkManager.OnServerDisconnectInternal") (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnServerDisconnect
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1188 :31) // Not a variable of known type: conn
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1188 :12) // OnServerDisconnect(conn) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkManager.OnServerReadyMessageInternal$Mirror.NetworkConnection.Mirror.ReadyMessage$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1191 :8) {
^entry (%_conn : none, %_msg : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1191 :42)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1191 :42)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1191 :66)
cbde.store %_msg, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1191 :66)
br ^0

^0: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1193 :12) // Not a variable of known type: logger
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1193 :23) // "NetworkManager.OnServerReadyMessageInternal" (StringLiteralExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1193 :12) // logger.Log("NetworkManager.OnServerReadyMessageInternal") (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnServerReady
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1194 :26) // Not a variable of known type: conn
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1194 :12) // OnServerReady(conn) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function OnServerAddPlayerInternal(none, none), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkManager.OnServerErrorInternal$Mirror.NetworkConnection.Mirror.ErrorMessage$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1222 :8) {
^entry (%_conn : none, %_msg : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1222 :35)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1222 :35)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1222 :59)
cbde.store %_msg, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1222 :59)
br ^0

^0: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1224 :12) // Not a variable of known type: logger
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1224 :23) // "NetworkManager.OnServerErrorInternal" (StringLiteralExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1224 :12) // logger.Log("NetworkManager.OnServerErrorInternal") (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnServerError
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1225 :26) // Not a variable of known type: conn
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1225 :32) // Not a variable of known type: msg
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1225 :32) // msg.value (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1225 :12) // OnServerError(conn, msg.value) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkManager.OnClientConnectInternal$Mirror.NetworkConnection.Mirror.ConnectMessage$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1232 :8) {
^entry (%_conn : none, %_message : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1232 :37)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1232 :37)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1232 :61)
cbde.store %_message, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1232 :61)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1234 :12) // Not a variable of known type: logger
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1234 :23) // "NetworkManager.OnClientConnectInternal" (StringLiteralExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1234 :12) // logger.Log("NetworkManager.OnClientConnectInternal") (InvocationExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1236 :16) // Not a variable of known type: authenticator
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1236 :33) // null (NullLiteralExpression)
%7 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1236 :16) // comparison of unknown type: authenticator != null
cond_br %7, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1236 :16)

^1: // SimpleBlock
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1239 :16) // Not a variable of known type: authenticator
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1239 :51) // Not a variable of known type: conn
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1239 :16) // authenticator.OnClientAuthenticate(conn) (InvocationExpression)
br ^3

^2: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnClientAuthenticated
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1244 :38) // Not a variable of known type: conn
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1244 :16) // OnClientAuthenticated(conn) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function OnClientAuthenticated(none), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkManager.OnClientDisconnectInternal$Mirror.NetworkConnection.Mirror.DisconnectMessage$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1270 :8) {
^entry (%_conn : none, %_msg : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1270 :40)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1270 :40)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1270 :64)
cbde.store %_msg, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1270 :64)
br ^0

^0: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1272 :12) // Not a variable of known type: logger
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1272 :23) // "NetworkManager.OnClientDisconnectInternal" (StringLiteralExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1272 :12) // logger.Log("NetworkManager.OnClientDisconnectInternal") (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnClientDisconnect
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1273 :31) // Not a variable of known type: conn
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1273 :12) // OnClientDisconnect(conn) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkManager.OnClientNotReadyMessageInternal$Mirror.NetworkConnection.Mirror.NotReadyMessage$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1276 :8) {
^entry (%_conn : none, %_msg : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1276 :45)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1276 :45)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1276 :69)
cbde.store %_msg, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1276 :69)
br ^0

^0: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1278 :12) // Not a variable of known type: logger
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1278 :23) // "NetworkManager.OnClientNotReadyMessageInternal" (StringLiteralExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1278 :12) // logger.Log("NetworkManager.OnClientNotReadyMessageInternal") (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ClientScene
%5 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1280 :12) // ClientScene.ready (SimpleMemberAccessExpression)
%6 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1280 :32) // false
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnClientNotReady
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1281 :29) // Not a variable of known type: conn
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1281 :12) // OnClientNotReady(conn) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkManager.OnClientErrorInternal$Mirror.NetworkConnection.Mirror.ErrorMessage$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1286 :8) {
^entry (%_conn : none, %_msg : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1286 :35)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1286 :35)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1286 :59)
cbde.store %_msg, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1286 :59)
br ^0

^0: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1288 :12) // Not a variable of known type: logger
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1288 :23) // "NetworkManager:OnClientErrorInternal" (StringLiteralExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1288 :12) // logger.Log("NetworkManager:OnClientErrorInternal") (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnClientError
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1289 :26) // Not a variable of known type: conn
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1289 :32) // Not a variable of known type: msg
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1289 :32) // msg.value (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1289 :12) // OnClientError(conn, msg.value) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function OnClientSceneInternal(none, none), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkManager.OnServerConnect$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1311 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1311 :44)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1311 :44)
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkManager.OnServerDisconnect$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1318 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1318 :47)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1318 :47)
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkServer
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1320 :53) // Not a variable of known type: conn
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1320 :12) // NetworkServer.DestroyPlayerForConnection(conn) (InvocationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1321 :12) // Not a variable of known type: logger
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1321 :23) // "OnServerDisconnect: Client disconnected." (StringLiteralExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1321 :12) // logger.Log("OnServerDisconnect: Client disconnected.") (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkManager.OnServerReady$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1329 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1329 :42)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1329 :42)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1331 :16) // Not a variable of known type: conn
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1331 :16) // conn.identity (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1331 :33) // null (NullLiteralExpression)
%4 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1331 :16) // comparison of unknown type: conn.identity == null
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1331 :16)

^1: // SimpleBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1334 :16) // Not a variable of known type: logger
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1334 :27) // "Ready with no player object" (StringLiteralExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1334 :16) // logger.Log("Ready with no player object") (InvocationExpression)
br ^2

^2: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkServer
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1336 :41) // Not a variable of known type: conn
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1336 :12) // NetworkServer.SetClientReady(conn) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function OnServerAddPlayer(none), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkManager.OnServerError$Mirror.NetworkConnection.int$(none, i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1359 :8) {
^entry (%_conn : none, %_errorCode : i32):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1359 :42)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1359 :42)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1359 :66)
cbde.store %_errorCode, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1359 :66)
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkManager.OnServerChangeScene$string$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1366 :8) {
^entry (%_newSceneName : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1366 :48)
cbde.store %_newSceneName, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1366 :48)
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkManager.OnServerSceneChanged$string$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1372 :8) {
^entry (%_sceneName : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1372 :49)
cbde.store %_sceneName, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1372 :49)
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkManager.OnClientConnect$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1383 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1383 :44)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1383 :44)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1388 :17) // Not a variable of known type: clientLoadedScene
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1388 :16) // !clientLoadedScene (LogicalNotExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1388 :16)

^1: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ClientScene
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1391 :21) // ClientScene.ready (SimpleMemberAccessExpression)
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1391 :20) // !ClientScene.ready (LogicalNotExpression)
cond_br %4, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1391 :20)

^3: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ClientScene
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1391 :58) // Not a variable of known type: conn
%6 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1391 :40) // ClientScene.Ready(conn) (InvocationExpression)
br ^4

^4: // BinaryBranchBlock
%7 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1392 :20) // Not a variable of known type: autoCreatePlayer
cond_br %7, ^5, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1392 :20)

^5: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ClientScene
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1394 :42) // Not a variable of known type: conn
%9 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1394 :20) // ClientScene.AddPlayer(conn) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.NetworkManager.OnClientDisconnect$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1404 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1404 :47)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1404 :47)
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: StopClient
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1406 :12) // StopClient() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkManager.OnClientError$Mirror.NetworkConnection.int$(none, i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1414 :8) {
^entry (%_conn : none, %_errorCode : i32):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1414 :42)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1414 :42)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1414 :66)
cbde.store %_errorCode, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1414 :66)
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkManager.OnClientNotReady$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1421 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1421 :45)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1421 :45)
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkManager.OnClientChangeScene$string.Mirror.SceneOperation.bool$(none, none, i1) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1430 :8) {
^entry (%_newSceneName : none, %_sceneOperation : none, %_customHandling : i1):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1430 :48)
cbde.store %_newSceneName, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1430 :48)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1430 :69)
cbde.store %_sceneOperation, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1430 :69)
%2 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1430 :100)
cbde.store %_customHandling, %2 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1430 :100)
br ^0

^0: // ExitBlock
return

}
// Skipping function OnClientSceneChanged(none), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkManager.OnStartHost$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1462 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkManager.OnStartServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1468 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkManager.OnStartClient$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1473 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkManager.OnStopServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1478 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkManager.OnStopClient$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1483 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkManager.OnStopHost$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManager.cs" :1488 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
