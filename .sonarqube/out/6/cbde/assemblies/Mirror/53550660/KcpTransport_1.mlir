// Skipping function Awake(), it contains poisonous unsupported syntaxes

func @_kcp2k.KcpTransport.ClientConnect$string$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :85 :8) {
^entry (%_address : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :85 :43)
cbde.store %_address, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :85 :43)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :87 :12) // Not a variable of known type: client
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :87 :27) // Not a variable of known type: address
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :87 :36) // Not a variable of known type: Port
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :87 :42) // Not a variable of known type: NoDelay
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :87 :51) // Not a variable of known type: Interval
%6 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :87 :61) // Not a variable of known type: FastResend
%7 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :87 :73) // Not a variable of known type: CongestionWindow
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :87 :91) // Not a variable of known type: SendWindowSize
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :87 :107) // Not a variable of known type: ReceiveWindowSize
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :87 :12) // client.Connect(address, Port, NoDelay, Interval, FastResend, CongestionWindow, SendWindowSize, ReceiveWindowSize) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_kcp2k.KcpTransport.ClientSend$int.System.ArraySegment$byte$$(i32, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :89 :8) {
^entry (%_channelId : i32, %_segment : none):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :89 :40)
cbde.store %_channelId, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :89 :40)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :89 :55)
cbde.store %_segment, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :89 :55)
br ^0

^0: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :91 :12) // Not a variable of known type: client
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :91 :24) // Not a variable of known type: segment
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :91 :12) // client.Send(segment) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_kcp2k.KcpTransport.LateUpdate$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :100 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :107 :17) // Identifier from another assembly: enabled
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :107 :16) // !enabled (LogicalNotExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :107 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :108 :16)

^2: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :110 :12) // Not a variable of known type: server
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :110 :12) // server.Tick() (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :111 :12) // Not a variable of known type: client
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :111 :12) // client.Tick() (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_kcp2k.KcpTransport.ServerUri$$() -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :115 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :117 :33) // new UriBuilder() (ObjectCreationExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :118 :12) // Not a variable of known type: builder
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :118 :12) // builder.Scheme (SimpleMemberAccessExpression)
%4 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :118 :29) // Scheme (IdentifierName)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :119 :12) // Not a variable of known type: builder
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :119 :12) // builder.Host (SimpleMemberAccessExpression)
// Entity from another assembly: Dns
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :119 :27) // Dns.GetHostName() (InvocationExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :120 :12) // Not a variable of known type: builder
%9 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :120 :12) // builder.Port (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :120 :27) // Not a variable of known type: Port
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :121 :19) // Not a variable of known type: builder
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :121 :19) // builder.Uri (SimpleMemberAccessExpression)
return %12 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :121 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_kcp2k.KcpTransport.ServerSend$int.int.System.ArraySegment$byte$$(i32, i32, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :125 :8) {
^entry (%_connectionId : i32, %_channelId : i32, %_segment : none):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :125 :40)
cbde.store %_connectionId, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :125 :40)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :125 :58)
cbde.store %_channelId, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :125 :58)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :125 :73)
cbde.store %_segment, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :125 :73)
br ^0

^0: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :127 :12) // Not a variable of known type: server
%4 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :127 :24)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :127 :38) // Not a variable of known type: segment
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :127 :12) // server.Send(connectionId, segment) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_kcp2k.KcpTransport.ServerDisconnect$int$(i32) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :129 :8) {
^entry (%_connectionId : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :129 :46)
cbde.store %_connectionId, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :129 :46)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :131 :12) // Not a variable of known type: server
%2 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :131 :30)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :131 :12) // server.Disconnect(connectionId) (InvocationExpression)
%4 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :132 :19) // true
return %4 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :132 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_kcp2k.KcpTransport.Shutdown$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :138 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
func @_kcp2k.KcpTransport.ToString$$() -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :143 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :145 :19) // "KCP" (StringLiteralExpression)
return %0 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :145 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_kcp2k.KcpTransport.OnGUI$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :157 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :159 :17) // Not a variable of known type: debugGUI
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :159 :16) // !debugGUI (LogicalNotExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :159 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :159 :27)

^2: // BinaryBranchBlock
// Entity from another assembly: GUILayout
%2 = constant 5 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :161 :41)
%3 = constant 100 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :161 :44)
%4 = constant 300 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :161 :49)
%5 = constant 300 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :161 :54)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :161 :32) // new Rect(5, 100, 300, 300) (ObjectCreationExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :161 :12) // GUILayout.BeginArea(new Rect(5, 100, 300, 300)) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ServerActive
%8 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :163 :16) // ServerActive() (InvocationExpression)
cond_br %8, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :163 :16)

^3: // SimpleBlock
// Entity from another assembly: GUILayout
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :165 :40) // "Box" (StringLiteralExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :165 :16) // GUILayout.BeginVertical("Box") (InvocationExpression)
// Entity from another assembly: GUILayout
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :166 :32) // "SERVER" (StringLiteralExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :166 :16) // GUILayout.Label("SERVER") (InvocationExpression)
// Entity from another assembly: GUILayout
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :167 :32) // "  connections: " (StringLiteralExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :167 :52) // Not a variable of known type: server
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :167 :52) // server.connections (SimpleMemberAccessExpression)
%16 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :167 :52) // server.connections.Count (SimpleMemberAccessExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :167 :32) // Binary expression on unsupported types "  connections: " + server.connections.Count
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :167 :16) // GUILayout.Label("  connections: " + server.connections.Count) (InvocationExpression)
// Entity from another assembly: GUILayout
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :168 :32) // "  SendQueue: " (StringLiteralExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetTotalSendQueue
%20 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :168 :50) // GetTotalSendQueue() (InvocationExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :168 :32) // Binary expression on unsupported types "  SendQueue: " + GetTotalSendQueue()
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :168 :16) // GUILayout.Label("  SendQueue: " + GetTotalSendQueue()) (InvocationExpression)
// Entity from another assembly: GUILayout
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :169 :32) // "  ReceiveQueue: " (StringLiteralExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetTotalReceiveQueue
%24 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :169 :53) // GetTotalReceiveQueue() (InvocationExpression)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :169 :32) // Binary expression on unsupported types "  ReceiveQueue: " + GetTotalReceiveQueue()
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :169 :16) // GUILayout.Label("  ReceiveQueue: " + GetTotalReceiveQueue()) (InvocationExpression)
// Entity from another assembly: GUILayout
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :170 :32) // "  SendBuffer: " (StringLiteralExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetTotalSendBuffer
%28 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :170 :51) // GetTotalSendBuffer() (InvocationExpression)
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :170 :32) // Binary expression on unsupported types "  SendBuffer: " + GetTotalSendBuffer()
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :170 :16) // GUILayout.Label("  SendBuffer: " + GetTotalSendBuffer()) (InvocationExpression)
// Entity from another assembly: GUILayout
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :171 :32) // "  ReceiveBuffer: " (StringLiteralExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetTotalReceiveBuffer
%32 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :171 :54) // GetTotalReceiveBuffer() (InvocationExpression)
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :171 :32) // Binary expression on unsupported types "  ReceiveBuffer: " + GetTotalReceiveBuffer()
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :171 :16) // GUILayout.Label("  ReceiveBuffer: " + GetTotalReceiveBuffer()) (InvocationExpression)
// Entity from another assembly: GUILayout
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :172 :16) // GUILayout.EndVertical() (InvocationExpression)
br ^4

^4: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ClientConnected
%36 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :175 :16) // ClientConnected() (InvocationExpression)
cond_br %36, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :175 :16)

^5: // SimpleBlock
// Entity from another assembly: GUILayout
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :177 :40) // "Box" (StringLiteralExpression)
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :177 :16) // GUILayout.BeginVertical("Box") (InvocationExpression)
// Entity from another assembly: GUILayout
%39 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :178 :32) // "CLIENT" (StringLiteralExpression)
%40 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :178 :16) // GUILayout.Label("CLIENT") (InvocationExpression)
// Entity from another assembly: GUILayout
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :179 :32) // "  SendQueue: " (StringLiteralExpression)
%42 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :179 :50) // Not a variable of known type: client
%43 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :179 :50) // client.connection (SimpleMemberAccessExpression)
%44 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :179 :50) // client.connection.SendQueueCount (SimpleMemberAccessExpression)
%45 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :179 :32) // Binary expression on unsupported types "  SendQueue: " + client.connection.SendQueueCount
%46 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :179 :16) // GUILayout.Label("  SendQueue: " + client.connection.SendQueueCount) (InvocationExpression)
// Entity from another assembly: GUILayout
%47 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :180 :32) // "  ReceiveQueue: " (StringLiteralExpression)
%48 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :180 :53) // Not a variable of known type: client
%49 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :180 :53) // client.connection (SimpleMemberAccessExpression)
%50 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :180 :53) // client.connection.ReceiveQueueCount (SimpleMemberAccessExpression)
%51 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :180 :32) // Binary expression on unsupported types "  ReceiveQueue: " + client.connection.ReceiveQueueCount
%52 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :180 :16) // GUILayout.Label("  ReceiveQueue: " + client.connection.ReceiveQueueCount) (InvocationExpression)
// Entity from another assembly: GUILayout
%53 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :181 :32) // "  SendBuffer: " (StringLiteralExpression)
%54 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :181 :51) // Not a variable of known type: client
%55 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :181 :51) // client.connection (SimpleMemberAccessExpression)
%56 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :181 :51) // client.connection.SendBufferCount (SimpleMemberAccessExpression)
%57 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :181 :32) // Binary expression on unsupported types "  SendBuffer: " + client.connection.SendBufferCount
%58 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :181 :16) // GUILayout.Label("  SendBuffer: " + client.connection.SendBufferCount) (InvocationExpression)
// Entity from another assembly: GUILayout
%59 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :182 :32) // "  ReceiveBuffer: " (StringLiteralExpression)
%60 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :182 :54) // Not a variable of known type: client
%61 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :182 :54) // client.connection (SimpleMemberAccessExpression)
%62 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :182 :54) // client.connection.ReceiveBufferCount (SimpleMemberAccessExpression)
%63 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :182 :32) // Binary expression on unsupported types "  ReceiveBuffer: " + client.connection.ReceiveBufferCount
%64 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :182 :16) // GUILayout.Label("  ReceiveBuffer: " + client.connection.ReceiveBufferCount) (InvocationExpression)
// Entity from another assembly: GUILayout
%65 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :183 :16) // GUILayout.EndVertical() (InvocationExpression)
br ^6

^6: // SimpleBlock
// Entity from another assembly: GUILayout
%66 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\MirrorTransport\\KcpTransport.cs" :186 :12) // GUILayout.EndArea() (InvocationExpression)
br ^7

^7: // ExitBlock
return

}
