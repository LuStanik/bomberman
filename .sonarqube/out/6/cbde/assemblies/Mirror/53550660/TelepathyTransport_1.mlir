func @_Mirror.TelepathyTransport.Awake$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :40 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: Telepathy
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :43 :12) // Telepathy.Logger (SimpleMemberAccessExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :43 :12) // Telepathy.Logger.Log (SimpleMemberAccessExpression)
// Entity from another assembly: Debug
// Entity from another assembly: Telepathy
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :44 :12) // Telepathy.Logger (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :44 :12) // Telepathy.Logger.LogWarning (SimpleMemberAccessExpression)
// Entity from another assembly: Debug
// Entity from another assembly: Telepathy
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :45 :12) // Telepathy.Logger (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :45 :12) // Telepathy.Logger.LogError (SimpleMemberAccessExpression)
// Entity from another assembly: Debug
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :48 :12) // Not a variable of known type: client
%7 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :48 :12) // client.NoDelay (SimpleMemberAccessExpression)
%8 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :48 :29) // Not a variable of known type: NoDelay
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :49 :12) // Not a variable of known type: client
%10 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :49 :12) // client.MaxMessageSize (SimpleMemberAccessExpression)
%11 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :49 :36) // Not a variable of known type: clientMaxMessageSize
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :50 :12) // Not a variable of known type: server
%13 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :50 :12) // server.NoDelay (SimpleMemberAccessExpression)
%14 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :50 :29) // Not a variable of known type: NoDelay
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :51 :12) // Not a variable of known type: server
%16 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :51 :12) // server.MaxMessageSize (SimpleMemberAccessExpression)
%17 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :51 :36) // Not a variable of known type: serverMaxMessageSize
// Entity from another assembly: Debug
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :53 :22) // "TelepathyTransport initialized!" (StringLiteralExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :53 :12) // Debug.Log("TelepathyTransport initialized!") (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.TelepathyTransport.Available$$() -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :56 :8) {
^entry :
br ^0

^0: // JumpBlock
// Entity from another assembly: Application
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :59 :19) // Application.platform (SimpleMemberAccessExpression)
// Entity from another assembly: RuntimePlatform
%1 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :59 :43) // RuntimePlatform.WebGLPlayer (SimpleMemberAccessExpression)
%2 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :59 :19) // comparison of unknown type: Application.platform != RuntimePlatform.WebGLPlayer
return %2 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :59 :12)

^1: // ExitBlock
cbde.unreachable

}
// Skipping function ClientConnect(none), it contains poisonous unsupported syntaxes

func @_Mirror.TelepathyTransport.ClientSend$int.System.ArraySegment$byte$$(i32, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :73 :8) {
^entry (%_channelId : i32, %_segment : none):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :73 :40)
cbde.store %_channelId, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :73 :40)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :73 :55)
cbde.store %_segment, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :73 :55)
br ^0

^0: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :77 :35) // Not a variable of known type: segment
%3 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :77 :35) // segment.Count (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :77 :30) // byte[segment.Count] (ArrayType)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :77 :26) // new byte[segment.Count] (ArrayCreationExpression)
// Entity from another assembly: Array
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :78 :23) // Not a variable of known type: segment
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :78 :23) // segment.Array (SimpleMemberAccessExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :78 :38) // Not a variable of known type: segment
%10 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :78 :38) // segment.Offset (SimpleMemberAccessExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :78 :54) // Not a variable of known type: data
%12 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :78 :60)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :78 :63) // Not a variable of known type: segment
%14 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :78 :63) // segment.Count (SimpleMemberAccessExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :78 :12) // Array.Copy(segment.Array, segment.Offset, data, 0, segment.Count) (InvocationExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :79 :12) // Not a variable of known type: client
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :79 :24) // Not a variable of known type: data
%18 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :79 :12) // client.Send(data) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function ProcessClientMessage(), it contains poisonous unsupported syntaxes

func @_Mirror.TelepathyTransport.LateUpdate$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :114 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :119 :17) // Identifier from another assembly: enabled
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :119 :16) // !enabled (LogicalNotExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :119 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :120 :16)

^2: // ForInitializerBlock
%2 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :123 :25)
%3 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :123 :21) // i
cbde.store %2, %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :123 :21)
br ^3

^3: // BinaryBranchBlock
%4 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :123 :28)
%5 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :123 :32) // Not a variable of known type: clientMaxReceivesPerTick
%6 = cmpi "slt", %4, %5 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :123 :28)
cond_br %6, ^4, ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :123 :28)

^4: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ProcessClientMessage
%7 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :126 :21) // ProcessClientMessage() (InvocationExpression)
%8 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :126 :20) // !ProcessClientMessage() (LogicalNotExpression)
cond_br %8, ^6, ^7 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :126 :20)

^6: // JumpBlock
br ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :128 :20) // break

^7: // BinaryBranchBlock
%9 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :133 :21) // Identifier from another assembly: enabled
%10 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :133 :20) // !enabled (LogicalNotExpression)
cond_br %10, ^8, ^9 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :133 :20)

^8: // JumpBlock
br ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :135 :20) // break

^9: // SimpleBlock
%11 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :123 :60)
%12 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :123 :58)
%13 = addi %11, %12 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :123 :58)
cbde.store %13, %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :123 :58)
br ^3

^5: // ForInitializerBlock
%14 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :140 :25)
%15 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :140 :21) // i
cbde.store %14, %15 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :140 :21)
br ^10

^10: // BinaryBranchBlock
%16 = cbde.load %15 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :140 :28)
%17 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :140 :32) // Not a variable of known type: serverMaxReceivesPerTick
%18 = cmpi "slt", %16, %17 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :140 :28)
cond_br %18, ^11, ^12 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :140 :28)

^11: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ProcessServerMessage
%19 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :143 :21) // ProcessServerMessage() (InvocationExpression)
%20 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :143 :20) // !ProcessServerMessage() (LogicalNotExpression)
cond_br %20, ^13, ^14 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :143 :20)

^13: // JumpBlock
br ^12 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :145 :20) // break

^14: // BinaryBranchBlock
%21 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :150 :21) // Identifier from another assembly: enabled
%22 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :150 :20) // !enabled (LogicalNotExpression)
cond_br %22, ^15, ^16 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :150 :20)

^15: // JumpBlock
br ^12 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :152 :20) // break

^16: // SimpleBlock
%23 = cbde.load %15 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :140 :60)
%24 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :140 :58)
%25 = addi %23, %24 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :140 :58)
cbde.store %25, %15 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :140 :58)
br ^10

^12: // ExitBlock
return

}
func @_Mirror.TelepathyTransport.ServerUri$$() -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :157 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :159 :33) // new UriBuilder() (ObjectCreationExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :160 :12) // Not a variable of known type: builder
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :160 :12) // builder.Scheme (SimpleMemberAccessExpression)
%4 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :160 :29) // Scheme (IdentifierName)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :161 :12) // Not a variable of known type: builder
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :161 :12) // builder.Host (SimpleMemberAccessExpression)
// Entity from another assembly: Dns
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :161 :27) // Dns.GetHostName() (InvocationExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :162 :12) // Not a variable of known type: builder
%9 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :162 :12) // builder.Port (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :162 :27) // Not a variable of known type: port
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :163 :19) // Not a variable of known type: builder
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :163 :19) // builder.Uri (SimpleMemberAccessExpression)
return %12 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :163 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.TelepathyTransport.ServerSend$int.int.System.ArraySegment$byte$$(i32, i32, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :169 :8) {
^entry (%_connectionId : i32, %_channelId : i32, %_segment : none):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :169 :40)
cbde.store %_connectionId, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :169 :40)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :169 :58)
cbde.store %_channelId, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :169 :58)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :169 :73)
cbde.store %_segment, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :169 :73)
br ^0

^0: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :173 :35) // Not a variable of known type: segment
%4 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :173 :35) // segment.Count (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :173 :30) // byte[segment.Count] (ArrayType)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :173 :26) // new byte[segment.Count] (ArrayCreationExpression)
// Entity from another assembly: Array
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :174 :23) // Not a variable of known type: segment
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :174 :23) // segment.Array (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :174 :38) // Not a variable of known type: segment
%11 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :174 :38) // segment.Offset (SimpleMemberAccessExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :174 :54) // Not a variable of known type: data
%13 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :174 :60)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :174 :63) // Not a variable of known type: segment
%15 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :174 :63) // segment.Count (SimpleMemberAccessExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :174 :12) // Array.Copy(segment.Array, segment.Offset, data, 0, segment.Count) (InvocationExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :177 :12) // Not a variable of known type: server
%18 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :177 :24)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :177 :38) // Not a variable of known type: data
%20 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :177 :12) // server.Send(connectionId, data) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function ProcessServerMessage(), it contains poisonous unsupported syntaxes

// Skipping function ServerGetClientAddress(i32), it contains poisonous unsupported syntaxes

func @_Mirror.TelepathyTransport.Shutdown$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :226 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: Debug
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :228 :22) // "TelepathyTransport Shutdown()" (StringLiteralExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :228 :12) // Debug.Log("TelepathyTransport Shutdown()") (InvocationExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :229 :12) // Not a variable of known type: client
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :229 :12) // client.Disconnect() (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :230 :12) // Not a variable of known type: server
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :230 :12) // server.Stop() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.TelepathyTransport.GetMaxPacketSize$int$(i32) -> i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :233 :8) {
^entry (%_channelId : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :233 :45)
cbde.store %_channelId, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :233 :45)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :235 :19) // Not a variable of known type: serverMaxMessageSize
return %1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\TelepathyTransport.cs" :235 :12)

^1: // ExitBlock
cbde.unreachable

}
// Skipping function ToString(), it contains poisonous unsupported syntaxes

