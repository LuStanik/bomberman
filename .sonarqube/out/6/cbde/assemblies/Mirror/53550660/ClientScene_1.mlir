func @_Mirror.ClientScene.Shutdown$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :75 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ClearSpawners
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :77 :12) // ClearSpawners() (InvocationExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :78 :12) // Not a variable of known type: spawnableObjects
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :78 :12) // spawnableObjects.Clear() (InvocationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :79 :30) // null (NullLiteralExpression)
%4 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :80 :20) // false
%5 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :81 :30) // false
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: DestroyAllClientObjects
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :82 :12) // DestroyAllClientObjects() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.ClientScene.InternalAddPlayer$Mirror.NetworkIdentity$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :89 :8) {
^entry (%_identity : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :89 :47)
cbde.store %_identity, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :89 :47)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :91 :12) // Not a variable of known type: logger
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :91 :23) // "ClientScene.InternalAddPlayer" (StringLiteralExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :91 :12) // logger.Log("ClientScene.InternalAddPlayer") (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :95 :26) // Not a variable of known type: identity
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :101 :16) // Not a variable of known type: readyConnection
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :101 :35) // null (NullLiteralExpression)
%7 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :101 :16) // comparison of unknown type: readyConnection != null
cond_br %7, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :101 :16)

^1: // SimpleBlock
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :103 :16) // Not a variable of known type: readyConnection
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :103 :16) // readyConnection.identity (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :103 :43) // Not a variable of known type: identity
br ^3

^2: // SimpleBlock
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :107 :16) // Not a variable of known type: logger
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :107 :34) // "No ready connection found for setting player controller during InternalAddPlayer" (StringLiteralExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :107 :16) // logger.LogWarning("No ready connection found for setting player controller during InternalAddPlayer") (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.ClientScene.ClearLocalPlayer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :115 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :117 :12) // Not a variable of known type: logger
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :117 :23) // "ClientScene.ClearLocalPlayer" (StringLiteralExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :117 :12) // logger.Log("ClientScene.ClearLocalPlayer") (InvocationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :119 :26) // null (NullLiteralExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.ClientScene.AddPlayer$Mirror.NetworkConnection$(none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :127 :8) {
^entry (%_readyConn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :127 :37)
cbde.store %_readyConn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :127 :37)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :130 :16) // Not a variable of known type: readyConn
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :130 :29) // null (NullLiteralExpression)
%3 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :130 :16) // comparison of unknown type: readyConn != null
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :130 :16)

^1: // SimpleBlock
%4 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :132 :24) // true
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :133 :34) // Not a variable of known type: readyConn
br ^2

^2: // BinaryBranchBlock
%6 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :136 :17) // Not a variable of known type: ready
%7 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :136 :16) // !ready (LogicalNotExpression)
cond_br %7, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :136 :16)

^3: // JumpBlock
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :138 :16) // Not a variable of known type: logger
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :138 :32) // "Must call AddPlayer() with a connection the first time to become ready." (StringLiteralExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :138 :16) // logger.LogError("Must call AddPlayer() with a connection the first time to become ready.") (InvocationExpression)
%11 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :139 :23) // false
return %11 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :139 :16)

^4: // BinaryBranchBlock
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :142 :16) // Not a variable of known type: readyConnection
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :142 :16) // readyConnection.identity (SimpleMemberAccessExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :142 :44) // null (NullLiteralExpression)
%15 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :142 :16) // comparison of unknown type: readyConnection.identity != null
cond_br %15, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :142 :16)

^5: // JumpBlock
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :144 :16) // Not a variable of known type: logger
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :144 :32) // "ClientScene.AddPlayer: a PlayerController was already added. Did you call AddPlayer twice?" (StringLiteralExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :144 :16) // logger.LogError("ClientScene.AddPlayer: a PlayerController was already added. Did you call AddPlayer twice?") (InvocationExpression)
%19 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :145 :23) // false
return %19 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :145 :16)

^6: // BinaryBranchBlock
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :148 :16) // Not a variable of known type: logger
%21 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :148 :16) // logger.LogEnabled() (InvocationExpression)
cond_br %21, ^7, ^8 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :148 :16)

^7: // SimpleBlock
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :148 :37) // Not a variable of known type: logger
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :148 :48) // "ClientScene.AddPlayer() called with connection [" (StringLiteralExpression)
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :148 :101) // Not a variable of known type: readyConnection
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :148 :48) // Binary expression on unsupported types "ClientScene.AddPlayer() called with connection [" + readyConnection
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :148 :119) // "]" (StringLiteralExpression)
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :148 :48) // Binary expression on unsupported types "ClientScene.AddPlayer() called with connection [" + readyConnection + "]"
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :148 :37) // logger.Log("ClientScene.AddPlayer() called with connection [" + readyConnection + "]") (InvocationExpression)
br ^8

^8: // JumpBlock
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :150 :12) // Not a variable of known type: readyConnection
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :150 :33) // new AddPlayerMessage() (ObjectCreationExpression)
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :150 :12) // readyConnection.Send(new AddPlayerMessage()) (InvocationExpression)
%32 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :151 :19) // true
return %32 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :151 :12)

^9: // ExitBlock
cbde.unreachable

}
func @_Mirror.ClientScene.Ready$Mirror.NetworkConnection$(none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :160 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :160 :33)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :160 :33)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :162 :16) // Not a variable of known type: ready
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :162 :16)

^1: // JumpBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :164 :16) // Not a variable of known type: logger
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :164 :32) // "A connection has already been set as ready. There can only be one." (StringLiteralExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :164 :16) // logger.LogError("A connection has already been set as ready. There can only be one.") (InvocationExpression)
%5 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :165 :23) // false
return %5 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :165 :16)

^2: // BinaryBranchBlock
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :168 :16) // Not a variable of known type: logger
%7 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :168 :16) // logger.LogEnabled() (InvocationExpression)
cond_br %7, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :168 :16)

^3: // SimpleBlock
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :168 :37) // Not a variable of known type: logger
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :168 :48) // "ClientScene.Ready() called with connection [" (StringLiteralExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :168 :97) // Not a variable of known type: conn
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :168 :48) // Binary expression on unsupported types "ClientScene.Ready() called with connection [" + conn
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :168 :104) // "]" (StringLiteralExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :168 :48) // Binary expression on unsupported types "ClientScene.Ready() called with connection [" + conn + "]"
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :168 :37) // logger.Log("ClientScene.Ready() called with connection [" + conn + "]") (InvocationExpression)
br ^4

^4: // BinaryBranchBlock
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :170 :16) // Not a variable of known type: conn
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :170 :24) // null (NullLiteralExpression)
%17 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :170 :16) // comparison of unknown type: conn != null
cond_br %17, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :170 :16)

^5: // JumpBlock
%18 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :174 :24) // true
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :175 :34) // Not a variable of known type: conn
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :176 :16) // Not a variable of known type: readyConnection
%21 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :176 :16) // readyConnection.isReady (SimpleMemberAccessExpression)
%22 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :176 :42) // true
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :179 :16) // Not a variable of known type: conn
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :179 :26) // new ReadyMessage() (ObjectCreationExpression)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :179 :16) // conn.Send(new ReadyMessage()) (InvocationExpression)
%26 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :181 :23) // true
return %26 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :181 :16)

^6: // JumpBlock
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :183 :12) // Not a variable of known type: logger
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :183 :28) // "Ready() called with invalid connection object: conn=null" (StringLiteralExpression)
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :183 :12) // logger.LogError("Ready() called with invalid connection object: conn=null") (InvocationExpression)
%30 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :184 :19) // false
return %30 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :184 :12)

^7: // ExitBlock
cbde.unreachable

}
// Skipping function HandleClientDisconnect(none), it contains poisonous unsupported syntaxes

// Skipping function ConsiderForSpawning(none), it contains poisonous unsupported syntaxes

// Skipping function PrepareToSpawnSceneObjects(), it contains poisonous unsupported syntaxes

// Skipping function GetPrefab(none, none), it contains poisonous unsupported syntaxes

// Skipping function RegisterPrefabIdentity(none), it contains poisonous unsupported syntaxes

// Skipping function RegisterPrefab(none, none), it contains poisonous unsupported syntaxes

func @_Mirror.ClientScene.RegisterPrefab$UnityEngine.GameObject$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :330 :8) {
^entry (%_prefab : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :330 :42)
cbde.store %_prefab, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :330 :42)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :332 :16) // Not a variable of known type: prefab
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :332 :26) // null (NullLiteralExpression)
%3 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :332 :16) // comparison of unknown type: prefab == null
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :332 :16)

^1: // JumpBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :334 :16) // Not a variable of known type: logger
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :334 :32) // "Could not register prefab because it was null" (StringLiteralExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :334 :16) // logger.LogError("Could not register prefab because it was null") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :335 :16)

^2: // BinaryBranchBlock
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :338 :39) // Not a variable of known type: prefab
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :338 :39) // prefab.GetComponent<NetworkIdentity>() (InvocationExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :339 :16) // Not a variable of known type: identity
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :339 :28) // null (NullLiteralExpression)
%12 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :339 :16) // comparison of unknown type: identity == null
cond_br %12, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :339 :16)

^3: // JumpBlock
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :341 :16) // Not a variable of known type: logger
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :341 :55) // Not a variable of known type: prefab
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :341 :55) // prefab.name (SimpleMemberAccessExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :341 :32) // $"Could not register '{prefab.name}' since it contains no NetworkIdentity component" (InterpolatedStringExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :341 :16) // logger.LogError($"Could not register '{prefab.name}' since it contains no NetworkIdentity component") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :342 :16)

^4: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: RegisterPrefabIdentity
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :345 :35) // Not a variable of known type: identity
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :345 :12) // RegisterPrefabIdentity(identity) (InvocationExpression)
br ^5

^5: // ExitBlock
return

}
// Skipping function RegisterPrefab(none, none, none, none), it contains poisonous unsupported syntaxes

// Skipping function RegisterPrefab(none, none, none), it contains poisonous unsupported syntaxes

// Skipping function RegisterPrefab(none, none, none, none), it contains poisonous unsupported syntaxes

// Skipping function RegisterPrefab(none, none, none), it contains poisonous unsupported syntaxes

func @_Mirror.ClientScene.UnregisterPrefab$UnityEngine.GameObject$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :578 :8) {
^entry (%_prefab : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :578 :44)
cbde.store %_prefab, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :578 :44)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :580 :16) // Not a variable of known type: prefab
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :580 :26) // null (NullLiteralExpression)
%3 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :580 :16) // comparison of unknown type: prefab == null
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :580 :16)

^1: // JumpBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :582 :16) // Not a variable of known type: logger
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :582 :32) // "Could not unregister prefab because it was null" (StringLiteralExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :582 :16) // logger.LogError("Could not unregister prefab because it was null") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :583 :16)

^2: // BinaryBranchBlock
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :586 :39) // Not a variable of known type: prefab
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :586 :39) // prefab.GetComponent<NetworkIdentity>() (InvocationExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :587 :16) // Not a variable of known type: identity
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :587 :28) // null (NullLiteralExpression)
%12 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :587 :16) // comparison of unknown type: identity == null
cond_br %12, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :587 :16)

^3: // JumpBlock
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :589 :16) // Not a variable of known type: logger
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :589 :32) // "Could not unregister '" (StringLiteralExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :589 :59) // Not a variable of known type: prefab
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :589 :59) // prefab.name (SimpleMemberAccessExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :589 :32) // Binary expression on unsupported types "Could not unregister '" + prefab.name
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :589 :73) // "' since it contains no NetworkIdentity component" (StringLiteralExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :589 :32) // Binary expression on unsupported types "Could not unregister '" + prefab.name + "' since it contains no NetworkIdentity component"
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :589 :16) // logger.LogError("Could not unregister '" + prefab.name + "' since it contains no NetworkIdentity component") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :590 :16)

^4: // SimpleBlock
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :593 :27) // Not a variable of known type: identity
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :593 :27) // identity.assetId (SimpleMemberAccessExpression)
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :595 :12) // Not a variable of known type: prefabs
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :595 :27) // Not a variable of known type: assetId
%26 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :595 :12) // prefabs.Remove(assetId) (InvocationExpression)
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :596 :12) // Not a variable of known type: spawnHandlers
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :596 :33) // Not a variable of known type: assetId
%29 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :596 :12) // spawnHandlers.Remove(assetId) (InvocationExpression)
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :597 :12) // Not a variable of known type: unspawnHandlers
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :597 :35) // Not a variable of known type: assetId
%32 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :597 :12) // unspawnHandlers.Remove(assetId) (InvocationExpression)
br ^5

^5: // ExitBlock
return

}
// Skipping function RegisterSpawnHandler(none, none, none), it contains poisonous unsupported syntaxes

// Skipping function RegisterSpawnHandler(none, none, none), it contains poisonous unsupported syntaxes

func @_Mirror.ClientScene.UnregisterSpawnHandler$System.Guid$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :667 :8) {
^entry (%_assetId : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :667 :50)
cbde.store %_assetId, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :667 :50)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :669 :12) // Not a variable of known type: spawnHandlers
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :669 :33) // Not a variable of known type: assetId
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :669 :12) // spawnHandlers.Remove(assetId) (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :670 :12) // Not a variable of known type: unspawnHandlers
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :670 :35) // Not a variable of known type: assetId
%6 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :670 :12) // unspawnHandlers.Remove(assetId) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.ClientScene.ClearSpawners$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :676 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :678 :12) // Not a variable of known type: prefabs
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :678 :12) // prefabs.Clear() (InvocationExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :679 :12) // Not a variable of known type: spawnHandlers
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :679 :12) // spawnHandlers.Clear() (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :680 :12) // Not a variable of known type: unspawnHandlers
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :680 :12) // unspawnHandlers.Clear() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function InvokeUnSpawnHandler(none, none), it contains poisonous unsupported syntaxes

// Skipping function DestroyAllClientObjects(), it contains poisonous unsupported syntaxes

// Skipping function ApplySpawnPayload(none, none), it contains poisonous unsupported syntaxes

// Skipping function OnSpawn(none), it contains poisonous unsupported syntaxes

// Skipping function FindOrSpawnObject(none, none), it contains poisonous unsupported syntaxes

// Skipping function GetExistingObject(none), it contains poisonous unsupported syntaxes

// Skipping function SpawnPrefab(none), it contains poisonous unsupported syntaxes

// Skipping function SpawnSceneObject(none), it contains poisonous unsupported syntaxes

// Skipping function GetAndRemoveSceneObject(none), it contains poisonous unsupported syntaxes

func @_Mirror.ClientScene.OnObjectSpawnStarted$Mirror.ObjectSpawnStartedMessage$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :886 :8) {
^entry (%__ : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :886 :50)
cbde.store %__, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :886 :50)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :888 :16) // Not a variable of known type: logger
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :888 :16) // logger.LogEnabled() (InvocationExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :888 :16)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :888 :37) // Not a variable of known type: logger
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :888 :48) // "SpawnStarted" (StringLiteralExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :888 :37) // logger.Log("SpawnStarted") (InvocationExpression)
br ^2

^2: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: PrepareToSpawnSceneObjects
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :890 :12) // PrepareToSpawnSceneObjects() (InvocationExpression)
%7 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :891 :30) // false
br ^3

^3: // ExitBlock
return

}
// Skipping function OnObjectSpawnFinished(none), it contains poisonous unsupported syntaxes

// Skipping function ClearNullFromSpawned(), it contains poisonous unsupported syntaxes

func @_Mirror.ClientScene.OnObjectHide$Mirror.ObjectHideMessage$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :935 :8) {
^entry (%_msg : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :935 :42)
cbde.store %_msg, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :935 :42)
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: DestroyObject
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :937 :26) // Not a variable of known type: msg
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :937 :26) // msg.netId (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :937 :12) // DestroyObject(msg.netId) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.ClientScene.OnObjectDestroy$Mirror.ObjectDestroyMessage$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :940 :8) {
^entry (%_msg : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :940 :45)
cbde.store %_msg, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :940 :45)
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: DestroyObject
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :942 :26) // Not a variable of known type: msg
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :942 :26) // msg.netId (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :942 :12) // DestroyObject(msg.netId) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function DestroyObject(none), it contains poisonous unsupported syntaxes

func @_Mirror.ClientScene.OnHostClientObjectDestroy$Mirror.ObjectDestroyMessage$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :983 :8) {
^entry (%_msg : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :983 :55)
cbde.store %_msg, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :983 :55)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :985 :16) // Not a variable of known type: logger
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :985 :16) // logger.LogEnabled() (InvocationExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :985 :16)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :985 :37) // Not a variable of known type: logger
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :985 :48) // "ClientScene.OnLocalObjectObjDestroy netId:" (StringLiteralExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :985 :95) // Not a variable of known type: msg
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :985 :95) // msg.netId (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :985 :48) // Binary expression on unsupported types "ClientScene.OnLocalObjectObjDestroy netId:" + msg.netId
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :985 :37) // logger.Log("ClientScene.OnLocalObjectObjDestroy netId:" + msg.netId) (InvocationExpression)
br ^2

^2: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkIdentity
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :987 :12) // NetworkIdentity.spawned (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :987 :43) // Not a variable of known type: msg
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :987 :43) // msg.netId (SimpleMemberAccessExpression)
%12 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :987 :12) // NetworkIdentity.spawned.Remove(msg.netId) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function OnHostClientObjectHide(none), it contains poisonous unsupported syntaxes

// Skipping function OnHostClientSpawn(none), it contains poisonous unsupported syntaxes

// Skipping function OnUpdateVarsMessage(none), it contains poisonous unsupported syntaxes

// Skipping function OnRPCMessage(none), it contains poisonous unsupported syntaxes

func @_Mirror.ClientScene.CheckForLocalPlayer$Mirror.NetworkIdentity$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :1041 :8) {
^entry (%_identity : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :1041 :40)
cbde.store %_identity, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :1041 :40)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :1043 :16) // Not a variable of known type: identity
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :1043 :28) // Not a variable of known type: localPlayer
%3 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :1043 :16) // comparison of unknown type: identity == localPlayer
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :1043 :16)

^1: // BinaryBranchBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :1046 :16) // Not a variable of known type: identity
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :1046 :16) // identity.connectionToServer (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :1046 :46) // Not a variable of known type: readyConnection
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :1047 :16) // Not a variable of known type: identity
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :1047 :16) // identity.OnStartLocalPlayer() (InvocationExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :1049 :20) // Not a variable of known type: logger
%10 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :1049 :20) // logger.LogEnabled() (InvocationExpression)
cond_br %10, ^3, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :1049 :20)

^3: // SimpleBlock
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :1049 :41) // Not a variable of known type: logger
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :1049 :52) // "ClientScene.OnOwnerMessage - player=" (StringLiteralExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :1049 :93) // Not a variable of known type: identity
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :1049 :93) // identity.name (SimpleMemberAccessExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :1049 :52) // Binary expression on unsupported types "ClientScene.OnOwnerMessage - player=" + identity.name
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ClientScene.cs" :1049 :41) // logger.Log("ClientScene.OnOwnerMessage - player=" + identity.name) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
