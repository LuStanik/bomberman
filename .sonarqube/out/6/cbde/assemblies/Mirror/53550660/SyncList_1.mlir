func @_Mirror.SyncList$T$.Reset$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :92 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :94 :25) // false
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :95 :12) // Not a variable of known type: changes
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :95 :12) // changes.Clear() (InvocationExpression)
%3 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :96 :27)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :97 :12) // Not a variable of known type: objects
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :97 :12) // objects.Clear() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function AddOperation(none, i32, none, none), it contains poisonous unsupported syntaxes

func @_Mirror.SyncList$T$.OnSerializeAll$Mirror.NetworkWriter$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :119 :8) {
^entry (%_writer : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :119 :35)
cbde.store %_writer, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :119 :35)
br ^0

^0: // ForInitializerBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :122 :12) // Not a variable of known type: writer
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :122 :37) // Not a variable of known type: objects
%3 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :122 :37) // objects.Count (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :122 :31) // (uint)objects.Count (CastExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :122 :12) // writer.WriteUInt32((uint)objects.Count) (InvocationExpression)
%6 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :124 :25)
%7 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :124 :21) // i
cbde.store %6, %7 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :124 :21)
br ^1

^1: // BinaryBranchBlock
%8 = cbde.load %7 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :124 :28)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :124 :32) // Not a variable of known type: objects
%10 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :124 :32) // objects.Count (SimpleMemberAccessExpression)
%11 = cmpi "slt", %8, %10 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :124 :28)
cond_br %11, ^2, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :124 :28)

^2: // SimpleBlock
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :126 :24) // Not a variable of known type: objects
%13 = cbde.load %7 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :126 :32)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :126 :24) // objects[i] (ElementAccessExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :127 :16) // Not a variable of known type: writer
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :127 :29) // Not a variable of known type: obj
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :127 :16) // writer.Write(obj) (InvocationExpression)
br ^4

^4: // SimpleBlock
%19 = cbde.load %7 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :124 :47)
%20 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :124 :47)
%21 = addi %19, %20 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :124 :47)
cbde.store %21, %7 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :124 :47)
br ^1

^3: // SimpleBlock
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :134 :12) // Not a variable of known type: writer
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :134 :37) // Not a variable of known type: changes
%24 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :134 :37) // changes.Count (SimpleMemberAccessExpression)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :134 :31) // (uint)changes.Count (CastExpression)
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :134 :12) // writer.WriteUInt32((uint)changes.Count) (InvocationExpression)
br ^5

^5: // ExitBlock
return

}
// Skipping function OnSerializeDelta(none), it contains poisonous unsupported syntaxes

func @_Mirror.SyncList$T$.OnDeserializeAll$Mirror.NetworkReader$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :169 :8) {
^entry (%_reader : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :169 :37)
cbde.store %_reader, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :169 :37)
br ^0

^0: // ForInitializerBlock
%1 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :172 :25) // true
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :175 :29) // Not a variable of known type: reader
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :175 :29) // reader.ReadUInt32() (InvocationExpression)
%4 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :175 :24) // (int)reader.ReadUInt32() (CastExpression)
%5 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :175 :16) // count
cbde.store %4, %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :175 :16)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :177 :12) // Not a variable of known type: objects
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :177 :12) // objects.Clear() (InvocationExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :178 :12) // Not a variable of known type: changes
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :178 :12) // changes.Clear() (InvocationExpression)
%10 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :180 :25)
%11 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :180 :21) // i
cbde.store %10, %11 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :180 :21)
br ^1

^1: // BinaryBranchBlock
%12 = cbde.load %11 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :180 :28)
%13 = cbde.load %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :180 :32)
%14 = cmpi "slt", %12, %13 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :180 :28)
cond_br %14, ^2, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :180 :28)

^2: // SimpleBlock
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :182 :24) // Not a variable of known type: reader
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :182 :24) // reader.Read<T>() (InvocationExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :183 :16) // Not a variable of known type: objects
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :183 :28) // Not a variable of known type: obj
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :183 :16) // objects.Add(obj) (InvocationExpression)
br ^4

^4: // SimpleBlock
%21 = cbde.load %11 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :180 :39)
%22 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :180 :39)
%23 = addi %21, %22 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :180 :39)
cbde.store %23, %11 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :180 :39)
br ^1

^3: // SimpleBlock
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :189 :32) // Not a variable of known type: reader
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :189 :32) // reader.ReadUInt32() (InvocationExpression)
%26 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :189 :27) // (int)reader.ReadUInt32() (CastExpression)
br ^5

^5: // ExitBlock
return

}
// Skipping function OnDeserializeDelta(none), it contains poisonous unsupported syntaxes

// Skipping function Add(none), it contains poisonous unsupported syntaxes

// Skipping function AddRange(none), it contains poisonous unsupported syntaxes

// Skipping function Clear(), it contains poisonous unsupported syntaxes

func @_Mirror.SyncList$T$.IndexOf$T$(none) -> i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :293 :8) {
^entry (%_item : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :293 :27)
cbde.store %_item, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :293 :27)
br ^0

^0: // ForInitializerBlock
%1 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :295 :25)
%2 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :295 :21) // i
cbde.store %1, %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :295 :21)
br ^1

^1: // BinaryBranchBlock
%3 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :295 :28)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :295 :32) // Not a variable of known type: objects
%5 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :295 :32) // objects.Count (SimpleMemberAccessExpression)
%6 = cmpi "slt", %3, %5 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :295 :28)
cond_br %6, ^2, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :295 :28)

^2: // BinaryBranchBlock
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :296 :20) // Not a variable of known type: comparer
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :296 :36) // Not a variable of known type: item
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :296 :42) // Not a variable of known type: objects
%10 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :296 :50)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :296 :42) // objects[i] (ElementAccessExpression)
%12 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :296 :20) // comparer.Equals(item, objects[i]) (InvocationExpression)
cond_br %12, ^4, ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :296 :20)

^4: // JumpBlock
%13 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :297 :27)
return %13 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :297 :20)

^5: // SimpleBlock
%14 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :295 :49)
%15 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :295 :47)
%16 = addi %14, %15 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :295 :47)
cbde.store %16, %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :295 :47)
br ^1

^3: // JumpBlock
%17 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :298 :20)
%18 = cbde.neg %17 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :298 :19)
return %18 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :298 :12)

^6: // ExitBlock
cbde.unreachable

}
func @_Mirror.SyncList$T$.FindIndex$System.Predicate$T$$(none) -> i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :301 :8) {
^entry (%_match : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :301 :29)
cbde.store %_match, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :301 :29)
br ^0

^0: // ForInitializerBlock
%1 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :303 :25)
%2 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :303 :21) // i
cbde.store %1, %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :303 :21)
br ^1

^1: // BinaryBranchBlock
%3 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :303 :28)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :303 :32) // Not a variable of known type: objects
%5 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :303 :32) // objects.Count (SimpleMemberAccessExpression)
%6 = cmpi "slt", %3, %5 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :303 :28)
cond_br %6, ^2, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :303 :28)

^2: // BinaryBranchBlock
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :304 :20) // Not a variable of known type: match
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :304 :26) // Not a variable of known type: objects
%9 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :304 :34)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :304 :26) // objects[i] (ElementAccessExpression)
%11 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :304 :20) // match(objects[i]) (InvocationExpression)
cond_br %11, ^4, ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :304 :20)

^4: // JumpBlock
%12 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :305 :27)
return %12 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :305 :20)

^5: // SimpleBlock
%13 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :303 :49)
%14 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :303 :47)
%15 = addi %13, %14 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :303 :47)
cbde.store %15, %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :303 :47)
br ^1

^3: // JumpBlock
%16 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :306 :20)
%17 = cbde.neg %16 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :306 :19)
return %17 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :306 :12)

^6: // ExitBlock
cbde.unreachable

}
// Skipping function Find(none), it contains poisonous unsupported syntaxes

func @_Mirror.SyncList$T$.FindAll$System.Predicate$T$$(none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :315 :8) {
^entry (%_match : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :315 :31)
cbde.store %_match, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :315 :31)
br ^0

^0: // ForInitializerBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :317 :30) // new List<T>() (ObjectCreationExpression)
%3 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :318 :25)
%4 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :318 :21) // i
cbde.store %3, %4 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :318 :21)
br ^1

^1: // BinaryBranchBlock
%5 = cbde.load %4 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :318 :28)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :318 :32) // Not a variable of known type: objects
%7 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :318 :32) // objects.Count (SimpleMemberAccessExpression)
%8 = cmpi "slt", %5, %7 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :318 :28)
cond_br %8, ^2, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :318 :28)

^2: // BinaryBranchBlock
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :319 :20) // Not a variable of known type: match
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :319 :26) // Not a variable of known type: objects
%11 = cbde.load %4 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :319 :34)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :319 :26) // objects[i] (ElementAccessExpression)
%13 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :319 :20) // match(objects[i]) (InvocationExpression)
cond_br %13, ^4, ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :319 :20)

^4: // SimpleBlock
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :320 :20) // Not a variable of known type: results
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :320 :32) // Not a variable of known type: objects
%16 = cbde.load %4 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :320 :40)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :320 :32) // objects[i] (ElementAccessExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :320 :20) // results.Add(objects[i]) (InvocationExpression)
br ^5

^5: // SimpleBlock
%19 = cbde.load %4 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :318 :49)
%20 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :318 :47)
%21 = addi %19, %20 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :318 :47)
cbde.store %21, %4 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :318 :47)
br ^1

^3: // JumpBlock
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :321 :19) // Not a variable of known type: results
return %22 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :321 :12)

^6: // ExitBlock
cbde.unreachable

}
// Skipping function Insert(i32, none), it contains poisonous unsupported syntaxes

// Skipping function InsertRange(i32, none), it contains poisonous unsupported syntaxes

func @_Mirror.SyncList$T$.Remove$T$(none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :339 :8) {
^entry (%_item : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :339 :27)
cbde.store %_item, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :339 :27)
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: IndexOf
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :341 :32) // Not a variable of known type: item
%2 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :341 :24) // IndexOf(item) (InvocationExpression)
%3 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :341 :16) // index
cbde.store %2, %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :341 :16)
%4 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :342 :26)
%5 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :342 :35)
%6 = cmpi "sge", %4, %5 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :342 :26)
%7 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :342 :17) // result
cbde.store %6, %7 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :342 :17)
%8 = cbde.load %7 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :343 :16)
cond_br %8, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :343 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: RemoveAt
%9 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :345 :25)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :345 :16) // RemoveAt(index) (InvocationExpression)
br ^2

^2: // JumpBlock
%11 = cbde.load %7 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :347 :19)
return %11 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :347 :12)

^3: // ExitBlock
cbde.unreachable

}
// Skipping function RemoveAt(i32), it contains poisonous unsupported syntaxes

// Skipping function RemoveAll(none), it contains poisonous unsupported syntaxes

func @_Mirror.SyncList$T$.Enumerator.MoveNext$$() -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :415 :12) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :417 :22) // Not a variable of known type: index
%1 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :417 :20) // Inc/Decrement of field or property index
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :417 :31) // Not a variable of known type: list
%3 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :417 :31) // list.Count (SimpleMemberAccessExpression)
%4 = cmpi "sge", %1, %3 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :417 :20)
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :417 :20)

^1: // JumpBlock
%5 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :419 :27) // false
return %5 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :419 :20)

^2: // JumpBlock
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :421 :26) // Not a variable of known type: list
%7 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :421 :31) // Not a variable of known type: index
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :421 :26) // list[index] (ElementAccessExpression)
%9 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :422 :23) // true
return %9 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :422 :16)

^3: // ExitBlock
cbde.unreachable

}
func @_Mirror.SyncList$T$.Enumerator.Dispose$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncList.cs" :427 :12) {
^entry :
br ^0

^0: // ExitBlock
return

}
