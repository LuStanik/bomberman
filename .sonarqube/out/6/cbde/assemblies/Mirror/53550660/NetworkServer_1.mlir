func @_Mirror.NetworkServer.Shutdown$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :77 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :79 :16) // Not a variable of known type: initialized
cond_br %0, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :79 :16)

^1: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: DisconnectAll
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :81 :16) // DisconnectAll() (InvocationExpression)
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :83 :21) // Not a variable of known type: dontListen
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :83 :20) // !dontListen (LogicalNotExpression)
cond_br %3, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :83 :20)

^3: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Transport
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :89 :20) // Transport.activeTransport (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :89 :20) // Transport.activeTransport.ServerStop() (InvocationExpression)
br ^4

^4: // SimpleBlock
%6 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :92 :30) // false
br ^2

^2: // SimpleBlock
%7 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :94 :25) // false
%8 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :95 :21) // false
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :96 :12) // Not a variable of known type: handlers
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :96 :12) // handlers.Clear() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CleanupNetworkIdentities
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :98 :12) // CleanupNetworkIdentities() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkIdentity
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :99 :12) // NetworkIdentity.ResetNextNetworkId() (InvocationExpression)
br ^5

^5: // ExitBlock
return

}
// Skipping function CleanupNetworkIdentities(), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkServer.Initialize$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :123 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :125 :16) // Not a variable of known type: initialized
cond_br %0, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :125 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :126 :16)

^2: // BinaryBranchBlock
%1 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :128 :26) // true
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :129 :16) // Not a variable of known type: logger
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :129 :16) // logger.LogEnabled() (InvocationExpression)
cond_br %3, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :129 :16)

^3: // SimpleBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :129 :37) // Not a variable of known type: logger
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :129 :48) // "NetworkServer Created version " (StringLiteralExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :129 :83) // Not a variable of known type: Version
%7 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :129 :83) // Version.Current (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :129 :48) // Binary expression on unsupported types "NetworkServer Created version " + Version.Current
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :129 :37) // logger.Log("NetworkServer Created version " + Version.Current) (InvocationExpression)
br ^4

^4: // SimpleBlock
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :132 :12) // Not a variable of known type: connections
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :132 :12) // connections.Clear() (InvocationExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :134 :12) // Not a variable of known type: logger
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Transport
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :134 :26) // Transport.activeTransport (SimpleMemberAccessExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :134 :55) // null (NullLiteralExpression)
%15 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :134 :26) // comparison of unknown type: Transport.activeTransport != null
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :134 :61) // "There was no active transport when calling NetworkServer.Listen, If you are calling Listen manually then make sure to set 'Transport.activeTransport' first" (StringLiteralExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :134 :12) // logger.Assert(Transport.activeTransport != null, "There was no active transport when calling NetworkServer.Listen, If you are calling Listen manually then make sure to set 'Transport.activeTransport' first") (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: AddTransportHandlers
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :135 :12) // AddTransportHandlers() (InvocationExpression)
br ^5

^5: // ExitBlock
return

}
func @_Mirror.NetworkServer.RegisterMessageHandlers$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :138 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnClientReadyMessage
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :140 :12) // RegisterHandler<ReadyMessage>(OnClientReadyMessage) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnCommandMessage
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :141 :12) // RegisterHandler<CommandMessage>(OnCommandMessage) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkTime
%2 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :142 :74) // false
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :142 :12) // RegisterHandler<NetworkPingMessage>(NetworkTime.OnServerPing, false) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkServer.Listen$int$(i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :149 :8) {
^entry (%_maxConns : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :149 :34)
cbde.store %_maxConns, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :149 :34)
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Initialize
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :151 :12) // Initialize() (InvocationExpression)
%2 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :152 :29)
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :155 :17) // Not a variable of known type: dontListen
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :155 :16) // !dontListen (LogicalNotExpression)
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :155 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Transport
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :157 :16) // Transport.activeTransport (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :157 :16) // Transport.activeTransport.ServerStart() (InvocationExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :158 :16) // Not a variable of known type: logger
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :158 :27) // "Server started listening" (StringLiteralExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :158 :16) // logger.Log("Server started listening") (InvocationExpression)
br ^2

^2: // SimpleBlock
%10 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :161 :21) // true
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: RegisterMessageHandlers
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :162 :12) // RegisterMessageHandlers() (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.NetworkServer.AddConnection$Mirror.NetworkConnectionToClient$(none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :171 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :171 :41)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :171 :41)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :173 :17) // Not a variable of known type: connections
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :173 :41) // Not a variable of known type: conn
%3 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :173 :41) // conn.connectionId (SimpleMemberAccessExpression)
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :173 :17) // connections.ContainsKey(conn.connectionId) (InvocationExpression)
%5 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :173 :16) // !connections.ContainsKey(conn.connectionId) (LogicalNotExpression)
cond_br %5, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :173 :16)

^1: // JumpBlock
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :177 :16) // Not a variable of known type: connections
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :177 :28) // Not a variable of known type: conn
%8 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :177 :28) // conn.connectionId (SimpleMemberAccessExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :177 :16) // connections[conn.connectionId] (ElementAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :177 :49) // Not a variable of known type: conn
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :178 :16) // Not a variable of known type: conn
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :178 :33) // Not a variable of known type: handlers
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :178 :16) // conn.SetHandlers(handlers) (InvocationExpression)
%14 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :179 :23) // true
return %14 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :179 :16)

^2: // JumpBlock
%15 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :182 :19) // false
return %15 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :182 :12)

^3: // ExitBlock
cbde.unreachable

}
func @_Mirror.NetworkServer.RemoveConnection$int$(i32) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :190 :8) {
^entry (%_connectionId : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :190 :44)
cbde.store %_connectionId, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :190 :44)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :192 :19) // Not a variable of known type: connections
%2 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :192 :38)
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :192 :19) // connections.Remove(connectionId) (InvocationExpression)
return %3 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :192 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.NetworkServer.SetLocalConnection$Mirror.ULocalConnectionToClient$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :199 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :199 :48)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :199 :48)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :201 :16) // Not a variable of known type: localConnection
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :201 :35) // null (NullLiteralExpression)
%3 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :201 :16) // comparison of unknown type: localConnection != null
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :201 :16)

^1: // JumpBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :203 :16) // Not a variable of known type: logger
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :203 :32) // "Local Connection already exists" (StringLiteralExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :203 :16) // logger.LogError("Local Connection already exists") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :204 :16)

^2: // SimpleBlock
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :207 :30) // Not a variable of known type: conn
br ^3

^3: // ExitBlock
return

}
func @_Mirror.NetworkServer.RemoveLocalConnection$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :210 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :212 :16) // Not a variable of known type: localConnection
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :212 :35) // null (NullLiteralExpression)
%2 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :212 :16) // comparison of unknown type: localConnection != null
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :212 :16)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :214 :16) // Not a variable of known type: localConnection
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :214 :16) // localConnection.Disconnect() (InvocationExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :215 :34) // null (NullLiteralExpression)
br ^2

^2: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: RemoveConnection
%6 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :217 :29)
%7 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :217 :12) // RemoveConnection(0) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function ActivateHostScene(), it contains poisonous unsupported syntaxes

// Skipping function SendToObservers(none, none, i32), it contains poisonous unsupported syntaxes

// Skipping function SendToAll(none, i32, i1), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkServer.SendToReady$T$$T.int$(none, i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :325 :8) {
^entry (%_msg : none, %_channelId : i32):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :325 :42)
cbde.store %_msg, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :325 :42)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :325 :49)
cbde.store %_channelId, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :325 :49)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :328 :17) // Not a variable of known type: active
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :328 :16) // !active (LogicalNotExpression)
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :328 :16)

^1: // JumpBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :330 :16) // Not a variable of known type: logger
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :330 :34) // "Can not send using NetworkServer.SendToReady<T>(T msg) because NetworkServer is not active" (StringLiteralExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :330 :16) // logger.LogWarning("Can not send using NetworkServer.SendToReady<T>(T msg) because NetworkServer is not active") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :331 :16)

^2: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SendToAll
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :334 :22) // Not a variable of known type: msg
%8 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :334 :27)
%9 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :334 :38) // true
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :334 :12) // SendToAll(msg, channelId, true) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function SendToReady(none, none, i1, i32), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkServer.SendToReady$T$$Mirror.NetworkIdentity.T.int$(none, none, i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :389 :8) {
^entry (%_identity : none, %_msg : none, %_channelId : i32):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :389 :42)
cbde.store %_identity, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :389 :42)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :389 :68)
cbde.store %_msg, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :389 :68)
%2 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :389 :75)
cbde.store %_channelId, %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :389 :75)
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SendToReady
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :392 :24) // Not a variable of known type: identity
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :392 :34) // Not a variable of known type: msg
%5 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :392 :39) // true
%6 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :392 :45)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :392 :12) // SendToReady(identity, msg, true, channelId) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkServer.DisconnectAll$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :399 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: DisconnectAllConnections
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :401 :12) // DisconnectAllConnections() (InvocationExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :402 :30) // null (NullLiteralExpression)
%2 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :404 :21) // false
br ^1

^1: // ExitBlock
return

}
// Skipping function DisconnectAllConnections(), it contains poisonous unsupported syntaxes

// Skipping function NoConnections(), it contains poisonous unsupported syntaxes

// Skipping function Update(), it contains poisonous unsupported syntaxes

// Skipping function CheckForInactiveConnections(), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkServer.AddTransportHandlers$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :489 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Transport
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :491 :12) // Transport.activeTransport (SimpleMemberAccessExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :491 :12) // Transport.activeTransport.OnServerConnected (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnConnected
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Transport
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :492 :12) // Transport.activeTransport (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :492 :12) // Transport.activeTransport.OnServerDataReceived (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnDataReceived
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Transport
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :493 :12) // Transport.activeTransport (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :493 :12) // Transport.activeTransport.OnServerDisconnected (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnDisconnected
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Transport
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :494 :12) // Transport.activeTransport (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :494 :12) // Transport.activeTransport.OnServerError (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnError
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkServer.OnConnected$int$(i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :497 :8) {
^entry (%_connectionId : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :497 :32)
cbde.store %_connectionId, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :497 :32)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :499 :16) // Not a variable of known type: logger
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :499 :16) // logger.LogEnabled() (InvocationExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :499 :16)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :499 :37) // Not a variable of known type: logger
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :499 :48) // "Server accepted client:" (StringLiteralExpression)
%5 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :499 :76)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :499 :48) // Binary expression on unsupported types "Server accepted client:" + connectionId
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :499 :37) // logger.Log("Server accepted client:" + connectionId) (InvocationExpression)
br ^2

^2: // BinaryBranchBlock
%8 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :502 :16)
%9 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :502 :32)
%10 = cmpi "sle", %8, %9 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :502 :16)
cond_br %10, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :502 :16)

^3: // JumpBlock
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :504 :16) // Not a variable of known type: logger
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :504 :32) // "Server.HandleConnect: invalid connectionId: " (StringLiteralExpression)
%13 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :504 :81)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :504 :32) // Binary expression on unsupported types "Server.HandleConnect: invalid connectionId: " + connectionId
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :504 :96) // " . Needs to be >0, because 0 is reserved for local player." (StringLiteralExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :504 :32) // Binary expression on unsupported types "Server.HandleConnect: invalid connectionId: " + connectionId + " . Needs to be >0, because 0 is reserved for local player."
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :504 :16) // logger.LogError("Server.HandleConnect: invalid connectionId: " + connectionId + " . Needs to be >0, because 0 is reserved for local player.") (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Transport
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :505 :16) // Transport.activeTransport (SimpleMemberAccessExpression)
%19 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :505 :59)
%20 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :505 :16) // Transport.activeTransport.ServerDisconnect(connectionId) (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :506 :16)

^4: // BinaryBranchBlock
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :510 :16) // Not a variable of known type: connections
%22 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :510 :40)
%23 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :510 :16) // connections.ContainsKey(connectionId) (InvocationExpression)
cond_br %23, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :510 :16)

^5: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Transport
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :512 :16) // Transport.activeTransport (SimpleMemberAccessExpression)
%25 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :512 :59)
%26 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :512 :16) // Transport.activeTransport.ServerDisconnect(connectionId) (InvocationExpression)
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :513 :20) // Not a variable of known type: logger
%28 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :513 :20) // logger.LogEnabled() (InvocationExpression)
cond_br %28, ^7, ^8 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :513 :20)

^7: // SimpleBlock
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :513 :41) // Not a variable of known type: logger
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :513 :52) // "Server connectionId " (StringLiteralExpression)
%31 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :513 :77)
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :513 :52) // Binary expression on unsupported types "Server connectionId " + connectionId
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :513 :92) // " already in use. kicked client:" (StringLiteralExpression)
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :513 :52) // Binary expression on unsupported types "Server connectionId " + connectionId + " already in use. kicked client:"
%35 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :513 :128)
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :513 :52) // Binary expression on unsupported types "Server connectionId " + connectionId + " already in use. kicked client:" + connectionId
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :513 :41) // logger.Log("Server connectionId " + connectionId + " already in use. kicked client:" + connectionId) (InvocationExpression)
br ^8

^8: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :514 :16)

^6: // BinaryBranchBlock
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :522 :16) // Not a variable of known type: connections
%39 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :522 :16) // connections.Count (SimpleMemberAccessExpression)
%40 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :522 :36) // Not a variable of known type: maxConnections
%41 = cmpi "slt", %39, %40 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :522 :16)
cond_br %41, ^9, ^10 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :522 :16)

^9: // SimpleBlock
%42 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :525 :79)
%43 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :525 :49) // new NetworkConnectionToClient(connectionId) (ObjectCreationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnConnected
%45 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :526 :28) // Not a variable of known type: conn
%46 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :526 :16) // OnConnected(conn) (InvocationExpression)
br ^11

^10: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Transport
%47 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :531 :16) // Transport.activeTransport (SimpleMemberAccessExpression)
%48 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :531 :59)
%49 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :531 :16) // Transport.activeTransport.ServerDisconnect(connectionId) (InvocationExpression)
%50 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :532 :20) // Not a variable of known type: logger
%51 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :532 :20) // logger.LogEnabled() (InvocationExpression)
cond_br %51, ^12, ^11 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :532 :20)

^12: // SimpleBlock
%52 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :532 :41) // Not a variable of known type: logger
%53 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :532 :52) // "Server full, kicked client:" (StringLiteralExpression)
%54 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :532 :84)
%55 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :532 :52) // Binary expression on unsupported types "Server full, kicked client:" + connectionId
%56 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :532 :41) // logger.Log("Server full, kicked client:" + connectionId) (InvocationExpression)
br ^11

^11: // ExitBlock
return

}
func @_Mirror.NetworkServer.OnConnected$Mirror.NetworkConnectionToClient$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :536 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :536 :41)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :536 :41)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :538 :16) // Not a variable of known type: logger
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :538 :16) // logger.LogEnabled() (InvocationExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :538 :16)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :538 :37) // Not a variable of known type: logger
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :538 :48) // "Server accepted client:" (StringLiteralExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :538 :76) // Not a variable of known type: conn
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :538 :48) // Binary expression on unsupported types "Server accepted client:" + conn
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :538 :37) // logger.Log("Server accepted client:" + conn) (InvocationExpression)
br ^2

^2: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: AddConnection
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :541 :26) // Not a variable of known type: conn
%9 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :541 :12) // AddConnection(conn) (InvocationExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :542 :12) // Not a variable of known type: conn
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :542 :31) // new ConnectMessage() (ObjectCreationExpression)
%12 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :542 :54)
%13 = cbde.neg %12 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :542 :53)
%14 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :542 :12) // conn.InvokeHandler(new ConnectMessage(), -1) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function OnDisconnected(i32), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkServer.OnDisconnected$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :559 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :559 :35)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :559 :35)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :561 :12) // Not a variable of known type: conn
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :561 :31) // new DisconnectMessage() (ObjectCreationExpression)
%3 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :561 :57)
%4 = cbde.neg %3 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :561 :56)
%5 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :561 :12) // conn.InvokeHandler(new DisconnectMessage(), -1) (InvocationExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :562 :16) // Not a variable of known type: logger
%7 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :562 :16) // logger.LogEnabled() (InvocationExpression)
cond_br %7, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :562 :16)

^1: // SimpleBlock
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :562 :37) // Not a variable of known type: logger
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :562 :48) // "Server lost client:" (StringLiteralExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :562 :72) // Not a variable of known type: conn
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :562 :48) // Binary expression on unsupported types "Server lost client:" + conn
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :562 :37) // logger.Log("Server lost client:" + conn) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
// Skipping function OnDataReceived(i32, none, i32), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkServer.OnError$int.System.Exception$(i32, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :577 :8) {
^entry (%_connectionId : i32, %_exception : none):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :577 :28)
cbde.store %_connectionId, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :577 :28)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :577 :46)
cbde.store %_exception, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :577 :46)
br ^0

^0: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :580 :12) // Not a variable of known type: logger
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :580 :32) // Not a variable of known type: exception
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :580 :12) // logger.LogException(exception) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkServer.RegisterHandler$T$$System.Action$Mirror.NetworkConnection.T$.bool$(none, i1) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :590 :8) {
^entry (%_handler : none, %_requireAuthentication : i1):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :590 :46)
cbde.store %_handler, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :590 :46)
%1 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :590 :84)
cbde.store %_requireAuthentication, %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :590 :84)
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: MessagePacker
%2 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :593 :26) // MessagePacker.GetId<T>() (InvocationExpression)
%3 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :593 :16) // msgType
cbde.store %2, %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :593 :16)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :594 :16) // Not a variable of known type: handlers
%5 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :594 :37)
%6 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :594 :16) // handlers.ContainsKey(msgType) (InvocationExpression)
cond_br %6, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :594 :16)

^1: // SimpleBlock
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :596 :16) // Not a variable of known type: logger
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :596 :89) // typeof(T) (TypeOfExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :596 :89) // typeof(T).FullName (SimpleMemberAccessExpression)
%10 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :596 :114)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :596 :34) // $"NetworkServer.RegisterHandler replacing handler for {typeof(T).FullName}, id={msgType}. If replacement is intentional, use ReplaceHandler instead to avoid this warning." (InterpolatedStringExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :596 :16) // logger.LogWarning($"NetworkServer.RegisterHandler replacing handler for {typeof(T).FullName}, id={msgType}. If replacement is intentional, use ReplaceHandler instead to avoid this warning.") (InvocationExpression)
br ^2

^2: // SimpleBlock
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :598 :12) // Not a variable of known type: handlers
%14 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :598 :21)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :598 :12) // handlers[msgType] (ElementAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: MessagePacker
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :598 :58) // Not a variable of known type: handler
%17 = cbde.load %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :598 :67)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :598 :32) // MessagePacker.WrapHandler(handler, requireAuthentication) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function RegisterHandler(none, i1), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkServer.ReplaceHandler$T$$System.Action$Mirror.NetworkConnection.T$.bool$(none, i1) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :621 :8) {
^entry (%_handler : none, %_requireAuthentication : i1):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :621 :45)
cbde.store %_handler, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :621 :45)
%1 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :621 :83)
cbde.store %_requireAuthentication, %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :621 :83)
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: MessagePacker
%2 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :624 :26) // MessagePacker.GetId<T>() (InvocationExpression)
%3 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :624 :16) // msgType
cbde.store %2, %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :624 :16)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :625 :12) // Not a variable of known type: handlers
%5 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :625 :21)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :625 :12) // handlers[msgType] (ElementAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: MessagePacker
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :625 :58) // Not a variable of known type: handler
%8 = cbde.load %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :625 :67)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :625 :32) // MessagePacker.WrapHandler(handler, requireAuthentication) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function ReplaceHandler(none, i1), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkServer.UnregisterHandler$T$$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :645 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: MessagePacker
%0 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :648 :26) // MessagePacker.GetId<T>() (InvocationExpression)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :648 :16) // msgType
cbde.store %0, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :648 :16)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :649 :12) // Not a variable of known type: handlers
%3 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :649 :28)
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :649 :12) // handlers.Remove(msgType) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkServer.ClearHandlers$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :655 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :657 :12) // Not a variable of known type: handlers
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :657 :12) // handlers.Clear() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkServer.SendToClientOfPlayer$T$$Mirror.NetworkIdentity.T.int$(none, none, i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :666 :8) {
^entry (%_identity : none, %_msg : none, %_channelId : i32):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :666 :51)
cbde.store %_identity, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :666 :51)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :666 :77)
cbde.store %_msg, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :666 :77)
%2 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :666 :84)
cbde.store %_channelId, %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :666 :84)
br ^0

^0: // BinaryBranchBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :669 :16) // Not a variable of known type: identity
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :669 :28) // null (NullLiteralExpression)
%5 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :669 :16) // comparison of unknown type: identity != null
cond_br %5, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :669 :16)

^1: // SimpleBlock
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :671 :16) // Not a variable of known type: identity
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :671 :16) // identity.connectionToClient (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :671 :49) // Not a variable of known type: msg
%9 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :671 :54)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :671 :16) // identity.connectionToClient.Send(msg, channelId) (InvocationExpression)
br ^3

^2: // SimpleBlock
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :675 :16) // Not a variable of known type: logger
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :675 :32) // "SendToClientOfPlayer: player has no NetworkIdentity: " (StringLiteralExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :675 :90) // Not a variable of known type: identity
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :675 :32) // Binary expression on unsupported types "SendToClientOfPlayer: player has no NetworkIdentity: " + identity
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :675 :16) // logger.LogError("SendToClientOfPlayer: player has no NetworkIdentity: " + identity) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function ReplacePlayerForConnection(none, none, none, i1), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkServer.ReplacePlayerForConnection$Mirror.NetworkConnection.UnityEngine.GameObject.bool$(none, none, i1) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :705 :8) {
^entry (%_conn : none, %_player : none, %_keepAuthority : i1):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :705 :54)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :705 :54)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :705 :78)
cbde.store %_player, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :705 :78)
%2 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :705 :97)
cbde.store %_keepAuthority, %2 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :705 :97)
br ^0

^0: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: InternalReplacePlayerForConnection
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :707 :54) // Not a variable of known type: conn
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :707 :60) // Not a variable of known type: player
%5 = cbde.load %2 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :707 :68)
%6 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :707 :19) // InternalReplacePlayerForConnection(conn, player, keepAuthority) (InvocationExpression)
return %6 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :707 :12)

^1: // ExitBlock
cbde.unreachable

}
// Skipping function AddPlayerForConnection(none, none, none), it contains poisonous unsupported syntaxes

// Skipping function SpawnObserversForConnection(none), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkServer.AddPlayerForConnection$Mirror.NetworkConnection.UnityEngine.GameObject$(none, none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :771 :8) {
^entry (%_conn : none, %_player : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :771 :50)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :771 :50)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :771 :74)
cbde.store %_player, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :771 :74)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :773 :39) // Not a variable of known type: player
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :773 :39) // player.GetComponent<NetworkIdentity>() (InvocationExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :774 :16) // Not a variable of known type: identity
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :774 :28) // null (NullLiteralExpression)
%7 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :774 :16) // comparison of unknown type: identity == null
cond_br %7, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :774 :16)

^1: // JumpBlock
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :776 :16) // Not a variable of known type: logger
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :776 :34) // "AddPlayer: playerGameObject has no NetworkIdentity. Please add a NetworkIdentity to " (StringLiteralExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :776 :123) // Not a variable of known type: player
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :776 :34) // Binary expression on unsupported types "AddPlayer: playerGameObject has no NetworkIdentity. Please add a NetworkIdentity to " + player
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :776 :16) // logger.LogWarning("AddPlayer: playerGameObject has no NetworkIdentity. Please add a NetworkIdentity to " + player) (InvocationExpression)
%13 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :777 :23) // false
return %13 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :777 :16)

^2: // BinaryBranchBlock
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :781 :16) // Not a variable of known type: conn
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :781 :16) // conn.identity (SimpleMemberAccessExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :781 :33) // null (NullLiteralExpression)
%17 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :781 :16) // comparison of unknown type: conn.identity != null
cond_br %17, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :781 :16)

^3: // JumpBlock
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :783 :16) // Not a variable of known type: logger
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :783 :27) // "AddPlayer: player object already exists" (StringLiteralExpression)
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :783 :16) // logger.Log("AddPlayer: player object already exists") (InvocationExpression)
%21 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :784 :23) // false
return %21 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :784 :16)

^4: // BinaryBranchBlock
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :789 :12) // Not a variable of known type: conn
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :789 :12) // conn.identity (SimpleMemberAccessExpression)
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :789 :28) // Not a variable of known type: identity
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :792 :12) // Not a variable of known type: identity
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :792 :36) // Not a variable of known type: conn
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :792 :12) // identity.SetClientOwner(conn) (InvocationExpression)
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :795 :16) // Not a variable of known type: conn
%29 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :795 :16) // conn is ULocalConnectionToClient (IsExpression)
cond_br %29, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :795 :16)

^5: // SimpleBlock
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :797 :16) // Not a variable of known type: identity
%31 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :797 :16) // identity.hasAuthority (SimpleMemberAccessExpression)
%32 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :797 :40) // true
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ClientScene
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :798 :46) // Not a variable of known type: identity
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :798 :16) // ClientScene.InternalAddPlayer(identity) (InvocationExpression)
br ^6

^6: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SetClientReady
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :802 :27) // Not a variable of known type: conn
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :802 :12) // SetClientReady(conn) (InvocationExpression)
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :804 :16) // Not a variable of known type: logger
%38 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :804 :16) // logger.LogEnabled() (InvocationExpression)
cond_br %38, ^7, ^8 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :804 :16)

^7: // SimpleBlock
%39 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :804 :37) // Not a variable of known type: logger
%40 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :804 :48) // "Adding new playerGameObject object netId: " (StringLiteralExpression)
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :804 :95) // Not a variable of known type: identity
%42 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :804 :95) // identity.netId (SimpleMemberAccessExpression)
%43 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :804 :48) // Binary expression on unsupported types "Adding new playerGameObject object netId: " + identity.netId
%44 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :804 :112) // " asset ID " (StringLiteralExpression)
%45 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :804 :48) // Binary expression on unsupported types "Adding new playerGameObject object netId: " + identity.netId + " asset ID "
%46 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :804 :127) // Not a variable of known type: identity
%47 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :804 :127) // identity.assetId (SimpleMemberAccessExpression)
%48 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :804 :48) // Binary expression on unsupported types "Adding new playerGameObject object netId: " + identity.netId + " asset ID " + identity.assetId
%49 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :804 :37) // logger.Log("Adding new playerGameObject object netId: " + identity.netId + " asset ID " + identity.assetId) (InvocationExpression)
br ^8

^8: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Respawn
%50 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :806 :20) // Not a variable of known type: identity
%51 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :806 :12) // Respawn(identity) (InvocationExpression)
%52 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :807 :19) // true
return %52 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :807 :12)

^9: // ExitBlock
cbde.unreachable

}
func @_Mirror.NetworkServer.Respawn$Mirror.NetworkIdentity$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :810 :8) {
^entry (%_identity : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :810 :28)
cbde.store %_identity, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :810 :28)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :812 :16) // Not a variable of known type: identity
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :812 :16) // identity.netId (SimpleMemberAccessExpression)
%3 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :812 :34)
%4 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :812 :16) // comparison of unknown type: identity.netId == 0
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :812 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Spawn
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :815 :22) // Not a variable of known type: identity
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :815 :22) // identity.gameObject (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :815 :43) // Not a variable of known type: identity
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :815 :43) // identity.connectionToClient (SimpleMemberAccessExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :815 :16) // Spawn(identity.gameObject, identity.connectionToClient) (InvocationExpression)
br ^3

^2: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SendSpawnMessage
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :820 :33) // Not a variable of known type: identity
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :820 :43) // Not a variable of known type: identity
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :820 :43) // identity.connectionToClient (SimpleMemberAccessExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :820 :16) // SendSpawnMessage(identity, identity.connectionToClient) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function InternalReplacePlayerForConnection(none, none, i1), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkServer.GetNetworkIdentity$UnityEngine.GameObject.outMirror.NetworkIdentity$(none, none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :873 :8) {
^entry (%_go : none, %_identity : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :873 :48)
cbde.store %_go, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :873 :48)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :873 :63)
cbde.store %_identity, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :873 :63)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :875 :23) // Not a variable of known type: go
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :875 :23) // go.GetComponent<NetworkIdentity>() (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :876 :16) // Not a variable of known type: identity
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :876 :28) // null (NullLiteralExpression)
%6 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :876 :16) // comparison of unknown type: identity == null
cond_br %6, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :876 :16)

^1: // JumpBlock
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :878 :16) // Not a variable of known type: logger
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :878 :46) // Not a variable of known type: go
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :878 :46) // go.name (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :878 :32) // $"GameObject {go.name} doesn't have NetworkIdentity." (InterpolatedStringExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :878 :16) // logger.LogError($"GameObject {go.name} doesn't have NetworkIdentity.") (InvocationExpression)
%12 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :879 :23) // false
return %12 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :879 :16)

^2: // JumpBlock
%13 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :881 :19) // true
return %13 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :881 :12)

^3: // ExitBlock
cbde.unreachable

}
func @_Mirror.NetworkServer.SetClientReady$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :889 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :889 :42)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :889 :42)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :891 :16) // Not a variable of known type: logger
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :891 :16) // logger.LogEnabled() (InvocationExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :891 :16)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :891 :37) // Not a variable of known type: logger
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :891 :48) // "SetClientReadyInternal for conn:" (StringLiteralExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :891 :85) // Not a variable of known type: conn
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :891 :48) // Binary expression on unsupported types "SetClientReadyInternal for conn:" + conn
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :891 :37) // logger.Log("SetClientReadyInternal for conn:" + conn) (InvocationExpression)
br ^2

^2: // BinaryBranchBlock
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :894 :12) // Not a variable of known type: conn
%9 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :894 :12) // conn.isReady (SimpleMemberAccessExpression)
%10 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :894 :27) // true
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :897 :16) // Not a variable of known type: conn
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :897 :16) // conn.identity (SimpleMemberAccessExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :897 :33) // null (NullLiteralExpression)
%14 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :897 :16) // comparison of unknown type: conn.identity != null
cond_br %14, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :897 :16)

^3: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SpawnObserversForConnection
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :898 :44) // Not a variable of known type: conn
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :898 :16) // SpawnObserversForConnection(conn) (InvocationExpression)
br ^4

^4: // ExitBlock
return

}
func @_Mirror.NetworkServer.ShowForConnection$Mirror.NetworkIdentity.Mirror.NetworkConnection$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :901 :8) {
^entry (%_identity : none, %_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :901 :47)
cbde.store %_identity, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :901 :47)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :901 :73)
cbde.store %_conn, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :901 :73)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :903 :16) // Not a variable of known type: conn
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :903 :16) // conn.isReady (SimpleMemberAccessExpression)
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :903 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SendSpawnMessage
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :904 :33) // Not a variable of known type: identity
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :904 :43) // Not a variable of known type: conn
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :904 :16) // SendSpawnMessage(identity, conn) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.NetworkServer.HideForConnection$Mirror.NetworkIdentity.Mirror.NetworkConnection$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :907 :8) {
^entry (%_identity : none, %_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :907 :47)
cbde.store %_identity, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :907 :47)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :907 :73)
cbde.store %_conn, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :907 :73)
br ^0

^0: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :909 :36) // new ObjectHideMessage             {                 netId = identity.netId             } (ObjectCreationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :911 :24) // Not a variable of known type: identity
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :911 :24) // identity.netId (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :913 :12) // Not a variable of known type: conn
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :913 :22) // Not a variable of known type: msg
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :913 :12) // conn.Send(msg) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function SetAllClientsNotReady(), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkServer.SetClientNotReady$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :933 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :933 :45)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :933 :45)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :935 :16) // Not a variable of known type: conn
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :935 :16) // conn.isReady (SimpleMemberAccessExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :935 :16)

^1: // BinaryBranchBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :937 :20) // Not a variable of known type: logger
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :937 :20) // logger.LogEnabled() (InvocationExpression)
cond_br %4, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :937 :20)

^3: // SimpleBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :937 :41) // Not a variable of known type: logger
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :937 :52) // "PlayerNotReady " (StringLiteralExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :937 :72) // Not a variable of known type: conn
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :937 :52) // Binary expression on unsupported types "PlayerNotReady " + conn
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :937 :41) // logger.Log("PlayerNotReady " + conn) (InvocationExpression)
br ^4

^4: // SimpleBlock
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :938 :16) // Not a variable of known type: conn
%11 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :938 :16) // conn.isReady (SimpleMemberAccessExpression)
%12 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :938 :31) // false
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :939 :16) // Not a variable of known type: conn
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :939 :16) // conn.RemoveObservers() (InvocationExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :941 :16) // Not a variable of known type: conn
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :941 :26) // new NotReadyMessage() (ObjectCreationExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :941 :16) // conn.Send(new NotReadyMessage()) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.NetworkServer.OnClientReadyMessage$Mirror.NetworkConnection.Mirror.ReadyMessage$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :950 :8) {
^entry (%_conn : none, %_msg : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :950 :41)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :950 :41)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :950 :65)
cbde.store %_msg, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :950 :65)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :952 :16) // Not a variable of known type: logger
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :952 :16) // logger.LogEnabled() (InvocationExpression)
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :952 :16)

^1: // SimpleBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :952 :37) // Not a variable of known type: logger
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :952 :48) // "Default handler for ready message from " (StringLiteralExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :952 :92) // Not a variable of known type: conn
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :952 :48) // Binary expression on unsupported types "Default handler for ready message from " + conn
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :952 :37) // logger.Log("Default handler for ready message from " + conn) (InvocationExpression)
br ^2

^2: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SetClientReady
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :953 :27) // Not a variable of known type: conn
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :953 :12) // SetClientReady(conn) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.NetworkServer.RemovePlayerForConnection$Mirror.NetworkConnection.bool$(none, i1) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :961 :8) {
^entry (%_conn : none, %_destroyServerObject : i1):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :961 :53)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :961 :53)
%1 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :961 :77)
cbde.store %_destroyServerObject, %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :961 :77)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :963 :16) // Not a variable of known type: conn
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :963 :16) // conn.identity (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :963 :33) // null (NullLiteralExpression)
%5 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :963 :16) // comparison of unknown type: conn.identity != null
cond_br %5, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :963 :16)

^1: // BinaryBranchBlock
%6 = cbde.load %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :965 :20)
cond_br %6, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :965 :20)

^3: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Destroy
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :966 :28) // Not a variable of known type: conn
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :966 :28) // conn.identity (SimpleMemberAccessExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :966 :28) // conn.identity.gameObject (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :966 :20) // Destroy(conn.identity.gameObject) (InvocationExpression)
br ^5

^4: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: UnSpawn
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :968 :28) // Not a variable of known type: conn
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :968 :28) // conn.identity (SimpleMemberAccessExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :968 :28) // conn.identity.gameObject (SimpleMemberAccessExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :968 :20) // UnSpawn(conn.identity.gameObject) (InvocationExpression)
br ^5

^5: // SimpleBlock
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :970 :16) // Not a variable of known type: conn
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :970 :16) // conn.identity (SimpleMemberAccessExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :970 :32) // null (NullLiteralExpression)
br ^6

^2: // BinaryBranchBlock
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :974 :20) // Not a variable of known type: logger
%19 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :974 :20) // logger.LogEnabled() (InvocationExpression)
cond_br %19, ^7, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :974 :20)

^7: // SimpleBlock
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :974 :41) // Not a variable of known type: logger
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :974 :66) // Not a variable of known type: conn
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :974 :52) // $"Connection {conn} has no identity" (InterpolatedStringExpression)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :974 :41) // logger.Log($"Connection {conn} has no identity") (InvocationExpression)
br ^6

^6: // ExitBlock
return

}
// Skipping function OnCommandMessage(none, none), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkServer.SpawnObject$UnityEngine.GameObject.Mirror.NetworkConnection$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1009 :8) {
^entry (%_obj : none, %_ownerConnection : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1009 :41)
cbde.store %_obj, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1009 :41)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1009 :57)
cbde.store %_ownerConnection, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1009 :57)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1011 :17) // Not a variable of known type: active
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1011 :16) // !active (LogicalNotExpression)
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1011 :16)

^1: // JumpBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1013 :16) // Not a variable of known type: logger
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1013 :32) // "SpawnObject for " (StringLiteralExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1013 :53) // Not a variable of known type: obj
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1013 :32) // Binary expression on unsupported types "SpawnObject for " + obj
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1013 :59) // ", NetworkServer is not active. Cannot spawn objects without an active server." (StringLiteralExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1013 :32) // Binary expression on unsupported types "SpawnObject for " + obj + ", NetworkServer is not active. Cannot spawn objects without an active server."
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1013 :16) // logger.LogError("SpawnObject for " + obj + ", NetworkServer is not active. Cannot spawn objects without an active server.") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1014 :16)

^2: // BinaryBranchBlock
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1017 :39) // Not a variable of known type: obj
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1017 :39) // obj.GetComponent<NetworkIdentity>() (InvocationExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1018 :16) // Not a variable of known type: identity
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1018 :28) // null (NullLiteralExpression)
%16 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1018 :16) // comparison of unknown type: identity == null
cond_br %16, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1018 :16)

^3: // JumpBlock
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1020 :16) // Not a variable of known type: logger
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1020 :32) // "SpawnObject " (StringLiteralExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1020 :49) // Not a variable of known type: obj
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1020 :32) // Binary expression on unsupported types "SpawnObject " + obj
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1020 :55) // " has no NetworkIdentity. Please add a NetworkIdentity to " (StringLiteralExpression)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1020 :32) // Binary expression on unsupported types "SpawnObject " + obj + " has no NetworkIdentity. Please add a NetworkIdentity to "
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1020 :117) // Not a variable of known type: obj
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1020 :32) // Binary expression on unsupported types "SpawnObject " + obj + " has no NetworkIdentity. Please add a NetworkIdentity to " + obj
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1020 :16) // logger.LogError("SpawnObject " + obj + " has no NetworkIdentity. Please add a NetworkIdentity to " + obj) (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1021 :16)

^4: // BinaryBranchBlock
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1024 :16) // Not a variable of known type: identity
%27 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1024 :16) // identity.SpawnedFromInstantiate (SimpleMemberAccessExpression)
cond_br %27, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1024 :16)

^5: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1028 :16)

^6: // BinaryBranchBlock
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1031 :12) // Not a variable of known type: identity
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1031 :12) // identity.connectionToClient (SimpleMemberAccessExpression)
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1031 :69) // Not a variable of known type: ownerConnection
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1031 :42) // (NetworkConnectionToClient)ownerConnection (CastExpression)
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1035 :16) // Not a variable of known type: ownerConnection
%33 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1035 :16) // ownerConnection is ULocalConnectionToClient (IsExpression)
cond_br %33, ^7, ^8 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1035 :16)

^7: // SimpleBlock
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1036 :16) // Not a variable of known type: identity
%35 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1036 :16) // identity.hasAuthority (SimpleMemberAccessExpression)
%36 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1036 :40) // true
br ^8

^8: // BinaryBranchBlock
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1038 :12) // Not a variable of known type: identity
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1038 :12) // identity.OnStartServer() (InvocationExpression)
%39 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1040 :16) // Not a variable of known type: logger
%40 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1040 :16) // logger.LogEnabled() (InvocationExpression)
cond_br %40, ^9, ^10 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1040 :16)

^9: // SimpleBlock
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1040 :37) // Not a variable of known type: logger
%42 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1040 :48) // "SpawnObject instance ID " (StringLiteralExpression)
%43 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1040 :77) // Not a variable of known type: identity
%44 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1040 :77) // identity.netId (SimpleMemberAccessExpression)
%45 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1040 :48) // Binary expression on unsupported types "SpawnObject instance ID " + identity.netId
%46 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1040 :94) // " asset ID " (StringLiteralExpression)
%47 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1040 :48) // Binary expression on unsupported types "SpawnObject instance ID " + identity.netId + " asset ID "
%48 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1040 :109) // Not a variable of known type: identity
%49 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1040 :109) // identity.assetId (SimpleMemberAccessExpression)
%50 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1040 :48) // Binary expression on unsupported types "SpawnObject instance ID " + identity.netId + " asset ID " + identity.assetId
%51 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1040 :37) // logger.Log("SpawnObject instance ID " + identity.netId + " asset ID " + identity.assetId) (InvocationExpression)
br ^10

^10: // SimpleBlock
%52 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1042 :12) // Not a variable of known type: identity
%53 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1042 :38) // true
%54 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1042 :12) // identity.RebuildObservers(true) (InvocationExpression)
br ^11

^11: // ExitBlock
return

}
// Skipping function SendSpawnMessage(none, none), it contains poisonous unsupported syntaxes

// Skipping function CreateSpawnMessagePayload(i1, none, none, none), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkServer.DestroyPlayerForConnection$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1108 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1108 :54)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1108 :54)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1111 :12) // Not a variable of known type: conn
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1111 :12) // conn.DestroyOwnedObjects() (InvocationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1112 :12) // Not a variable of known type: conn
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1112 :12) // conn.identity (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1112 :28) // null (NullLiteralExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkServer.Spawn$UnityEngine.GameObject.Mirror.NetworkConnection$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1121 :8) {
^entry (%_obj : none, %_ownerConnection : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1121 :33)
cbde.store %_obj, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1121 :33)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1121 :49)
cbde.store %_ownerConnection, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1121 :49)
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: VerifyCanSpawn
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1123 :31) // Not a variable of known type: obj
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1123 :16) // VerifyCanSpawn(obj) (InvocationExpression)
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1123 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SpawnObject
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1125 :28) // Not a variable of known type: obj
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1125 :33) // Not a variable of known type: ownerConnection
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1125 :16) // SpawnObject(obj, ownerConnection) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.NetworkServer.Spawn$UnityEngine.GameObject.UnityEngine.GameObject$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1135 :8) {
^entry (%_obj : none, %_ownerPlayer : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1135 :33)
cbde.store %_obj, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1135 :33)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1135 :49)
cbde.store %_ownerPlayer, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1135 :49)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1137 :39) // Not a variable of known type: ownerPlayer
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1137 :39) // ownerPlayer.GetComponent<NetworkIdentity>() (InvocationExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1138 :16) // Not a variable of known type: identity
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1138 :28) // null (NullLiteralExpression)
%7 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1138 :16) // comparison of unknown type: identity == null
cond_br %7, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1138 :16)

^1: // JumpBlock
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1140 :16) // Not a variable of known type: logger
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1140 :32) // "Player object has no NetworkIdentity" (StringLiteralExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1140 :16) // logger.LogError("Player object has no NetworkIdentity") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1141 :16)

^2: // BinaryBranchBlock
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1144 :16) // Not a variable of known type: identity
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1144 :16) // identity.connectionToClient (SimpleMemberAccessExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1144 :47) // null (NullLiteralExpression)
%14 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1144 :16) // comparison of unknown type: identity.connectionToClient == null
cond_br %14, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1144 :16)

^3: // JumpBlock
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1146 :16) // Not a variable of known type: logger
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1146 :32) // "Player object is not a player." (StringLiteralExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1146 :16) // logger.LogError("Player object is not a player.") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1147 :16)

^4: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Spawn
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1150 :18) // Not a variable of known type: obj
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1150 :23) // Not a variable of known type: identity
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1150 :23) // identity.connectionToClient (SimpleMemberAccessExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1150 :12) // Spawn(obj, identity.connectionToClient) (InvocationExpression)
br ^5

^5: // ExitBlock
return

}
// Skipping function Spawn(none, none, none), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkServer.CheckForPrefab$UnityEngine.GameObject$(none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1172 :8) {
^entry (%_obj : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1172 :35)
cbde.store %_obj, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1172 :35)
br ^0

^0: // JumpBlock
// Entity from another assembly: UnityEditor
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1176 :19) // UnityEditor.PrefabUtility (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1176 :65) // Not a variable of known type: obj
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1176 :19) // UnityEditor.PrefabUtility.IsPartOfPrefabAsset(obj) (InvocationExpression)
return %3 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1176 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.NetworkServer.VerifyCanSpawn$UnityEngine.GameObject$(none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1187 :8) {
^entry (%_obj : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1187 :35)
cbde.store %_obj, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1187 :35)
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CheckForPrefab
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1189 :31) // Not a variable of known type: obj
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1189 :16) // CheckForPrefab(obj) (InvocationExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1189 :16)

^1: // JumpBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1191 :16) // Not a variable of known type: logger
// Entity from another assembly: LogType
%4 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1191 :33) // LogType.Error (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1191 :48) // "GameObject {0} is a prefab, it can't be spawned. This will cause errors in builds." (StringLiteralExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1191 :134) // Not a variable of known type: obj
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1191 :134) // obj.name (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1191 :16) // logger.LogFormat(LogType.Error, "GameObject {0} is a prefab, it can't be spawned. This will cause errors in builds.", obj.name) (InvocationExpression)
%9 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1192 :23) // false
return %9 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1192 :16)

^2: // JumpBlock
%10 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1195 :19) // true
return %10 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkServer.cs" :1195 :12)

^3: // ExitBlock
cbde.unreachable

}
// Skipping function DestroyObject(none, i1), it contains poisonous unsupported syntaxes

// Skipping function Destroy(none), it contains poisonous unsupported syntaxes

// Skipping function UnSpawn(none), it contains poisonous unsupported syntaxes

// Skipping function ValidateSceneObject(none), it contains poisonous unsupported syntaxes

// Skipping function SpawnObjects(), it contains poisonous unsupported syntaxes

