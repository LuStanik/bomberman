func @_Mirror.SyncSet$T$.Reset$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :41 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :43 :25) // false
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :44 :12) // Not a variable of known type: changes
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :44 :12) // changes.Clear() (InvocationExpression)
%3 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :45 :27)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :46 :12) // Not a variable of known type: objects
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :46 :12) // objects.Clear() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function AddOperation(none, none), it contains poisonous unsupported syntaxes

// Skipping function OnSerializeAll(none), it contains poisonous unsupported syntaxes

// Skipping function OnSerializeDelta(none), it contains poisonous unsupported syntaxes

func @_Mirror.SyncSet$T$.OnDeserializeAll$Mirror.NetworkReader$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :118 :8) {
^entry (%_reader : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :118 :37)
cbde.store %_reader, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :118 :37)
br ^0

^0: // ForInitializerBlock
%1 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :121 :25) // true
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :124 :29) // Not a variable of known type: reader
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :124 :29) // reader.ReadUInt32() (InvocationExpression)
%4 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :124 :24) // (int)reader.ReadUInt32() (CastExpression)
%5 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :124 :16) // count
cbde.store %4, %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :124 :16)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :126 :12) // Not a variable of known type: objects
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :126 :12) // objects.Clear() (InvocationExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :127 :12) // Not a variable of known type: changes
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :127 :12) // changes.Clear() (InvocationExpression)
%10 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :129 :25)
%11 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :129 :21) // i
cbde.store %10, %11 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :129 :21)
br ^1

^1: // BinaryBranchBlock
%12 = cbde.load %11 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :129 :28)
%13 = cbde.load %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :129 :32)
%14 = cmpi "slt", %12, %13 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :129 :28)
cond_br %14, ^2, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :129 :28)

^2: // SimpleBlock
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :131 :24) // Not a variable of known type: reader
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :131 :24) // reader.Read<T>() (InvocationExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :132 :16) // Not a variable of known type: objects
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :132 :28) // Not a variable of known type: obj
%20 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :132 :16) // objects.Add(obj) (InvocationExpression)
br ^4

^4: // SimpleBlock
%21 = cbde.load %11 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :129 :39)
%22 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :129 :39)
%23 = addi %21, %22 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :129 :39)
cbde.store %23, %11 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :129 :39)
br ^1

^3: // SimpleBlock
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :138 :32) // Not a variable of known type: reader
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :138 :32) // reader.ReadUInt32() (InvocationExpression)
%26 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :138 :27) // (int)reader.ReadUInt32() (CastExpression)
br ^5

^5: // ExitBlock
return

}
// Skipping function OnDeserializeDelta(none), it contains poisonous unsupported syntaxes

func @_Mirror.SyncSet$T$.Add$T$(none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :195 :8) {
^entry (%_item : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :195 :24)
cbde.store %_item, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :195 :24)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :197 :16) // Not a variable of known type: objects
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :197 :28) // Not a variable of known type: item
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :197 :16) // objects.Add(item) (InvocationExpression)
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :197 :16)

^1: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: AddOperation
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :199 :29) // Not a variable of known type: Operation
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :199 :29) // Operation.OP_ADD (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :199 :47) // Not a variable of known type: item
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :199 :16) // AddOperation(Operation.OP_ADD, item) (InvocationExpression)
%8 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :200 :23) // true
return %8 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :200 :16)

^2: // JumpBlock
%9 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :202 :19) // false
return %9 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :202 :12)

^3: // ExitBlock
cbde.unreachable

}
func @_Mirror.SyncSet$T$.System.Collections.Generic.ICollection$T$.Add$T$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :205 :8) {
^entry (%_item : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :205 :32)
cbde.store %_item, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :205 :32)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :207 :16) // Not a variable of known type: objects
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :207 :28) // Not a variable of known type: item
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :207 :16) // objects.Add(item) (InvocationExpression)
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :207 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: AddOperation
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :209 :29) // Not a variable of known type: Operation
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :209 :29) // Operation.OP_ADD (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :209 :47) // Not a variable of known type: item
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :209 :16) // AddOperation(Operation.OP_ADD, item) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.SyncSet$T$.Clear$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :213 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :215 :12) // Not a variable of known type: objects
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :215 :12) // objects.Clear() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: AddOperation
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :216 :25) // Not a variable of known type: Operation
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :216 :25) // Operation.OP_CLEAR (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :216 :12) // AddOperation(Operation.OP_CLEAR) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.SyncSet$T$.Remove$T$(none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :223 :8) {
^entry (%_item : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :223 :27)
cbde.store %_item, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :223 :27)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :225 :16) // Not a variable of known type: objects
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :225 :31) // Not a variable of known type: item
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :225 :16) // objects.Remove(item) (InvocationExpression)
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :225 :16)

^1: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: AddOperation
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :227 :29) // Not a variable of known type: Operation
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :227 :29) // Operation.OP_REMOVE (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :227 :50) // Not a variable of known type: item
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :227 :16) // AddOperation(Operation.OP_REMOVE, item) (InvocationExpression)
%8 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :228 :23) // true
return %8 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :228 :16)

^2: // JumpBlock
%9 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :230 :19) // false
return %9 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncSet.cs" :230 :12)

^3: // ExitBlock
cbde.unreachable

}
// Skipping function ExceptWith(none), it contains poisonous unsupported syntaxes

// Skipping function IntersectWith(none), it contains poisonous unsupported syntaxes

// Skipping function IntersectWithSet(none), it contains poisonous unsupported syntaxes

// Skipping function SymmetricExceptWith(none), it contains poisonous unsupported syntaxes

// Skipping function UnionWith(none), it contains poisonous unsupported syntaxes

