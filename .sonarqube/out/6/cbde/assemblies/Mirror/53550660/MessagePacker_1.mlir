func @_Mirror.MessagePacker.GetId$T$$$() -> i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\MessagePacker.cs" :25 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\MessagePacker.cs" :30 :19) // typeof(T) (TypeOfExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\MessagePacker.cs" :30 :19) // typeof(T).FullName (SimpleMemberAccessExpression)
%2 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\MessagePacker.cs" :30 :19) // typeof(T).FullName.GetStableHashCode() (InvocationExpression)
%3 = constant 65535 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\MessagePacker.cs" :30 :60)
%4 = and %2, %3 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\MessagePacker.cs" :30 :19)
return %4 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\MessagePacker.cs" :30 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.MessagePacker.Pack$T$$T.Mirror.NetworkWriter$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\MessagePacker.cs" :36 :8) {
^entry (%_message : none, %_writer : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\MessagePacker.cs" :36 :35)
cbde.store %_message, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\MessagePacker.cs" :36 :35)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\MessagePacker.cs" :36 :46)
cbde.store %_writer, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\MessagePacker.cs" :36 :46)
br ^0

^0: // SimpleBlock
%2 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\MessagePacker.cs" :39 :26) // GetId<T>() (InvocationExpression)
%3 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\MessagePacker.cs" :39 :16) // msgType
cbde.store %2, %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\MessagePacker.cs" :39 :16)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\MessagePacker.cs" :40 :12) // Not a variable of known type: writer
%5 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\MessagePacker.cs" :40 :39)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\MessagePacker.cs" :40 :31) // (ushort)msgType (CastExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\MessagePacker.cs" :40 :12) // writer.WriteUInt16((ushort)msgType) (InvocationExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\MessagePacker.cs" :43 :12) // Not a variable of known type: writer
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\MessagePacker.cs" :43 :25) // Not a variable of known type: message
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\MessagePacker.cs" :43 :12) // writer.Write(message) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function Unpack(none, i32), it contains poisonous unsupported syntaxes

