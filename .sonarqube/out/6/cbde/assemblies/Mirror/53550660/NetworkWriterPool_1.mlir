func @_Mirror.PooledNetworkWriter.Dispose$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkWriterPool.cs" :9 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkWriterPool
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkWriterPool.cs" :11 :38) // this (ThisExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkWriterPool.cs" :11 :12) // NetworkWriterPool.Recycle(this) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkWriterPool.GetWriter$$() -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkWriterPool.cs" :34 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkWriterPool.cs" :37 :41) // Not a variable of known type: pool
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkWriterPool.cs" :37 :41) // pool.Take() (InvocationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkWriterPool.cs" :38 :12) // Not a variable of known type: writer
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkWriterPool.cs" :38 :12) // writer.Reset() (InvocationExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkWriterPool.cs" :39 :19) // Not a variable of known type: writer
return %5 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkWriterPool.cs" :39 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.NetworkWriterPool.Recycle$Mirror.PooledNetworkWriter$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkWriterPool.cs" :46 :8) {
^entry (%_writer : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkWriterPool.cs" :46 :35)
cbde.store %_writer, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkWriterPool.cs" :46 :35)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkWriterPool.cs" :48 :12) // Not a variable of known type: pool
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkWriterPool.cs" :48 :24) // Not a variable of known type: writer
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkWriterPool.cs" :48 :12) // pool.Return(writer) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
