func @_Mirror.ULocalConnectionToClient.Send$System.ArraySegment$byte$.int$(none, i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :15 :8) {
^entry (%_segment : none, %_channelId : i32):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :15 :36)
cbde.store %_segment, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :15 :36)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :15 :64)
cbde.store %_channelId, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :15 :64)
br ^0

^0: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :17 :12) // Not a variable of known type: connectionToServer
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :17 :12) // connectionToServer.buffer (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :17 :44) // Not a variable of known type: segment
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :17 :12) // connectionToServer.buffer.Write(segment) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.ULocalConnectionToClient.DisconnectInternal$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :24 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :28 :22) // false
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: RemoveObservers
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :29 :12) // RemoveObservers() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.ULocalConnectionToClient.Disconnect$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :35 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: DisconnectInternal
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :37 :12) // DisconnectInternal() (InvocationExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :38 :12) // Not a variable of known type: connectionToServer
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :38 :12) // connectionToServer.DisconnectInternal() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.LocalConnectionBuffer.Write$System.ArraySegment$byte$$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :50 :8) {
^entry (%_segment : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :50 :26)
cbde.store %_segment, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :50 :26)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :52 :12) // Not a variable of known type: writer
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :52 :44) // Not a variable of known type: segment
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :52 :12) // writer.WriteBytesAndSizeSegment(segment) (InvocationExpression)
%4 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :53 :12) // Not a variable of known type: packetCount
%5 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :53 :12) // Inc/Decrement of field or property packetCount
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :56 :12) // Not a variable of known type: reader
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :56 :12) // reader.buffer (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :56 :28) // Not a variable of known type: writer
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :56 :28) // writer.ToArraySegment() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.LocalConnectionBuffer.HasPackets$$() -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :59 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :61 :19) // Not a variable of known type: packetCount
%1 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :61 :33)
%2 = cmpi "sgt", %0, %1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :61 :19)
return %2 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :61 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.LocalConnectionBuffer.GetNextPacket$$() -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :63 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :65 :40) // Not a variable of known type: reader
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :65 :40) // reader.ReadBytesAndSizeSegment() (InvocationExpression)
%3 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :66 :12) // Not a variable of known type: packetCount
%4 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :66 :12) // Inc/Decrement of field or property packetCount
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :68 :19) // Not a variable of known type: packet
return %5 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :68 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.LocalConnectionBuffer.ResetBuffer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :71 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :73 :12) // Not a variable of known type: writer
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :73 :12) // writer.Reset() (InvocationExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :74 :12) // Not a variable of known type: reader
%3 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :74 :12) // reader.Position (SimpleMemberAccessExpression)
%4 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :74 :30)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.ULocalConnectionToServer.Send$System.ArraySegment$byte$.int$(none, i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :89 :8) {
^entry (%_segment : none, %_channelId : i32):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :89 :36)
cbde.store %_segment, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :89 :36)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :89 :64)
cbde.store %_channelId, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :89 :64)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :91 :16) // Not a variable of known type: segment
%3 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :91 :16) // segment.Count (SimpleMemberAccessExpression)
%4 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :91 :33)
%5 = cmpi "eq", %3, %4 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :91 :16)
cond_br %5, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :91 :16)

^1: // JumpBlock
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :93 :16) // Not a variable of known type: logger
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :93 :32) // "LocalConnection.SendBytes cannot send zero bytes" (StringLiteralExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :93 :16) // logger.LogError("LocalConnection.SendBytes cannot send zero bytes") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :94 :16)

^2: // SimpleBlock
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :98 :12) // Not a variable of known type: connectionToClient
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :98 :48) // Not a variable of known type: segment
%11 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :98 :57)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :98 :12) // connectionToClient.TransportReceive(segment, channelId) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.ULocalConnectionToServer.Update$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :101 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :104 :19) // Not a variable of known type: buffer
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :104 :19) // buffer.HasPackets() (InvocationExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :104 :19)

^1: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :106 :44) // Not a variable of known type: buffer
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :106 :44) // buffer.GetNextPacket() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: TransportReceive
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :110 :33) // Not a variable of known type: packet
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Channels
%6 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :110 :41)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :110 :16) // TransportReceive(packet, Channels.DefaultReliable) (InvocationExpression)
br ^0

^2: // SimpleBlock
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :113 :12) // Not a variable of known type: buffer
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :113 :12) // buffer.ResetBuffer() (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.ULocalConnectionToServer.DisconnectInternal$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :119 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :123 :22) // false
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ClientScene
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :124 :47) // this (ThisExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :124 :12) // ClientScene.HandleClientDisconnect(this) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.ULocalConnectionToServer.Disconnect$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :130 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :132 :12) // Not a variable of known type: connectionToClient
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :132 :12) // connectionToClient.DisconnectInternal() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: DisconnectInternal
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\LocalConnections.cs" :133 :12) // DisconnectInternal() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
