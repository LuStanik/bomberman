func @_Mirror.NetworkTime.LocalTime$$() -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :42 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :44 :19) // Not a variable of known type: stopwatch
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :44 :19) // stopwatch.Elapsed (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :44 :19) // stopwatch.Elapsed.TotalSeconds (SimpleMemberAccessExpression)
return %2 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :44 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.NetworkTime.Reset$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :47 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :49 :48) // Not a variable of known type: PingWindowSize
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :49 :19) // new ExponentialMovingAverage(PingWindowSize) (ObjectCreationExpression)
%2 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :50 :51) // Not a variable of known type: PingWindowSize
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :50 :22) // new ExponentialMovingAverage(PingWindowSize) (ObjectCreationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :51 :24) // double (PredefinedType)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :51 :24) // double.MinValue (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :52 :24) // double (PredefinedType)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :52 :24) // double.MaxValue (SimpleMemberAccessExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkTime.UpdateClient$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :55 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: Time
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :57 :16) // Time.time (SimpleMemberAccessExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :57 :28) // Not a variable of known type: lastPingTime
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :57 :16) // Binary expression on unsupported types Time.time - lastPingTime
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :57 :44) // Not a variable of known type: PingFrequency
%4 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :57 :16) // comparison of unknown type: Time.time - lastPingTime >= PingFrequency
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :57 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: LocalTime
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :59 :72) // LocalTime() (InvocationExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :59 :49) // new NetworkPingMessage(LocalTime()) (ObjectCreationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkClient
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :60 :35) // Not a variable of known type: pingMessage
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :60 :16) // NetworkClient.Send(pingMessage) (InvocationExpression)
// Entity from another assembly: Time
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :61 :31) // Time.time (SimpleMemberAccessExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.NetworkTime.OnServerPing$Mirror.NetworkConnection.Mirror.NetworkPingMessage$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :68 :8) {
^entry (%_conn : none, %_msg : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :68 :42)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :68 :42)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :68 :66)
cbde.store %_msg, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :68 :66)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :70 :16) // Not a variable of known type: logger
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :70 :16) // logger.LogEnabled() (InvocationExpression)
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :70 :16)

^1: // SimpleBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :70 :37) // Not a variable of known type: logger
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :70 :48) // "OnPingServerMessage  conn=" (StringLiteralExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :70 :79) // Not a variable of known type: conn
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :70 :48) // Binary expression on unsupported types "OnPingServerMessage  conn=" + conn
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :70 :37) // logger.Log("OnPingServerMessage  conn=" + conn) (InvocationExpression)
br ^2

^2: // SimpleBlock
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :72 :41) // new NetworkPongMessage             {                 clientTime = msg.clientTime,                 serverTime = LocalTime()             } (ObjectCreationExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :74 :29) // Not a variable of known type: msg
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :74 :29) // msg.clientTime (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: LocalTime
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :75 :29) // LocalTime() (InvocationExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :78 :12) // Not a variable of known type: conn
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :78 :22) // Not a variable of known type: pongMsg
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkTime.cs" :78 :12) // conn.Send(pongMsg) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function OnClientPong(none), it contains poisonous unsupported syntaxes

