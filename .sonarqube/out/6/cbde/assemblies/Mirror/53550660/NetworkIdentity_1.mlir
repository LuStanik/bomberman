func @_Mirror.NetworkIdentity.CreateNetworkBehavioursCache$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :239 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :241 :37) // GetComponents<NetworkBehaviour>() (InvocationExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :242 :16) // Not a variable of known type: networkBehavioursCache
%2 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :242 :16) // networkBehavioursCache.Length (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :242 :48) // byte (PredefinedType)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :242 :48) // byte.MaxValue (SimpleMemberAccessExpression)
%5 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :242 :16) // comparison of unknown type: networkBehavioursCache.Length > byte.MaxValue
cond_br %5, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :242 :16)

^1: // SimpleBlock
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :244 :16) // Not a variable of known type: logger
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :244 :40) // byte (PredefinedType)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :244 :40) // byte.MaxValue (SimpleMemberAccessExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :244 :117) // Identifier from another assembly: name
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :244 :32) // $"Only {byte.MaxValue} NetworkBehaviour components are allowed for NetworkIdentity: {name} because we send the index as byte." (InterpolatedStringExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :244 :160) // this (ThisExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :244 :16) // logger.LogError($"Only {byte.MaxValue} NetworkBehaviour components are allowed for NetworkIdentity: {name} because we send the index as byte.", this) (InvocationExpression)
// Entity from another assembly: Array
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :246 :57) // byte (PredefinedType)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :246 :57) // byte.MaxValue (SimpleMemberAccessExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :246 :33) // Not a variable of known type: networkBehavioursCache
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :246 :16) // Array.Resize(ref networkBehavioursCache, byte.MaxValue) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
// Skipping function SetClientOwner(none), it contains poisonous unsupported syntaxes

// Skipping function RemoveObserverInternal(none), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkIdentity.Awake$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :403 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :405 :16) // Not a variable of known type: hasSpawned
cond_br %0, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :405 :16)

^1: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :407 :16) // Not a variable of known type: logger
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :407 :35) // Identifier from another assembly: name
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :407 :32) // $"{name} has already spawned. Don't call Instantiate for NetworkIdentities that were in the scene since the beginning (aka scene objects).  Otherwise the client won't know which object to use for a SpawnSceneObject message." (InterpolatedStringExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :407 :16) // logger.LogError($"{name} has already spawned. Don't call Instantiate for NetworkIdentities that were in the scene since the beginning (aka scene objects).  Otherwise the client won't know which object to use for a SpawnSceneObject message.") (InvocationExpression)
%5 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :409 :41) // true
// Entity from another assembly: Destroy
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :410 :24) // Identifier from another assembly: gameObject
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :410 :16) // Destroy(gameObject) (InvocationExpression)
br ^2

^2: // SimpleBlock
%8 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :413 :25) // true
br ^3

^3: // ExitBlock
return

}
func @_Mirror.NetworkIdentity.OnValidate$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :416 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :420 :25) // false
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SetupIDs
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :423 :12) // SetupIDs() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkIdentity.ThisIsASceneObjectWithPrefabParent$outUnityEngine.GameObject$(none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :433 :8) {
^entry (%_prefab : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :433 :48)
cbde.store %_prefab, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :433 :48)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :435 :21) // null (NullLiteralExpression)
// Entity from another assembly: PrefabUtility
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :437 :54) // Identifier from another assembly: gameObject
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :437 :17) // PrefabUtility.IsPartOfPrefabInstance(gameObject) (InvocationExpression)
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :437 :16) // !PrefabUtility.IsPartOfPrefabInstance(gameObject) (LogicalNotExpression)
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :437 :16)

^1: // JumpBlock
%5 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :439 :23) // false
return %5 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :439 :16)

^2: // BinaryBranchBlock
// Entity from another assembly: PrefabUtility
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :441 :68) // Identifier from another assembly: gameObject
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :441 :21) // PrefabUtility.GetCorrespondingObjectFromSource(gameObject) (InvocationExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :443 :16) // Not a variable of known type: prefab
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :443 :26) // null (NullLiteralExpression)
%10 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :443 :16) // comparison of unknown type: prefab == null
cond_br %10, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :443 :16)

^3: // JumpBlock
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :445 :16) // Not a variable of known type: logger
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :445 :32) // "Failed to find prefab parent for scene object [name:" (StringLiteralExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :445 :89) // Identifier from another assembly: gameObject
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :445 :89) // gameObject.name (SimpleMemberAccessExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :445 :32) // Binary expression on unsupported types "Failed to find prefab parent for scene object [name:" + gameObject.name
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :445 :107) // "]" (StringLiteralExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :445 :32) // Binary expression on unsupported types "Failed to find prefab parent for scene object [name:" + gameObject.name + "]"
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :445 :16) // logger.LogError("Failed to find prefab parent for scene object [name:" + gameObject.name + "]") (InvocationExpression)
%19 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :446 :23) // false
return %19 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :446 :16)

^4: // JumpBlock
%20 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :448 :19) // true
return %20 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :448 :12)

^5: // ExitBlock
cbde.unreachable

}
// Skipping function GetRandomUInt(), it contains poisonous unsupported syntaxes

// Skipping function AssignSceneID(), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkIdentity.SetSceneIdSceneHashPartInternal$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :569 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :577 :31) // Identifier from another assembly: gameObject
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :577 :31) // gameObject.scene (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :577 :31) // gameObject.scene.path (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :577 :31) // gameObject.scene.path.ToLower() (InvocationExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :580 :34) // Not a variable of known type: scenePath
%6 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :580 :34) // scenePath.GetStableHashCode() (InvocationExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :580 :28) // (uint)scenePath.GetStableHashCode() (CastExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :583 :39) // Not a variable of known type: pathHash
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :583 :32) // (ulong)pathHash (CastExpression)
%11 = constant 32 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :583 :51)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :583 :32) // Binary expression on unsupported types (ulong)pathHash << 32
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :586 :23) // Not a variable of known type: sceneId
%15 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :586 :33) // 0xFFFFFFFF (NumericLiteralExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :586 :23) // Binary expression on unsupported types sceneId & 0xFFFFFFFF
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :586 :47) // Not a variable of known type: shiftedHash
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :586 :22) // Binary expression on unsupported types (sceneId & 0xFFFFFFFF) | shiftedHash
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :589 :16) // Not a variable of known type: logger
%20 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :589 :16) // logger.LogEnabled() (InvocationExpression)
cond_br %20, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :589 :16)

^1: // SimpleBlock
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :589 :37) // Not a variable of known type: logger
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :589 :48) // Identifier from another assembly: name
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :589 :55) // " in scene=" (StringLiteralExpression)
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :589 :48) // Binary expression on unsupported types name + " in scene="
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :589 :70) // Identifier from another assembly: gameObject
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :589 :70) // gameObject.scene (SimpleMemberAccessExpression)
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :589 :70) // gameObject.scene.name (SimpleMemberAccessExpression)
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :589 :48) // Binary expression on unsupported types name + " in scene=" + gameObject.scene.name
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :589 :94) // " scene index hash(" (StringLiteralExpression)
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :589 :48) // Binary expression on unsupported types name + " in scene=" + gameObject.scene.name + " scene index hash("
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :589 :117) // Not a variable of known type: pathHash
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :589 :135) // "X" (StringLiteralExpression)
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :589 :117) // pathHash.ToString("X") (InvocationExpression)
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :589 :48) // Binary expression on unsupported types name + " in scene=" + gameObject.scene.name + " scene index hash(" + pathHash.ToString("X")
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :589 :142) // ") copied into sceneId: " (StringLiteralExpression)
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :589 :48) // Binary expression on unsupported types name + " in scene=" + gameObject.scene.name + " scene index hash(" + pathHash.ToString("X") + ") copied into sceneId: "
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :589 :170) // Not a variable of known type: sceneId
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :589 :187) // "X" (StringLiteralExpression)
%39 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :589 :170) // sceneId.ToString("X") (InvocationExpression)
%40 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :589 :48) // Binary expression on unsupported types name + " in scene=" + gameObject.scene.name + " scene index hash(" + pathHash.ToString("X") + ") copied into sceneId: " + sceneId.ToString("X")
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :589 :37) // logger.Log(name + " in scene=" + gameObject.scene.name + " scene index hash(" + pathHash.ToString("X") + ") copied into sceneId: " + sceneId.ToString("X")) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
// Skipping function SetupIDs(), it contains poisonous unsupported syntaxes

// Skipping function OnDestroy(), it contains poisonous unsupported syntaxes

// Skipping function OnStartServer(), it contains poisonous unsupported syntaxes

// Skipping function OnStopServer(), it contains poisonous unsupported syntaxes

// Skipping function OnStartClient(), it contains poisonous unsupported syntaxes

// Skipping function OnStartLocalPlayer(), it contains poisonous unsupported syntaxes

// Skipping function NotifyAuthority(), it contains poisonous unsupported syntaxes

// Skipping function OnStartAuthority(), it contains poisonous unsupported syntaxes

// Skipping function OnStopAuthority(), it contains poisonous unsupported syntaxes

// Skipping function OnSetHostVisibility(i1), it contains poisonous unsupported syntaxes

// Skipping function OnCheckObserver(none), it contains poisonous unsupported syntaxes

// Skipping function OnStopClient(), it contains poisonous unsupported syntaxes

// Skipping function OnSerializeSafely(none, none, i1), it contains poisonous unsupported syntaxes

// Skipping function OnSerializeAllSafely(i1, none, i32, none, i32), it contains poisonous unsupported syntaxes

// Skipping function OnDeserializeSafely(none, none, i1), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkIdentity.OnDeserializeAllSafely$Mirror.NetworkReader.bool$(none, i1) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1050 :8) {
^entry (%_reader : none, %_initialState : i1):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1050 :45)
cbde.store %_reader, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1050 :45)
%1 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1050 :67)
cbde.store %_initialState, %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1050 :67)
br ^0

^0: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1053 :44) // Not a variable of known type: NetworkBehaviours
br ^1

^1: // BinaryBranchBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1054 :19) // Not a variable of known type: reader
%5 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1054 :19) // reader.Position (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1054 :37) // Not a variable of known type: reader
%7 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1054 :37) // reader.Length (SimpleMemberAccessExpression)
%8 = cmpi "slt", %5, %7 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1054 :19)
cond_br %8, ^2, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1054 :19)

^2: // BinaryBranchBlock
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1057 :29) // Not a variable of known type: reader
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1057 :29) // reader.ReadByte() (InvocationExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1058 :20) // Not a variable of known type: index
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1058 :28) // Not a variable of known type: components
%14 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1058 :28) // components.Length (SimpleMemberAccessExpression)
%15 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1058 :20) // comparison of unknown type: index < components.Length
cond_br %15, ^4, ^1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1058 :20)

^4: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnDeserializeSafely
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1061 :40) // Not a variable of known type: components
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1061 :51) // Not a variable of known type: index
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1061 :40) // components[index] (ElementAccessExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1061 :59) // Not a variable of known type: reader
%20 = cbde.load %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1061 :67)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1061 :20) // OnDeserializeSafely(components[index], reader, initialState) (InvocationExpression)
br ^1

^3: // ExitBlock
return

}
// Skipping function HandleRemoteCall(i32, i32, none, none, none), it contains poisonous unsupported syntaxes

// Skipping function GetCommandInfo(i32, i32), it contains poisonous unsupported syntaxes

// Skipping function ClearObservers(), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkIdentity.AddObserver$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1142 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1142 :34)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1142 :34)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1144 :16) // Not a variable of known type: observers
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1144 :29) // null (NullLiteralExpression)
%3 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1144 :16) // comparison of unknown type: observers == null
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1144 :16)

^1: // JumpBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1146 :16) // Not a variable of known type: logger
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1146 :32) // "AddObserver for " (StringLiteralExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1146 :53) // Identifier from another assembly: gameObject
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1146 :32) // Binary expression on unsupported types "AddObserver for " + gameObject
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1146 :66) // " observer list is null" (StringLiteralExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1146 :32) // Binary expression on unsupported types "AddObserver for " + gameObject + " observer list is null"
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1146 :16) // logger.LogError("AddObserver for " + gameObject + " observer list is null") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1147 :16)

^2: // BinaryBranchBlock
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1150 :16) // Not a variable of known type: observers
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1150 :38) // Not a variable of known type: conn
%13 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1150 :38) // conn.connectionId (SimpleMemberAccessExpression)
%14 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1150 :16) // observers.ContainsKey(conn.connectionId) (InvocationExpression)
cond_br %14, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1150 :16)

^3: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1154 :16)

^4: // BinaryBranchBlock
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1157 :16) // Not a variable of known type: logger
%16 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1157 :16) // logger.LogEnabled() (InvocationExpression)
cond_br %16, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1157 :16)

^5: // SimpleBlock
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1157 :37) // Not a variable of known type: logger
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1157 :48) // "Added observer " (StringLiteralExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1157 :68) // Not a variable of known type: conn
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1157 :68) // conn.address (SimpleMemberAccessExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1157 :48) // Binary expression on unsupported types "Added observer " + conn.address
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1157 :83) // " added for " (StringLiteralExpression)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1157 :48) // Binary expression on unsupported types "Added observer " + conn.address + " added for "
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1157 :99) // Identifier from another assembly: gameObject
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1157 :48) // Binary expression on unsupported types "Added observer " + conn.address + " added for " + gameObject
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1157 :37) // logger.Log("Added observer " + conn.address + " added for " + gameObject) (InvocationExpression)
br ^6

^6: // SimpleBlock
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1159 :12) // Not a variable of known type: observers
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1159 :22) // Not a variable of known type: conn
%29 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1159 :22) // conn.connectionId (SimpleMemberAccessExpression)
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1159 :12) // observers[conn.connectionId] (ElementAccessExpression)
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1159 :43) // Not a variable of known type: conn
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1160 :12) // Not a variable of known type: conn
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1160 :30) // this (ThisExpression)
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1160 :12) // conn.AddToVisList(this) (InvocationExpression)
br ^7

^7: // ExitBlock
return

}
func @_Mirror.NetworkIdentity.GetNewObservers$System.Collections.Generic.HashSet$Mirror.NetworkConnection$.bool$(none, i1) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1172 :8) {
^entry (%_observersSet : none, %_initialize : i1):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1172 :38)
cbde.store %_observersSet, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1172 :38)
%1 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1172 :79)
cbde.store %_initialize, %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1172 :79)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1174 :12) // Not a variable of known type: observersSet
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1174 :12) // observersSet.Clear() (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1176 :16) // Not a variable of known type: visibility
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1176 :30) // null (NullLiteralExpression)
%6 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1176 :16) // comparison of unknown type: visibility != null
cond_br %6, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1176 :16)

^1: // JumpBlock
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1178 :16) // Not a variable of known type: visibility
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1178 :46) // Not a variable of known type: observersSet
%9 = cbde.load %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1178 :60)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1178 :16) // visibility.OnRebuildObservers(observersSet, initialize) (InvocationExpression)
%11 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1179 :23) // true
return %11 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1179 :16)

^2: // JumpBlock
%12 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1184 :19) // false
return %12 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1184 :12)

^3: // ExitBlock
cbde.unreachable

}
// Skipping function AddAllReadyServerConnectionsToObservers(), it contains poisonous unsupported syntaxes

// Skipping function RebuildObservers(i1), it contains poisonous unsupported syntaxes

// Skipping function AssignClientAuthority(none), it contains poisonous unsupported syntaxes

// Skipping function RemoveClientAuthority(), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkIdentity.Reset$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1397 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ResetSyncObjects
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1400 :12) // ResetSyncObjects() (InvocationExpression)
%1 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1402 :25) // false
%2 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1403 :28) // false
%3 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1404 :23) // false
%4 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1405 :23) // false
%5 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1407 :20)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1408 :33) // null (NullLiteralExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1409 :33) // null (NullLiteralExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1410 :37) // null (NullLiteralExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ClearObservers
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1412 :12) // ClearObservers() (InvocationExpression)
%10 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1414 :16) // Not a variable of known type: isLocalPlayer
cond_br %10, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1414 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ClientScene
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkIdentity.cs" :1416 :16) // ClientScene.ClearLocalPlayer() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
// Skipping function ServerUpdate(), it contains poisonous unsupported syntaxes

// Skipping function SendUpdateVarsMessage(), it contains poisonous unsupported syntaxes

// Skipping function ClearAllComponentsDirtyBits(), it contains poisonous unsupported syntaxes

// Skipping function ClearDirtyComponentsDirtyBits(), it contains poisonous unsupported syntaxes

// Skipping function ResetSyncObjects(), it contains poisonous unsupported syntaxes

