func @_Mirror.NetworkManagerHUD.Awake$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :33 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :35 :22) // GetComponent<NetworkManager>() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function OnGUI(), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkManagerHUD.StartButtons$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :72 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkClient
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :74 :17) // NetworkClient.active (SimpleMemberAccessExpression)
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :74 :16) // !NetworkClient.active (LogicalNotExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :74 :16)

^1: // BinaryBranchBlock
// Entity from another assembly: Application
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :77 :20) // Application.platform (SimpleMemberAccessExpression)
// Entity from another assembly: RuntimePlatform
%3 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :77 :44) // RuntimePlatform.WebGLPlayer (SimpleMemberAccessExpression)
%4 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :77 :20) // comparison of unknown type: Application.platform != RuntimePlatform.WebGLPlayer
cond_br %4, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :77 :20)

^3: // BinaryBranchBlock
// Entity from another assembly: GUILayout
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :79 :41) // "Host (Server + Client)" (StringLiteralExpression)
%6 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :79 :24) // GUILayout.Button("Host (Server + Client)") (InvocationExpression)
cond_br %6, ^5, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :79 :24)

^5: // SimpleBlock
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :81 :24) // Not a variable of known type: manager
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :81 :24) // manager.StartHost() (InvocationExpression)
br ^4

^4: // BinaryBranchBlock
// Entity from another assembly: GUILayout
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :86 :16) // GUILayout.BeginHorizontal() (InvocationExpression)
// Entity from another assembly: GUILayout
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :87 :37) // "Client" (StringLiteralExpression)
%11 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :87 :20) // GUILayout.Button("Client") (InvocationExpression)
cond_br %11, ^6, ^7 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :87 :20)

^6: // SimpleBlock
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :89 :20) // Not a variable of known type: manager
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :89 :20) // manager.StartClient() (InvocationExpression)
br ^7

^7: // BinaryBranchBlock
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :91 :16) // Not a variable of known type: manager
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :91 :16) // manager.networkAddress (SimpleMemberAccessExpression)
// Entity from another assembly: GUILayout
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :91 :61) // Not a variable of known type: manager
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :91 :61) // manager.networkAddress (SimpleMemberAccessExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :91 :41) // GUILayout.TextField(manager.networkAddress) (InvocationExpression)
// Entity from another assembly: GUILayout
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :92 :16) // GUILayout.EndHorizontal() (InvocationExpression)
// Entity from another assembly: Application
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :95 :20) // Application.platform (SimpleMemberAccessExpression)
// Entity from another assembly: RuntimePlatform
%21 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :95 :44) // RuntimePlatform.WebGLPlayer (SimpleMemberAccessExpression)
%22 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :95 :20) // comparison of unknown type: Application.platform == RuntimePlatform.WebGLPlayer
cond_br %22, ^8, ^9 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :95 :20)

^8: // SimpleBlock
// Entity from another assembly: GUILayout
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :98 :34) // "(  WebGL cannot be server  )" (StringLiteralExpression)
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :98 :20) // GUILayout.Box("(  WebGL cannot be server  )") (InvocationExpression)
br ^10

^9: // BinaryBranchBlock
// Entity from another assembly: GUILayout
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :102 :41) // "Server Only" (StringLiteralExpression)
%26 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :102 :24) // GUILayout.Button("Server Only") (InvocationExpression)
cond_br %26, ^11, ^10 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :102 :24)

^11: // SimpleBlock
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :102 :57) // Not a variable of known type: manager
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :102 :57) // manager.StartServer() (InvocationExpression)
br ^10

^2: // BinaryBranchBlock
// Entity from another assembly: GUILayout
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :108 :32) // "Connecting to " (StringLiteralExpression)
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :108 :51) // Not a variable of known type: manager
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :108 :51) // manager.networkAddress (SimpleMemberAccessExpression)
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :108 :32) // Binary expression on unsupported types "Connecting to " + manager.networkAddress
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :108 :76) // ".." (StringLiteralExpression)
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :108 :32) // Binary expression on unsupported types "Connecting to " + manager.networkAddress + ".."
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :108 :16) // GUILayout.Label("Connecting to " + manager.networkAddress + "..") (InvocationExpression)
// Entity from another assembly: GUILayout
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :109 :37) // "Cancel Connection Attempt" (StringLiteralExpression)
%37 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :109 :20) // GUILayout.Button("Cancel Connection Attempt") (InvocationExpression)
cond_br %37, ^12, ^10 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :109 :20)

^12: // SimpleBlock
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :111 :20) // Not a variable of known type: manager
%39 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :111 :20) // manager.StopClient() (InvocationExpression)
br ^10

^10: // ExitBlock
return

}
func @_Mirror.NetworkManagerHUD.StatusLabels$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :116 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkServer
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :119 :16) // NetworkServer.active (SimpleMemberAccessExpression)
cond_br %0, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :119 :16)

^1: // SimpleBlock
// Entity from another assembly: GUILayout
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :121 :32) // "Server: active. Transport: " (StringLiteralExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Transport
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :121 :64) // Transport.activeTransport (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :121 :32) // Binary expression on unsupported types "Server: active. Transport: " + Transport.activeTransport
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :121 :16) // GUILayout.Label("Server: active. Transport: " + Transport.activeTransport) (InvocationExpression)
br ^2

^2: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkClient
%5 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :123 :16) // NetworkClient.isConnected (SimpleMemberAccessExpression)
cond_br %5, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :123 :16)

^3: // SimpleBlock
// Entity from another assembly: GUILayout
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :125 :32) // "Client: address=" (StringLiteralExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :125 :53) // Not a variable of known type: manager
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :125 :53) // manager.networkAddress (SimpleMemberAccessExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :125 :32) // Binary expression on unsupported types "Client: address=" + manager.networkAddress
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkManagerHUD.cs" :125 :16) // GUILayout.Label("Client: address=" + manager.networkAddress) (InvocationExpression)
br ^4

^4: // ExitBlock
return

}
// Skipping function StopButtons(), it contains poisonous unsupported syntaxes

