func @_Mirror.NetworkAuthenticator.OnStartServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkAuthenticator.cs" :37 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkAuthenticator.OnStopServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkAuthenticator.cs" :43 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkAuthenticator.ServerAccept$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkAuthenticator.cs" :51 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkAuthenticator.cs" :51 :36)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkAuthenticator.cs" :51 :36)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkAuthenticator.cs" :53 :12) // Not a variable of known type: OnServerAuthenticated
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkAuthenticator.cs" :53 :41) // Not a variable of known type: conn
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkAuthenticator.cs" :53 :12) // OnServerAuthenticated.Invoke(conn) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkAuthenticator.ServerReject$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkAuthenticator.cs" :56 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkAuthenticator.cs" :56 :36)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkAuthenticator.cs" :56 :36)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkAuthenticator.cs" :58 :12) // Not a variable of known type: conn
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkAuthenticator.cs" :58 :12) // conn.Disconnect() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkAuthenticator.OnStartClient$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkAuthenticator.cs" :69 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkAuthenticator.OnStopClient$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkAuthenticator.cs" :75 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkAuthenticator.ClientAccept$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkAuthenticator.cs" :83 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkAuthenticator.cs" :83 :36)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkAuthenticator.cs" :83 :36)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkAuthenticator.cs" :85 :12) // Not a variable of known type: OnClientAuthenticated
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkAuthenticator.cs" :85 :41) // Not a variable of known type: conn
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkAuthenticator.cs" :85 :12) // OnClientAuthenticated.Invoke(conn) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkAuthenticator.ClientReject$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkAuthenticator.cs" :88 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkAuthenticator.cs" :88 :36)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkAuthenticator.cs" :88 :36)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkAuthenticator.cs" :91 :12) // Not a variable of known type: conn
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkAuthenticator.cs" :91 :12) // conn.isAuthenticated (SimpleMemberAccessExpression)
%3 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkAuthenticator.cs" :91 :35) // false
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkAuthenticator.cs" :94 :12) // Not a variable of known type: conn
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkAuthenticator.cs" :94 :12) // conn.Disconnect() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function OnValidate(), it contains poisonous unsupported syntaxes

