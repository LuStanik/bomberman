func @_Mirror.Logging.EditorLogSettingsLoader.Init$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :8 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: LoadLogSettingsIntoDictionary
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :12 :12) // LoadLogSettingsIntoDictionary() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Logging.EditorLogSettingsLoader.LoadLogSettingsIntoDictionary$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :15 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: FindLogSettings
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :17 :35) // FindLogSettings() (InvocationExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :18 :16) // Not a variable of known type: settings
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :18 :28) // null (NullLiteralExpression)
%4 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :18 :16) // comparison of unknown type: settings != null
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :18 :16)

^1: // SimpleBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :20 :16) // Not a variable of known type: settings
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: LogFactory
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :20 :44) // LogFactory.loggers (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :20 :16) // settings.LoadIntoDictionary(LogFactory.loggers) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.Logging.EditorLogSettingsLoader.FindLogSettings$$() -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :25 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :27 :16) // Not a variable of known type: cache
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :27 :25) // null (NullLiteralExpression)
%2 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :27 :16) // comparison of unknown type: cache != null
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :27 :16)

^1: // JumpBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :28 :23) // Not a variable of known type: cache
return %3 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :28 :16)

^2: // BinaryBranchBlock
// Entity from another assembly: AssetDatabase
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :30 :59) // "t:" (StringLiteralExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :30 :66) // nameof(LogSettings) (InvocationExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :30 :59) // Binary expression on unsupported types "t:" + nameof(LogSettings)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :30 :34) // AssetDatabase.FindAssets("t:" + nameof(LogSettings)) (InvocationExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :31 :16) // Not a variable of known type: assetGuids
%10 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :31 :16) // assetGuids.Length (SimpleMemberAccessExpression)
%11 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :31 :37)
%12 = cmpi "eq", %10, %11 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :31 :16)
cond_br %12, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :31 :16)

^3: // JumpBlock
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :32 :23) // null (NullLiteralExpression)
return %13 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :32 :16)

^4: // BinaryBranchBlock
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :34 :31) // Not a variable of known type: assetGuids
%15 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :34 :42)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :34 :31) // assetGuids[0] (ElementAccessExpression)
// Entity from another assembly: AssetDatabase
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :36 :56) // Not a variable of known type: firstGuid
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :36 :26) // AssetDatabase.GUIDToAssetPath(firstGuid) (InvocationExpression)
// Entity from another assembly: AssetDatabase
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :37 :63) // Not a variable of known type: path
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :37 :20) // AssetDatabase.LoadAssetAtPath<LogSettings>(path) (InvocationExpression)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :39 :16) // Not a variable of known type: assetGuids
%24 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :39 :16) // assetGuids.Length (SimpleMemberAccessExpression)
%25 = constant 2 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :39 :36)
%26 = cmpi "sgt", %24, %25 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :39 :16)
cond_br %26, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :39 :16)

^5: // SimpleBlock
// Entity from another assembly: Debug
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :41 :33) // "Found more than one LogSettings, Delete extra settings. Using first asset found: " (StringLiteralExpression)
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :41 :119) // Not a variable of known type: path
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :41 :33) // Binary expression on unsupported types "Found more than one LogSettings, Delete extra settings. Using first asset found: " + path
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :41 :16) // Debug.LogWarning("Found more than one LogSettings, Delete extra settings. Using first asset found: " + path) (InvocationExpression)
br ^6

^6: // JumpBlock
// Entity from another assembly: Debug
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :43 :25) // Not a variable of known type: cache
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :43 :34) // null (NullLiteralExpression)
%33 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :43 :25) // comparison of unknown type: cache != null
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :43 :40) // "Failed to load asset at: " (StringLiteralExpression)
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :43 :70) // Not a variable of known type: path
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :43 :40) // Binary expression on unsupported types "Failed to load asset at: " + path
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :43 :12) // Debug.Assert(cache != null, "Failed to load asset at: " + path) (InvocationExpression)
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :45 :19) // Not a variable of known type: cache
return %38 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\EditorLogSettingsLoader.cs" :45 :12)

^7: // ExitBlock
cbde.unreachable

}
