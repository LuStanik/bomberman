func @_Mirror.NetworkConnection.SetHandlers$System.Collections.Generic.Dictionary$int.Mirror.NetworkMessageDelegate$$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :103 :8) {
^entry (%_handlers : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :103 :34)
cbde.store %_handlers, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :103 :34)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :105 :30) // Not a variable of known type: handlers
br ^1

^1: // ExitBlock
return

}
// Skipping function Send(none, i32), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkConnection.ValidatePacketSize$System.ArraySegment$byte$.int$(none, i32) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :131 :8) {
^entry (%_segment : none, %_channelId : i32):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :131 :58)
cbde.store %_segment, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :131 :58)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :131 :86)
cbde.store %_channelId, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :131 :86)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :133 :16) // Not a variable of known type: segment
%3 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :133 :16) // segment.Count (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Transport
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :133 :32) // Transport.activeTransport (SimpleMemberAccessExpression)
%5 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :133 :75)
%6 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :133 :32) // Transport.activeTransport.GetMaxPacketSize(channelId) (InvocationExpression)
%7 = cmpi "sgt", %3, %6 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :133 :16)
cond_br %7, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :133 :16)

^1: // JumpBlock
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :135 :16) // Not a variable of known type: logger
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :135 :32) // "NetworkConnection.ValidatePacketSize: cannot send packet larger than " (StringLiteralExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Transport
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :135 :106) // Transport.activeTransport (SimpleMemberAccessExpression)
%11 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :135 :149)
%12 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :135 :106) // Transport.activeTransport.GetMaxPacketSize(channelId) (InvocationExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :135 :32) // Binary expression on unsupported types "NetworkConnection.ValidatePacketSize: cannot send packet larger than " + Transport.activeTransport.GetMaxPacketSize(channelId)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :135 :162) // " bytes" (StringLiteralExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :135 :32) // Binary expression on unsupported types "NetworkConnection.ValidatePacketSize: cannot send packet larger than " + Transport.activeTransport.GetMaxPacketSize(channelId) + " bytes"
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :135 :16) // logger.LogError("NetworkConnection.ValidatePacketSize: cannot send packet larger than " + Transport.activeTransport.GetMaxPacketSize(channelId) + " bytes") (InvocationExpression)
%17 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :136 :23) // false
return %17 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :136 :16)

^2: // BinaryBranchBlock
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :139 :16) // Not a variable of known type: segment
%19 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :139 :16) // segment.Count (SimpleMemberAccessExpression)
%20 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :139 :33)
%21 = cmpi "eq", %19, %20 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :139 :16)
cond_br %21, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :139 :16)

^3: // JumpBlock
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :142 :16) // Not a variable of known type: logger
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :142 :32) // "NetworkConnection.ValidatePacketSize: cannot send zero bytes" (StringLiteralExpression)
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :142 :16) // logger.LogError("NetworkConnection.ValidatePacketSize: cannot send zero bytes") (InvocationExpression)
%25 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :143 :23) // false
return %25 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :143 :16)

^4: // JumpBlock
%26 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :147 :19) // true
return %26 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :147 :12)

^5: // ExitBlock
cbde.unreachable

}
func @_Mirror.NetworkConnection.ToString$$() -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :154 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :156 :33) // Not a variable of known type: connectionId
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :156 :19) // $"connection({connectionId})" (InterpolatedStringExpression)
return %1 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :156 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.NetworkConnection.AddToVisList$Mirror.NetworkIdentity$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :159 :8) {
^entry (%_identity : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :159 :35)
cbde.store %_identity, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :159 :35)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :161 :12) // Not a variable of known type: visList
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :161 :24) // Not a variable of known type: identity
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :161 :12) // visList.Add(identity) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkServer
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :164 :44) // Not a variable of known type: identity
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :164 :54) // this (ThisExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :164 :12) // NetworkServer.ShowForConnection(identity, this) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkConnection.RemoveFromVisList$Mirror.NetworkIdentity.bool$(none, i1) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :167 :8) {
^entry (%_identity : none, %_isDestroyed : i1):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :167 :40)
cbde.store %_identity, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :167 :40)
%1 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :167 :66)
cbde.store %_isDestroyed, %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :167 :66)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :169 :12) // Not a variable of known type: visList
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :169 :27) // Not a variable of known type: identity
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :169 :12) // visList.Remove(identity) (InvocationExpression)
%5 = cbde.load %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :171 :17)
%6 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :171 :16) // !isDestroyed (LogicalNotExpression)
cond_br %6, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :171 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkServer
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :174 :48) // Not a variable of known type: identity
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :174 :58) // this (ThisExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :174 :16) // NetworkServer.HideForConnection(identity, this) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
// Skipping function RemoveObservers(), it contains poisonous unsupported syntaxes

// Skipping function InvokeHandler(i32, none, i32), it contains poisonous unsupported syntaxes

// Skipping function InvokeHandler(none, i32), it contains poisonous unsupported syntaxes

// Skipping function TransportReceive(none, i32), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkConnection.AddOwnedObject$Mirror.NetworkIdentity$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :276 :8) {
^entry (%_obj : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :276 :37)
cbde.store %_obj, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :276 :37)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :278 :12) // Not a variable of known type: clientOwnedObjects
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :278 :35) // Not a variable of known type: obj
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :278 :12) // clientOwnedObjects.Add(obj) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkConnection.RemoveOwnedObject$Mirror.NetworkIdentity$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :281 :8) {
^entry (%_obj : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :281 :40)
cbde.store %_obj, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :281 :40)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :283 :12) // Not a variable of known type: clientOwnedObjects
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :283 :38) // Not a variable of known type: obj
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkConnection.cs" :283 :12) // clientOwnedObjects.Remove(obj) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function DestroyOwnedObjects(), it contains poisonous unsupported syntaxes

