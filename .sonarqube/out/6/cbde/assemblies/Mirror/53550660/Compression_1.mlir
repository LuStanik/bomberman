func @_Mirror.Compression.CompressQuaternion$UnityEngine.Quaternion$(none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :69 :8) {
^entry (%_value : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :69 :46)
cbde.store %_value, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :69 :46)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :71 :20) // Not a variable of known type: value
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :71 :20) // value.normalized (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: FindLargestIndex
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :73 :48) // Not a variable of known type: value
%4 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :73 :31) // FindLargestIndex(value) (InvocationExpression)
%5 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :73 :16) // largestIndex
cbde.store %4, %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :73 :16)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetSmallerDimensions
%6 = cbde.load %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :74 :49)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :74 :63) // Not a variable of known type: value
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :74 :28) // GetSmallerDimensions(largestIndex, value) (InvocationExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :77 :16) // Not a variable of known type: value
%11 = cbde.load %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :77 :22)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :77 :16) // value[largestIndex] (ElementAccessExpression)
%13 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :77 :38)
%14 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :77 :16) // comparison of unknown type: value[largestIndex] < 0
cond_br %14, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :77 :16)

^1: // SimpleBlock
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :79 :16) // Not a variable of known type: small
%16 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :79 :26)
%17 = cbde.neg %16 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :79 :25)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :79 :16) // Binary expression on unsupported types small *= -1
br ^2

^2: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ScaleToUInt
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :82 :33) // Not a variable of known type: small
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :82 :33) // small.x (SimpleMemberAccessExpression)
%21 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :82 :42) // QuaternionMinValue (IdentifierName)
%22 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :82 :62) // QuaternionMaxValue (IdentifierName)
%23 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :82 :82)
%24 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :82 :85) // QuaternionUintRange (IdentifierName)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :82 :21) // ScaleToUInt(small.x, QuaternionMinValue, QuaternionMaxValue, 0, QuaternionUintRange) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ScaleToUInt
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :83 :33) // Not a variable of known type: small
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :83 :33) // small.y (SimpleMemberAccessExpression)
%29 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :83 :42) // QuaternionMinValue (IdentifierName)
%30 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :83 :62) // QuaternionMaxValue (IdentifierName)
%31 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :83 :82)
%32 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :83 :85) // QuaternionUintRange (IdentifierName)
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :83 :21) // ScaleToUInt(small.y, QuaternionMinValue, QuaternionMaxValue, 0, QuaternionUintRange) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ScaleToUInt
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :84 :33) // Not a variable of known type: small
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :84 :33) // small.z (SimpleMemberAccessExpression)
%37 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :84 :42) // QuaternionMinValue (IdentifierName)
%38 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :84 :62) // QuaternionMaxValue (IdentifierName)
%39 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :84 :82)
%40 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :84 :85) // QuaternionUintRange (IdentifierName)
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :84 :21) // ScaleToUInt(small.z, QuaternionMinValue, QuaternionMaxValue, 0, QuaternionUintRange) (InvocationExpression)
%43 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :87 :26) // Not a variable of known type: a
%44 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :87 :30) // Not a variable of known type: b
%45 = constant 10 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :87 :35)
%46 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :87 :30) // Binary expression on unsupported types b << 10
%47 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :87 :26) // Binary expression on unsupported types a | b << 10
%48 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :87 :40) // Not a variable of known type: c
%49 = constant 20 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :87 :45)
%50 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :87 :40) // Binary expression on unsupported types c << 20
%51 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :87 :26) // Binary expression on unsupported types a | b << 10 | c << 20
%52 = cbde.load %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :87 :56)
%53 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :87 :50) // (uint)largestIndex (CastExpression)
%54 = constant 30 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :87 :72)
%55 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :87 :50) // Binary expression on unsupported types (uint)largestIndex << 30
%56 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :87 :26) // Binary expression on unsupported types a | b << 10 | c << 20 | (uint)largestIndex << 30
%58 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :89 :19) // Not a variable of known type: packed
return %58 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :89 :12)

^3: // ExitBlock
cbde.unreachable

}
// Skipping function FindLargestIndex(none), it contains poisonous unsupported syntaxes

// Skipping function GetSmallerDimensions(i32, none), it contains poisonous unsupported syntaxes

func @_Mirror.Compression.DecompressQuaternion$uint$(none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :138 :8) {
^entry (%_packed : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :138 :54)
cbde.store %_packed, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :138 :54)
br ^0

^0: // JumpBlock
%1 = constant 1023 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :141 :30)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :145 :21) // Not a variable of known type: packed
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :145 :30) // Not a variable of known type: mask
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :145 :21) // Binary expression on unsupported types packed & mask
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :146 :22) // Not a variable of known type: packed
%9 = constant 10 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :146 :32)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :146 :22) // Binary expression on unsupported types packed >> 10
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :146 :38) // Not a variable of known type: mask
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :146 :21) // Binary expression on unsupported types (packed >> 10) & mask
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :147 :22) // Not a variable of known type: packed
%15 = constant 20 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :147 :32)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :147 :22) // Binary expression on unsupported types packed >> 20
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :147 :38) // Not a variable of known type: mask
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :147 :21) // Binary expression on unsupported types (packed >> 20) & mask
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :148 :33) // Not a variable of known type: packed
%21 = constant 30 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :148 :43)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :148 :33) // Binary expression on unsupported types packed >> 30
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :148 :49) // Not a variable of known type: mask
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :148 :32) // Binary expression on unsupported types (packed >> 30) & mask
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ScaleFromUInt
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :150 :36) // Not a variable of known type: a
%27 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :150 :39) // QuaternionMinValue (IdentifierName)
%28 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :150 :59) // QuaternionMaxValue (IdentifierName)
%29 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :150 :79)
%30 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :150 :82) // QuaternionUintRange (IdentifierName)
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :150 :22) // ScaleFromUInt(a, QuaternionMinValue, QuaternionMaxValue, 0, QuaternionUintRange) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ScaleFromUInt
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :151 :36) // Not a variable of known type: b
%34 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :151 :39) // QuaternionMinValue (IdentifierName)
%35 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :151 :59) // QuaternionMaxValue (IdentifierName)
%36 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :151 :79)
%37 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :151 :82) // QuaternionUintRange (IdentifierName)
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :151 :22) // ScaleFromUInt(b, QuaternionMinValue, QuaternionMaxValue, 0, QuaternionUintRange) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ScaleFromUInt
%40 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :152 :36) // Not a variable of known type: c
%41 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :152 :39) // QuaternionMinValue (IdentifierName)
%42 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :152 :59) // QuaternionMaxValue (IdentifierName)
%43 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :152 :79)
%44 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :152 :82) // QuaternionUintRange (IdentifierName)
%45 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :152 :22) // ScaleFromUInt(c, QuaternionMinValue, QuaternionMaxValue, 0, QuaternionUintRange) (InvocationExpression)
%47 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :154 :40) // Not a variable of known type: x
%48 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :154 :43) // Not a variable of known type: y
%49 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :154 :46) // Not a variable of known type: z
%50 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :154 :28) // new Vector3(x, y, z) (ObjectCreationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: FromSmallerDimensions
%52 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :155 :43) // Not a variable of known type: largestIndex
%53 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :155 :57) // Not a variable of known type: small
%54 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :155 :21) // FromSmallerDimensions(largestIndex, small) (InvocationExpression)
%55 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :156 :19) // Not a variable of known type: result
return %55 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :156 :12)

^1: // ExitBlock
cbde.unreachable

}
// Skipping function FromSmallerDimensions(none, none), it contains poisonous unsupported syntaxes

func @_Mirror.Compression.ScaleToUInt$float.float.float.uint.uint$(none, none, none, none, none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :193 :8) {
^entry (%_value : none, %_minFloat : none, %_maxFloat : none, %_minUint : none, %_maxUint : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :193 :39)
cbde.store %_value, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :193 :39)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :193 :52)
cbde.store %_minFloat, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :193 :52)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :193 :68)
cbde.store %_maxFloat, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :193 :68)
%3 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :193 :84)
cbde.store %_minUint, %3 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :193 :84)
%4 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :193 :98)
cbde.store %_maxUint, %4 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :193 :98)
br ^0

^0: // BinaryBranchBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :196 :16) // Not a variable of known type: value
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :196 :24) // Not a variable of known type: maxFloat
%7 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :196 :16) // comparison of unknown type: value > maxFloat
cond_br %7, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :196 :16)

^1: // JumpBlock
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :196 :43) // Not a variable of known type: maxUint
return %8 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :196 :36)

^2: // BinaryBranchBlock
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :197 :16) // Not a variable of known type: value
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :197 :24) // Not a variable of known type: minFloat
%11 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :197 :16) // comparison of unknown type: value < minFloat
cond_br %11, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :197 :16)

^3: // JumpBlock
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :197 :43) // Not a variable of known type: minUint
return %12 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :197 :36)

^4: // JumpBlock
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :199 :31) // Not a variable of known type: maxFloat
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :199 :42) // Not a variable of known type: minFloat
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :199 :31) // Binary expression on unsupported types maxFloat - minFloat
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :200 :29) // Not a variable of known type: maxUint
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :200 :39) // Not a variable of known type: minUint
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :200 :29) // Binary expression on unsupported types maxUint - minUint
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :203 :35) // Not a variable of known type: value
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :203 :43) // Not a variable of known type: minFloat
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :203 :35) // Binary expression on unsupported types value - minFloat
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :203 :55) // Not a variable of known type: rangeFloat
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :203 :34) // Binary expression on unsupported types (value - minFloat) / rangeFloat
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :205 :29) // Not a variable of known type: valueRelative
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :205 :45) // Not a variable of known type: rangeUint
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :205 :29) // Binary expression on unsupported types valueRelative * rangeUint
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :205 :57) // Not a variable of known type: minUint
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :205 :29) // Binary expression on unsupported types valueRelative * rangeUint + minUint
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :207 :25) // Not a variable of known type: outValue
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :207 :19) // (uint)outValue (CastExpression)
return %34 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :207 :12)

^5: // ExitBlock
cbde.unreachable

}
func @_Mirror.Compression.ScaleFromUInt$uint.float.float.uint.uint$(none, none, none, none, none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :219 :8) {
^entry (%_value : none, %_minFloat : none, %_maxFloat : none, %_minUint : none, %_maxUint : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :219 :42)
cbde.store %_value, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :219 :42)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :219 :54)
cbde.store %_minFloat, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :219 :54)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :219 :70)
cbde.store %_maxFloat, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :219 :70)
%3 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :219 :86)
cbde.store %_minUint, %3 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :219 :86)
%4 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :219 :100)
cbde.store %_maxUint, %4 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :219 :100)
br ^0

^0: // BinaryBranchBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :222 :16) // Not a variable of known type: value
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :222 :24) // Not a variable of known type: maxUint
%7 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :222 :16) // comparison of unknown type: value > maxUint
cond_br %7, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :222 :16)

^1: // JumpBlock
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :222 :42) // Not a variable of known type: maxFloat
return %8 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :222 :35)

^2: // BinaryBranchBlock
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :223 :16) // Not a variable of known type: value
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :223 :24) // Not a variable of known type: minUint
%11 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :223 :16) // comparison of unknown type: value < minUint
cond_br %11, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :223 :16)

^3: // JumpBlock
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :223 :42) // Not a variable of known type: minFloat
return %12 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :223 :35)

^4: // JumpBlock
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :225 :31) // Not a variable of known type: maxFloat
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :225 :42) // Not a variable of known type: minFloat
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :225 :31) // Binary expression on unsupported types maxFloat - minFloat
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :226 :29) // Not a variable of known type: maxUint
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :226 :39) // Not a variable of known type: minUint
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :226 :29) // Binary expression on unsupported types maxUint - minUint
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :230 :35) // Not a variable of known type: value
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :230 :43) // Not a variable of known type: minUint
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :230 :35) // Binary expression on unsupported types value - minUint
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :230 :61) // Not a variable of known type: rangeUint
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :230 :54) // (float)rangeUint (CastExpression)
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :230 :34) // Binary expression on unsupported types (value - minUint) / (float)rangeUint
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :232 :29) // Not a variable of known type: valueRelative
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :232 :45) // Not a variable of known type: rangeFloat
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :232 :29) // Binary expression on unsupported types valueRelative * rangeFloat
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :232 :58) // Not a variable of known type: minFloat
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :232 :29) // Binary expression on unsupported types valueRelative * rangeFloat + minFloat
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :233 :19) // Not a variable of known type: outValue
return %34 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Compression.cs" :233 :12)

^5: // ExitBlock
cbde.unreachable

}
