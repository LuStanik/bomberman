func @_Mirror.Logging.NetworkLogSettings.Reset$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\NetworkLogSettings.cs" :17 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: EditorLogSettingsLoader
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\NetworkLogSettings.cs" :19 :43) // EditorLogSettingsLoader.FindLogSettings() (InvocationExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\NetworkLogSettings.cs" :20 :16) // Not a variable of known type: existingSettings
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\NetworkLogSettings.cs" :20 :36) // null (NullLiteralExpression)
%4 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\NetworkLogSettings.cs" :20 :16) // comparison of unknown type: existingSettings != null
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\NetworkLogSettings.cs" :20 :16)

^1: // SimpleBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\NetworkLogSettings.cs" :22 :27) // Not a variable of known type: existingSettings
// Entity from another assembly: UnityEditor
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\NetworkLogSettings.cs" :24 :16) // UnityEditor.EditorUtility (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\NetworkLogSettings.cs" :24 :51) // this (ThisExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\NetworkLogSettings.cs" :24 :16) // UnityEditor.EditorUtility.SetDirty(this) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.Logging.NetworkLogSettings.Awake$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\NetworkLogSettings.cs" :29 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: RefreshDictionary
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\NetworkLogSettings.cs" :31 :12) // RefreshDictionary() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Logging.NetworkLogSettings.OnValidate$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\NetworkLogSettings.cs" :34 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: RefreshDictionary
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\NetworkLogSettings.cs" :37 :12) // RefreshDictionary() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Logging.NetworkLogSettings.RefreshDictionary$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\NetworkLogSettings.cs" :40 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\NetworkLogSettings.cs" :42 :12) // Not a variable of known type: settings
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: LogFactory
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\NetworkLogSettings.cs" :42 :40) // LogFactory.loggers (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\NetworkLogSettings.cs" :42 :12) // settings.LoadIntoDictionary(LogFactory.loggers) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
