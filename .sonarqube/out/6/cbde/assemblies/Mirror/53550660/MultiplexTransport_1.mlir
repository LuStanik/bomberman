// Skipping function Awake(), it contains poisonous unsupported syntaxes

// Skipping function OnEnable(), it contains poisonous unsupported syntaxes

// Skipping function OnDisable(), it contains poisonous unsupported syntaxes

// Skipping function Available(), it contains poisonous unsupported syntaxes

// Skipping function ClientConnect(none), it contains poisonous unsupported syntaxes

// Skipping function ClientConnect(none), it contains poisonous unsupported syntaxes

// Skipping function ClientConnected(), it contains poisonous unsupported syntaxes

func @_Mirror.MultiplexTransport.ClientDisconnect$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :100 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :102 :24) // Not a variable of known type: available
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :102 :16) // (object)available (CastExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :102 :37) // null (NullLiteralExpression)
%3 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :102 :16) // comparison of unknown type: (object)available != null
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :102 :16)

^1: // SimpleBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :103 :16) // Not a variable of known type: available
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :103 :16) // available.ClientDisconnect() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.MultiplexTransport.ClientSend$int.System.ArraySegment$byte$$(i32, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :106 :8) {
^entry (%_channelId : i32, %_segment : none):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :106 :40)
cbde.store %_channelId, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :106 :40)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :106 :55)
cbde.store %_segment, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :106 :55)
br ^0

^0: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :108 :12) // Not a variable of known type: available
%3 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :108 :33)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :108 :44) // Not a variable of known type: segment
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :108 :12) // available.ClientSend(channelId, segment) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.MultiplexTransport.FromBaseId$int.int$(i32, i32) -> i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :119 :8) {
^entry (%_transportId : i32, %_connectionId : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :119 :23)
cbde.store %_transportId, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :119 :23)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :119 :40)
cbde.store %_connectionId, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :119 :40)
br ^0

^0: // JumpBlock
%2 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :121 :19)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :121 :34) // Not a variable of known type: transports
%4 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :121 :34) // transports.Length (SimpleMemberAccessExpression)
%5 = muli %2, %4 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :121 :19)
%6 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :121 :54)
%7 = addi %5, %6 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :121 :19)
return %7 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :121 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.MultiplexTransport.ToBaseId$int$(i32) -> i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :124 :8) {
^entry (%_connectionId : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :124 :21)
cbde.store %_connectionId, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :124 :21)
br ^0

^0: // JumpBlock
%1 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :126 :19)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :126 :34) // Not a variable of known type: transports
%3 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :126 :34) // transports.Length (SimpleMemberAccessExpression)
%4 = divis %1, %3 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :126 :19)
return %4 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :126 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.MultiplexTransport.ToTransportId$int$(i32) -> i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :129 :8) {
^entry (%_connectionId : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :129 :26)
cbde.store %_connectionId, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :129 :26)
br ^0

^0: // JumpBlock
%1 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :131 :19)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :131 :34) // Not a variable of known type: transports
%3 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :131 :34) // transports.Length (SimpleMemberAccessExpression)
%4 = remis %1, %3 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :131 :19)
return %4 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :131 :12)

^1: // ExitBlock
cbde.unreachable

}
// Skipping function AddServerCallbacks(), it contains poisonous unsupported syntaxes

func @_Mirror.MultiplexTransport.ServerUri$$() -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :167 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :169 :19) // Not a variable of known type: transports
%1 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :169 :30)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :169 :19) // transports[0] (ElementAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :169 :19) // transports[0].ServerUri() (InvocationExpression)
return %3 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :169 :12)

^1: // ExitBlock
cbde.unreachable

}
// Skipping function ServerActive(), it contains poisonous unsupported syntaxes

func @_Mirror.MultiplexTransport.ServerGetClientAddress$int$(i32) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :186 :8) {
^entry (%_connectionId : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :186 :54)
cbde.store %_connectionId, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :186 :54)
br ^0

^0: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ToBaseId
%1 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :188 :44)
%2 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :188 :35) // ToBaseId(connectionId) (InvocationExpression)
%3 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :188 :16) // baseConnectionId
cbde.store %2, %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :188 :16)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ToTransportId
%4 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :189 :44)
%5 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :189 :30) // ToTransportId(connectionId) (InvocationExpression)
%6 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :189 :16) // transportId
cbde.store %5, %6 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :189 :16)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :190 :19) // Not a variable of known type: transports
%8 = cbde.load %6 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :190 :30)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :190 :19) // transports[transportId] (ElementAccessExpression)
%10 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :190 :66)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :190 :19) // transports[transportId].ServerGetClientAddress(baseConnectionId) (InvocationExpression)
return %11 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :190 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.MultiplexTransport.ServerDisconnect$int$(i32) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :193 :8) {
^entry (%_connectionId : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :193 :46)
cbde.store %_connectionId, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :193 :46)
br ^0

^0: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ToBaseId
%1 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :195 :44)
%2 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :195 :35) // ToBaseId(connectionId) (InvocationExpression)
%3 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :195 :16) // baseConnectionId
cbde.store %2, %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :195 :16)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ToTransportId
%4 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :196 :44)
%5 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :196 :30) // ToTransportId(connectionId) (InvocationExpression)
%6 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :196 :16) // transportId
cbde.store %5, %6 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :196 :16)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :197 :19) // Not a variable of known type: transports
%8 = cbde.load %6 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :197 :30)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :197 :19) // transports[transportId] (ElementAccessExpression)
%10 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :197 :60)
%11 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :197 :19) // transports[transportId].ServerDisconnect(baseConnectionId) (InvocationExpression)
return %11 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :197 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.MultiplexTransport.ServerSend$int.int.System.ArraySegment$byte$$(i32, i32, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :200 :8) {
^entry (%_connectionId : i32, %_channelId : i32, %_segment : none):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :200 :40)
cbde.store %_connectionId, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :200 :40)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :200 :58)
cbde.store %_channelId, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :200 :58)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :200 :73)
cbde.store %_segment, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :200 :73)
br ^0

^0: // ForInitializerBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ToBaseId
%3 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :202 :44)
%4 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :202 :35) // ToBaseId(connectionId) (InvocationExpression)
%5 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :202 :16) // baseConnectionId
cbde.store %4, %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :202 :16)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ToTransportId
%6 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :203 :44)
%7 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :203 :30) // ToTransportId(connectionId) (InvocationExpression)
%8 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :203 :16) // transportId
cbde.store %7, %8 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :203 :16)
%9 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :205 :25)
%10 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :205 :21) // i
cbde.store %9, %10 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :205 :21)
br ^1

^1: // BinaryBranchBlock
%11 = cbde.load %10 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :205 :28)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :205 :32) // Not a variable of known type: transports
%13 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :205 :32) // transports.Length (SimpleMemberAccessExpression)
%14 = cmpi "slt", %11, %13 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :205 :28)
cond_br %14, ^2, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :205 :28)

^2: // BinaryBranchBlock
%15 = cbde.load %10 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :207 :20)
%16 = cbde.load %8 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :207 :25)
%17 = cmpi "eq", %15, %16 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :207 :20)
cond_br %17, ^4, ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :207 :20)

^4: // SimpleBlock
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :209 :20) // Not a variable of known type: transports
%19 = cbde.load %10 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :209 :31)
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :209 :20) // transports[i] (ElementAccessExpression)
%21 = cbde.load %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :209 :45)
%22 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :209 :63)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :209 :74) // Not a variable of known type: segment
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :209 :20) // transports[i].ServerSend(baseConnectionId, channelId, segment) (InvocationExpression)
br ^5

^5: // SimpleBlock
%25 = cbde.load %10 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :205 :53)
%26 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :205 :51)
%27 = addi %25, %26 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :205 :51)
cbde.store %27, %10 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\MultiplexTransport.cs" :205 :51)
br ^1

^3: // ExitBlock
return

}
// Skipping function ServerStart(), it contains poisonous unsupported syntaxes

// Skipping function ServerStop(), it contains poisonous unsupported syntaxes

// Skipping function GetMaxPacketSize(i32), it contains poisonous unsupported syntaxes

// Skipping function Shutdown(), it contains poisonous unsupported syntaxes

// Skipping function ToString(), it contains poisonous unsupported syntaxes

