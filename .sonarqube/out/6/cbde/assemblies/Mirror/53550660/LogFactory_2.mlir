func @_Mirror.LogFactory.GetLogger$T$$UnityEngine.LogType$(none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :21 :8) {
^entry (%_defaultLogLevel : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :21 :43)
cbde.store %_defaultLogLevel, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :21 :43)
br ^0

^0: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetLogger
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :23 :29) // typeof(T) (TypeOfExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :23 :29) // typeof(T).Name (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :23 :45) // Not a variable of known type: defaultLogLevel
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :23 :19) // GetLogger(typeof(T).Name, defaultLogLevel) (InvocationExpression)
return %4 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :23 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.LogFactory.GetLogger$System.Type.UnityEngine.LogType$(none, none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :26 :8) {
^entry (%_type : none, %_defaultLogLevel : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :26 :40)
cbde.store %_type, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :26 :40)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :26 :58)
cbde.store %_defaultLogLevel, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :26 :58)
br ^0

^0: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetLogger
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :28 :29) // Not a variable of known type: type
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :28 :29) // type.Name (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :28 :40) // Not a variable of known type: defaultLogLevel
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :28 :19) // GetLogger(type.Name, defaultLogLevel) (InvocationExpression)
return %5 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :28 :12)

^1: // ExitBlock
cbde.unreachable

}
// Skipping function GetLogger(none, none), it contains poisonous unsupported syntaxes

// Skipping function EnableDebugMode(), it contains poisonous unsupported syntaxes

// Skipping function ReplaceLogHandler(none), it contains poisonous unsupported syntaxes

func @_Mirror.ILoggerExtensions.LogError$UnityEngine.ILogger.object$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :79 :8) {
^entry (%_logger : none, %_message : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :79 :36)
cbde.store %_logger, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :79 :36)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :79 :57)
cbde.store %_message, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :79 :57)
br ^0

^0: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :81 :12) // Not a variable of known type: logger
// Entity from another assembly: LogType
%3 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :81 :23) // LogType.Error (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :81 :38) // Not a variable of known type: message
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :81 :12) // logger.Log(LogType.Error, message) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.ILoggerExtensions.Assert$UnityEngine.ILogger.bool.string$(none, i1, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :84 :8) {
^entry (%_logger : none, %_condition : i1, %_message : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :84 :34)
cbde.store %_logger, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :84 :34)
%1 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :84 :55)
cbde.store %_condition, %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :84 :55)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :84 :71)
cbde.store %_message, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :84 :71)
br ^0

^0: // BinaryBranchBlock
%3 = cbde.load %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :86 :17)
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :86 :16) // !condition (LogicalNotExpression)
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :86 :16)

^1: // SimpleBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :87 :16) // Not a variable of known type: logger
// Entity from another assembly: LogType
%6 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :87 :27) // LogType.Assert (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :87 :43) // Not a variable of known type: message
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :87 :16) // logger.Log(LogType.Assert, message) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.ILoggerExtensions.LogWarning$UnityEngine.ILogger.object$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :90 :8) {
^entry (%_logger : none, %_message : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :90 :38)
cbde.store %_logger, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :90 :38)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :90 :59)
cbde.store %_message, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :90 :59)
br ^0

^0: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :92 :12) // Not a variable of known type: logger
// Entity from another assembly: LogType
%3 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :92 :23) // LogType.Warning (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :92 :40) // Not a variable of known type: message
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\LogFactory.cs" :92 :12) // logger.Log(LogType.Warning, message) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
