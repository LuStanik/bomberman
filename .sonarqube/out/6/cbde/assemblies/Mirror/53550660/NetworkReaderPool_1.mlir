func @_Mirror.PooledNetworkReader.Dispose$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :12 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkReaderPool
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :14 :38) // this (ThisExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :14 :12) // NetworkReaderPool.Recycle(this) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkReaderPool.GetReader$byte$$$(none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :36 :8) {
^entry (%_bytes : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :36 :52)
cbde.store %_bytes, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :36 :52)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :39 :41) // Not a variable of known type: pool
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :39 :41) // pool.Take() (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :40 :12) // Not a variable of known type: reader
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :40 :12) // reader.buffer (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :40 :51) // Not a variable of known type: bytes
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :40 :28) // new ArraySegment<byte>(bytes) (ObjectCreationExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :41 :12) // Not a variable of known type: reader
%9 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :41 :12) // reader.Position (SimpleMemberAccessExpression)
%10 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :41 :30)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :42 :19) // Not a variable of known type: reader
return %11 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :42 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.NetworkReaderPool.GetReader$System.ArraySegment$byte$$(none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :49 :8) {
^entry (%_segment : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :49 :52)
cbde.store %_segment, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :49 :52)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :52 :41) // Not a variable of known type: pool
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :52 :41) // pool.Take() (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :53 :12) // Not a variable of known type: reader
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :53 :12) // reader.buffer (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :53 :28) // Not a variable of known type: segment
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :54 :12) // Not a variable of known type: reader
%8 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :54 :12) // reader.Position (SimpleMemberAccessExpression)
%9 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :54 :30)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :55 :19) // Not a variable of known type: reader
return %10 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :55 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.NetworkReaderPool.Recycle$Mirror.PooledNetworkReader$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :62 :8) {
^entry (%_reader : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :62 :35)
cbde.store %_reader, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :62 :35)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :64 :12) // Not a variable of known type: pool
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :64 :24) // Not a variable of known type: reader
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReaderPool.cs" :64 :12) // pool.Return(reader) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
