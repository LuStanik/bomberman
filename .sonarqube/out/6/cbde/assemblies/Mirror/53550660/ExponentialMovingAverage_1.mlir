func @_Mirror.ExponentialMovingAverage.Add$double$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ExponentialMovingAverage.cs" :16 :8) {
^entry (%_newValue : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ExponentialMovingAverage.cs" :16 :24)
cbde.store %_newValue, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ExponentialMovingAverage.cs" :16 :24)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ExponentialMovingAverage.cs" :20 :16) // Not a variable of known type: initialized
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ExponentialMovingAverage.cs" :20 :16)

^1: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ExponentialMovingAverage.cs" :22 :31) // Not a variable of known type: newValue
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ExponentialMovingAverage.cs" :22 :42) // Not a variable of known type: Value
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ExponentialMovingAverage.cs" :22 :31) // Binary expression on unsupported types newValue - Value
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ExponentialMovingAverage.cs" :23 :16) // Not a variable of known type: Value
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ExponentialMovingAverage.cs" :23 :25) // Not a variable of known type: alpha
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ExponentialMovingAverage.cs" :23 :33) // Not a variable of known type: delta
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ExponentialMovingAverage.cs" :23 :25) // Binary expression on unsupported types alpha * delta
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ExponentialMovingAverage.cs" :23 :16) // Binary expression on unsupported types Value += alpha * delta
%11 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ExponentialMovingAverage.cs" :24 :23)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ExponentialMovingAverage.cs" :24 :27) // Not a variable of known type: alpha
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ExponentialMovingAverage.cs" :24 :23) // Binary expression on unsupported types 1 - alpha
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ExponentialMovingAverage.cs" :24 :37) // Not a variable of known type: Var
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ExponentialMovingAverage.cs" :24 :43) // Not a variable of known type: alpha
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ExponentialMovingAverage.cs" :24 :51) // Not a variable of known type: delta
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ExponentialMovingAverage.cs" :24 :43) // Binary expression on unsupported types alpha * delta
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ExponentialMovingAverage.cs" :24 :59) // Not a variable of known type: delta
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ExponentialMovingAverage.cs" :24 :43) // Binary expression on unsupported types alpha * delta * delta
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ExponentialMovingAverage.cs" :24 :37) // Binary expression on unsupported types Var + alpha * delta * delta
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ExponentialMovingAverage.cs" :24 :22) // Binary expression on unsupported types (1 - alpha) * (Var + alpha * delta * delta)
br ^3

^2: // SimpleBlock
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ExponentialMovingAverage.cs" :28 :24) // Not a variable of known type: newValue
%23 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\ExponentialMovingAverage.cs" :29 :30) // true
br ^3

^3: // ExitBlock
return

}
