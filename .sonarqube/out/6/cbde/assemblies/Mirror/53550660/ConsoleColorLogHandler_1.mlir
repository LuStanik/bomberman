func @_Mirror.Logging.ConsoleColorLogHandler.LogException$System.Exception.UnityEngine.Object$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\ConsoleColorLogHandler.cs" :14 :8) {
^entry (%_exception : none, %_context : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\ConsoleColorLogHandler.cs" :14 :33)
cbde.store %_exception, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\ConsoleColorLogHandler.cs" :14 :33)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\ConsoleColorLogHandler.cs" :14 :54)
cbde.store %_context, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\ConsoleColorLogHandler.cs" :14 :54)
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: Console
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\ConsoleColorLogHandler.cs" :16 :12) // Console.ForegroundColor (SimpleMemberAccessExpression)
// Entity from another assembly: ConsoleColor
%3 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\ConsoleColorLogHandler.cs" :16 :38) // ConsoleColor.Red (SimpleMemberAccessExpression)
// Entity from another assembly: Console
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\ConsoleColorLogHandler.cs" :17 :44) // Not a variable of known type: exception
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\ConsoleColorLogHandler.cs" :17 :44) // exception.Message (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\ConsoleColorLogHandler.cs" :17 :30) // $"Exception: {exception.Message}" (InterpolatedStringExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\ConsoleColorLogHandler.cs" :17 :12) // Console.WriteLine($"Exception: {exception.Message}") (InvocationExpression)
%8 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\ConsoleColorLogHandler.cs" :18 :16) // Not a variable of known type: showExceptionStackTrace
cond_br %8, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\ConsoleColorLogHandler.cs" :18 :16)

^1: // SimpleBlock
// Entity from another assembly: Console
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\ConsoleColorLogHandler.cs" :20 :41) // Not a variable of known type: exception
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\ConsoleColorLogHandler.cs" :20 :41) // exception.StackTrace (SimpleMemberAccessExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\ConsoleColorLogHandler.cs" :20 :34) // $"    {exception.StackTrace}" (InterpolatedStringExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\ConsoleColorLogHandler.cs" :20 :16) // Console.WriteLine($"    {exception.StackTrace}") (InvocationExpression)
br ^2

^2: // SimpleBlock
// Entity from another assembly: Console
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Logging\\ConsoleColorLogHandler.cs" :22 :12) // Console.ResetColor() (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function LogFormat(none, none, none, none), it contains poisonous unsupported syntaxes

