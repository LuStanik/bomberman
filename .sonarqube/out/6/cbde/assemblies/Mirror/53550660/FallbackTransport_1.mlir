// Skipping function Awake(), it contains poisonous unsupported syntaxes

func @_Mirror.FallbackTransport.OnEnable$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :26 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :28 :12) // Not a variable of known type: available
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :28 :12) // available.enabled (SimpleMemberAccessExpression)
%2 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :28 :32) // true
br ^1

^1: // ExitBlock
return

}
func @_Mirror.FallbackTransport.OnDisable$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :31 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :33 :12) // Not a variable of known type: available
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :33 :12) // available.enabled (SimpleMemberAccessExpression)
%2 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :33 :32) // false
br ^1

^1: // ExitBlock
return

}
// Skipping function GetAvailableTransport(), it contains poisonous unsupported syntaxes

func @_Mirror.FallbackTransport.Available$$() -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :49 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :51 :19) // Not a variable of known type: available
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :51 :19) // available.Available() (InvocationExpression)
return %1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :51 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.FallbackTransport.ClientConnect$string$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :54 :8) {
^entry (%_address : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :54 :43)
cbde.store %_address, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :54 :43)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :56 :12) // Not a variable of known type: available
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :56 :12) // available.OnClientConnected (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :56 :42) // Not a variable of known type: OnClientConnected
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :57 :12) // Not a variable of known type: available
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :57 :12) // available.OnClientDataReceived (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :57 :45) // Not a variable of known type: OnClientDataReceived
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :58 :12) // Not a variable of known type: available
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :58 :12) // available.OnClientError (SimpleMemberAccessExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :58 :38) // Not a variable of known type: OnClientError
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :59 :12) // Not a variable of known type: available
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :59 :12) // available.OnClientDisconnected (SimpleMemberAccessExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :59 :45) // Not a variable of known type: OnClientDisconnected
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :60 :12) // Not a variable of known type: available
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :60 :36) // Not a variable of known type: address
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :60 :12) // available.ClientConnect(address) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function ClientConnect(none), it contains poisonous unsupported syntaxes

func @_Mirror.FallbackTransport.ClientConnected$$() -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :83 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :85 :19) // Not a variable of known type: available
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :85 :19) // available.ClientConnected() (InvocationExpression)
return %1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :85 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.FallbackTransport.ClientDisconnect$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :88 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :90 :12) // Not a variable of known type: available
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :90 :12) // available.ClientDisconnect() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.FallbackTransport.ClientSend$int.System.ArraySegment$byte$$(i32, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :93 :8) {
^entry (%_channelId : i32, %_segment : none):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :93 :40)
cbde.store %_channelId, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :93 :40)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :93 :55)
cbde.store %_segment, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :93 :55)
br ^0

^0: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :95 :12) // Not a variable of known type: available
%3 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :95 :33)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :95 :44) // Not a variable of known type: segment
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :95 :12) // available.ClientSend(channelId, segment) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.FallbackTransport.ServerActive$$() -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :102 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :104 :19) // Not a variable of known type: available
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :104 :19) // available.ServerActive() (InvocationExpression)
return %1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :104 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.FallbackTransport.ServerGetClientAddress$int$(i32) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :107 :8) {
^entry (%_connectionId : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :107 :54)
cbde.store %_connectionId, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :107 :54)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :109 :19) // Not a variable of known type: available
%2 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :109 :52)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :109 :19) // available.ServerGetClientAddress(connectionId) (InvocationExpression)
return %3 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :109 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.FallbackTransport.ServerDisconnect$int$(i32) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :112 :8) {
^entry (%_connectionId : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :112 :46)
cbde.store %_connectionId, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :112 :46)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :114 :19) // Not a variable of known type: available
%2 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :114 :46)
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :114 :19) // available.ServerDisconnect(connectionId) (InvocationExpression)
return %3 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :114 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.FallbackTransport.ServerSend$int.int.System.ArraySegment$byte$$(i32, i32, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :117 :8) {
^entry (%_connectionId : i32, %_channelId : i32, %_segment : none):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :117 :40)
cbde.store %_connectionId, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :117 :40)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :117 :58)
cbde.store %_channelId, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :117 :58)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :117 :73)
cbde.store %_segment, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :117 :73)
br ^0

^0: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :119 :12) // Not a variable of known type: available
%4 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :119 :33)
%5 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :119 :47)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :119 :58) // Not a variable of known type: segment
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :119 :12) // available.ServerSend(connectionId, channelId, segment) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.FallbackTransport.ServerStart$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :122 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :124 :12) // Not a variable of known type: available
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :124 :12) // available.OnServerConnected (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :124 :42) // Not a variable of known type: OnServerConnected
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :125 :12) // Not a variable of known type: available
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :125 :12) // available.OnServerDataReceived (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :125 :45) // Not a variable of known type: OnServerDataReceived
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :126 :12) // Not a variable of known type: available
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :126 :12) // available.OnServerError (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :126 :38) // Not a variable of known type: OnServerError
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :127 :12) // Not a variable of known type: available
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :127 :12) // available.OnServerDisconnected (SimpleMemberAccessExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :127 :45) // Not a variable of known type: OnServerDisconnected
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :128 :12) // Not a variable of known type: available
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :128 :12) // available.ServerStart() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.FallbackTransport.ServerStop$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :131 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :133 :12) // Not a variable of known type: available
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :133 :12) // available.ServerStop() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.FallbackTransport.Shutdown$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :136 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :138 :12) // Not a variable of known type: available
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :138 :12) // available.Shutdown() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function GetMaxPacketSize(i32), it contains poisonous unsupported syntaxes

func @_Mirror.FallbackTransport.ToString$$() -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :163 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :165 :19) // Not a variable of known type: available
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :165 :19) // available.ToString() (InvocationExpression)
return %1 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\FallbackTransport.cs" :165 :12)

^1: // ExitBlock
cbde.unreachable

}
