func @_Mirror.SyncIDictionary$TKey.TValue$.Reset$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :38 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :40 :25) // false
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :41 :12) // Not a variable of known type: changes
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :41 :12) // changes.Clear() (InvocationExpression)
%3 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :42 :27)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :43 :12) // Not a variable of known type: objects
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :43 :12) // objects.Clear() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function AddOperation(none, none, none), it contains poisonous unsupported syntaxes

// Skipping function OnSerializeAll(none), it contains poisonous unsupported syntaxes

// Skipping function OnSerializeDelta(none), it contains poisonous unsupported syntaxes

func @_Mirror.SyncIDictionary$TKey.TValue$.OnDeserializeAll$Mirror.NetworkReader$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :126 :8) {
^entry (%_reader : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :126 :37)
cbde.store %_reader, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :126 :37)
br ^0

^0: // ForInitializerBlock
%1 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :129 :25) // true
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :132 :29) // Not a variable of known type: reader
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :132 :29) // reader.ReadUInt32() (InvocationExpression)
%4 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :132 :24) // (int)reader.ReadUInt32() (CastExpression)
%5 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :132 :16) // count
cbde.store %4, %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :132 :16)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :134 :12) // Not a variable of known type: objects
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :134 :12) // objects.Clear() (InvocationExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :135 :12) // Not a variable of known type: changes
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :135 :12) // changes.Clear() (InvocationExpression)
%10 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :137 :25)
%11 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :137 :21) // i
cbde.store %10, %11 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :137 :21)
br ^1

^1: // BinaryBranchBlock
%12 = cbde.load %11 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :137 :28)
%13 = cbde.load %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :137 :32)
%14 = cmpi "slt", %12, %13 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :137 :28)
cond_br %14, ^2, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :137 :28)

^2: // SimpleBlock
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :139 :27) // Not a variable of known type: reader
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :139 :27) // reader.Read<TKey>() (InvocationExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :140 :29) // Not a variable of known type: reader
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :140 :29) // reader.Read<TValue>() (InvocationExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :141 :16) // Not a variable of known type: objects
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :141 :28) // Not a variable of known type: key
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :141 :33) // Not a variable of known type: obj
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :141 :16) // objects.Add(key, obj) (InvocationExpression)
br ^4

^4: // SimpleBlock
%25 = cbde.load %11 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :137 :39)
%26 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :137 :39)
%27 = addi %25, %26 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :137 :39)
cbde.store %27, %11 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :137 :39)
br ^1

^3: // SimpleBlock
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :147 :32) // Not a variable of known type: reader
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :147 :32) // reader.ReadUInt32() (InvocationExpression)
%30 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :147 :27) // (int)reader.ReadUInt32() (CastExpression)
br ^5

^5: // ExitBlock
return

}
// Skipping function OnDeserializeDelta(none), it contains poisonous unsupported syntaxes

// Skipping function Clear(), it contains poisonous unsupported syntaxes

// Skipping function Remove(none), it contains poisonous unsupported syntaxes

func @_Mirror.SyncIDictionary$TKey.TValue$.Add$TKey.TValue$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :246 :8) {
^entry (%_key : none, %_value : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :246 :24)
cbde.store %_key, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :246 :24)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :246 :34)
cbde.store %_value, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :246 :34)
br ^0

^0: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :248 :12) // Not a variable of known type: objects
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :248 :24) // Not a variable of known type: key
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :248 :29) // Not a variable of known type: value
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :248 :12) // objects.Add(key, value) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: AddOperation
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :249 :25) // Not a variable of known type: Operation
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :249 :25) // Operation.OP_ADD (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :249 :43) // Not a variable of known type: key
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :249 :48) // Not a variable of known type: value
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :249 :12) // AddOperation(Operation.OP_ADD, key, value) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function Contains(none), it contains poisonous unsupported syntaxes

// Skipping function CopyTo(none, i32), it contains poisonous unsupported syntaxes

func @_Mirror.SyncIDictionary$TKey.TValue$.Remove$System.Collections.Generic.KeyValuePair$TKey.TValue$$(none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :278 :8) {
^entry (%_item : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :278 :27)
cbde.store %_item, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :278 :27)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :280 :26) // Not a variable of known type: objects
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :280 :41) // Not a variable of known type: item
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :280 :41) // item.Key (SimpleMemberAccessExpression)
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :280 :26) // objects.Remove(item.Key) (InvocationExpression)
%5 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :280 :17) // result
cbde.store %4, %5 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :280 :17)
%6 = cbde.load %5 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :281 :16)
cond_br %6, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :281 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: AddOperation
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :283 :29) // Not a variable of known type: Operation
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :283 :29) // Operation.OP_REMOVE (SimpleMemberAccessExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :283 :50) // Not a variable of known type: item
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :283 :50) // item.Key (SimpleMemberAccessExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :283 :60) // Not a variable of known type: item
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :283 :60) // item.Value (SimpleMemberAccessExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :283 :16) // AddOperation(Operation.OP_REMOVE, item.Key, item.Value) (InvocationExpression)
br ^2

^2: // JumpBlock
%14 = cbde.load %5 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :285 :19)
return %14 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\SyncDictionary.cs" :285 :12)

^3: // ExitBlock
cbde.unreachable

}
