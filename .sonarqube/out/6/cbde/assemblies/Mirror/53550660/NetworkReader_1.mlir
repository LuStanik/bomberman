// Skipping function ReadBlittable(), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkReader.ReadBytes$byte$$.int$(none, i32) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :129 :8) {
^entry (%_bytes : none, %_count : i32):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :129 :32)
cbde.store %_bytes, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :129 :32)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :129 :46)
cbde.store %_count, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :129 :46)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :132 :16)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :132 :24) // Not a variable of known type: bytes
%4 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :132 :24) // bytes.Length (SimpleMemberAccessExpression)
%5 = cmpi "sgt", %2, %4 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :132 :16)
cond_br %5, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :132 :16)

^1: // JumpBlock
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :134 :47) // "ReadBytes can't read " (StringLiteralExpression)
%7 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :134 :73)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :134 :47) // Binary expression on unsupported types "ReadBytes can't read " + count
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :134 :81) // " + bytes because the passed byte[] only has length " (StringLiteralExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :134 :47) // Binary expression on unsupported types "ReadBytes can't read " + count + " + bytes because the passed byte[] only has length "
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :134 :137) // Not a variable of known type: bytes
%12 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :134 :137) // bytes.Length (SimpleMemberAccessExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :134 :47) // Binary expression on unsupported types "ReadBytes can't read " + count + " + bytes because the passed byte[] only has length " + bytes.Length
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :134 :22) // new EndOfStreamException("ReadBytes can't read " + count + " + bytes because the passed byte[] only has length " + bytes.Length) (ObjectCreationExpression)
cbde.throw %14 :  none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :134 :16)

^2: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ReadBytesSegment
%15 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :137 :55)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :137 :38) // ReadBytesSegment(count) (InvocationExpression)
// Entity from another assembly: Array
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :138 :23) // Not a variable of known type: data
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :138 :23) // data.Array (SimpleMemberAccessExpression)
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :138 :35) // Not a variable of known type: data
%21 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :138 :35) // data.Offset (SimpleMemberAccessExpression)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :138 :48) // Not a variable of known type: bytes
%23 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :138 :55)
%24 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :138 :58)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :138 :12) // Array.Copy(data.Array, data.Offset, bytes, 0, count) (InvocationExpression)
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :139 :19) // Not a variable of known type: bytes
return %26 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :139 :12)

^3: // ExitBlock
cbde.unreachable

}
func @_Mirror.NetworkReader.ReadBytesSegment$int$(i32) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :148 :8) {
^entry (%_count : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :148 :51)
cbde.store %_count, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :148 :51)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :151 :16) // Not a variable of known type: Position
%2 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :151 :27)
%3 = addi %1, %2 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :151 :16)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :151 :35) // Not a variable of known type: buffer
%5 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :151 :35) // buffer.Count (SimpleMemberAccessExpression)
%6 = cmpi "sgt", %3, %5 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :151 :16)
cond_br %6, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :151 :16)

^1: // JumpBlock
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :153 :47) // "ReadBytesSegment can't read " (StringLiteralExpression)
%8 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :153 :80)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :153 :47) // Binary expression on unsupported types "ReadBytesSegment can't read " + count
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :153 :88) // " bytes because it would read past the end of the stream. " (StringLiteralExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :153 :47) // Binary expression on unsupported types "ReadBytesSegment can't read " + count + " bytes because it would read past the end of the stream. "
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ToString
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :153 :150) // ToString() (InvocationExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :153 :47) // Binary expression on unsupported types "ReadBytesSegment can't read " + count + " bytes because it would read past the end of the stream. " + ToString()
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :153 :22) // new EndOfStreamException("ReadBytesSegment can't read " + count + " bytes because it would read past the end of the stream. " + ToString()) (ObjectCreationExpression)
cbde.throw %14 :  none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :153 :16)

^2: // JumpBlock
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :157 :63) // Not a variable of known type: buffer
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :157 :63) // buffer.Array (SimpleMemberAccessExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :157 :77) // Not a variable of known type: buffer
%18 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :157 :77) // buffer.Offset (SimpleMemberAccessExpression)
%19 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :157 :93) // Not a variable of known type: Position
%20 = addi %18, %19 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :157 :77)
%21 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :157 :103)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :157 :40) // new ArraySegment<byte>(buffer.Array, buffer.Offset + Position, count) (ObjectCreationExpression)
%24 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :158 :12) // Not a variable of known type: Position
%25 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :158 :24)
%26 = addi %24, %25 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :158 :12)
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :159 :19) // Not a variable of known type: result
return %27 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :159 :12)

^3: // ExitBlock
cbde.unreachable

}
func @_Mirror.NetworkReader.ToString$$() -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :163 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :165 :40) // Not a variable of known type: Position
%1 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :165 :55) // Not a variable of known type: Length
// Entity from another assembly: BitConverter
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :165 :93) // Not a variable of known type: buffer
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :165 :93) // buffer.Array (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :165 :107) // Not a variable of known type: buffer
%5 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :165 :107) // buffer.Offset (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :165 :122) // Not a variable of known type: buffer
%7 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :165 :122) // buffer.Count (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :165 :71) // BitConverter.ToString(buffer.Array, buffer.Offset, buffer.Count) (InvocationExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :165 :19) // $"NetworkReader pos={Position} len={Length} buffer={BitConverter.ToString(buffer.Array, buffer.Offset, buffer.Count)}" (InterpolatedStringExpression)
return %9 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :165 :12)

^1: // ExitBlock
cbde.unreachable

}
// Skipping function Read(), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkReaderExtensions.ReadString$Mirror.NetworkReader$(none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :215 :8) {
^entry (%_reader : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :215 :40)
cbde.store %_reader, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :215 :40)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :218 :26) // Not a variable of known type: reader
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :218 :26) // reader.ReadUInt16() (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :221 :16) // Not a variable of known type: size
%5 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :221 :24)
%6 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :221 :16) // comparison of unknown type: size == 0
cond_br %6, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :221 :16)

^1: // JumpBlock
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :222 :23) // null (NullLiteralExpression)
return %7 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :222 :16)

^2: // BinaryBranchBlock
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :224 :27) // Not a variable of known type: size
%9 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :224 :34)
%10 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :224 :27) // Binary expression on unsupported types size - 1
%11 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :224 :16) // realSize
cbde.store %10, %11 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :224 :16)
%12 = cbde.load %11 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :227 :16)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkWriter
%13 = constant 32768 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :227 :28)
%14 = cmpi "sge", %12, %13 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :227 :16)
cond_br %14, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :227 :16)

^3: // JumpBlock
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :229 :47) // "ReadString too long: " (StringLiteralExpression)
%16 = cbde.load %11 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :229 :73)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :229 :47) // Binary expression on unsupported types "ReadString too long: " + realSize
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :229 :84) // ". Limit is: " (StringLiteralExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :229 :47) // Binary expression on unsupported types "ReadString too long: " + realSize + ". Limit is: "
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkWriter
%20 = constant 32768 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :229 :101)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :229 :47) // Binary expression on unsupported types "ReadString too long: " + realSize + ". Limit is: " + NetworkWriter.MaxStringLength
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :229 :22) // new EndOfStreamException("ReadString too long: " + realSize + ". Limit is: " + NetworkWriter.MaxStringLength) (ObjectCreationExpression)
cbde.throw %22 :  none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :229 :16)

^4: // JumpBlock
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :232 :38) // Not a variable of known type: reader
%24 = cbde.load %11 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :232 :62)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :232 :38) // reader.ReadBytesSegment(realSize) (InvocationExpression)
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :235 :19) // Not a variable of known type: encoding
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :235 :38) // Not a variable of known type: data
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :235 :38) // data.Array (SimpleMemberAccessExpression)
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :235 :50) // Not a variable of known type: data
%31 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :235 :50) // data.Offset (SimpleMemberAccessExpression)
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :235 :63) // Not a variable of known type: data
%33 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :235 :63) // data.Count (SimpleMemberAccessExpression)
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :235 :19) // encoding.GetString(data.Array, data.Offset, data.Count) (InvocationExpression)
return %34 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :235 :12)

^5: // ExitBlock
cbde.unreachable

}
// Skipping function ReadBytesAndSize(none), it contains poisonous unsupported syntaxes

// Skipping function ReadBytesAndSizeSegment(none), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkReaderExtensions.ReadBytes$Mirror.NetworkReader.int$(none, i32) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :271 :8) {
^entry (%_reader : none, %_count : i32):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :271 :39)
cbde.store %_reader, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :271 :39)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :271 :66)
cbde.store %_count, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :271 :66)
br ^0

^0: // JumpBlock
%2 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :273 :36)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :273 :31) // byte[count] (ArrayType)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :273 :27) // new byte[count] (ArrayCreationExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :274 :12) // Not a variable of known type: reader
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :274 :29) // Not a variable of known type: bytes
%8 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :274 :36)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :274 :12) // reader.ReadBytes(bytes, count) (InvocationExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :275 :19) // Not a variable of known type: bytes
return %10 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :275 :12)

^1: // ExitBlock
cbde.unreachable

}
// Skipping function ReadTransform(none), it contains poisonous unsupported syntaxes

// Skipping function ReadGameObject(none), it contains poisonous unsupported syntaxes

// Skipping function ReadNetworkIdentity(none), it contains poisonous unsupported syntaxes

// Skipping function ReadNetworkBehaviour(none), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkReaderExtensions.ReadNetworkBehaviour$T$$Mirror.NetworkReader$(none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :326 :8) {
^entry (%_reader : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :326 :48)
cbde.store %_reader, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :326 :48)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :328 :19) // Not a variable of known type: reader
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :328 :19) // reader.ReadNetworkBehaviour() (InvocationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :328 :19) // reader.ReadNetworkBehaviour() as T (AsExpression)
return %3 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :328 :12)

^1: // ExitBlock
cbde.unreachable

}
// Skipping function ReadNetworkBehaviourSyncVar(none), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkReaderExtensions.ReadList$T$$Mirror.NetworkReader$(none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :345 :8) {
^entry (%_reader : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :345 :42)
cbde.store %_reader, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :345 :42)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :347 :25) // Not a variable of known type: reader
%2 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :347 :25) // reader.ReadInt32() (InvocationExpression)
%3 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :347 :16) // length
cbde.store %2, %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :347 :16)
%4 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :348 :16)
%5 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :348 :25)
%6 = cmpi "slt", %4, %5 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :348 :16)
cond_br %6, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :348 :16)

^1: // JumpBlock
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :349 :23) // null (NullLiteralExpression)
return %7 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :349 :16)

^2: // ForInitializerBlock
%8 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :350 :41)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :350 :29) // new List<T>(length) (ObjectCreationExpression)
%11 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :351 :25)
%12 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :351 :21) // i
cbde.store %11, %12 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :351 :21)
br ^3

^3: // BinaryBranchBlock
%13 = cbde.load %12 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :351 :28)
%14 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :351 :32)
%15 = cmpi "slt", %13, %14 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :351 :28)
cond_br %15, ^4, ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :351 :28)

^4: // SimpleBlock
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :353 :16) // Not a variable of known type: result
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :353 :27) // Not a variable of known type: reader
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :353 :27) // reader.Read<T>() (InvocationExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :353 :16) // result.Add(reader.Read<T>()) (InvocationExpression)
br ^6

^6: // SimpleBlock
%20 = cbde.load %12 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :351 :40)
%21 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :351 :40)
%22 = addi %20, %21 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :351 :40)
cbde.store %22, %12 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :351 :40)
br ^3

^5: // JumpBlock
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :355 :19) // Not a variable of known type: result
return %23 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :355 :12)

^7: // ExitBlock
cbde.unreachable

}
func @_Mirror.NetworkReaderExtensions.ReadArray$T$$Mirror.NetworkReader$(none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :358 :8) {
^entry (%_reader : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :358 :39)
cbde.store %_reader, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :358 :39)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :360 :25) // Not a variable of known type: reader
%2 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :360 :25) // reader.ReadInt32() (InvocationExpression)
%3 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :360 :16) // length
cbde.store %2, %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :360 :16)
%4 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :361 :16)
%5 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :361 :25)
%6 = cmpi "slt", %4, %5 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :361 :16)
cond_br %6, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :361 :16)

^1: // JumpBlock
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :362 :23) // null (NullLiteralExpression)
return %7 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :362 :16)

^2: // ForInitializerBlock
%8 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :363 :31)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :363 :29) // T[length] (ArrayType)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :363 :25) // new T[length] (ArrayCreationExpression)
%12 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :364 :25)
%13 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :364 :21) // i
cbde.store %12, %13 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :364 :21)
br ^3

^3: // BinaryBranchBlock
%14 = cbde.load %13 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :364 :28)
%15 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :364 :32)
%16 = cmpi "slt", %14, %15 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :364 :28)
cond_br %16, ^4, ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :364 :28)

^4: // SimpleBlock
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :366 :16) // Not a variable of known type: result
%18 = cbde.load %13 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :366 :23)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :366 :16) // result[i] (ElementAccessExpression)
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :366 :28) // Not a variable of known type: reader
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :366 :28) // reader.Read<T>() (InvocationExpression)
br ^6

^6: // SimpleBlock
%22 = cbde.load %13 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :364 :40)
%23 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :364 :40)
%24 = addi %22, %23 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :364 :40)
cbde.store %24, %13 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :364 :40)
br ^3

^5: // JumpBlock
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :368 :19) // Not a variable of known type: result
return %25 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :368 :12)

^7: // ExitBlock
cbde.unreachable

}
func @_Mirror.NetworkReaderExtensions.ReadUri$Mirror.NetworkReader$(none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :371 :8) {
^entry (%_reader : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :371 :34)
cbde.store %_reader, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :371 :34)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :373 :27) // Not a variable of known type: reader
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :373 :27) // reader.ReadString() (InvocationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :373 :19) // new Uri(reader.ReadString()) (ObjectCreationExpression)
return %3 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkReader.cs" :373 :12)

^1: // ExitBlock
cbde.unreachable

}
