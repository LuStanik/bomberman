func @_Mirror.NetworkBehaviour.getSyncVarHookGuard$ulong$(none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :96 :8) {
^entry (%_dirtyBit : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :96 :43)
cbde.store %_dirtyBit, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :96 :43)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :98 :20) // Not a variable of known type: syncVarHookGuard
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :98 :39) // Not a variable of known type: dirtyBit
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :98 :20) // Binary expression on unsupported types syncVarHookGuard & dirtyBit
%4 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :98 :52) // 0UL (NumericLiteralExpression)
%5 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :98 :19) // comparison of unknown type: (syncVarHookGuard & dirtyBit) != 0UL
return %5 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :98 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.NetworkBehaviour.setSyncVarHookGuard$ulong.bool$(none, i1) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :101 :8) {
^entry (%_dirtyBit : none, %_value : i1):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :101 :43)
cbde.store %_dirtyBit, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :101 :43)
%1 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :101 :59)
cbde.store %_value, %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :101 :59)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.load %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :103 :16)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :103 :16)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :104 :16) // Not a variable of known type: syncVarHookGuard
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :104 :36) // Not a variable of known type: dirtyBit
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :104 :16) // Binary expression on unsupported types syncVarHookGuard |= dirtyBit
br ^3

^2: // SimpleBlock
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :106 :16) // Not a variable of known type: syncVarHookGuard
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :106 :37) // Not a variable of known type: dirtyBit
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :106 :36) // ~dirtyBit (BitwiseNotExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :106 :16) // Binary expression on unsupported types syncVarHookGuard &= ~dirtyBit
br ^3

^3: // ExitBlock
return

}
func @_Mirror.NetworkBehaviour.InitSyncObject$Mirror.SyncObject$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :164 :8) {
^entry (%_syncObject : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :164 :38)
cbde.store %_syncObject, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :164 :38)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :166 :16) // Not a variable of known type: syncObject
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :166 :30) // null (NullLiteralExpression)
%3 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :166 :16) // comparison of unknown type: syncObject == null
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :166 :16)

^1: // SimpleBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :167 :16) // Not a variable of known type: logger
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :167 :32) // "Uninitialized SyncObject. Manually call the constructor on your SyncList, SyncSet or SyncDictionary" (StringLiteralExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :167 :16) // logger.LogError("Uninitialized SyncObject. Manually call the constructor on your SyncList, SyncSet or SyncDictionary") (InvocationExpression)
br ^3

^2: // SimpleBlock
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :169 :16) // Not a variable of known type: syncObjects
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :169 :32) // Not a variable of known type: syncObject
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :169 :16) // syncObjects.Add(syncObject) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function SendCommandInternal(none, none, none, i32, i1), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkBehaviour.SendRPCInternal$System.Type.string.Mirror.NetworkWriter.int.bool$(none, none, none, i32, i1) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :213 :8) {
^entry (%_invokeClass : none, %_rpcName : none, %_writer : none, %_channelId : i32, %_excludeOwner : i1):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :213 :39)
cbde.store %_invokeClass, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :213 :39)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :213 :57)
cbde.store %_rpcName, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :213 :57)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :213 :73)
cbde.store %_writer, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :213 :73)
%3 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :213 :95)
cbde.store %_channelId, %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :213 :95)
%4 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :213 :110)
cbde.store %_excludeOwner, %4 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :213 :110)
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkServer
%5 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :216 :17) // NetworkServer.active (SimpleMemberAccessExpression)
%6 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :216 :16) // !NetworkServer.active (LogicalNotExpression)
cond_br %6, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :216 :16)

^1: // JumpBlock
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :218 :16) // Not a variable of known type: logger
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :218 :32) // "RPC Function " (StringLiteralExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :218 :50) // Not a variable of known type: rpcName
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :218 :32) // Binary expression on unsupported types "RPC Function " + rpcName
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :218 :60) // " called on Client." (StringLiteralExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :218 :32) // Binary expression on unsupported types "RPC Function " + rpcName + " called on Client."
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :218 :16) // logger.LogError("RPC Function " + rpcName + " called on Client.") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :219 :16)

^2: // BinaryBranchBlock
%14 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :222 :17) // Not a variable of known type: isServer
%15 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :222 :16) // !isServer (LogicalNotExpression)
cond_br %15, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :222 :16)

^3: // JumpBlock
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :224 :16) // Not a variable of known type: logger
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :224 :34) // "ClientRpc " (StringLiteralExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :224 :49) // Not a variable of known type: rpcName
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :224 :34) // Binary expression on unsupported types "ClientRpc " + rpcName
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :224 :59) // " called on un-spawned object: " (StringLiteralExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :224 :34) // Binary expression on unsupported types "ClientRpc " + rpcName + " called on un-spawned object: "
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :224 :94) // Identifier from another assembly: name
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :224 :34) // Binary expression on unsupported types "ClientRpc " + rpcName + " called on un-spawned object: " + name
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :224 :16) // logger.LogWarning("ClientRpc " + rpcName + " called on un-spawned object: " + name) (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :225 :16)

^4: // SimpleBlock
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :229 :33) // new RpcMessage             {                 netId = netId,                 componentIndex = ComponentIndex,                 // type+func so Inventory.RpcUse != Equipment.RpcUse                 functionHash = RemoteCallHelper.GetMethodHash(invokeClass, rpcName),                 // segment to avoid reader allocations                 payload = writer.ToArraySegment()             } (ObjectCreationExpression)
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :231 :24) // Not a variable of known type: netId
%27 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :232 :33) // Not a variable of known type: ComponentIndex
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: RemoteCallHelper
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :234 :62) // Not a variable of known type: invokeClass
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :234 :75) // Not a variable of known type: rpcName
%30 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :234 :31) // RemoteCallHelper.GetMethodHash(invokeClass, rpcName) (InvocationExpression)
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :236 :26) // Not a variable of known type: writer
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :236 :26) // writer.ToArraySegment() (InvocationExpression)
%34 = cbde.load %4 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :241 :33)
%35 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :241 :32) // !excludeOwner (LogicalNotExpression)
%36 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :241 :17) // includeOwner
cbde.store %35, %36 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :241 :17)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkServer
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :242 :38) // Not a variable of known type: netIdentity
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :242 :51) // Not a variable of known type: message
%39 = cbde.load %36 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :242 :60)
%40 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :242 :74)
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :242 :12) // NetworkServer.SendToReady(netIdentity, message, includeOwner, channelId) (InvocationExpression)
br ^5

^5: // ExitBlock
return

}
// Skipping function SendTargetRPCInternal(none, none, none, none, i32), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkBehaviour.SyncVarGameObjectEqual$UnityEngine.GameObject.uint$(none, none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :299 :8) {
^entry (%_newGameObject : none, %_netIdField : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :299 :46)
cbde.store %_newGameObject, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :299 :46)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :299 :72)
cbde.store %_netIdField, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :299 :72)
br ^0

^0: // BinaryBranchBlock
%2 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :301 :28)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :302 :16) // Not a variable of known type: newGameObject
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :302 :33) // null (NullLiteralExpression)
%6 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :302 :16) // comparison of unknown type: newGameObject != null
cond_br %6, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :302 :16)

^1: // BinaryBranchBlock
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :304 :43) // Not a variable of known type: newGameObject
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :304 :43) // newGameObject.GetComponent<NetworkIdentity>() (InvocationExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :305 :20) // Not a variable of known type: identity
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :305 :32) // null (NullLiteralExpression)
%12 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :305 :20) // comparison of unknown type: identity != null
cond_br %12, ^3, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :305 :20)

^3: // BinaryBranchBlock
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :307 :31) // Not a variable of known type: identity
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :307 :31) // identity.netId (SimpleMemberAccessExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :308 :24) // Not a variable of known type: newNetId
%16 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :308 :36)
%17 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :308 :24) // comparison of unknown type: newNetId == 0
cond_br %17, ^4, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :308 :24)

^4: // SimpleBlock
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :310 :24) // Not a variable of known type: logger
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :310 :42) // "SetSyncVarGameObject GameObject " (StringLiteralExpression)
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :310 :79) // Not a variable of known type: newGameObject
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :310 :42) // Binary expression on unsupported types "SetSyncVarGameObject GameObject " + newGameObject
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :310 :95) // " has a zero netId. Maybe it is not spawned yet?" (StringLiteralExpression)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :310 :42) // Binary expression on unsupported types "SetSyncVarGameObject GameObject " + newGameObject + " has a zero netId. Maybe it is not spawned yet?"
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :310 :24) // logger.LogWarning("SetSyncVarGameObject GameObject " + newGameObject + " has a zero netId. Maybe it is not spawned yet?") (InvocationExpression)
br ^2

^2: // JumpBlock
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :315 :19) // Not a variable of known type: newNetId
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :315 :31) // Not a variable of known type: netIdField
%27 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :315 :19) // comparison of unknown type: newNetId == netIdField
return %27 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :315 :12)

^5: // ExitBlock
cbde.unreachable

}
func @_Mirror.NetworkBehaviour.SetSyncVarGameObject$UnityEngine.GameObject.refUnityEngine.GameObject.ulong.refuint$(none, none, none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :319 :8) {
^entry (%_newGameObject : none, %_gameObjectField : none, %_dirtyBit : none, %_netIdField : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :319 :44)
cbde.store %_newGameObject, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :319 :44)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :319 :70)
cbde.store %_gameObjectField, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :319 :70)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :319 :102)
cbde.store %_dirtyBit, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :319 :102)
%3 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :319 :118)
cbde.store %_netIdField, %3 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :319 :118)
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: getSyncVarHookGuard
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :321 :36) // Not a variable of known type: dirtyBit
%5 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :321 :16) // getSyncVarHookGuard(dirtyBit) (InvocationExpression)
cond_br %5, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :321 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :322 :16)

^2: // BinaryBranchBlock
%6 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :324 :28)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :325 :16) // Not a variable of known type: newGameObject
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :325 :33) // null (NullLiteralExpression)
%10 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :325 :16) // comparison of unknown type: newGameObject != null
cond_br %10, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :325 :16)

^3: // BinaryBranchBlock
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :327 :43) // Not a variable of known type: newGameObject
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :327 :43) // newGameObject.GetComponent<NetworkIdentity>() (InvocationExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :328 :20) // Not a variable of known type: identity
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :328 :32) // null (NullLiteralExpression)
%16 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :328 :20) // comparison of unknown type: identity != null
cond_br %16, ^5, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :328 :20)

^5: // BinaryBranchBlock
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :330 :31) // Not a variable of known type: identity
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :330 :31) // identity.netId (SimpleMemberAccessExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :331 :24) // Not a variable of known type: newNetId
%20 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :331 :36)
%21 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :331 :24) // comparison of unknown type: newNetId == 0
cond_br %21, ^6, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :331 :24)

^6: // SimpleBlock
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :333 :24) // Not a variable of known type: logger
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :333 :42) // "SetSyncVarGameObject GameObject " (StringLiteralExpression)
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :333 :79) // Not a variable of known type: newGameObject
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :333 :42) // Binary expression on unsupported types "SetSyncVarGameObject GameObject " + newGameObject
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :333 :95) // " has a zero netId. Maybe it is not spawned yet?" (StringLiteralExpression)
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :333 :42) // Binary expression on unsupported types "SetSyncVarGameObject GameObject " + newGameObject + " has a zero netId. Maybe it is not spawned yet?"
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :333 :24) // logger.LogWarning("SetSyncVarGameObject GameObject " + newGameObject + " has a zero netId. Maybe it is not spawned yet?") (InvocationExpression)
br ^4

^4: // BinaryBranchBlock
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :338 :16) // Not a variable of known type: logger
%30 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :338 :16) // logger.LogEnabled() (InvocationExpression)
cond_br %30, ^7, ^8 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :338 :16)

^7: // SimpleBlock
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :338 :37) // Not a variable of known type: logger
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :338 :48) // "SetSyncVar GameObject " (StringLiteralExpression)
// Entity from another assembly: GetType
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :338 :75) // GetType() (InvocationExpression)
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :338 :75) // GetType().Name (SimpleMemberAccessExpression)
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :338 :48) // Binary expression on unsupported types "SetSyncVar GameObject " + GetType().Name
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :338 :92) // " bit [" (StringLiteralExpression)
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :338 :48) // Binary expression on unsupported types "SetSyncVar GameObject " + GetType().Name + " bit ["
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :338 :103) // Not a variable of known type: dirtyBit
%39 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :338 :48) // Binary expression on unsupported types "SetSyncVar GameObject " + GetType().Name + " bit [" + dirtyBit
%40 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :338 :114) // "] netfieldId:" (StringLiteralExpression)
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :338 :48) // Binary expression on unsupported types "SetSyncVar GameObject " + GetType().Name + " bit [" + dirtyBit + "] netfieldId:"
%42 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :338 :132) // Not a variable of known type: netIdField
%43 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :338 :48) // Binary expression on unsupported types "SetSyncVar GameObject " + GetType().Name + " bit [" + dirtyBit + "] netfieldId:" + netIdField
%44 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :338 :145) // "->" (StringLiteralExpression)
%45 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :338 :48) // Binary expression on unsupported types "SetSyncVar GameObject " + GetType().Name + " bit [" + dirtyBit + "] netfieldId:" + netIdField + "->"
%46 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :338 :152) // Not a variable of known type: newNetId
%47 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :338 :48) // Binary expression on unsupported types "SetSyncVar GameObject " + GetType().Name + " bit [" + dirtyBit + "] netfieldId:" + netIdField + "->" + newNetId
%48 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :338 :37) // logger.Log("SetSyncVar GameObject " + GetType().Name + " bit [" + dirtyBit + "] netfieldId:" + netIdField + "->" + newNetId) (InvocationExpression)
br ^8

^8: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SetDirtyBit
%49 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :339 :24) // Not a variable of known type: dirtyBit
%50 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :339 :12) // SetDirtyBit(dirtyBit) (InvocationExpression)
%51 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :341 :30) // Not a variable of known type: newGameObject
%52 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :342 :25) // Not a variable of known type: newNetId
br ^9

^9: // ExitBlock
return

}
// Skipping function GetSyncVarGameObject(none, none), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkBehaviour.SyncVarNetworkIdentityEqual$Mirror.NetworkIdentity.uint$(none, none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :365 :8) {
^entry (%_newIdentity : none, %_netIdField : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :365 :51)
cbde.store %_newIdentity, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :365 :51)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :365 :80)
cbde.store %_netIdField, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :365 :80)
br ^0

^0: // BinaryBranchBlock
%2 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :367 :28)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :368 :16) // Not a variable of known type: newIdentity
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :368 :31) // null (NullLiteralExpression)
%6 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :368 :16) // comparison of unknown type: newIdentity != null
cond_br %6, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :368 :16)

^1: // BinaryBranchBlock
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :370 :27) // Not a variable of known type: newIdentity
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :370 :27) // newIdentity.netId (SimpleMemberAccessExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :371 :20) // Not a variable of known type: newNetId
%10 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :371 :32)
%11 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :371 :20) // comparison of unknown type: newNetId == 0
cond_br %11, ^3, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :371 :20)

^3: // SimpleBlock
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :373 :20) // Not a variable of known type: logger
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :373 :38) // "SetSyncVarNetworkIdentity NetworkIdentity " (StringLiteralExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :373 :85) // Not a variable of known type: newIdentity
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :373 :38) // Binary expression on unsupported types "SetSyncVarNetworkIdentity NetworkIdentity " + newIdentity
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :373 :99) // " has a zero netId. Maybe it is not spawned yet?" (StringLiteralExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :373 :38) // Binary expression on unsupported types "SetSyncVarNetworkIdentity NetworkIdentity " + newIdentity + " has a zero netId. Maybe it is not spawned yet?"
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :373 :20) // logger.LogWarning("SetSyncVarNetworkIdentity NetworkIdentity " + newIdentity + " has a zero netId. Maybe it is not spawned yet?") (InvocationExpression)
br ^2

^2: // JumpBlock
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :378 :19) // Not a variable of known type: newNetId
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :378 :31) // Not a variable of known type: netIdField
%21 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :378 :19) // comparison of unknown type: newNetId == netIdField
return %21 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :378 :12)

^4: // ExitBlock
cbde.unreachable

}
func @_Mirror.NetworkBehaviour.SetSyncVarNetworkIdentity$Mirror.NetworkIdentity.refMirror.NetworkIdentity.ulong.refuint$(none, none, none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :382 :8) {
^entry (%_newIdentity : none, %_identityField : none, %_dirtyBit : none, %_netIdField : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :382 :49)
cbde.store %_newIdentity, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :382 :49)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :382 :78)
cbde.store %_identityField, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :382 :78)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :382 :113)
cbde.store %_dirtyBit, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :382 :113)
%3 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :382 :129)
cbde.store %_netIdField, %3 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :382 :129)
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: getSyncVarHookGuard
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :384 :36) // Not a variable of known type: dirtyBit
%5 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :384 :16) // getSyncVarHookGuard(dirtyBit) (InvocationExpression)
cond_br %5, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :384 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :385 :16)

^2: // BinaryBranchBlock
%6 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :387 :28)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :388 :16) // Not a variable of known type: newIdentity
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :388 :31) // null (NullLiteralExpression)
%10 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :388 :16) // comparison of unknown type: newIdentity != null
cond_br %10, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :388 :16)

^3: // BinaryBranchBlock
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :390 :27) // Not a variable of known type: newIdentity
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :390 :27) // newIdentity.netId (SimpleMemberAccessExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :391 :20) // Not a variable of known type: newNetId
%14 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :391 :32)
%15 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :391 :20) // comparison of unknown type: newNetId == 0
cond_br %15, ^5, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :391 :20)

^5: // SimpleBlock
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :393 :20) // Not a variable of known type: logger
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :393 :38) // "SetSyncVarNetworkIdentity NetworkIdentity " (StringLiteralExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :393 :85) // Not a variable of known type: newIdentity
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :393 :38) // Binary expression on unsupported types "SetSyncVarNetworkIdentity NetworkIdentity " + newIdentity
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :393 :99) // " has a zero netId. Maybe it is not spawned yet?" (StringLiteralExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :393 :38) // Binary expression on unsupported types "SetSyncVarNetworkIdentity NetworkIdentity " + newIdentity + " has a zero netId. Maybe it is not spawned yet?"
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :393 :20) // logger.LogWarning("SetSyncVarNetworkIdentity NetworkIdentity " + newIdentity + " has a zero netId. Maybe it is not spawned yet?") (InvocationExpression)
br ^4

^4: // BinaryBranchBlock
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :397 :16) // Not a variable of known type: logger
%24 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :397 :16) // logger.LogEnabled() (InvocationExpression)
cond_br %24, ^6, ^7 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :397 :16)

^6: // SimpleBlock
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :397 :37) // Not a variable of known type: logger
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :397 :48) // "SetSyncVarNetworkIdentity NetworkIdentity " (StringLiteralExpression)
// Entity from another assembly: GetType
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :397 :95) // GetType() (InvocationExpression)
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :397 :95) // GetType().Name (SimpleMemberAccessExpression)
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :397 :48) // Binary expression on unsupported types "SetSyncVarNetworkIdentity NetworkIdentity " + GetType().Name
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :397 :112) // " bit [" (StringLiteralExpression)
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :397 :48) // Binary expression on unsupported types "SetSyncVarNetworkIdentity NetworkIdentity " + GetType().Name + " bit ["
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :397 :123) // Not a variable of known type: dirtyBit
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :397 :48) // Binary expression on unsupported types "SetSyncVarNetworkIdentity NetworkIdentity " + GetType().Name + " bit [" + dirtyBit
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :397 :134) // "] netIdField:" (StringLiteralExpression)
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :397 :48) // Binary expression on unsupported types "SetSyncVarNetworkIdentity NetworkIdentity " + GetType().Name + " bit [" + dirtyBit + "] netIdField:"
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :397 :152) // Not a variable of known type: netIdField
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :397 :48) // Binary expression on unsupported types "SetSyncVarNetworkIdentity NetworkIdentity " + GetType().Name + " bit [" + dirtyBit + "] netIdField:" + netIdField
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :397 :165) // "->" (StringLiteralExpression)
%39 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :397 :48) // Binary expression on unsupported types "SetSyncVarNetworkIdentity NetworkIdentity " + GetType().Name + " bit [" + dirtyBit + "] netIdField:" + netIdField + "->"
%40 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :397 :172) // Not a variable of known type: newNetId
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :397 :48) // Binary expression on unsupported types "SetSyncVarNetworkIdentity NetworkIdentity " + GetType().Name + " bit [" + dirtyBit + "] netIdField:" + netIdField + "->" + newNetId
%42 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :397 :37) // logger.Log("SetSyncVarNetworkIdentity NetworkIdentity " + GetType().Name + " bit [" + dirtyBit + "] netIdField:" + netIdField + "->" + newNetId) (InvocationExpression)
br ^7

^7: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SetDirtyBit
%43 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :398 :24) // Not a variable of known type: dirtyBit
%44 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :398 :12) // SetDirtyBit(dirtyBit) (InvocationExpression)
%45 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :399 :25) // Not a variable of known type: newNetId
%46 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :401 :28) // Not a variable of known type: newIdentity
br ^8

^8: // ExitBlock
return

}
func @_Mirror.NetworkBehaviour.GetSyncVarNetworkIdentity$uint.refMirror.NetworkIdentity$(none, none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :406 :8) {
^entry (%_netId : none, %_identityField : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :406 :60)
cbde.store %_netId, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :406 :60)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :406 :72)
cbde.store %_identityField, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :406 :72)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :409 :16) // Not a variable of known type: isServer
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :409 :16)

^1: // JumpBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :411 :23) // Not a variable of known type: identityField
return %3 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :411 :16)

^2: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: NetworkIdentity
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :416 :12) // NetworkIdentity.spawned (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :416 :48) // Not a variable of known type: netId
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :416 :59) // Not a variable of known type: identityField
%7 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :416 :12) // NetworkIdentity.spawned.TryGetValue(netId, out identityField) (InvocationExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :417 :19) // Not a variable of known type: identityField
return %8 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :417 :12)

^3: // ExitBlock
cbde.unreachable

}
func @_Mirror.NetworkBehaviour.SyncVarNetworkBehaviourEqual$T$$T.Mirror.NetworkBehaviour.NetworkBehaviourSyncVar$(none, none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :420 :8) {
^entry (%_newBehaviour : none, %_syncField : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :420 :55)
cbde.store %_newBehaviour, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :420 :55)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :420 :71)
cbde.store %_syncField, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :420 :71)
br ^0

^0: // BinaryBranchBlock
%2 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :422 :28)
%4 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :423 :36)
%5 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :423 :16) // newComponentIndex
cbde.store %4, %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :423 :16)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :424 :16) // Not a variable of known type: newBehaviour
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :424 :32) // null (NullLiteralExpression)
%8 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :424 :16) // comparison of unknown type: newBehaviour != null
cond_br %8, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :424 :16)

^1: // BinaryBranchBlock
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :426 :27) // Not a variable of known type: newBehaviour
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :426 :27) // newBehaviour.netId (SimpleMemberAccessExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :427 :36) // Not a variable of known type: newBehaviour
%12 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :427 :36) // newBehaviour.ComponentIndex (SimpleMemberAccessExpression)
cbde.store %12, %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :427 :16)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :428 :20) // Not a variable of known type: newNetId
%14 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :428 :32)
%15 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :428 :20) // comparison of unknown type: newNetId == 0
cond_br %15, ^3, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :428 :20)

^3: // SimpleBlock
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :430 :20) // Not a variable of known type: logger
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :430 :38) // "SetSyncVarNetworkIdentity NetworkIdentity " (StringLiteralExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :430 :85) // Not a variable of known type: newBehaviour
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :430 :38) // Binary expression on unsupported types "SetSyncVarNetworkIdentity NetworkIdentity " + newBehaviour
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :430 :100) // " has a zero netId. Maybe it is not spawned yet?" (StringLiteralExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :430 :38) // Binary expression on unsupported types "SetSyncVarNetworkIdentity NetworkIdentity " + newBehaviour + " has a zero netId. Maybe it is not spawned yet?"
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :430 :20) // logger.LogWarning("SetSyncVarNetworkIdentity NetworkIdentity " + newBehaviour + " has a zero netId. Maybe it is not spawned yet?") (InvocationExpression)
br ^2

^2: // JumpBlock
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :435 :19) // Not a variable of known type: syncField
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :435 :36) // Not a variable of known type: newNetId
%25 = cbde.load %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :435 :46)
%26 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :435 :19) // syncField.Equals(newNetId, newComponentIndex) (InvocationExpression)
return %26 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :435 :12)

^4: // ExitBlock
cbde.unreachable

}
func @_Mirror.NetworkBehaviour.SetSyncVarNetworkBehaviour$T$$T.refT.ulong.refMirror.NetworkBehaviour.NetworkBehaviourSyncVar$(none, none, none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :439 :8) {
^entry (%_newBehaviour : none, %_behaviourField : none, %_dirtyBit : none, %_syncField : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :439 :53)
cbde.store %_newBehaviour, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :439 :53)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :439 :69)
cbde.store %_behaviourField, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :439 :69)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :439 :91)
cbde.store %_dirtyBit, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :439 :91)
%3 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :439 :107)
cbde.store %_syncField, %3 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :439 :107)
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: getSyncVarHookGuard
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :441 :36) // Not a variable of known type: dirtyBit
%5 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :441 :16) // getSyncVarHookGuard(dirtyBit) (InvocationExpression)
cond_br %5, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :441 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :442 :16)

^2: // BinaryBranchBlock
%6 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :444 :28)
%8 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :445 :33)
%9 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :445 :16) // componentIndex
cbde.store %8, %9 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :445 :16)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :446 :16) // Not a variable of known type: newBehaviour
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :446 :32) // null (NullLiteralExpression)
%12 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :446 :16) // comparison of unknown type: newBehaviour != null
cond_br %12, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :446 :16)

^3: // BinaryBranchBlock
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :448 :27) // Not a variable of known type: newBehaviour
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :448 :27) // newBehaviour.netId (SimpleMemberAccessExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :449 :33) // Not a variable of known type: newBehaviour
%16 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :449 :33) // newBehaviour.ComponentIndex (SimpleMemberAccessExpression)
cbde.store %16, %9 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :449 :16)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :450 :20) // Not a variable of known type: newNetId
%18 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :450 :32)
%19 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :450 :20) // comparison of unknown type: newNetId == 0
cond_br %19, ^5, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :450 :20)

^5: // SimpleBlock
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :452 :20) // Not a variable of known type: logger
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :452 :41) // nameof(SetSyncVarNetworkBehaviour) (InvocationExpression)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :452 :38) // $"{nameof(SetSyncVarNetworkBehaviour)} NetworkIdentity " (InterpolatedStringExpression)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :452 :97) // Not a variable of known type: newBehaviour
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :452 :38) // Binary expression on unsupported types $"{nameof(SetSyncVarNetworkBehaviour)} NetworkIdentity " + newBehaviour
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :452 :112) // " has a zero netId. Maybe it is not spawned yet?" (StringLiteralExpression)
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :452 :38) // Binary expression on unsupported types $"{nameof(SetSyncVarNetworkBehaviour)} NetworkIdentity " + newBehaviour + " has a zero netId. Maybe it is not spawned yet?"
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :452 :20) // logger.LogWarning($"{nameof(SetSyncVarNetworkBehaviour)} NetworkIdentity " + newBehaviour + " has a zero netId. Maybe it is not spawned yet?") (InvocationExpression)
br ^4

^4: // BinaryBranchBlock
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :457 :47) // Not a variable of known type: syncField
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :458 :52) // Not a variable of known type: newNetId
%31 = cbde.load %9 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :458 :62)
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :458 :24) // new NetworkBehaviourSyncVar(newNetId, componentIndex) (ObjectCreationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SetDirtyBit
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :460 :24) // Not a variable of known type: dirtyBit
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :460 :12) // SetDirtyBit(dirtyBit) (InvocationExpression)
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :463 :29) // Not a variable of known type: newBehaviour
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :465 :16) // Not a variable of known type: logger
%37 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :465 :16) // logger.LogEnabled() (InvocationExpression)
cond_br %37, ^6, ^7 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :465 :16)

^6: // SimpleBlock
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :465 :37) // Not a variable of known type: logger
// Entity from another assembly: GetType
%39 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :465 :94) // GetType() (InvocationExpression)
%40 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :465 :94) // GetType().Name (SimpleMemberAccessExpression)
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :465 :116) // Not a variable of known type: dirtyBit
%42 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :465 :139) // Not a variable of known type: oldField
%43 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :465 :151) // Not a variable of known type: syncField
%44 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :465 :48) // $"SetSyncVarNetworkBehaviour NetworkIdentity {GetType().Name} bit [{dirtyBit}] netIdField:{oldField}->{syncField}" (InterpolatedStringExpression)
%45 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :465 :37) // logger.Log($"SetSyncVarNetworkBehaviour NetworkIdentity {GetType().Name} bit [{dirtyBit}] netIdField:{oldField}->{syncField}") (InvocationExpression)
br ^7

^7: // ExitBlock
return

}
// Skipping function GetSyncVarNetworkBehaviour(none, none), it contains poisonous unsupported syntaxes

// Skipping function Equals(none), it contains poisonous unsupported syntaxes

// Skipping function Equals(none, i32), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkBehaviour.NetworkBehaviourSyncVar.ToString$$() -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :515 :12) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :517 :33) // Not a variable of known type: netId
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :517 :51) // Not a variable of known type: componentIndex
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :517 :23) // $"[netId:{netId} compIndex:{componentIndex}]" (InterpolatedStringExpression)
return %2 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :517 :16)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.NetworkBehaviour.SyncVarEqual$T$$T.refT$(none, none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :521 :8) {
^entry (%_value : none, %_fieldValue : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :521 :39)
cbde.store %_value, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :521 :39)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :521 :48)
cbde.store %_fieldValue, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :521 :48)
br ^0

^0: // JumpBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :524 :19) // EqualityComparer<T> (GenericName)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :524 :19) // EqualityComparer<T>.Default (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :524 :54) // Not a variable of known type: value
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :524 :61) // Not a variable of known type: fieldValue
%6 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :524 :19) // EqualityComparer<T>.Default.Equals(value, fieldValue) (InvocationExpression)
return %6 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :524 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.NetworkBehaviour.SetSyncVar$T$$T.refT.ulong$(none, none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :527 :8) {
^entry (%_value : none, %_fieldValue : none, %_dirtyBit : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :527 :37)
cbde.store %_value, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :527 :37)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :527 :46)
cbde.store %_fieldValue, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :527 :46)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :527 :64)
cbde.store %_dirtyBit, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :527 :64)
br ^0

^0: // BinaryBranchBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :529 :16) // Not a variable of known type: logger
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :529 :16) // logger.LogEnabled() (InvocationExpression)
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :529 :16)

^1: // SimpleBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :529 :37) // Not a variable of known type: logger
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :529 :48) // "SetSyncVar " (StringLiteralExpression)
// Entity from another assembly: GetType
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :529 :64) // GetType() (InvocationExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :529 :64) // GetType().Name (SimpleMemberAccessExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :529 :48) // Binary expression on unsupported types "SetSyncVar " + GetType().Name
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :529 :81) // " bit [" (StringLiteralExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :529 :48) // Binary expression on unsupported types "SetSyncVar " + GetType().Name + " bit ["
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :529 :92) // Not a variable of known type: dirtyBit
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :529 :48) // Binary expression on unsupported types "SetSyncVar " + GetType().Name + " bit [" + dirtyBit
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :529 :103) // "] " (StringLiteralExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :529 :48) // Binary expression on unsupported types "SetSyncVar " + GetType().Name + " bit [" + dirtyBit + "] "
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :529 :110) // Not a variable of known type: fieldValue
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :529 :48) // Binary expression on unsupported types "SetSyncVar " + GetType().Name + " bit [" + dirtyBit + "] " + fieldValue
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :529 :123) // "->" (StringLiteralExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :529 :48) // Binary expression on unsupported types "SetSyncVar " + GetType().Name + " bit [" + dirtyBit + "] " + fieldValue + "->"
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :529 :130) // Not a variable of known type: value
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :529 :48) // Binary expression on unsupported types "SetSyncVar " + GetType().Name + " bit [" + dirtyBit + "] " + fieldValue + "->" + value
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :529 :37) // logger.Log("SetSyncVar " + GetType().Name + " bit [" + dirtyBit + "] " + fieldValue + "->" + value) (InvocationExpression)
br ^2

^2: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SetDirtyBit
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :530 :24) // Not a variable of known type: dirtyBit
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :530 :12) // SetDirtyBit(dirtyBit) (InvocationExpression)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :531 :25) // Not a variable of known type: value
br ^3

^3: // ExitBlock
return

}
func @_Mirror.NetworkBehaviour.SetDirtyBit$ulong$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :540 :8) {
^entry (%_dirtyBit : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :540 :32)
cbde.store %_dirtyBit, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :540 :32)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :542 :12) // Not a variable of known type: syncVarDirtyBits
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :542 :32) // Not a variable of known type: dirtyBit
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :542 :12) // Binary expression on unsupported types syncVarDirtyBits |= dirtyBit
br ^1

^1: // ExitBlock
return

}
func @_Mirror.NetworkBehaviour.ClearAllDirtyBits$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :549 :8) {
^entry :
br ^0

^0: // ForInitializerBlock
// Entity from another assembly: Time
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :551 :27) // Time.time (SimpleMemberAccessExpression)
%1 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :552 :31) // 0L (NumericLiteralExpression)
%2 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :558 :25)
%3 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :558 :21) // i
cbde.store %2, %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :558 :21)
br ^1

^1: // BinaryBranchBlock
%4 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :558 :28)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :558 :32) // Not a variable of known type: syncObjects
%6 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :558 :32) // syncObjects.Count (SimpleMemberAccessExpression)
%7 = cmpi "slt", %4, %6 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :558 :28)
cond_br %7, ^2, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :558 :28)

^2: // SimpleBlock
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :560 :16) // Not a variable of known type: syncObjects
%9 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :560 :28)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :560 :16) // syncObjects[i] (ElementAccessExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :560 :16) // syncObjects[i].Flush() (InvocationExpression)
br ^4

^4: // SimpleBlock
%12 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :558 :53)
%13 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :558 :51)
%14 = addi %12, %13 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :558 :51)
cbde.store %14, %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :558 :51)
br ^1

^3: // ExitBlock
return

}
func @_Mirror.NetworkBehaviour.AnySyncObjectDirty$$() -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :564 :8) {
^entry :
br ^0

^0: // ForInitializerBlock
%0 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :569 :25)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :569 :21) // i
cbde.store %0, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :569 :21)
br ^1

^1: // BinaryBranchBlock
%2 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :569 :28)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :569 :32) // Not a variable of known type: syncObjects
%4 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :569 :32) // syncObjects.Count (SimpleMemberAccessExpression)
%5 = cmpi "slt", %2, %4 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :569 :28)
cond_br %5, ^2, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :569 :28)

^2: // BinaryBranchBlock
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :571 :20) // Not a variable of known type: syncObjects
%7 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :571 :32)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :571 :20) // syncObjects[i] (ElementAccessExpression)
%9 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :571 :20) // syncObjects[i].IsDirty (SimpleMemberAccessExpression)
cond_br %9, ^4, ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :571 :20)

^4: // JumpBlock
%10 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :573 :27) // true
return %10 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :573 :20)

^5: // SimpleBlock
%11 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :569 :53)
%12 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :569 :51)
%13 = addi %11, %12 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :569 :51)
cbde.store %13, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :569 :51)
br ^1

^3: // JumpBlock
%14 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :576 :19) // false
return %14 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :576 :12)

^6: // ExitBlock
cbde.unreachable

}
// Skipping function IsDirty(), it contains poisonous unsupported syntaxes

// Skipping function OnSerialize(none, i1), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkBehaviour.OnDeserialize$Mirror.NetworkReader.bool$(none, i1) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :624 :8) {
^entry (%_reader : none, %_initialState : i1):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :624 :42)
cbde.store %_reader, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :624 :42)
%1 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :624 :64)
cbde.store %_initialState, %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :624 :64)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.load %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :626 :16)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :626 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: DeSerializeObjectsAll
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :628 :38) // Not a variable of known type: reader
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :628 :16) // DeSerializeObjectsAll(reader) (InvocationExpression)
br ^3

^2: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: DeSerializeObjectsDelta
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :632 :40) // Not a variable of known type: reader
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :632 :16) // DeSerializeObjectsDelta(reader) (InvocationExpression)
br ^3

^3: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: DeserializeSyncVars
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :635 :32) // Not a variable of known type: reader
%8 = cbde.load %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :635 :40)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :635 :12) // DeserializeSyncVars(reader, initialState) (InvocationExpression)
br ^4

^4: // ExitBlock
return

}
func @_Mirror.NetworkBehaviour.SerializeSyncVars$Mirror.NetworkWriter.bool$(none, i1) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :639 :8) {
^entry (%_writer : none, %_initialState : i1):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :639 :49)
cbde.store %_writer, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :639 :49)
%1 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :639 :71)
cbde.store %_initialState, %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :639 :71)
br ^0

^0: // JumpBlock
%2 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :641 :19) // false
return %2 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :641 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.NetworkBehaviour.DeserializeSyncVars$Mirror.NetworkReader.bool$(none, i1) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :653 :8) {
^entry (%_reader : none, %_initialState : i1):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :653 :51)
cbde.store %_reader, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :653 :51)
%1 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :653 :73)
cbde.store %_initialState, %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :653 :73)
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkBehaviour.DirtyObjectBits$$() -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :664 :8) {
^entry :
br ^0

^0: // ForInitializerBlock
%0 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :666 :33)
%2 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :667 :25)
%3 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :667 :21) // i
cbde.store %2, %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :667 :21)
br ^1

^1: // BinaryBranchBlock
%4 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :667 :28)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :667 :32) // Not a variable of known type: syncObjects
%6 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :667 :32) // syncObjects.Count (SimpleMemberAccessExpression)
%7 = cmpi "slt", %4, %6 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :667 :28)
cond_br %7, ^2, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :667 :28)

^2: // BinaryBranchBlock
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :669 :40) // Not a variable of known type: syncObjects
%9 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :669 :52)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :669 :40) // syncObjects[i] (ElementAccessExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :670 :20) // Not a variable of known type: syncObject
%13 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :670 :20) // syncObject.IsDirty (SimpleMemberAccessExpression)
cond_br %13, ^4, ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :670 :20)

^4: // SimpleBlock
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :672 :20) // Not a variable of known type: dirtyObjects
%15 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :672 :36) // 1UL (NumericLiteralExpression)
%16 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :672 :43)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :672 :36) // Binary expression on unsupported types 1UL << i
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :672 :20) // Binary expression on unsupported types dirtyObjects |= 1UL << i
br ^5

^5: // SimpleBlock
%19 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :667 :51)
%20 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :667 :51)
%21 = addi %19, %20 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :667 :51)
cbde.store %21, %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :667 :51)
br ^1

^3: // JumpBlock
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :675 :19) // Not a variable of known type: dirtyObjects
return %22 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :675 :12)

^6: // ExitBlock
cbde.unreachable

}
func @_Mirror.NetworkBehaviour.SerializeObjectsAll$Mirror.NetworkWriter$(none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :678 :8) {
^entry (%_writer : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :678 :40)
cbde.store %_writer, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :678 :40)
br ^0

^0: // ForInitializerBlock
%1 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :680 :25) // false
%2 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :680 :17) // dirty
cbde.store %1, %2 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :680 :17)
%3 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :681 :25)
%4 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :681 :21) // i
cbde.store %3, %4 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :681 :21)
br ^1

^1: // BinaryBranchBlock
%5 = cbde.load %4 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :681 :28)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :681 :32) // Not a variable of known type: syncObjects
%7 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :681 :32) // syncObjects.Count (SimpleMemberAccessExpression)
%8 = cmpi "slt", %5, %7 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :681 :28)
cond_br %8, ^2, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :681 :28)

^2: // SimpleBlock
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :683 :40) // Not a variable of known type: syncObjects
%10 = cbde.load %4 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :683 :52)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :683 :40) // syncObjects[i] (ElementAccessExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :684 :16) // Not a variable of known type: syncObject
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :684 :42) // Not a variable of known type: writer
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :684 :16) // syncObject.OnSerializeAll(writer) (InvocationExpression)
%16 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :685 :24) // true
cbde.store %16, %2 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :685 :16)
br ^4

^4: // SimpleBlock
%17 = cbde.load %4 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :681 :51)
%18 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :681 :51)
%19 = addi %17, %18 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :681 :51)
cbde.store %19, %4 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :681 :51)
br ^1

^3: // JumpBlock
%20 = cbde.load %2 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :687 :19)
return %20 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :687 :12)

^5: // ExitBlock
cbde.unreachable

}
func @_Mirror.NetworkBehaviour.SerializeObjectsDelta$Mirror.NetworkWriter$(none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :690 :8) {
^entry (%_writer : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :690 :42)
cbde.store %_writer, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :690 :42)
br ^0

^0: // ForInitializerBlock
%1 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :692 :25) // false
%2 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :692 :17) // dirty
cbde.store %1, %2 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :692 :17)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :694 :12) // Not a variable of known type: writer
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: DirtyObjectBits
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :694 :31) // DirtyObjectBits() (InvocationExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :694 :12) // writer.WriteUInt64(DirtyObjectBits()) (InvocationExpression)
%6 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :696 :25)
%7 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :696 :21) // i
cbde.store %6, %7 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :696 :21)
br ^1

^1: // BinaryBranchBlock
%8 = cbde.load %7 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :696 :28)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :696 :32) // Not a variable of known type: syncObjects
%10 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :696 :32) // syncObjects.Count (SimpleMemberAccessExpression)
%11 = cmpi "slt", %8, %10 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :696 :28)
cond_br %11, ^2, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :696 :28)

^2: // BinaryBranchBlock
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :698 :40) // Not a variable of known type: syncObjects
%13 = cbde.load %7 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :698 :52)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :698 :40) // syncObjects[i] (ElementAccessExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :699 :20) // Not a variable of known type: syncObject
%17 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :699 :20) // syncObject.IsDirty (SimpleMemberAccessExpression)
cond_br %17, ^4, ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :699 :20)

^4: // SimpleBlock
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :701 :20) // Not a variable of known type: syncObject
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :701 :48) // Not a variable of known type: writer
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :701 :20) // syncObject.OnSerializeDelta(writer) (InvocationExpression)
%21 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :702 :28) // true
cbde.store %21, %2 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :702 :20)
br ^5

^5: // SimpleBlock
%22 = cbde.load %7 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :696 :51)
%23 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :696 :51)
%24 = addi %22, %23 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :696 :51)
cbde.store %24, %7 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :696 :51)
br ^1

^3: // JumpBlock
%25 = cbde.load %2 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :705 :19)
return %25 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :705 :12)

^6: // ExitBlock
cbde.unreachable

}
func @_Mirror.NetworkBehaviour.DeSerializeObjectsAll$Mirror.NetworkReader$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :708 :8) {
^entry (%_reader : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :708 :44)
cbde.store %_reader, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :708 :44)
br ^0

^0: // ForInitializerBlock
%1 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :710 :25)
%2 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :710 :21) // i
cbde.store %1, %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :710 :21)
br ^1

^1: // BinaryBranchBlock
%3 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :710 :28)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :710 :32) // Not a variable of known type: syncObjects
%5 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :710 :32) // syncObjects.Count (SimpleMemberAccessExpression)
%6 = cmpi "slt", %3, %5 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :710 :28)
cond_br %6, ^2, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :710 :28)

^2: // SimpleBlock
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :712 :40) // Not a variable of known type: syncObjects
%8 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :712 :52)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :712 :40) // syncObjects[i] (ElementAccessExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :713 :16) // Not a variable of known type: syncObject
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :713 :44) // Not a variable of known type: reader
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :713 :16) // syncObject.OnDeserializeAll(reader) (InvocationExpression)
br ^4

^4: // SimpleBlock
%14 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :710 :51)
%15 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :710 :51)
%16 = addi %14, %15 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :710 :51)
cbde.store %16, %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :710 :51)
br ^1

^3: // ExitBlock
return

}
func @_Mirror.NetworkBehaviour.DeSerializeObjectsDelta$Mirror.NetworkReader$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :717 :8) {
^entry (%_reader : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :717 :46)
cbde.store %_reader, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :717 :46)
br ^0

^0: // ForInitializerBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :719 :26) // Not a variable of known type: reader
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :719 :26) // reader.ReadUInt64() (InvocationExpression)
%4 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :720 :25)
%5 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :720 :21) // i
cbde.store %4, %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :720 :21)
br ^1

^1: // BinaryBranchBlock
%6 = cbde.load %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :720 :28)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :720 :32) // Not a variable of known type: syncObjects
%8 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :720 :32) // syncObjects.Count (SimpleMemberAccessExpression)
%9 = cmpi "slt", %6, %8 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :720 :28)
cond_br %9, ^2, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :720 :28)

^2: // BinaryBranchBlock
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :722 :40) // Not a variable of known type: syncObjects
%11 = cbde.load %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :722 :52)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :722 :40) // syncObjects[i] (ElementAccessExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :723 :21) // Not a variable of known type: dirty
%15 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :723 :30) // 1UL (NumericLiteralExpression)
%16 = cbde.load %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :723 :37)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :723 :30) // Binary expression on unsupported types 1UL << i
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :723 :21) // Binary expression on unsupported types dirty & (1UL << i)
%19 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :723 :44)
%20 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :723 :20) // comparison of unknown type: (dirty & (1UL << i)) != 0
cond_br %20, ^4, ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :723 :20)

^4: // SimpleBlock
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :725 :20) // Not a variable of known type: syncObject
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :725 :50) // Not a variable of known type: reader
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :725 :20) // syncObject.OnDeserializeDelta(reader) (InvocationExpression)
br ^5

^5: // SimpleBlock
%24 = cbde.load %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :720 :51)
%25 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :720 :51)
%26 = addi %24, %25 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :720 :51)
cbde.store %26, %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :720 :51)
br ^1

^3: // ExitBlock
return

}
// Skipping function ResetSyncObjects(), it contains poisonous unsupported syntaxes

func @_Mirror.NetworkBehaviour.OnStopClient$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :742 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkBehaviour.OnStartServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :749 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkBehaviour.OnStopServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :755 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkBehaviour.OnStartClient$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :761 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkBehaviour.OnStartLocalPlayer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :767 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkBehaviour.OnStartAuthority$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :774 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
func @_Mirror.NetworkBehaviour.OnStopAuthority$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\NetworkBehaviour.cs" :780 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
