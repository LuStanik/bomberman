func @_Mirror.Examples.Benchmark.PlayerMovement.Update$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\PlayerMovement.cs" :8 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\PlayerMovement.cs" :10 :17) // Identifier from another assembly: isLocalPlayer
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\PlayerMovement.cs" :10 :16) // !isLocalPlayer (LogicalNotExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\PlayerMovement.cs" :10 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\PlayerMovement.cs" :10 :32)

^2: // SimpleBlock
// Entity from another assembly: Input
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\PlayerMovement.cs" :12 :36) // "Horizontal" (StringLiteralExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\PlayerMovement.cs" :12 :22) // Input.GetAxis("Horizontal") (InvocationExpression)
// Entity from another assembly: Input
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\PlayerMovement.cs" :13 :36) // "Vertical" (StringLiteralExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\PlayerMovement.cs" :13 :22) // Input.GetAxis("Vertical") (InvocationExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\PlayerMovement.cs" :15 :38) // Not a variable of known type: h
%9 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\PlayerMovement.cs" :15 :41)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\PlayerMovement.cs" :15 :44) // Not a variable of known type: v
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\PlayerMovement.cs" :15 :26) // new Vector3(h, 0, v) (ObjectCreationExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\PlayerMovement.cs" :16 :12) // Identifier from another assembly: transform
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\PlayerMovement.cs" :16 :12) // transform.position (SimpleMemberAccessExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\PlayerMovement.cs" :16 :34) // Not a variable of known type: dir
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\PlayerMovement.cs" :16 :34) // dir.normalized (SimpleMemberAccessExpression)
// Entity from another assembly: Time
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\PlayerMovement.cs" :16 :52) // Time.deltaTime (SimpleMemberAccessExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\PlayerMovement.cs" :16 :69) // Not a variable of known type: speed
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\PlayerMovement.cs" :16 :52) // Binary expression on unsupported types Time.deltaTime * speed
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\PlayerMovement.cs" :16 :34) // Binary expression on unsupported types dir.normalized * (Time.deltaTime * speed)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\PlayerMovement.cs" :16 :12) // Binary expression on unsupported types transform.position += dir.normalized * (Time.deltaTime * speed)
// No identifier name for binary assignment expression
br ^3

^3: // ExitBlock
return

}
