func @_Mirror.Examples.MultipleAdditiveScenes.PhysicsCollision.OnValidate$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :12 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :14 :16) // Not a variable of known type: rigidbody3D
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :14 :31) // null (NullLiteralExpression)
%2 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :14 :16) // comparison of unknown type: rigidbody3D == null
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :14 :16)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :15 :30) // GetComponent<Rigidbody>() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.Examples.MultipleAdditiveScenes.PhysicsCollision.Start$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :18 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :20 :12) // Not a variable of known type: rigidbody3D
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :20 :12) // rigidbody3D.isKinematic (SimpleMemberAccessExpression)
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :20 :39) // Identifier from another assembly: isServer
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :20 :38) // !isServer (LogicalNotExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.MultipleAdditiveScenes.PhysicsCollision.OnCollisionStay$UnityEngine.Collision$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :23 :8) {
^entry (%_other : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :24 :29)
cbde.store %_other, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :24 :29)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :26 :16) // Not a variable of known type: other
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :26 :16) // other.gameObject (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :26 :44) // "Player" (StringLiteralExpression)
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :26 :16) // other.gameObject.CompareTag("Player") (InvocationExpression)
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :26 :16)

^1: // BinaryBranchBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :29 :36) // Not a variable of known type: other
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :29 :36) // other.contacts (SimpleMemberAccessExpression)
%7 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :29 :51)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :29 :36) // other.contacts[0] (ElementAccessExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :29 :36) // other.contacts[0].normal (SimpleMemberAccessExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :32 :16) // Not a variable of known type: direction
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :32 :16) // direction.y (SimpleMemberAccessExpression)
%13 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :32 :30)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :33 :28) // Not a variable of known type: direction
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :33 :28) // direction.normalized (SimpleMemberAccessExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :36 :20) // Not a variable of known type: other
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :36 :20) // other.gameObject (SimpleMemberAccessExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :36 :20) // other.gameObject.GetComponent<NetworkIdentity>() (InvocationExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :36 :20) // other.gameObject.GetComponent<NetworkIdentity>().connectionToClient (SimpleMemberAccessExpression)
%20 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :36 :20) // other.gameObject.GetComponent<NetworkIdentity>().connectionToClient.connectionId (SimpleMemberAccessExpression)
// Entity from another assembly: NetworkConnection
%21 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :36 :104)
%22 = cmpi "eq", %20, %21 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :36 :20)
cond_br %22, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :36 :20)

^3: // SimpleBlock
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :37 :20) // Not a variable of known type: rigidbody3D
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :37 :41) // Not a variable of known type: direction
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :37 :53) // Not a variable of known type: force
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :37 :41) // Binary expression on unsupported types direction * force
%27 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :37 :61) // .5f (NumericLiteralExpression)
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :37 :41) // Binary expression on unsupported types direction * force * .5f
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :37 :20) // rigidbody3D.AddForce(direction * force * .5f) (InvocationExpression)
br ^2

^4: // SimpleBlock
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :39 :20) // Not a variable of known type: rigidbody3D
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :39 :41) // Not a variable of known type: direction
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :39 :53) // Not a variable of known type: force
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :39 :41) // Binary expression on unsupported types direction * force
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsCollision.cs" :39 :20) // rigidbody3D.AddForce(direction * force) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
