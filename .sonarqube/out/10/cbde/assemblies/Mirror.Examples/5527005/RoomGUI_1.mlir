// Skipping function RefreshRoomPlayers(none), it contains poisonous unsupported syntaxes

func @_Mirror.Examples.MultipleMatch.RoomGUI.SetOwner$bool$(i1) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\RoomGUI.cs" :42 :8) {
^entry (%_owner : i1):
%0 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\RoomGUI.cs" :42 :29)
cbde.store %_owner, %0 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\RoomGUI.cs" :42 :29)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\RoomGUI.cs" :44 :12) // this (ThisExpression)
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\RoomGUI.cs" :44 :12) // this.owner (SimpleMemberAccessExpression)
%3 = cbde.load %0 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\RoomGUI.cs" :44 :25)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\RoomGUI.cs" :45 :12) // Not a variable of known type: cancelButton
%5 = cbde.load %0 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\RoomGUI.cs" :45 :35)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\RoomGUI.cs" :45 :12) // cancelButton.SetActive(owner) (InvocationExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\RoomGUI.cs" :46 :12) // Not a variable of known type: leaveButton
%8 = cbde.load %0 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\RoomGUI.cs" :46 :35)
%9 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\RoomGUI.cs" :46 :34) // !owner (LogicalNotExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\RoomGUI.cs" :46 :12) // leaveButton.SetActive(!owner) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
