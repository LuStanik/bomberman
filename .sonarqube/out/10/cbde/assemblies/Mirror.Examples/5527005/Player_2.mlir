func @_Mirror.Examples.Chat.Player.CmdSend$string$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\Player.cs" :12 :8) {
^entry (%_message : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\Player.cs" :13 :28)
cbde.store %_message, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\Player.cs" :13 :28)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\Player.cs" :15 :16) // Not a variable of known type: message
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\Player.cs" :15 :16) // message.Trim() (InvocationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\Player.cs" :15 :34) // "" (StringLiteralExpression)
%4 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\Player.cs" :15 :16) // comparison of unknown type: message.Trim() != ""
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\Player.cs" :15 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: RpcReceive
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\Player.cs" :16 :27) // Not a variable of known type: message
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\Player.cs" :16 :27) // message.Trim() (InvocationExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\Player.cs" :16 :16) // RpcReceive(message.Trim()) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
// Skipping function RpcReceive(none), it contains poisonous unsupported syntaxes

