func @_Mirror.Examples.NetworkRoom.NetworkRoomManagerExt.OnRoomServerSceneChanged$string$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomManagerExt.cs" :16 :8) {
^entry (%_sceneName : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomManagerExt.cs" :16 :54)
cbde.store %_sceneName, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomManagerExt.cs" :16 :54)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomManagerExt.cs" :19 :16) // Not a variable of known type: sceneName
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomManagerExt.cs" :19 :29) // Identifier from another assembly: GameplayScene
%3 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomManagerExt.cs" :19 :16) // comparison of unknown type: sceneName == GameplayScene
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomManagerExt.cs" :19 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Spawner
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomManagerExt.cs" :21 :16) // Spawner.InitialSpawn() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.Examples.NetworkRoom.NetworkRoomManagerExt.OnRoomServerSceneLoadedForPlayer$Mirror.NetworkConnection.UnityEngine.GameObject.UnityEngine.GameObject$(none, none, none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomManagerExt.cs" :33 :8) {
^entry (%_conn : none, %_roomPlayer : none, %_gamePlayer : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomManagerExt.cs" :33 :62)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomManagerExt.cs" :33 :62)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomManagerExt.cs" :33 :86)
cbde.store %_roomPlayer, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomManagerExt.cs" :33 :86)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomManagerExt.cs" :33 :109)
cbde.store %_gamePlayer, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomManagerExt.cs" :33 :109)
br ^0

^0: // JumpBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomManagerExt.cs" :35 :38) // Not a variable of known type: gamePlayer
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomManagerExt.cs" :35 :38) // gamePlayer.GetComponent<PlayerScore>() (InvocationExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomManagerExt.cs" :36 :12) // Not a variable of known type: playerScore
%7 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomManagerExt.cs" :36 :12) // playerScore.index (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomManagerExt.cs" :36 :32) // Not a variable of known type: roomPlayer
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomManagerExt.cs" :36 :32) // roomPlayer.GetComponent<NetworkRoomPlayer>() (InvocationExpression)
%10 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomManagerExt.cs" :36 :32) // roomPlayer.GetComponent<NetworkRoomPlayer>().index (SimpleMemberAccessExpression)
%11 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomManagerExt.cs" :37 :19) // true
return %11 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomManagerExt.cs" :37 :12)

^1: // ExitBlock
cbde.unreachable

}
// Skipping function OnRoomStopClient(), it contains poisonous unsupported syntaxes

// Skipping function OnRoomStopServer(), it contains poisonous unsupported syntaxes

func @_Mirror.Examples.NetworkRoom.NetworkRoomManagerExt.OnRoomServerPlayersReady$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomManagerExt.cs" :71 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomManagerExt.cs" :77 :30) // true
br ^1

^1: // ExitBlock
return

}
// Skipping function OnGUI(), it contains poisonous unsupported syntaxes

