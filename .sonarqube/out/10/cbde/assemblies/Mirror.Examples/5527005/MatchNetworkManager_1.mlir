func @_Mirror.Examples.MultipleMatch.MatchNetworkManager.Awake$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :22 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :24 :12) // base (BaseExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :24 :12) // base.Awake() (InvocationExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :25 :12) // Not a variable of known type: canvasController
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :25 :12) // canvasController.InitializeData() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.MultipleMatch.MatchNetworkManager.OnServerReady$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :37 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :37 :43)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :37 :43)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :39 :12) // base (BaseExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :39 :31) // Not a variable of known type: conn
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :39 :12) // base.OnServerReady(conn) (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :40 :12) // Not a variable of known type: canvasController
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :40 :43) // Not a variable of known type: conn
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :40 :12) // canvasController.OnServerReady(conn) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.MultipleMatch.MatchNetworkManager.OnServerDisconnect$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :48 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :48 :48)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :48 :48)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :50 :12) // Not a variable of known type: canvasController
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :50 :48) // Not a variable of known type: conn
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :50 :12) // canvasController.OnServerDisconnect(conn) (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :51 :12) // base (BaseExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :51 :36) // Not a variable of known type: conn
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :51 :12) // base.OnServerDisconnect(conn) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.MultipleMatch.MatchNetworkManager.OnClientConnect$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :63 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :63 :45)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :63 :45)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :65 :12) // base (BaseExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :65 :33) // Not a variable of known type: conn
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :65 :12) // base.OnClientConnect(conn) (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :66 :12) // Not a variable of known type: canvasController
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :66 :45) // Not a variable of known type: conn
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :66 :12) // canvasController.OnClientConnect(conn) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.MultipleMatch.MatchNetworkManager.OnClientDisconnect$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :74 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :74 :48)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :74 :48)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :76 :12) // Not a variable of known type: canvasController
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :76 :12) // canvasController.OnClientDisconnect() (InvocationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :77 :12) // base (BaseExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :77 :36) // Not a variable of known type: conn
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :77 :12) // base.OnClientDisconnect(conn) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.MultipleMatch.MatchNetworkManager.OnStartServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :88 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :90 :16) // Identifier from another assembly: mode
// Entity from another assembly: NetworkManagerMode
%1 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :90 :24) // NetworkManagerMode.ServerOnly (SimpleMemberAccessExpression)
%2 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :90 :16) // comparison of unknown type: mode == NetworkManagerMode.ServerOnly
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :90 :16)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :91 :16) // Not a variable of known type: canvas
%4 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :91 :33) // true
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :91 :16) // canvas.SetActive(true) (InvocationExpression)
br ^2

^2: // SimpleBlock
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :93 :12) // Not a variable of known type: canvasController
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :93 :12) // canvasController.OnStartServer() (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Examples.MultipleMatch.MatchNetworkManager.OnStartClient$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :99 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :101 :12) // Not a variable of known type: canvas
%1 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :101 :29) // true
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :101 :12) // canvas.SetActive(true) (InvocationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :102 :12) // Not a variable of known type: canvasController
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :102 :12) // canvasController.OnStartClient() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.MultipleMatch.MatchNetworkManager.OnStopServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :108 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :110 :12) // Not a variable of known type: canvasController
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :110 :12) // canvasController.OnStopServer() (InvocationExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :111 :12) // Not a variable of known type: canvas
%3 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :111 :29) // false
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :111 :12) // canvas.SetActive(false) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.MultipleMatch.MatchNetworkManager.OnStopClient$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :117 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :119 :12) // Not a variable of known type: canvasController
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchNetworkManager.cs" :119 :12) // canvasController.OnStopClient() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
