func @_Mirror.Examples.NetworkRoom.Spawner.InitialSpawn$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :6 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: NetworkServer
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :8 :17) // NetworkServer.active (SimpleMemberAccessExpression)
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :8 :16) // !NetworkServer.active (LogicalNotExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :8 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :8 :39)

^2: // ForInitializerBlock
%2 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :10 :25)
%3 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :10 :21) // i
cbde.store %2, %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :10 :21)
br ^3

^3: // BinaryBranchBlock
%4 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :10 :28)
%5 = constant 10 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :10 :32)
%6 = cmpi "slt", %4, %5 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :10 :28)
cond_br %6, ^4, ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :10 :28)

^4: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SpawnReward
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :11 :16) // SpawnReward() (InvocationExpression)
br ^6

^6: // SimpleBlock
%8 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :10 :36)
%9 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :10 :36)
%10 = addi %8, %9 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :10 :36)
cbde.store %10, %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :10 :36)
br ^3

^5: // ExitBlock
return

}
func @_Mirror.Examples.NetworkRoom.Spawner.SpawnReward$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :14 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: NetworkServer
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :16 :17) // NetworkServer.active (SimpleMemberAccessExpression)
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :16 :16) // !NetworkServer.active (LogicalNotExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :16 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :16 :39)

^2: // SimpleBlock
// Entity from another assembly: Random
%2 = constant 19 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :18 :62)
%3 = cbde.neg %2 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :18 :61)
%4 = constant 20 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :18 :66)
%5 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :18 :48) // Random.Range(-19, 20) (InvocationExpression)
%6 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :18 :71)
// Entity from another assembly: Random
%7 = constant 19 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :18 :88)
%8 = cbde.neg %7 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :18 :87)
%9 = constant 20 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :18 :92)
%10 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :18 :74) // Random.Range(-19, 20) (InvocationExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :18 :36) // new Vector3(Random.Range(-19, 20), 1, Random.Range(-19, 20)) (ObjectCreationExpression)
// Entity from another assembly: NetworkServer
// Entity from another assembly: Instantiate
// Entity from another assembly: NetworkManager
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :19 :68) // NetworkManager.singleton (SimpleMemberAccessExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :19 :45) // (NetworkRoomManagerExt)NetworkManager.singleton (CastExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :19 :44) // ((NetworkRoomManagerExt)NetworkManager.singleton).rewardPrefab (SimpleMemberAccessExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :19 :108) // Not a variable of known type: spawnPosition
// Entity from another assembly: Quaternion
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :19 :123) // Quaternion.identity (SimpleMemberAccessExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :19 :32) // Instantiate(((NetworkRoomManagerExt)NetworkManager.singleton).rewardPrefab, spawnPosition, Quaternion.identity) (InvocationExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Spawner.cs" :19 :12) // NetworkServer.Spawn(Instantiate(((NetworkRoomManagerExt)NetworkManager.singleton).rewardPrefab, spawnPosition, Quaternion.identity)) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
