func @_Mirror.Examples.Pong.Player.FixedUpdate$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Pong\\Scripts\\Player.cs" :10 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Pong\\Scripts\\Player.cs" :14 :16) // Identifier from another assembly: isLocalPlayer
cond_br %0, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Pong\\Scripts\\Player.cs" :14 :16)

^1: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Pong\\Scripts\\Player.cs" :15 :16) // Not a variable of known type: rigidbody2d
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Pong\\Scripts\\Player.cs" :15 :16) // rigidbody2d.velocity (SimpleMemberAccessExpression)
%3 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Pong\\Scripts\\Player.cs" :15 :51)
// Entity from another assembly: Input
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Pong\\Scripts\\Player.cs" :15 :71) // "Vertical" (StringLiteralExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Pong\\Scripts\\Player.cs" :15 :54) // Input.GetAxisRaw("Vertical") (InvocationExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Pong\\Scripts\\Player.cs" :15 :39) // new Vector2(0, Input.GetAxisRaw("Vertical")) (ObjectCreationExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Pong\\Scripts\\Player.cs" :15 :86) // Not a variable of known type: speed
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Pong\\Scripts\\Player.cs" :15 :39) // Binary expression on unsupported types new Vector2(0, Input.GetAxisRaw("Vertical")) * speed
// Entity from another assembly: Time
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Pong\\Scripts\\Player.cs" :15 :94) // Time.fixedDeltaTime (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Pong\\Scripts\\Player.cs" :15 :39) // Binary expression on unsupported types new Vector2(0, Input.GetAxisRaw("Vertical")) * speed * Time.fixedDeltaTime
br ^2

^2: // ExitBlock
return

}
