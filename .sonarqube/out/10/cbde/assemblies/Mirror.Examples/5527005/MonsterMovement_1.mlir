func @_Mirror.Examples.Benchmark.MonsterMovement.OnStartServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :14 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :16 :20) // Identifier from another assembly: transform
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :16 :20) // transform.position (SimpleMemberAccessExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.Benchmark.MonsterMovement.Update$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :19 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :22 :16) // Not a variable of known type: moving
cond_br %0, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :22 :16)

^1: // BinaryBranchBlock
// Entity from another assembly: Vector3
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :24 :37) // Identifier from another assembly: transform
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :24 :37) // transform.position (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :24 :57) // Not a variable of known type: destination
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :24 :20) // Vector3.Distance(transform.position, destination) (InvocationExpression)
%5 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :24 :73) // 0.01f (NumericLiteralExpression)
%6 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :24 :20) // comparison of unknown type: Vector3.Distance(transform.position, destination) <= 0.01f
cond_br %6, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :24 :20)

^3: // SimpleBlock
%7 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :26 :29) // false
br ^5

^4: // SimpleBlock
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :30 :20) // Identifier from another assembly: transform
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :30 :20) // transform.position (SimpleMemberAccessExpression)
// Entity from another assembly: Vector3
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :30 :61) // Identifier from another assembly: transform
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :30 :61) // transform.position (SimpleMemberAccessExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :30 :81) // Not a variable of known type: destination
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :30 :94) // Not a variable of known type: speed
// Entity from another assembly: Time
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :30 :102) // Time.deltaTime (SimpleMemberAccessExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :30 :94) // Binary expression on unsupported types speed * Time.deltaTime
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :30 :41) // Vector3.MoveTowards(transform.position, destination, speed * Time.deltaTime) (InvocationExpression)
br ^5

^2: // BinaryBranchBlock
// Entity from another assembly: Random
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :35 :26) // Random.value (SimpleMemberAccessExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :36 :20) // Not a variable of known type: r
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :36 :24) // Not a variable of known type: movementProbability
// Entity from another assembly: Time
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :36 :46) // Time.deltaTime (SimpleMemberAccessExpression)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :36 :24) // Binary expression on unsupported types movementProbability * Time.deltaTime
%23 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :36 :20) // comparison of unknown type: r < movementProbability * Time.deltaTime
cond_br %23, ^6, ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :36 :20)

^6: // BinaryBranchBlock
// Entity from another assembly: Random
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :38 :40) // Random.insideUnitCircle (SimpleMemberAccessExpression)
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :39 :46) // Not a variable of known type: circlePos
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :39 :46) // circlePos.x (SimpleMemberAccessExpression)
%28 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :39 :59)
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :39 :62) // Not a variable of known type: circlePos
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :39 :62) // circlePos.y (SimpleMemberAccessExpression)
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :39 :34) // new Vector3(circlePos.x, 0, circlePos.y) (ObjectCreationExpression)
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :40 :35) // Identifier from another assembly: transform
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :40 :35) // transform.position (SimpleMemberAccessExpression)
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :40 :56) // Not a variable of known type: dir
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :40 :62) // Not a variable of known type: movementDistance
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :40 :56) // Binary expression on unsupported types dir * movementDistance
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :40 :35) // Binary expression on unsupported types transform.position + dir * movementDistance
// Entity from another assembly: Vector3
%40 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :44 :41) // Not a variable of known type: start
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :44 :48) // Not a variable of known type: dest
%42 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :44 :24) // Vector3.Distance(start, dest) (InvocationExpression)
%43 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :44 :57) // Not a variable of known type: movementDistance
%44 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :44 :24) // comparison of unknown type: Vector3.Distance(start, dest) <= movementDistance
cond_br %44, ^7, ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :44 :24)

^7: // SimpleBlock
%45 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :46 :38) // Not a variable of known type: dest
%46 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\MonsterMovement.cs" :47 :33) // true
br ^5

^5: // ExitBlock
return

}
