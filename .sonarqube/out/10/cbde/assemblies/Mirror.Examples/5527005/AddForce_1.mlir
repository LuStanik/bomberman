func @_Mirror.Examples.RigidbodyPhysics.AddForce.Start$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\RigidbodyPhysics\\Scripts\\AddForce.cs" :9 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\RigidbodyPhysics\\Scripts\\AddForce.cs" :11 :12) // Not a variable of known type: rigidbody3d
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\RigidbodyPhysics\\Scripts\\AddForce.cs" :11 :12) // rigidbody3d.isKinematic (SimpleMemberAccessExpression)
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\RigidbodyPhysics\\Scripts\\AddForce.cs" :11 :39) // Identifier from another assembly: isServer
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\RigidbodyPhysics\\Scripts\\AddForce.cs" :11 :38) // !isServer (LogicalNotExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function Update(), it contains poisonous unsupported syntaxes

