// Skipping function Awake(), it contains poisonous unsupported syntaxes

func @_Mirror.Examples.MultipleAdditiveScenes.PhysicsSimulator.FixedUpdate$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsSimulator.cs" :28 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: NetworkServer
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsSimulator.cs" :30 :17) // NetworkServer.active (SimpleMemberAccessExpression)
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsSimulator.cs" :30 :16) // !NetworkServer.active (LogicalNotExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsSimulator.cs" :30 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsSimulator.cs" :30 :39)

^2: // BinaryBranchBlock
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsSimulator.cs" :32 :16) // Not a variable of known type: simulatePhysicsScene
cond_br %2, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsSimulator.cs" :32 :16)

^3: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsSimulator.cs" :33 :16) // Not a variable of known type: physicsScene
// Entity from another assembly: Time
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsSimulator.cs" :33 :38) // Time.fixedDeltaTime (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsSimulator.cs" :33 :16) // physicsScene.Simulate(Time.fixedDeltaTime) (InvocationExpression)
br ^4

^4: // BinaryBranchBlock
%6 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsSimulator.cs" :35 :16) // Not a variable of known type: simulatePhysicsScene2D
cond_br %6, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsSimulator.cs" :35 :16)

^5: // SimpleBlock
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsSimulator.cs" :36 :16) // Not a variable of known type: physicsScene2D
// Entity from another assembly: Time
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsSimulator.cs" :36 :40) // Time.fixedDeltaTime (SimpleMemberAccessExpression)
%9 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PhysicsSimulator.cs" :36 :16) // physicsScene2D.Simulate(Time.fixedDeltaTime) (InvocationExpression)
br ^6

^6: // ExitBlock
return

}
