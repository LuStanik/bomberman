func @_Mirror.Examples.Basic.BasicNetManager.OnServerAddPlayer$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\BasicNetManager.cs" :29 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\BasicNetManager.cs" :29 :47)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\BasicNetManager.cs" :29 :47)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\BasicNetManager.cs" :31 :12) // base (BaseExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\BasicNetManager.cs" :31 :35) // Not a variable of known type: conn
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\BasicNetManager.cs" :31 :12) // base.OnServerAddPlayer(conn) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ResetPlayerNumbers
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\BasicNetManager.cs" :32 :12) // ResetPlayerNumbers() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.Basic.BasicNetManager.OnServerDisconnect$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\BasicNetManager.cs" :40 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\BasicNetManager.cs" :40 :48)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\BasicNetManager.cs" :40 :48)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\BasicNetManager.cs" :42 :12) // base (BaseExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\BasicNetManager.cs" :42 :36) // Not a variable of known type: conn
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\BasicNetManager.cs" :42 :12) // base.OnServerDisconnect(conn) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ResetPlayerNumbers
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\BasicNetManager.cs" :43 :12) // ResetPlayerNumbers() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function ResetPlayerNumbers(), it contains poisonous unsupported syntaxes

