// Skipping function PlayerNumberChanged(i32, i32), it contains poisonous unsupported syntaxes

// Skipping function PlayerDataChanged(i32, i32), it contains poisonous unsupported syntaxes

// Skipping function PlayerColorChanged(none, none), it contains poisonous unsupported syntaxes

func @_Mirror.Examples.Basic.Player.OnStartServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :59 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :61 :12) // base (BaseExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :61 :12) // base.OnStartServer() (InvocationExpression)
// Entity from another assembly: NetworkManager
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :64 :30) // NetworkManager.singleton (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :64 :13) // (BasicNetManager)NetworkManager.singleton (CastExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :64 :12) // ((BasicNetManager)NetworkManager.singleton).playersList (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :64 :72) // this (ThisExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :64 :12) // ((BasicNetManager)NetworkManager.singleton).playersList.Add(this) (InvocationExpression)
// Entity from another assembly: Random
%7 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :67 :42) // 0f (NumericLiteralExpression)
%8 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :67 :46) // 1f (NumericLiteralExpression)
%9 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :67 :50) // 0.9f (NumericLiteralExpression)
%10 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :67 :56) // 0.9f (NumericLiteralExpression)
%11 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :67 :62) // 1f (NumericLiteralExpression)
%12 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :67 :66) // 1f (NumericLiteralExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :67 :26) // Random.ColorHSV(0f, 1f, 0.9f, 0.9f, 1f, 1f) (InvocationExpression)
// Entity from another assembly: InvokeRepeating
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :70 :28) // nameof(UpdateData) (InvocationExpression)
%15 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :70 :48)
%16 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :70 :51)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :70 :12) // InvokeRepeating(nameof(UpdateData), 1, 1) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.Basic.Player.OnStopServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :77 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: CancelInvoke
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :79 :12) // CancelInvoke() (InvocationExpression)
// Entity from another assembly: NetworkManager
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :80 :30) // NetworkManager.singleton (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :80 :13) // (BasicNetManager)NetworkManager.singleton (CastExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :80 :12) // ((BasicNetManager)NetworkManager.singleton).playersList (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :80 :75) // this (ThisExpression)
%5 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :80 :12) // ((BasicNetManager)NetworkManager.singleton).playersList.Remove(this) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.Basic.Player.UpdateData$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :84 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: Random
%0 = constant 100 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :87 :38)
%1 = constant 1000 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :87 :43)
%2 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :87 :25) // Random.Range(100, 1000) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.Basic.Player.OnStartClient$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :94 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: NetworkManager
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :97 :30) // NetworkManager.singleton (SimpleMemberAccessExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :97 :13) // (BasicNetManager)NetworkManager.singleton (CastExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :97 :12) // ((BasicNetManager)NetworkManager.singleton).mainPanel (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :97 :12) // ((BasicNetManager)NetworkManager.singleton).mainPanel.gameObject (SimpleMemberAccessExpression)
%4 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :97 :87) // true
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :97 :12) // ((BasicNetManager)NetworkManager.singleton).mainPanel.gameObject.SetActive(true) (InvocationExpression)
// Entity from another assembly: Instantiate
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :100 :35) // Not a variable of known type: playerUIPrefab
// Entity from another assembly: NetworkManager
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :100 :69) // NetworkManager.singleton (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :100 :52) // (BasicNetManager)NetworkManager.singleton (CastExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :100 :51) // ((BasicNetManager)NetworkManager.singleton).playersPanel (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :100 :23) // Instantiate(playerUIPrefab, ((BasicNetManager)NetworkManager.singleton).playersPanel) (InvocationExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :103 :12) // Not a variable of known type: playerUI
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :103 :12) // playerUI.GetComponent<PlayerUI>() (InvocationExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :103 :56) // this (ThisExpression)
%14 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :103 :62) // Identifier from another assembly: isLocalPlayer
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :103 :12) // playerUI.GetComponent<PlayerUI>().SetPlayer(this, isLocalPlayer) (InvocationExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :106 :12) // Not a variable of known type: OnPlayerNumberChanged
%17 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :106 :41) // Not a variable of known type: playerNumber
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :106 :12) // OnPlayerNumberChanged.Invoke(playerNumber) (InvocationExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :107 :12) // Not a variable of known type: OnPlayerColorChanged
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :107 :40) // Not a variable of known type: playerColor
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :107 :12) // OnPlayerColorChanged.Invoke(playerColor) (InvocationExpression)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :108 :12) // Not a variable of known type: OnPlayerDataChanged
%23 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :108 :39) // Not a variable of known type: playerData
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :108 :12) // OnPlayerDataChanged.Invoke(playerData) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.Basic.Player.OnStopClient$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :115 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: Destroy
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :118 :20) // Not a variable of known type: playerUI
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :118 :12) // Destroy(playerUI) (InvocationExpression)
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :121 :16) // Identifier from another assembly: isLocalPlayer
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :121 :16)

^1: // SimpleBlock
// Entity from another assembly: NetworkManager
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :122 :34) // NetworkManager.singleton (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :122 :17) // (BasicNetManager)NetworkManager.singleton (CastExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :122 :16) // ((BasicNetManager)NetworkManager.singleton).mainPanel (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :122 :16) // ((BasicNetManager)NetworkManager.singleton).mainPanel.gameObject (SimpleMemberAccessExpression)
%7 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :122 :91) // false
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\Player.cs" :122 :16) // ((BasicNetManager)NetworkManager.singleton).mainPanel.gameObject.SetActive(false) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
