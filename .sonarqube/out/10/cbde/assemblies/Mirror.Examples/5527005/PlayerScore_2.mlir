func @_Mirror.Examples.NetworkRoom.PlayerScore.OnGUI$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\PlayerScore.cs" :12 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: GUI
%0 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\PlayerScore.cs" :14 :29) // 10f (NumericLiteralExpression)
%1 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\PlayerScore.cs" :14 :36) // Not a variable of known type: index
%2 = constant 110 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\PlayerScore.cs" :14 :44)
%3 = muli %1, %2 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\PlayerScore.cs" :14 :36)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\PlayerScore.cs" :14 :29) // Binary expression on unsupported types 10f + (index * 110)
%5 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\PlayerScore.cs" :14 :50) // 10f (NumericLiteralExpression)
%6 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\PlayerScore.cs" :14 :55) // 100f (NumericLiteralExpression)
%7 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\PlayerScore.cs" :14 :61) // 25f (NumericLiteralExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\PlayerScore.cs" :14 :20) // new Rect(10f + (index * 110), 10f, 100f, 25f) (ObjectCreationExpression)
%9 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\PlayerScore.cs" :14 :71) // Not a variable of known type: index
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\PlayerScore.cs" :14 :80) // Not a variable of known type: score
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\PlayerScore.cs" :14 :95) // "0000000" (StringLiteralExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\PlayerScore.cs" :14 :80) // score.ToString("0000000") (InvocationExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\PlayerScore.cs" :14 :67) // $"P{index}: {score.ToString("0000000")}" (InterpolatedStringExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\PlayerScore.cs" :14 :12) // GUI.Box(new Rect(10f + (index * 110), 10f, 100f, 25f), $"P{index}: {score.ToString("0000000")}") (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
