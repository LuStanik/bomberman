func @_Mirror.Examples.MultipleMatch.CellGUI.Awake$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CellGUI.cs" :18 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CellGUI.cs" :20 :12) // Not a variable of known type: matchController
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CellGUI.cs" :20 :12) // matchController.MatchCells (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CellGUI.cs" :20 :43) // Not a variable of known type: cellValue
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CellGUI.cs" :20 :54) // this (ThisExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CellGUI.cs" :20 :12) // matchController.MatchCells.Add(cellValue, this) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.MultipleMatch.CellGUI.MakePlay$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CellGUI.cs" :23 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CellGUI.cs" :25 :16) // Not a variable of known type: matchController
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CellGUI.cs" :25 :16) // matchController.currentPlayer (SimpleMemberAccessExpression)
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CellGUI.cs" :25 :16) // matchController.currentPlayer.isLocalPlayer (SimpleMemberAccessExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CellGUI.cs" :25 :16)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CellGUI.cs" :26 :16) // Not a variable of known type: matchController
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CellGUI.cs" :26 :44) // Not a variable of known type: cellValue
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CellGUI.cs" :26 :16) // matchController.CmdMakePlay(cellValue) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
// Skipping function SetPlayer(none), it contains poisonous unsupported syntaxes

