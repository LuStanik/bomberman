// Skipping function Awake(), it contains poisonous unsupported syntaxes

// Skipping function OnToggleClicked(), it contains poisonous unsupported syntaxes

func @_Mirror.Examples.MultipleMatch.MatchGUI.GetMatchId$$() -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchGUI.cs" :31 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchGUI.cs" :33 :19) // Not a variable of known type: matchId
return %0 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchGUI.cs" :33 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.Examples.MultipleMatch.MatchGUI.SetMatchInfo$Mirror.Examples.MultipleMatch.MatchInfo$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchGUI.cs" :36 :8) {
^entry (%_infos : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchGUI.cs" :36 :33)
cbde.store %_infos, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchGUI.cs" :36 :33)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchGUI.cs" :38 :22) // Not a variable of known type: infos
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchGUI.cs" :38 :22) // infos.matchId (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchGUI.cs" :39 :12) // Not a variable of known type: matchName
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchGUI.cs" :39 :12) // matchName.text (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchGUI.cs" :39 :29) // "Match " (StringLiteralExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchGUI.cs" :39 :40) // Not a variable of known type: infos
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchGUI.cs" :39 :40) // infos.matchId (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchGUI.cs" :39 :40) // infos.matchId.ToString() (InvocationExpression)
%9 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchGUI.cs" :39 :75)
%10 = constant 8 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchGUI.cs" :39 :78)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchGUI.cs" :39 :40) // infos.matchId.ToString().Substring(0, 8) (InvocationExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchGUI.cs" :39 :29) // Binary expression on unsupported types "Match " + infos.matchId.ToString().Substring(0, 8)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchGUI.cs" :40 :12) // Not a variable of known type: playerCount
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchGUI.cs" :40 :12) // playerCount.text (SimpleMemberAccessExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchGUI.cs" :40 :31) // Not a variable of known type: infos
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchGUI.cs" :40 :31) // infos.players (SimpleMemberAccessExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchGUI.cs" :40 :47) // " / " (StringLiteralExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchGUI.cs" :40 :31) // Binary expression on unsupported types infos.players + " / "
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchGUI.cs" :40 :55) // Not a variable of known type: infos
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchGUI.cs" :40 :55) // infos.maxPlayers (SimpleMemberAccessExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchGUI.cs" :40 :31) // Binary expression on unsupported types infos.players + " / " + infos.maxPlayers
br ^1

^1: // ExitBlock
return

}
