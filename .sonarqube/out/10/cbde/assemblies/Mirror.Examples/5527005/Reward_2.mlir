func @_Mirror.Examples.NetworkRoom.Reward.OnValidate$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :12 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :14 :16) // Not a variable of known type: randomColor
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :14 :31) // null (NullLiteralExpression)
%2 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :14 :16) // comparison of unknown type: randomColor == null
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :14 :16)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :15 :30) // GetComponent<RandomColor>() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.Examples.NetworkRoom.Reward.OnTriggerEnter$UnityEngine.Collider$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :18 :8) {
^entry (%_other : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :19 :28)
cbde.store %_other, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :19 :28)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :21 :16) // Not a variable of known type: other
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :21 :16) // other.gameObject (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :21 :44) // "Player" (StringLiteralExpression)
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :21 :16) // other.gameObject.CompareTag("Player") (InvocationExpression)
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :21 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ClaimPrize
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :23 :27) // Not a variable of known type: other
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :23 :27) // other.gameObject (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :23 :16) // ClaimPrize(other.gameObject) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.Examples.NetworkRoom.Reward.ClaimPrize$UnityEngine.GameObject$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :29 :8) {
^entry (%_player : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :29 :31)
cbde.store %_player, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :29 :31)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :31 :16) // Not a variable of known type: available
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :31 :16)

^1: // BinaryBranchBlock
%2 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :35 :28) // false
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :37 :32) // Not a variable of known type: randomColor
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :37 :32) // randomColor.color (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :41 :39) // Not a variable of known type: color
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :41 :39) // color.r (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :41 :51) // Not a variable of known type: color
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :41 :51) // color.g (SimpleMemberAccessExpression)
%10 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :41 :38) // Binary expression on unsupported types (color.r) + (color.g)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :41 :63) // Not a variable of known type: color
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :41 :63) // color.b (SimpleMemberAccessExpression)
%13 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :41 :38) // Binary expression on unsupported types (color.r) + (color.g) + (color.b)
%14 = constant 3 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :41 :75)
%15 = divis %13, %14 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :41 :37)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :41 :30) // (uint)(((color.r) + (color.g) + (color.b)) / 3) (CastExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :42 :20) // Not a variable of known type: logger
%19 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :42 :20) // logger.LogEnabled() (InvocationExpression)
cond_br %19, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :42 :20)

^3: // SimpleBlock
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :42 :41) // Not a variable of known type: logger
// Entity from another assembly: LogType
%21 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :42 :58) // LogType.Log (SimpleMemberAccessExpression)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :42 :71) // "Scored {0} points R:{1} G:{2} B:{3}" (StringLiteralExpression)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :42 :110) // Not a variable of known type: points
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :42 :118) // Not a variable of known type: color
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :42 :118) // color.r (SimpleMemberAccessExpression)
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :42 :127) // Not a variable of known type: color
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :42 :127) // color.g (SimpleMemberAccessExpression)
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :42 :136) // Not a variable of known type: color
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :42 :136) // color.b (SimpleMemberAccessExpression)
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :42 :41) // logger.LogFormat(LogType.Log, "Scored {0} points R:{1} G:{2} B:{3}", points, color.r, color.g, color.b) (InvocationExpression)
br ^4

^4: // SimpleBlock
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :45 :16) // Not a variable of known type: player
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :45 :16) // player.GetComponent<PlayerScore>() (InvocationExpression)
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :45 :16) // player.GetComponent<PlayerScore>().score (SimpleMemberAccessExpression)
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :45 :60) // Not a variable of known type: points
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :45 :16) // Binary expression on unsupported types player.GetComponent<PlayerScore>().score += points
// No identifier name for binary assignment expression
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Spawner
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :48 :16) // Spawner.SpawnReward() (InvocationExpression)
// Entity from another assembly: NetworkServer
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :51 :38) // Identifier from another assembly: gameObject
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\Reward.cs" :51 :16) // NetworkServer.Destroy(gameObject) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
