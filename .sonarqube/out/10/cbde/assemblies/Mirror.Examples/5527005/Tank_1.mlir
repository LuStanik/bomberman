func @_Mirror.Examples.Tanks.Tank.Update$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :19 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :22 :17) // Identifier from another assembly: isLocalPlayer
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :22 :16) // !isLocalPlayer (LogicalNotExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :22 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :22 :32)

^2: // BinaryBranchBlock
// Entity from another assembly: Input
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :25 :45) // "Horizontal" (StringLiteralExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :25 :31) // Input.GetAxis("Horizontal") (InvocationExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :26 :12) // Identifier from another assembly: transform
%6 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :26 :29)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :26 :32) // Not a variable of known type: horizontal
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :26 :45) // Not a variable of known type: rotationSpeed
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :26 :32) // Binary expression on unsupported types horizontal * rotationSpeed
// Entity from another assembly: Time
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :26 :61) // Time.deltaTime (SimpleMemberAccessExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :26 :32) // Binary expression on unsupported types horizontal * rotationSpeed * Time.deltaTime
%12 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :26 :77)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :26 :12) // transform.Rotate(0, horizontal * rotationSpeed * Time.deltaTime, 0) (InvocationExpression)
// Entity from another assembly: Input
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :29 :43) // "Vertical" (StringLiteralExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :29 :29) // Input.GetAxis("Vertical") (InvocationExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :30 :30) // Identifier from another assembly: transform
// Entity from another assembly: Vector3
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :30 :59) // Vector3.forward (SimpleMemberAccessExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :30 :30) // transform.TransformDirection(Vector3.forward) (InvocationExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :31 :12) // Not a variable of known type: agent
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :31 :12) // agent.velocity (SimpleMemberAccessExpression)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :31 :29) // Not a variable of known type: forward
// Entity from another assembly: Mathf
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :31 :49) // Not a variable of known type: vertical
%25 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :31 :59)
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :31 :39) // Mathf.Max(vertical, 0) (InvocationExpression)
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :31 :29) // Binary expression on unsupported types forward * Mathf.Max(vertical, 0)
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :31 :64) // Not a variable of known type: agent
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :31 :64) // agent.speed (SimpleMemberAccessExpression)
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :31 :29) // Binary expression on unsupported types forward * Mathf.Max(vertical, 0) * agent.speed
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :32 :12) // Not a variable of known type: animator
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :32 :29) // "Moving" (StringLiteralExpression)
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :32 :39) // Not a variable of known type: agent
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :32 :39) // agent.velocity (SimpleMemberAccessExpression)
// Entity from another assembly: Vector3
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :32 :57) // Vector3.zero (SimpleMemberAccessExpression)
%36 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :32 :39) // comparison of unknown type: agent.velocity != Vector3.zero
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :32 :12) // animator.SetBool("Moving", agent.velocity != Vector3.zero) (InvocationExpression)
// Entity from another assembly: Input
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :35 :33) // Not a variable of known type: shootKey
%39 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :35 :16) // Input.GetKeyDown(shootKey) (InvocationExpression)
cond_br %39, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :35 :16)

^3: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CmdFire
%40 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :37 :16) // CmdFire() (InvocationExpression)
br ^4

^4: // ExitBlock
return

}
func @_Mirror.Examples.Tanks.Tank.CmdFire$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :42 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: Instantiate
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :45 :48) // Not a variable of known type: projectilePrefab
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :45 :66) // Not a variable of known type: projectileMount
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :45 :66) // projectileMount.position (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :45 :92) // Identifier from another assembly: transform
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :45 :92) // transform.rotation (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :45 :36) // Instantiate(projectilePrefab, projectileMount.position, transform.rotation) (InvocationExpression)
// Entity from another assembly: NetworkServer
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :46 :32) // Not a variable of known type: projectile
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :46 :12) // NetworkServer.Spawn(projectile) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: RpcOnFire
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :47 :12) // RpcOnFire() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.Tanks.Tank.RpcOnFire$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :51 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :54 :12) // Not a variable of known type: animator
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :54 :32) // "Shoot" (StringLiteralExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Tank.cs" :54 :12) // animator.SetTrigger("Shoot") (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
