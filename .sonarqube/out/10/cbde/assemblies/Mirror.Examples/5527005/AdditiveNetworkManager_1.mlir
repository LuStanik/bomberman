func @_Mirror.Examples.Additive.AdditiveNetworkManager.OnStartServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\AdditiveNetworkManager.cs" :18 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\AdditiveNetworkManager.cs" :20 :12) // base (BaseExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\AdditiveNetworkManager.cs" :20 :12) // base.OnStartServer() (InvocationExpression)
// Entity from another assembly: StartCoroutine
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: LoadSubScenes
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\AdditiveNetworkManager.cs" :23 :27) // LoadSubScenes() (InvocationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\AdditiveNetworkManager.cs" :23 :12) // StartCoroutine(LoadSubScenes()) (InvocationExpression)
// Entity from another assembly: Instantiate
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\AdditiveNetworkManager.cs" :26 :24) // Not a variable of known type: Zone
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\AdditiveNetworkManager.cs" :26 :12) // Instantiate(Zone) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function LoadSubScenes(), it contains poisonous unsupported syntaxes

func @_Mirror.Examples.Additive.AdditiveNetworkManager.OnStopServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\AdditiveNetworkManager.cs" :40 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: StartCoroutine
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: UnloadScenes
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\AdditiveNetworkManager.cs" :42 :27) // UnloadScenes() (InvocationExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\AdditiveNetworkManager.cs" :42 :12) // StartCoroutine(UnloadScenes()) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.Additive.AdditiveNetworkManager.OnStopClient$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\AdditiveNetworkManager.cs" :45 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: StartCoroutine
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: UnloadScenes
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\AdditiveNetworkManager.cs" :47 :27) // UnloadScenes() (InvocationExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\AdditiveNetworkManager.cs" :47 :12) // StartCoroutine(UnloadScenes()) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function UnloadScenes(), it contains poisonous unsupported syntaxes

