func @_Mirror.Examples.Tanks.Projectile.OnStartServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Projectile.cs" :10 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: Invoke
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Projectile.cs" :12 :19) // nameof(DestroySelf) (InvocationExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Projectile.cs" :12 :40) // Not a variable of known type: destroyAfter
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Projectile.cs" :12 :12) // Invoke(nameof(DestroySelf), destroyAfter) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.Tanks.Projectile.Start$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Projectile.cs" :17 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Projectile.cs" :19 :12) // Not a variable of known type: rigidBody
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Projectile.cs" :19 :31) // Identifier from another assembly: transform
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Projectile.cs" :19 :31) // transform.forward (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Projectile.cs" :19 :51) // Not a variable of known type: force
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Projectile.cs" :19 :31) // Binary expression on unsupported types transform.forward * force
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Projectile.cs" :19 :12) // rigidBody.AddForce(transform.forward * force) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.Tanks.Projectile.DestroySelf$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Projectile.cs" :23 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: NetworkServer
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Projectile.cs" :26 :34) // Identifier from another assembly: gameObject
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Projectile.cs" :26 :12) // NetworkServer.Destroy(gameObject) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.Tanks.Projectile.OnTriggerEnter$UnityEngine.Collider$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Projectile.cs" :31 :8) {
^entry (%_co : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Projectile.cs" :32 :28)
cbde.store %_co, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Projectile.cs" :32 :28)
br ^0

^0: // SimpleBlock
// Entity from another assembly: NetworkServer
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Projectile.cs" :34 :34) // Identifier from another assembly: gameObject
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Tanks\\Scripts\\Projectile.cs" :34 :12) // NetworkServer.Destroy(gameObject) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
