func @_Mirror.Examples.MultipleAdditiveScenes.Spawner.InitialSpawn$UnityEngine.SceneManagement.Scene$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :7 :8) {
^entry (%_scene : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :7 :42)
cbde.store %_scene, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :7 :42)
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: NetworkServer
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :9 :17) // NetworkServer.active (SimpleMemberAccessExpression)
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :9 :16) // !NetworkServer.active (LogicalNotExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :9 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :9 :39)

^2: // ForInitializerBlock
%3 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :11 :25)
%4 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :11 :21) // i
cbde.store %3, %4 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :11 :21)
br ^3

^3: // BinaryBranchBlock
%5 = cbde.load %4 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :11 :28)
%6 = constant 10 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :11 :32)
%7 = cmpi "slt", %5, %6 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :11 :28)
cond_br %7, ^4, ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :11 :28)

^4: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SpawnReward
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :12 :28) // Not a variable of known type: scene
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :12 :16) // SpawnReward(scene) (InvocationExpression)
br ^6

^6: // SimpleBlock
%10 = cbde.load %4 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :11 :36)
%11 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :11 :36)
%12 = addi %10, %11 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :11 :36)
cbde.store %12, %4 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :11 :36)
br ^3

^5: // ExitBlock
return

}
func @_Mirror.Examples.MultipleAdditiveScenes.Spawner.SpawnReward$UnityEngine.SceneManagement.Scene$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :15 :8) {
^entry (%_scene : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :15 :41)
cbde.store %_scene, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :15 :41)
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: NetworkServer
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :17 :17) // NetworkServer.active (SimpleMemberAccessExpression)
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :17 :16) // !NetworkServer.active (LogicalNotExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :17 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :17 :39)

^2: // SimpleBlock
// Entity from another assembly: Random
%3 = constant 19 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :19 :62)
%4 = cbde.neg %3 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :19 :61)
%5 = constant 20 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :19 :66)
%6 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :19 :48) // Random.Range(-19, 20) (InvocationExpression)
%7 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :19 :71)
// Entity from another assembly: Random
%8 = constant 19 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :19 :88)
%9 = cbde.neg %8 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :19 :87)
%10 = constant 20 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :19 :92)
%11 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :19 :74) // Random.Range(-19, 20) (InvocationExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :19 :36) // new Vector3(Random.Range(-19, 20), 1, Random.Range(-19, 20)) (ObjectCreationExpression)
// Entity from another assembly: Instantiate
// Entity from another assembly: NetworkManager
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :20 :67) // NetworkManager.singleton (SimpleMemberAccessExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :20 :45) // (MultiSceneNetManager)NetworkManager.singleton (CastExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :20 :44) // ((MultiSceneNetManager)NetworkManager.singleton).rewardPrefab (SimpleMemberAccessExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :20 :107) // Not a variable of known type: spawnPosition
// Entity from another assembly: Quaternion
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :20 :122) // Quaternion.identity (SimpleMemberAccessExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :20 :32) // Instantiate(((MultiSceneNetManager)NetworkManager.singleton).rewardPrefab, spawnPosition, Quaternion.identity) (InvocationExpression)
// Entity from another assembly: SceneManager
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :21 :47) // Not a variable of known type: reward
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :21 :55) // Not a variable of known type: scene
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :21 :12) // SceneManager.MoveGameObjectToScene(reward, scene) (InvocationExpression)
// Entity from another assembly: NetworkServer
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :22 :32) // Not a variable of known type: reward
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\Spawner.cs" :22 :12) // NetworkServer.Spawn(reward) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
