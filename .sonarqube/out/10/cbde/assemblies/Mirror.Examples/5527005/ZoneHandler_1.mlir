func @_Mirror.Examples.Additive.ZoneHandler.OnTriggerEnter$UnityEngine.Collider$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :17 :8) {
^entry (%_other : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :17 :28)
cbde.store %_other, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :17 :28)
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: NetworkServer
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :19 :17) // NetworkServer.active (SimpleMemberAccessExpression)
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :19 :16) // !NetworkServer.active (LogicalNotExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :19 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :19 :39)

^2: // BinaryBranchBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :21 :16) // Not a variable of known type: logger
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :21 :16) // logger.LogEnabled() (InvocationExpression)
cond_br %4, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :21 :16)

^3: // SimpleBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :21 :37) // Not a variable of known type: logger
// Entity from another assembly: LogType
%6 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :21 :54) // LogType.Log (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :21 :67) // "Loading {0}" (StringLiteralExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :21 :82) // Not a variable of known type: subScene
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :21 :37) // logger.LogFormat(LogType.Log, "Loading {0}", subScene) (InvocationExpression)
br ^4

^4: // SimpleBlock
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :23 :46) // Not a variable of known type: other
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :23 :46) // other.gameObject (SimpleMemberAccessExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :23 :46) // other.gameObject.GetComponent<NetworkIdentity>() (InvocationExpression)
// Entity from another assembly: NetworkServer
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :24 :47) // Not a variable of known type: networkIdentity
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :24 :64) // new SceneMessage { sceneName = subScene, sceneOperation = SceneOperation.LoadAdditive } (ObjectCreationExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :24 :95) // Not a variable of known type: subScene
// Entity from another assembly: SceneOperation
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :24 :122) // SceneOperation.LoadAdditive (SimpleMemberAccessExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :24 :12) // NetworkServer.SendToClientOfPlayer(networkIdentity, new SceneMessage { sceneName = subScene, sceneOperation = SceneOperation.LoadAdditive }) (InvocationExpression)
br ^5

^5: // ExitBlock
return

}
func @_Mirror.Examples.Additive.ZoneHandler.OnTriggerExit$UnityEngine.Collider$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :27 :8) {
^entry (%_other : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :27 :27)
cbde.store %_other, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :27 :27)
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: NetworkServer
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :29 :17) // NetworkServer.active (SimpleMemberAccessExpression)
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :29 :16) // !NetworkServer.active (LogicalNotExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :29 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :29 :39)

^2: // BinaryBranchBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :31 :16) // Not a variable of known type: logger
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :31 :16) // logger.LogEnabled() (InvocationExpression)
cond_br %4, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :31 :16)

^3: // SimpleBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :31 :37) // Not a variable of known type: logger
// Entity from another assembly: LogType
%6 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :31 :54) // LogType.Log (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :31 :67) // "Unloading {0}" (StringLiteralExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :31 :84) // Not a variable of known type: subScene
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :31 :37) // logger.LogFormat(LogType.Log, "Unloading {0}", subScene) (InvocationExpression)
br ^4

^4: // SimpleBlock
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :33 :46) // Not a variable of known type: other
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :33 :46) // other.gameObject (SimpleMemberAccessExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :33 :46) // other.gameObject.GetComponent<NetworkIdentity>() (InvocationExpression)
// Entity from another assembly: NetworkServer
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :34 :47) // Not a variable of known type: networkIdentity
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :34 :64) // new SceneMessage { sceneName = subScene, sceneOperation = SceneOperation.UnloadAdditive } (ObjectCreationExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :34 :95) // Not a variable of known type: subScene
// Entity from another assembly: SceneOperation
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :34 :122) // SceneOperation.UnloadAdditive (SimpleMemberAccessExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\ZoneHandler.cs" :34 :12) // NetworkServer.SendToClientOfPlayer(networkIdentity, new SceneMessage { sceneName = subScene, sceneOperation = SceneOperation.UnloadAdditive }) (InvocationExpression)
br ^5

^5: // ExitBlock
return

}
