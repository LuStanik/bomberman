func @_Mirror.Examples.Chat.ChatNetworkManager.SetHostname$string$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :12 :8) {
^entry (%_hostname : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :12 :32)
cbde.store %_hostname, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :12 :32)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :14 :29) // Not a variable of known type: hostname
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.Chat.ChatNetworkManager.OnStartServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :22 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :24 :12) // base (BaseExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :24 :12) // base.OnStartServer() (InvocationExpression)
// Entity from another assembly: NetworkServer
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnCreatePlayer
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :25 :12) // NetworkServer.RegisterHandler<CreatePlayerMessage>(OnCreatePlayer) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.Chat.ChatNetworkManager.OnClientConnect$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :28 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :28 :45)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :28 :45)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :30 :12) // base (BaseExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :30 :33) // Not a variable of known type: conn
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :30 :12) // base.OnClientConnect(conn) (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :33 :12) // Not a variable of known type: conn
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :33 :22) // new CreatePlayerMessage { name = PlayerName } (ObjectCreationExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :33 :55) // Not a variable of known type: PlayerName
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :33 :12) // conn.Send(new CreatePlayerMessage { name = PlayerName }) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.Chat.ChatNetworkManager.OnCreatePlayer$Mirror.NetworkConnection.Mirror.Examples.Chat.ChatNetworkManager.CreatePlayerMessage$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :36 :8) {
^entry (%_connection : none, %_createPlayerMessage : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :36 :28)
cbde.store %_connection, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :36 :28)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :36 :58)
cbde.store %_createPlayerMessage, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :36 :58)
br ^0

^0: // SimpleBlock
// Entity from another assembly: Instantiate
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :39 :46) // Identifier from another assembly: playerPrefab
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :39 :34) // Instantiate(playerPrefab) (InvocationExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :40 :12) // Not a variable of known type: playergo
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :40 :12) // playergo.GetComponent<Player>() (InvocationExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :40 :12) // playergo.GetComponent<Player>().playerName (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :40 :57) // Not a variable of known type: createPlayerMessage
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :40 :57) // createPlayerMessage.name (SimpleMemberAccessExpression)
// Entity from another assembly: NetworkServer
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :43 :49) // Not a variable of known type: connection
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :43 :61) // Not a variable of known type: playergo
%12 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :43 :12) // NetworkServer.AddPlayerForConnection(connection, playergo) (InvocationExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :45 :12) // Not a variable of known type: chatWindow
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :45 :12) // chatWindow.gameObject (SimpleMemberAccessExpression)
%15 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :45 :44) // true
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatNetworkManager.cs" :45 :12) // chatWindow.gameObject.SetActive(true) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
