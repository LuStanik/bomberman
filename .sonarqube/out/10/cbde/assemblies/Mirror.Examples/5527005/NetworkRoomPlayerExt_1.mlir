func @_Mirror.Examples.NetworkRoom.NetworkRoomPlayerExt.OnStartClient$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :10 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :12 :16) // Not a variable of known type: logger
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :12 :16) // logger.LogEnabled() (InvocationExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :12 :16)

^1: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :12 :37) // Not a variable of known type: logger
// Entity from another assembly: LogType
%3 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :12 :54) // LogType.Log (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :12 :67) // "OnStartClient {0}" (StringLiteralExpression)
// Entity from another assembly: SceneManager
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :12 :88) // SceneManager.GetActiveScene() (InvocationExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :12 :88) // SceneManager.GetActiveScene().path (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :12 :37) // logger.LogFormat(LogType.Log, "OnStartClient {0}", SceneManager.GetActiveScene().path) (InvocationExpression)
br ^2

^2: // SimpleBlock
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :14 :12) // base (BaseExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :14 :12) // base.OnStartClient() (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Examples.NetworkRoom.NetworkRoomPlayerExt.OnClientEnterRoom$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :17 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :19 :16) // Not a variable of known type: logger
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :19 :16) // logger.LogEnabled() (InvocationExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :19 :16)

^1: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :19 :37) // Not a variable of known type: logger
// Entity from another assembly: LogType
%3 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :19 :54) // LogType.Log (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :19 :67) // "OnClientEnterRoom {0}" (StringLiteralExpression)
// Entity from another assembly: SceneManager
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :19 :92) // SceneManager.GetActiveScene() (InvocationExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :19 :92) // SceneManager.GetActiveScene().path (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :19 :37) // logger.LogFormat(LogType.Log, "OnClientEnterRoom {0}", SceneManager.GetActiveScene().path) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.Examples.NetworkRoom.NetworkRoomPlayerExt.OnClientExitRoom$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :22 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :24 :16) // Not a variable of known type: logger
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :24 :16) // logger.LogEnabled() (InvocationExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :24 :16)

^1: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :24 :37) // Not a variable of known type: logger
// Entity from another assembly: LogType
%3 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :24 :54) // LogType.Log (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :24 :67) // "OnClientExitRoom {0}" (StringLiteralExpression)
// Entity from another assembly: SceneManager
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :24 :91) // SceneManager.GetActiveScene() (InvocationExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :24 :91) // SceneManager.GetActiveScene().path (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :24 :37) // logger.LogFormat(LogType.Log, "OnClientExitRoom {0}", SceneManager.GetActiveScene().path) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.Examples.NetworkRoom.NetworkRoomPlayerExt.ReadyStateChanged$bool.bool$(i1, i1) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :27 :8) {
^entry (%__ : i1, %_newReadyState : i1):
%0 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :27 :47)
cbde.store %__, %0 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :27 :47)
%1 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :27 :55)
cbde.store %_newReadyState, %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :27 :55)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :29 :16) // Not a variable of known type: logger
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :29 :16) // logger.LogEnabled() (InvocationExpression)
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :29 :16)

^1: // SimpleBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :29 :37) // Not a variable of known type: logger
// Entity from another assembly: LogType
%5 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :29 :54) // LogType.Log (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :29 :67) // "ReadyStateChanged {0}" (StringLiteralExpression)
%7 = cbde.load %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :29 :92)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Room\\Scripts\\NetworkRoomPlayerExt.cs" :29 :37) // logger.LogFormat(LogType.Log, "ReadyStateChanged {0}", newReadyState) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
