func @_Mirror.Examples.MultipleAdditiveScenes.PlayerController.OnValidate$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PlayerController.cs" :13 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PlayerController.cs" :15 :16) // Not a variable of known type: characterController
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PlayerController.cs" :15 :39) // null (NullLiteralExpression)
%2 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PlayerController.cs" :15 :16) // comparison of unknown type: characterController == null
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PlayerController.cs" :15 :16)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PlayerController.cs" :16 :38) // GetComponent<CharacterController>() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.Examples.MultipleAdditiveScenes.PlayerController.Start$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PlayerController.cs" :19 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PlayerController.cs" :21 :12) // Not a variable of known type: characterController
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PlayerController.cs" :21 :12) // characterController.enabled (SimpleMemberAccessExpression)
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PlayerController.cs" :21 :42) // Identifier from another assembly: isLocalPlayer
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.MultipleAdditiveScenes.PlayerController.OnStartLocalPlayer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PlayerController.cs" :24 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: Camera
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PlayerController.cs" :26 :12) // Camera.main (SimpleMemberAccessExpression)
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PlayerController.cs" :26 :12) // Camera.main.orthographic (SimpleMemberAccessExpression)
%2 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PlayerController.cs" :26 :39) // false
// Entity from another assembly: Camera
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PlayerController.cs" :27 :12) // Camera.main (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PlayerController.cs" :27 :12) // Camera.main.transform (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PlayerController.cs" :27 :44) // Identifier from another assembly: transform
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PlayerController.cs" :27 :12) // Camera.main.transform.SetParent(transform) (InvocationExpression)
// Entity from another assembly: Camera
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PlayerController.cs" :28 :12) // Camera.main (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PlayerController.cs" :28 :12) // Camera.main.transform (SimpleMemberAccessExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PlayerController.cs" :28 :12) // Camera.main.transform.localPosition (SimpleMemberAccessExpression)
%10 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PlayerController.cs" :28 :62) // 0f (NumericLiteralExpression)
%11 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PlayerController.cs" :28 :66) // 3f (NumericLiteralExpression)
%12 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PlayerController.cs" :28 :71) // 8f (NumericLiteralExpression)
%13 = cbde.neg %12 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PlayerController.cs" :28 :70)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PlayerController.cs" :28 :50) // new Vector3(0f, 3f, -8f) (ObjectCreationExpression)
// Entity from another assembly: Camera
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PlayerController.cs" :29 :12) // Camera.main (SimpleMemberAccessExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PlayerController.cs" :29 :12) // Camera.main.transform (SimpleMemberAccessExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PlayerController.cs" :29 :12) // Camera.main.transform.localEulerAngles (SimpleMemberAccessExpression)
%18 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PlayerController.cs" :29 :65) // 10f (NumericLiteralExpression)
%19 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PlayerController.cs" :29 :70) // 0f (NumericLiteralExpression)
%20 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PlayerController.cs" :29 :74) // 0f (NumericLiteralExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\PlayerController.cs" :29 :53) // new Vector3(10f, 0f, 0f) (ObjectCreationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function OnDisable(), it contains poisonous unsupported syntaxes

// Skipping function Update(), it contains poisonous unsupported syntaxes

// Skipping function FixedUpdate(), it contains poisonous unsupported syntaxes

