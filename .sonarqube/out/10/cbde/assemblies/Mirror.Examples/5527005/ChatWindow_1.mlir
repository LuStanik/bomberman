func @_Mirror.Examples.Chat.ChatWindow.Awake$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatWindow.cs" :14 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Player
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatWindow.cs" :16 :12) // Player.OnMessage (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnPlayerMessage
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatWindow.cs" :16 :12) // Binary expression on unsupported types Player.OnMessage += OnPlayerMessage
// No identifier name for binary assignment expression
br ^1

^1: // ExitBlock
return

}
// Skipping function OnPlayerMessage(none, none), it contains poisonous unsupported syntaxes

func @_Mirror.Examples.Chat.ChatWindow.OnSend$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatWindow.cs" :29 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatWindow.cs" :31 :16) // Not a variable of known type: chatMessage
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatWindow.cs" :31 :16) // chatMessage.text (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatWindow.cs" :31 :16) // chatMessage.text.Trim() (InvocationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatWindow.cs" :31 :43) // "" (StringLiteralExpression)
%4 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatWindow.cs" :31 :16) // comparison of unknown type: chatMessage.text.Trim() == ""
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatWindow.cs" :31 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatWindow.cs" :32 :16)

^2: // SimpleBlock
// Entity from another assembly: NetworkClient
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatWindow.cs" :35 :28) // NetworkClient.connection (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatWindow.cs" :35 :28) // NetworkClient.connection.identity (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatWindow.cs" :35 :28) // NetworkClient.connection.identity.GetComponent<Player>() (InvocationExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatWindow.cs" :38 :12) // Not a variable of known type: player
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatWindow.cs" :38 :27) // Not a variable of known type: chatMessage
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatWindow.cs" :38 :27) // chatMessage.text (SimpleMemberAccessExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatWindow.cs" :38 :27) // chatMessage.text.Trim() (InvocationExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatWindow.cs" :38 :12) // player.CmdSend(chatMessage.text.Trim()) (InvocationExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatWindow.cs" :40 :12) // Not a variable of known type: chatMessage
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatWindow.cs" :40 :12) // chatMessage.text (SimpleMemberAccessExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatWindow.cs" :40 :31) // "" (StringLiteralExpression)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Examples.Chat.ChatWindow.AppendMessage$string$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatWindow.cs" :43 :8) {
^entry (%_message : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatWindow.cs" :43 :36)
cbde.store %_message, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatWindow.cs" :43 :36)
br ^0

^0: // SimpleBlock
// Entity from another assembly: StartCoroutine
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: AppendAndScroll
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatWindow.cs" :45 :43) // Not a variable of known type: message
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatWindow.cs" :45 :27) // AppendAndScroll(message) (InvocationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Chat\\Scripts\\ChatWindow.cs" :45 :12) // StartCoroutine(AppendAndScroll(message)) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function AppendAndScroll(none), it contains poisonous unsupported syntaxes

