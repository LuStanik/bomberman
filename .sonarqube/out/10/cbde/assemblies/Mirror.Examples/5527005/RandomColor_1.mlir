func @_Mirror.Examples.Additive.RandomColor.OnStartServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\RandomColor.cs" :6 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\RandomColor.cs" :8 :12) // base (BaseExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\RandomColor.cs" :8 :12) // base.OnStartServer() (InvocationExpression)
// Entity from another assembly: Random
%2 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\RandomColor.cs" :9 :36) // 0f (NumericLiteralExpression)
%3 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\RandomColor.cs" :9 :40) // 1f (NumericLiteralExpression)
%4 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\RandomColor.cs" :9 :44) // 1f (NumericLiteralExpression)
%5 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\RandomColor.cs" :9 :48) // 1f (NumericLiteralExpression)
%6 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\RandomColor.cs" :9 :52) // 0.5f (NumericLiteralExpression)
%7 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\RandomColor.cs" :9 :58) // 1f (NumericLiteralExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\RandomColor.cs" :9 :20) // Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.Additive.RandomColor.SetColor$UnityEngine.Color32.UnityEngine.Color32$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\RandomColor.cs" :20 :8) {
^entry (%__ : none, %_newColor : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\RandomColor.cs" :20 :22)
cbde.store %__, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\RandomColor.cs" :20 :22)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\RandomColor.cs" :20 :33)
cbde.store %_newColor, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\RandomColor.cs" :20 :33)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\RandomColor.cs" :22 :16) // Not a variable of known type: cachedMaterial
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\RandomColor.cs" :22 :34) // null (NullLiteralExpression)
%4 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\RandomColor.cs" :22 :16) // comparison of unknown type: cachedMaterial == null
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\RandomColor.cs" :22 :16)

^1: // SimpleBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\RandomColor.cs" :22 :57) // GetComponentInChildren<Renderer>() (InvocationExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\RandomColor.cs" :22 :57) // GetComponentInChildren<Renderer>().material (SimpleMemberAccessExpression)
br ^2

^2: // SimpleBlock
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\RandomColor.cs" :23 :12) // Not a variable of known type: cachedMaterial
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\RandomColor.cs" :23 :12) // cachedMaterial.color (SimpleMemberAccessExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\RandomColor.cs" :23 :35) // Not a variable of known type: newColor
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Examples.Additive.RandomColor.OnDestroy$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\RandomColor.cs" :26 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: Destroy
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\RandomColor.cs" :28 :20) // Not a variable of known type: cachedMaterial
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\AdditiveScenes\\Scripts\\RandomColor.cs" :28 :12) // Destroy(cachedMaterial) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
