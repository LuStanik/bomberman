func @_Mirror.Examples.Benchmark.BenchmarkNetworkManager.SpawnAll$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :11 :8) {
^entry :
br ^0

^0: // ForInitializerBlock
// Entity from another assembly: Mathf
%0 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :14 :36) // Not a variable of known type: spawnAmount
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :14 :25) // Mathf.Sqrt(spawnAmount) (InvocationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :18 :28) // Not a variable of known type: sqrt
%4 = cbde.neg %3 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :18 :27)
%5 = constant 2 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :18 :35)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :18 :27) // Binary expression on unsupported types -sqrt / 2
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :18 :39) // Not a variable of known type: interleave
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :18 :27) // Binary expression on unsupported types -sqrt / 2 * interleave
%10 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :21 :26)
%11 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :21 :16) // spawned
cbde.store %10, %11 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :21 :16)
%12 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :22 :30)
%13 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :22 :21) // spawnX
cbde.store %12, %13 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :22 :21)
br ^1

^1: // BinaryBranchBlock
%14 = cbde.load %13 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :22 :33)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :22 :42) // Not a variable of known type: sqrt
%16 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :22 :33) // comparison of unknown type: spawnX < sqrt
cond_br %16, ^2, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :22 :33)

^2: // ForInitializerBlock
%17 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :24 :34)
%18 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :24 :25) // spawnZ
cbde.store %17, %18 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :24 :25)
br ^4

^4: // BinaryBranchBlock
%19 = cbde.load %18 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :24 :37)
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :24 :46) // Not a variable of known type: sqrt
%21 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :24 :37) // comparison of unknown type: spawnZ < sqrt
cond_br %21, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :24 :37)

^5: // BinaryBranchBlock
%22 = cbde.load %11 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :28 :24)
%23 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :28 :34) // Not a variable of known type: spawnAmount
%24 = cmpi "slt", %22, %23 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :28 :24)
cond_br %24, ^7, ^8 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :28 :24)

^7: // SimpleBlock
// Entity from another assembly: Instantiate
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :31 :52) // Not a variable of known type: spawnPrefab
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :31 :40) // Instantiate(spawnPrefab) (InvocationExpression)
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :32 :34) // Not a variable of known type: offset
%29 = cbde.load %13 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :32 :43)
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :32 :52) // Not a variable of known type: interleave
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :32 :43) // Binary expression on unsupported types spawnX * interleave
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :32 :34) // Binary expression on unsupported types offset + spawnX * interleave
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :33 :34) // Not a variable of known type: offset
%35 = cbde.load %18 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :33 :43)
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :33 :52) // Not a variable of known type: interleave
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :33 :43) // Binary expression on unsupported types spawnZ * interleave
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :33 :34) // Binary expression on unsupported types offset + spawnZ * interleave
%40 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :34 :24) // Not a variable of known type: go
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :34 :24) // go.transform (SimpleMemberAccessExpression)
%42 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :34 :24) // go.transform.position (SimpleMemberAccessExpression)
%43 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :34 :60) // Not a variable of known type: x
%44 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :34 :63)
%45 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :34 :66) // Not a variable of known type: z
%46 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :34 :48) // new Vector3(x, 0, z) (ObjectCreationExpression)
// Entity from another assembly: NetworkServer
%47 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :37 :44) // Not a variable of known type: go
%48 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :37 :24) // NetworkServer.Spawn(go) (InvocationExpression)
%49 = cbde.load %11 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :38 :26)
%50 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :38 :24)
%51 = addi %49, %50 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :38 :24)
cbde.store %51, %11 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :38 :24)
br ^8

^8: // SimpleBlock
%52 = cbde.load %18 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :24 :54)
%53 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :24 :52)
%54 = addi %52, %53 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :24 :52)
cbde.store %54, %18 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :24 :52)
br ^4

^6: // SimpleBlock
%55 = cbde.load %13 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :22 :50)
%56 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :22 :48)
%57 = addi %55, %56 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :22 :48)
cbde.store %57, %13 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :22 :48)
br ^1

^3: // ExitBlock
return

}
func @_Mirror.Examples.Benchmark.BenchmarkNetworkManager.OnStartServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :44 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :46 :12) // base (BaseExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :46 :12) // base.OnStartServer() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SpawnAll
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Benchmark\\Scripts\\BenchmarkNetworkManager.cs" :47 :12) // SpawnAll() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
