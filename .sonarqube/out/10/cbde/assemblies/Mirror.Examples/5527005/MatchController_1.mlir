func @_Mirror.Examples.MultipleMatch.MatchController.Awake$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :34 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :36 :31) // FindObjectOfType<CanvasController>() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.MultipleMatch.MatchController.OnStartServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :39 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: StartCoroutine
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: AddPlayersToMatchController
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :41 :27) // AddPlayersToMatchController() (InvocationExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :41 :12) // StartCoroutine(AddPlayersToMatchController()) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function AddPlayersToMatchController(), it contains poisonous unsupported syntaxes

func @_Mirror.Examples.MultipleMatch.MatchController.OnStartClient$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :55 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :57 :12) // Not a variable of known type: matchPlayerData
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :57 :12) // matchPlayerData.Callback (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: UpdateWins
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :57 :12) // Binary expression on unsupported types matchPlayerData.Callback += UpdateWins
// No identifier name for binary assignment expression
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :59 :12) // Not a variable of known type: canvasGroup
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :59 :12) // canvasGroup.alpha (SimpleMemberAccessExpression)
%5 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :59 :32) // 1f (NumericLiteralExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :60 :12) // Not a variable of known type: canvasGroup
%7 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :60 :12) // canvasGroup.interactable (SimpleMemberAccessExpression)
%8 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :60 :39) // true
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :61 :12) // Not a variable of known type: canvasGroup
%10 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :61 :12) // canvasGroup.blocksRaycasts (SimpleMemberAccessExpression)
%11 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :61 :41) // true
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :63 :12) // Not a variable of known type: exitButton
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :63 :12) // exitButton.gameObject (SimpleMemberAccessExpression)
%14 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :63 :44) // false
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :63 :12) // exitButton.gameObject.SetActive(false) (InvocationExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :64 :12) // Not a variable of known type: playAgainButton
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :64 :12) // playAgainButton.gameObject (SimpleMemberAccessExpression)
%18 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :64 :49) // false
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :64 :12) // playAgainButton.gameObject.SetActive(false) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.MultipleMatch.MatchController.UpdateGameUI$Mirror.NetworkIdentity.Mirror.NetworkIdentity$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :67 :8) {
^entry (%__ : none, %_newPlayerTurn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :67 :33)
cbde.store %__, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :67 :33)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :67 :52)
cbde.store %_newPlayerTurn, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :67 :52)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :69 :17) // Not a variable of known type: newPlayerTurn
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :69 :16) // !newPlayerTurn (LogicalNotExpression)
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :69 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :69 :32)

^2: // BinaryBranchBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :71 :16) // Not a variable of known type: newPlayerTurn
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :71 :16) // newPlayerTurn.gameObject (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :71 :16) // newPlayerTurn.gameObject.GetComponent<NetworkIdentity>() (InvocationExpression)
%7 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :71 :16) // newPlayerTurn.gameObject.GetComponent<NetworkIdentity>().isLocalPlayer (SimpleMemberAccessExpression)
cond_br %7, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :71 :16)

^3: // SimpleBlock
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :73 :16) // Not a variable of known type: gameText
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :73 :16) // gameText.text (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :73 :32) // "Your Turn" (StringLiteralExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :74 :16) // Not a variable of known type: gameText
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :74 :16) // gameText.color (SimpleMemberAccessExpression)
// Entity from another assembly: Color
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :74 :33) // Color.blue (SimpleMemberAccessExpression)
br ^5

^4: // SimpleBlock
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :78 :16) // Not a variable of known type: gameText
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :78 :16) // gameText.text (SimpleMemberAccessExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :78 :32) // "Their Turn" (StringLiteralExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :79 :16) // Not a variable of known type: gameText
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :79 :16) // gameText.color (SimpleMemberAccessExpression)
// Entity from another assembly: Color
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :79 :33) // Color.red (SimpleMemberAccessExpression)
br ^5

^5: // ExitBlock
return

}
func @_Mirror.Examples.MultipleMatch.MatchController.UpdateWins$Mirror.SyncIDictionary$Mirror.NetworkIdentity.Mirror.Examples.MultipleMatch.MatchPlayerData$.Operation.Mirror.NetworkIdentity.Mirror.Examples.MultipleMatch.MatchPlayerData$(none, none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :83 :8) {
^entry (%_op : none, %_key : none, %_matchPlayerData : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :83 :31)
cbde.store %_op, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :83 :31)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :83 :94)
cbde.store %_key, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :83 :94)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :83 :115)
cbde.store %_matchPlayerData, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :83 :115)
br ^0

^0: // BinaryBranchBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :85 :16) // Not a variable of known type: key
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :85 :16) // key.gameObject (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :85 :16) // key.gameObject.GetComponent<NetworkIdentity>() (InvocationExpression)
%6 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :85 :16) // key.gameObject.GetComponent<NetworkIdentity>().isLocalPlayer (SimpleMemberAccessExpression)
cond_br %6, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :85 :16)

^1: // SimpleBlock
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :87 :16) // Not a variable of known type: winCountLocal
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :87 :16) // winCountLocal.text (SimpleMemberAccessExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :87 :47) // Not a variable of known type: matchPlayerData
%10 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :87 :47) // matchPlayerData.playerIndex (SimpleMemberAccessExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :87 :78) // Not a variable of known type: matchPlayerData
%12 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :87 :78) // matchPlayerData.wins (SimpleMemberAccessExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :87 :37) // $"Player {matchPlayerData.playerIndex}\n{matchPlayerData.wins}" (InterpolatedStringExpression)
br ^3

^2: // SimpleBlock
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :91 :16) // Not a variable of known type: winCountOpponent
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :91 :16) // winCountOpponent.text (SimpleMemberAccessExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :91 :50) // Not a variable of known type: matchPlayerData
%17 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :91 :50) // matchPlayerData.playerIndex (SimpleMemberAccessExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :91 :81) // Not a variable of known type: matchPlayerData
%19 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :91 :81) // matchPlayerData.wins (SimpleMemberAccessExpression)
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :91 :40) // $"Player {matchPlayerData.playerIndex}\n{matchPlayerData.wins}" (InterpolatedStringExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function CmdMakePlay(none, none), it contains poisonous unsupported syntaxes

func @_Mirror.Examples.MultipleMatch.MatchController.CheckWinner$Mirror.Examples.MultipleMatch.CellValue$(none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :131 :8) {
^entry (%_currentScore : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :131 :25)
cbde.store %_currentScore, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :131 :25)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :133 :17) // Not a variable of known type: currentScore
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :133 :32) // Not a variable of known type: CellValue
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :133 :32) // CellValue.TopRow (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :133 :17) // Binary expression on unsupported types currentScore & CellValue.TopRow
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :133 :53) // Not a variable of known type: CellValue
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :133 :53) // CellValue.TopRow (SimpleMemberAccessExpression)
%7 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :133 :16) // comparison of unknown type: (currentScore & CellValue.TopRow) == CellValue.TopRow
cond_br %7, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :133 :16)

^1: // JumpBlock
%8 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :134 :23) // true
return %8 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :134 :16)

^2: // BinaryBranchBlock
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :135 :17) // Not a variable of known type: currentScore
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :135 :32) // Not a variable of known type: CellValue
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :135 :32) // CellValue.MidRow (SimpleMemberAccessExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :135 :17) // Binary expression on unsupported types currentScore & CellValue.MidRow
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :135 :53) // Not a variable of known type: CellValue
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :135 :53) // CellValue.MidRow (SimpleMemberAccessExpression)
%15 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :135 :16) // comparison of unknown type: (currentScore & CellValue.MidRow) == CellValue.MidRow
cond_br %15, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :135 :16)

^3: // JumpBlock
%16 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :136 :23) // true
return %16 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :136 :16)

^4: // BinaryBranchBlock
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :137 :17) // Not a variable of known type: currentScore
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :137 :32) // Not a variable of known type: CellValue
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :137 :32) // CellValue.BotRow (SimpleMemberAccessExpression)
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :137 :17) // Binary expression on unsupported types currentScore & CellValue.BotRow
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :137 :53) // Not a variable of known type: CellValue
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :137 :53) // CellValue.BotRow (SimpleMemberAccessExpression)
%23 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :137 :16) // comparison of unknown type: (currentScore & CellValue.BotRow) == CellValue.BotRow
cond_br %23, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :137 :16)

^5: // JumpBlock
%24 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :138 :23) // true
return %24 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :138 :16)

^6: // BinaryBranchBlock
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :139 :17) // Not a variable of known type: currentScore
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :139 :32) // Not a variable of known type: CellValue
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :139 :32) // CellValue.LeftCol (SimpleMemberAccessExpression)
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :139 :17) // Binary expression on unsupported types currentScore & CellValue.LeftCol
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :139 :54) // Not a variable of known type: CellValue
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :139 :54) // CellValue.LeftCol (SimpleMemberAccessExpression)
%31 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :139 :16) // comparison of unknown type: (currentScore & CellValue.LeftCol) == CellValue.LeftCol
cond_br %31, ^7, ^8 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :139 :16)

^7: // JumpBlock
%32 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :140 :23) // true
return %32 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :140 :16)

^8: // BinaryBranchBlock
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :141 :17) // Not a variable of known type: currentScore
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :141 :32) // Not a variable of known type: CellValue
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :141 :32) // CellValue.MidCol (SimpleMemberAccessExpression)
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :141 :17) // Binary expression on unsupported types currentScore & CellValue.MidCol
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :141 :53) // Not a variable of known type: CellValue
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :141 :53) // CellValue.MidCol (SimpleMemberAccessExpression)
%39 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :141 :16) // comparison of unknown type: (currentScore & CellValue.MidCol) == CellValue.MidCol
cond_br %39, ^9, ^10 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :141 :16)

^9: // JumpBlock
%40 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :142 :23) // true
return %40 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :142 :16)

^10: // BinaryBranchBlock
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :143 :17) // Not a variable of known type: currentScore
%42 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :143 :32) // Not a variable of known type: CellValue
%43 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :143 :32) // CellValue.RightCol (SimpleMemberAccessExpression)
%44 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :143 :17) // Binary expression on unsupported types currentScore & CellValue.RightCol
%45 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :143 :55) // Not a variable of known type: CellValue
%46 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :143 :55) // CellValue.RightCol (SimpleMemberAccessExpression)
%47 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :143 :16) // comparison of unknown type: (currentScore & CellValue.RightCol) == CellValue.RightCol
cond_br %47, ^11, ^12 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :143 :16)

^11: // JumpBlock
%48 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :144 :23) // true
return %48 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :144 :16)

^12: // BinaryBranchBlock
%49 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :145 :17) // Not a variable of known type: currentScore
%50 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :145 :32) // Not a variable of known type: CellValue
%51 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :145 :32) // CellValue.Diag1 (SimpleMemberAccessExpression)
%52 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :145 :17) // Binary expression on unsupported types currentScore & CellValue.Diag1
%53 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :145 :52) // Not a variable of known type: CellValue
%54 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :145 :52) // CellValue.Diag1 (SimpleMemberAccessExpression)
%55 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :145 :16) // comparison of unknown type: (currentScore & CellValue.Diag1) == CellValue.Diag1
cond_br %55, ^13, ^14 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :145 :16)

^13: // JumpBlock
%56 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :146 :23) // true
return %56 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :146 :16)

^14: // BinaryBranchBlock
%57 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :147 :17) // Not a variable of known type: currentScore
%58 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :147 :32) // Not a variable of known type: CellValue
%59 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :147 :32) // CellValue.Diag2 (SimpleMemberAccessExpression)
%60 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :147 :17) // Binary expression on unsupported types currentScore & CellValue.Diag2
%61 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :147 :52) // Not a variable of known type: CellValue
%62 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :147 :52) // CellValue.Diag2 (SimpleMemberAccessExpression)
%63 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :147 :16) // comparison of unknown type: (currentScore & CellValue.Diag2) == CellValue.Diag2
cond_br %63, ^15, ^16 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :147 :16)

^15: // JumpBlock
%64 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :148 :23) // true
return %64 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :148 :16)

^16: // JumpBlock
%65 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :150 :19) // false
return %65 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :150 :12)

^17: // ExitBlock
cbde.unreachable

}
func @_Mirror.Examples.MultipleMatch.MatchController.RpcUpdateCell$Mirror.Examples.MultipleMatch.CellValue.Mirror.NetworkIdentity$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :153 :8) {
^entry (%_cellValue : none, %_player : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :154 :34)
cbde.store %_cellValue, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :154 :34)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :154 :55)
cbde.store %_player, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :154 :55)
br ^0

^0: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :156 :12) // Not a variable of known type: MatchCells
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :156 :23) // Not a variable of known type: cellValue
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :156 :12) // MatchCells[cellValue] (ElementAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :156 :44) // Not a variable of known type: player
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :156 :12) // MatchCells[cellValue].SetPlayer(player) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function RpcShowWinner(none), it contains poisonous unsupported syntaxes

func @_Mirror.Examples.MultipleMatch.MatchController.RequestPlayAgain$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :186 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :189 :12) // Not a variable of known type: playAgainButton
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :189 :12) // playAgainButton.gameObject (SimpleMemberAccessExpression)
%2 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :189 :49) // false
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :189 :12) // playAgainButton.gameObject.SetActive(false) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CmdPlayAgain
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :190 :12) // CmdPlayAgain() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.MultipleMatch.MatchController.CmdPlayAgain$Mirror.NetworkConnectionToClient$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :193 :8) {
^entry (%_sender : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :194 :33)
cbde.store %_sender, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :194 :33)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :196 :17) // Not a variable of known type: playAgain
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :196 :16) // !playAgain (LogicalNotExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :196 :16)

^1: // SimpleBlock
%3 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :198 :28) // true
br ^3

^2: // SimpleBlock
%4 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :202 :28) // false
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: RestartGame
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :203 :16) // RestartGame() (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function RestartGame(), it contains poisonous unsupported syntaxes

// Skipping function RpcRestartGame(), it contains poisonous unsupported syntaxes

func @_Mirror.Examples.MultipleMatch.MatchController.RequestExitGame$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :242 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :245 :12) // Not a variable of known type: exitButton
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :245 :12) // exitButton.gameObject (SimpleMemberAccessExpression)
%2 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :245 :44) // false
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :245 :12) // exitButton.gameObject.SetActive(false) (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :246 :12) // Not a variable of known type: playAgainButton
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :246 :12) // playAgainButton.gameObject (SimpleMemberAccessExpression)
%6 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :246 :49) // false
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :246 :12) // playAgainButton.gameObject.SetActive(false) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CmdRequestExitGame
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :247 :12) // CmdRequestExitGame() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.MultipleMatch.MatchController.CmdRequestExitGame$Mirror.NetworkConnectionToClient$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :250 :8) {
^entry (%_sender : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :251 :39)
cbde.store %_sender, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :251 :39)
br ^0

^0: // SimpleBlock
// Entity from another assembly: StartCoroutine
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ServerEndMatch
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :253 :42) // Not a variable of known type: sender
%2 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :253 :50) // false
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :253 :27) // ServerEndMatch(sender, false) (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :253 :12) // StartCoroutine(ServerEndMatch(sender, false)) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function OnPlayerDisconnected(none), it contains poisonous unsupported syntaxes

// Skipping function ServerEndMatch(none, i1), it contains poisonous unsupported syntaxes

func @_Mirror.Examples.MultipleMatch.MatchController.RpcExitGame$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :307 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :310 :12) // Not a variable of known type: canvasController
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\MatchController.cs" :310 :12) // canvasController.OnMatchEnded() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
