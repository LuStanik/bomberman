// Skipping function OnServerAddPlayer(none), it contains poisonous unsupported syntaxes

func @_Mirror.Examples.Pong.NetworkManagerPong.OnServerDisconnect$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Pong\\Scripts\\NetworkManagerPong.cs" :29 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Pong\\Scripts\\NetworkManagerPong.cs" :29 :48)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Pong\\Scripts\\NetworkManagerPong.cs" :29 :48)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Pong\\Scripts\\NetworkManagerPong.cs" :32 :16) // Not a variable of known type: ball
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Pong\\Scripts\\NetworkManagerPong.cs" :32 :24) // null (NullLiteralExpression)
%3 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Pong\\Scripts\\NetworkManagerPong.cs" :32 :16) // comparison of unknown type: ball != null
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Pong\\Scripts\\NetworkManagerPong.cs" :32 :16)

^1: // SimpleBlock
// Entity from another assembly: NetworkServer
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Pong\\Scripts\\NetworkManagerPong.cs" :33 :38) // Not a variable of known type: ball
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Pong\\Scripts\\NetworkManagerPong.cs" :33 :16) // NetworkServer.Destroy(ball) (InvocationExpression)
br ^2

^2: // SimpleBlock
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Pong\\Scripts\\NetworkManagerPong.cs" :36 :12) // base (BaseExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Pong\\Scripts\\NetworkManagerPong.cs" :36 :36) // Not a variable of known type: conn
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Pong\\Scripts\\NetworkManagerPong.cs" :36 :12) // base.OnServerDisconnect(conn) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
