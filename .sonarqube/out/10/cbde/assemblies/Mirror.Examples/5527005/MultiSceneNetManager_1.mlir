func @_Mirror.Examples.MultipleAdditiveScenes.MultiSceneNetManager.OnServerAddPlayer$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\MultiSceneNetManager.cs" :33 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\MultiSceneNetManager.cs" :33 :47)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\MultiSceneNetManager.cs" :33 :47)
br ^0

^0: // SimpleBlock
// Entity from another assembly: StartCoroutine
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnServerAddPlayerDelayed
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\MultiSceneNetManager.cs" :35 :52) // Not a variable of known type: conn
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\MultiSceneNetManager.cs" :35 :27) // OnServerAddPlayerDelayed(conn) (InvocationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\MultiSceneNetManager.cs" :35 :12) // StartCoroutine(OnServerAddPlayerDelayed(conn)) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function OnServerAddPlayerDelayed(none), it contains poisonous unsupported syntaxes

func @_Mirror.Examples.MultipleAdditiveScenes.MultiSceneNetManager.OnStartServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\MultiSceneNetManager.cs" :69 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: StartCoroutine
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ServerLoadSubScenes
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\MultiSceneNetManager.cs" :71 :27) // ServerLoadSubScenes() (InvocationExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\MultiSceneNetManager.cs" :71 :12) // StartCoroutine(ServerLoadSubScenes()) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function ServerLoadSubScenes(), it contains poisonous unsupported syntaxes

func @_Mirror.Examples.MultipleAdditiveScenes.MultiSceneNetManager.OnStopServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\MultiSceneNetManager.cs" :94 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: NetworkServer
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\MultiSceneNetManager.cs" :96 :36) // new SceneMessage { sceneName = gameScene, sceneOperation = SceneOperation.UnloadAdditive } (ObjectCreationExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\MultiSceneNetManager.cs" :96 :67) // Not a variable of known type: gameScene
// Entity from another assembly: SceneOperation
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\MultiSceneNetManager.cs" :96 :95) // SceneOperation.UnloadAdditive (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\MultiSceneNetManager.cs" :96 :12) // NetworkServer.SendToAll(new SceneMessage { sceneName = gameScene, sceneOperation = SceneOperation.UnloadAdditive }) (InvocationExpression)
// Entity from another assembly: StartCoroutine
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ServerUnloadSubScenes
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\MultiSceneNetManager.cs" :97 :27) // ServerUnloadSubScenes() (InvocationExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\MultiSceneNetManager.cs" :97 :12) // StartCoroutine(ServerUnloadSubScenes()) (InvocationExpression)
%6 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\MultiSceneNetManager.cs" :98 :26)
br ^1

^1: // ExitBlock
return

}
// Skipping function ServerUnloadSubScenes(), it contains poisonous unsupported syntaxes

func @_Mirror.Examples.MultipleAdditiveScenes.MultiSceneNetManager.OnStopClient$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\MultiSceneNetManager.cs" :116 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\MultiSceneNetManager.cs" :119 :16) // Identifier from another assembly: mode
// Entity from another assembly: NetworkManagerMode
%1 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\MultiSceneNetManager.cs" :119 :24) // NetworkManagerMode.ClientOnly (SimpleMemberAccessExpression)
%2 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\MultiSceneNetManager.cs" :119 :16) // comparison of unknown type: mode == NetworkManagerMode.ClientOnly
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\MultiSceneNetManager.cs" :119 :16)

^1: // SimpleBlock
// Entity from another assembly: StartCoroutine
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ClientUnloadSubScenes
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\MultiSceneNetManager.cs" :120 :31) // ClientUnloadSubScenes() (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleAdditiveScenes\\Scripts\\MultiSceneNetManager.cs" :120 :16) // StartCoroutine(ClientUnloadSubScenes()) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
// Skipping function ClientUnloadSubScenes(), it contains poisonous unsupported syntaxes

