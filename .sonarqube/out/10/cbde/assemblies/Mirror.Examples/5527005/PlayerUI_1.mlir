func @_Mirror.Examples.Basic.PlayerUI.SetPlayer$Mirror.Examples.Basic.Player.bool$(none, i1) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :21 :8) {
^entry (%_player : none, %_isLocalPlayer : i1):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :21 :30)
cbde.store %_player, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :21 :30)
%1 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :21 :45)
cbde.store %_isLocalPlayer, %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :21 :45)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :24 :12) // this (ThisExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :24 :12) // this.player (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :24 :26) // Not a variable of known type: player
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :27 :12) // Not a variable of known type: player
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :27 :12) // player.OnPlayerNumberChanged (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnPlayerNumberChanged
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :27 :12) // Binary expression on unsupported types player.OnPlayerNumberChanged += OnPlayerNumberChanged
// No identifier name for binary assignment expression
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :28 :12) // Not a variable of known type: player
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :28 :12) // player.OnPlayerColorChanged (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnPlayerColorChanged
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :28 :12) // Binary expression on unsupported types player.OnPlayerColorChanged += OnPlayerColorChanged
// No identifier name for binary assignment expression
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :29 :12) // Not a variable of known type: player
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :29 :12) // player.OnPlayerDataChanged (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnPlayerDataChanged
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :29 :12) // Binary expression on unsupported types player.OnPlayerDataChanged += OnPlayerDataChanged
// No identifier name for binary assignment expression
%14 = cbde.load %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :32 :16)
cond_br %14, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :32 :16)

^1: // SimpleBlock
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :33 :16) // Not a variable of known type: image
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :33 :16) // image.color (SimpleMemberAccessExpression)
%17 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :33 :40) // 1f (NumericLiteralExpression)
%18 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :33 :44) // 1f (NumericLiteralExpression)
%19 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :33 :48) // 1f (NumericLiteralExpression)
%20 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :33 :52) // 0.1f (NumericLiteralExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :33 :30) // new Color(1f, 1f, 1f, 0.1f) (ObjectCreationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.Examples.Basic.PlayerUI.OnDisable$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :36 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :38 :12) // Not a variable of known type: player
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :38 :12) // player.OnPlayerNumberChanged (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnPlayerNumberChanged
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :38 :12) // Binary expression on unsupported types player.OnPlayerNumberChanged -= OnPlayerNumberChanged
// No identifier name for binary assignment expression
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :39 :12) // Not a variable of known type: player
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :39 :12) // player.OnPlayerColorChanged (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnPlayerColorChanged
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :39 :12) // Binary expression on unsupported types player.OnPlayerColorChanged -= OnPlayerColorChanged
// No identifier name for binary assignment expression
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :40 :12) // Not a variable of known type: player
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :40 :12) // player.OnPlayerDataChanged (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnPlayerDataChanged
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :40 :12) // Binary expression on unsupported types player.OnPlayerDataChanged -= OnPlayerDataChanged
// No identifier name for binary assignment expression
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.Basic.PlayerUI.OnPlayerNumberChanged$int$(i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :44 :8) {
^entry (%_newPlayerNumber : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :44 :35)
cbde.store %_newPlayerNumber, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :44 :35)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :46 :12) // Not a variable of known type: playerNameText
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :46 :12) // playerNameText.text (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :46 :34) // string (PredefinedType)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :46 :48) // "Player {0:00}" (StringLiteralExpression)
%5 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :46 :65)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :46 :34) // string.Format("Player {0:00}", newPlayerNumber) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.Basic.PlayerUI.OnPlayerColorChanged$UnityEngine.Color32$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :50 :8) {
^entry (%_newPlayerColor : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :50 :34)
cbde.store %_newPlayerColor, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :50 :34)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :52 :12) // Not a variable of known type: playerNameText
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :52 :12) // playerNameText.color (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :52 :35) // Not a variable of known type: newPlayerColor
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.Basic.PlayerUI.OnPlayerDataChanged$int$(i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :56 :8) {
^entry (%_newPlayerData : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :56 :33)
cbde.store %_newPlayerData, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :56 :33)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :59 :12) // Not a variable of known type: playerDataText
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :59 :12) // playerDataText.text (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :59 :34) // string (PredefinedType)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :59 :48) // "Data: {0:000}" (StringLiteralExpression)
%5 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :59 :65)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Basic\\Scripts\\PlayerUI.cs" :59 :34) // string.Format("Data: {0:000}", newPlayerData) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
