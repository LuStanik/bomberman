func @_Mirror.Examples.MultipleMatch.CanvasController.InitializeData$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :77 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :79 :12) // Not a variable of known type: playerMatches
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :79 :12) // playerMatches.Clear() (InvocationExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :80 :12) // Not a variable of known type: openMatches
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :80 :12) // openMatches.Clear() (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :81 :12) // Not a variable of known type: matchConnections
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :81 :12) // matchConnections.Clear() (InvocationExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :82 :12) // Not a variable of known type: waitingConnections
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :82 :12) // waitingConnections.Clear() (InvocationExpression)
// Entity from another assembly: Guid
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :83 :31) // Guid.Empty (SimpleMemberAccessExpression)
// Entity from another assembly: Guid
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :84 :31) // Guid.Empty (SimpleMemberAccessExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.MultipleMatch.CanvasController.ResetCanvas$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :88 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: InitializeData
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :90 :12) // InitializeData() (InvocationExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :91 :12) // Not a variable of known type: lobbyView
%2 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :91 :32) // false
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :91 :12) // lobbyView.SetActive(false) (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :92 :12) // Not a variable of known type: roomView
%5 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :92 :31) // false
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :92 :12) // roomView.SetActive(false) (InvocationExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :93 :12) // Identifier from another assembly: gameObject
%8 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :93 :33) // false
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :93 :12) // gameObject.SetActive(false) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.MultipleMatch.CanvasController.SelectMatch$System.Guid$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :104 :8) {
^entry (%_matchId : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :104 :32)
cbde.store %_matchId, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :104 :32)
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: NetworkClient
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :106 :17) // NetworkClient.active (SimpleMemberAccessExpression)
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :106 :16) // !NetworkClient.active (LogicalNotExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :106 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :106 :39)

^2: // BinaryBranchBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :108 :16) // Not a variable of known type: matchId
// Entity from another assembly: Guid
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :108 :27) // Guid.Empty (SimpleMemberAccessExpression)
%5 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :108 :16) // comparison of unknown type: matchId == Guid.Empty
cond_br %5, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :108 :16)

^3: // SimpleBlock
// Entity from another assembly: Guid
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :110 :32) // Guid.Empty (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :111 :16) // Not a variable of known type: joinButton
%8 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :111 :16) // joinButton.interactable (SimpleMemberAccessExpression)
%9 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :111 :42) // false
br ^5

^4: // BinaryBranchBlock
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :115 :21) // Not a variable of known type: openMatches
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :115 :21) // openMatches.Keys (SimpleMemberAccessExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :115 :47) // Not a variable of known type: matchId
%13 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :115 :21) // openMatches.Keys.Contains(matchId) (InvocationExpression)
%14 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :115 :20) // !openMatches.Keys.Contains(matchId) (LogicalNotExpression)
cond_br %14, ^6, ^7 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :115 :20)

^6: // JumpBlock
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :117 :20) // Not a variable of known type: joinButton
%16 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :117 :20) // joinButton.interactable (SimpleMemberAccessExpression)
%17 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :117 :46) // false
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :118 :20)

^7: // SimpleBlock
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :121 :32) // Not a variable of known type: matchId
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :122 :34) // Not a variable of known type: openMatches
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :122 :46) // Not a variable of known type: matchId
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :122 :34) // openMatches[matchId] (ElementAccessExpression)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :123 :16) // Not a variable of known type: joinButton
%24 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :123 :16) // joinButton.interactable (SimpleMemberAccessExpression)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :123 :42) // Not a variable of known type: infos
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :123 :42) // infos.players (SimpleMemberAccessExpression)
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :123 :58) // Not a variable of known type: infos
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :123 :58) // infos.maxPlayers (SimpleMemberAccessExpression)
%29 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :123 :42) // comparison of unknown type: infos.players < infos.maxPlayers
br ^5

^5: // ExitBlock
return

}
func @_Mirror.Examples.MultipleMatch.CanvasController.RequestCreateMatch$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :130 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: NetworkClient
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :132 :17) // NetworkClient.active (SimpleMemberAccessExpression)
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :132 :16) // !NetworkClient.active (LogicalNotExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :132 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :132 :39)

^2: // SimpleBlock
// Entity from another assembly: NetworkClient
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :134 :12) // NetworkClient.connection (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :134 :42) // new ServerMatchMessage { serverMatchOperation = ServerMatchOperation.Create } (ObjectCreationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :134 :90) // Not a variable of known type: ServerMatchOperation
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :134 :90) // ServerMatchOperation.Create (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :134 :12) // NetworkClient.connection.Send(new ServerMatchMessage { serverMatchOperation = ServerMatchOperation.Create }) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function RequestJoinMatch(), it contains poisonous unsupported syntaxes

// Skipping function RequestLeaveMatch(), it contains poisonous unsupported syntaxes

// Skipping function RequestCancelMatch(), it contains poisonous unsupported syntaxes

// Skipping function RequestReadyChange(), it contains poisonous unsupported syntaxes

// Skipping function RequestStartMatch(), it contains poisonous unsupported syntaxes

func @_Mirror.Examples.MultipleMatch.CanvasController.OnMatchEnded$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :192 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: NetworkClient
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :194 :17) // NetworkClient.active (SimpleMemberAccessExpression)
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :194 :16) // !NetworkClient.active (LogicalNotExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :194 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :194 :39)

^2: // SimpleBlock
// Entity from another assembly: Guid
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :196 :31) // Guid.Empty (SimpleMemberAccessExpression)
// Entity from another assembly: Guid
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :197 :31) // Guid.Empty (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ShowLobbyView
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :198 :12) // ShowLobbyView() (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function SendMatchList(none), it contains poisonous unsupported syntaxes

func @_Mirror.Examples.MultipleMatch.CanvasController.OnStartServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :228 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: NetworkServer
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :230 :17) // NetworkServer.active (SimpleMemberAccessExpression)
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :230 :16) // !NetworkServer.active (LogicalNotExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :230 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :230 :39)

^2: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: InitializeData
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :232 :12) // InitializeData() (InvocationExpression)
// Entity from another assembly: NetworkServer
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnServerMatchMessage
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :233 :12) // NetworkServer.RegisterHandler<ServerMatchMessage>(OnServerMatchMessage) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Examples.MultipleMatch.CanvasController.OnServerReady$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :236 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :236 :36)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :236 :36)
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: NetworkServer
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :238 :17) // NetworkServer.active (SimpleMemberAccessExpression)
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :238 :16) // !NetworkServer.active (LogicalNotExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :238 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :238 :39)

^2: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :240 :12) // Not a variable of known type: waitingConnections
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :240 :35) // Not a variable of known type: conn
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :240 :12) // waitingConnections.Add(conn) (InvocationExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :241 :12) // Not a variable of known type: playerInfos
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :241 :28) // Not a variable of known type: conn
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :241 :34) // new PlayerInfo { playerIndex = this.playerIndex, ready = false } (ObjectCreationExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :241 :65) // this (ThisExpression)
%10 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :241 :65) // this.playerIndex (SimpleMemberAccessExpression)
%11 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :241 :91) // false
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :241 :12) // playerInfos.Add(conn, new PlayerInfo { playerIndex = this.playerIndex, ready = false }) (InvocationExpression)
%13 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :242 :12) // Not a variable of known type: playerIndex
%14 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :242 :12) // Inc/Decrement of field or property playerIndex
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SendMatchList
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :244 :12) // SendMatchList() (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function OnServerDisconnect(none), it contains poisonous unsupported syntaxes

func @_Mirror.Examples.MultipleMatch.CanvasController.OnStopServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :303 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ResetCanvas
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :305 :12) // ResetCanvas() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.MultipleMatch.CanvasController.OnClientConnect$Mirror.NetworkConnection$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :308 :8) {
^entry (%_conn : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :308 :38)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :308 :38)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :310 :12) // Not a variable of known type: playerInfos
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :310 :28) // Not a variable of known type: conn
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :310 :34) // new PlayerInfo { playerIndex = this.playerIndex, ready = false } (ObjectCreationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :310 :65) // this (ThisExpression)
%5 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :310 :65) // this.playerIndex (SimpleMemberAccessExpression)
%6 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :310 :91) // false
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :310 :12) // playerInfos.Add(conn, new PlayerInfo { playerIndex = this.playerIndex, ready = false }) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Examples.MultipleMatch.CanvasController.OnStartClient$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :313 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: NetworkClient
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :315 :17) // NetworkClient.active (SimpleMemberAccessExpression)
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :315 :16) // !NetworkClient.active (LogicalNotExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :315 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :315 :39)

^2: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: InitializeData
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :317 :12) // InitializeData() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ShowLobbyView
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :318 :12) // ShowLobbyView() (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :319 :12) // Not a variable of known type: createButton
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :319 :12) // createButton.gameObject (SimpleMemberAccessExpression)
%6 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :319 :46) // true
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :319 :12) // createButton.gameObject.SetActive(true) (InvocationExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :320 :12) // Not a variable of known type: joinButton
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :320 :12) // joinButton.gameObject (SimpleMemberAccessExpression)
%10 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :320 :44) // true
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :320 :12) // joinButton.gameObject.SetActive(true) (InvocationExpression)
// Entity from another assembly: NetworkClient
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnClientMatchMessage
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :321 :12) // NetworkClient.RegisterHandler<ClientMatchMessage>(OnClientMatchMessage) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Examples.MultipleMatch.CanvasController.OnClientDisconnect$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :324 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: NetworkClient
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :326 :17) // NetworkClient.active (SimpleMemberAccessExpression)
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :326 :16) // !NetworkClient.active (LogicalNotExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :326 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :326 :39)

^2: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: InitializeData
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :328 :12) // InitializeData() (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Examples.MultipleMatch.CanvasController.OnStopClient$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :331 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ResetCanvas
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :333 :12) // ResetCanvas() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function OnServerMatchMessage(none, none), it contains poisonous unsupported syntaxes

// Skipping function OnServerPlayerReady(none, none), it contains poisonous unsupported syntaxes

// Skipping function OnServerLeaveMatch(none, none), it contains poisonous unsupported syntaxes

// Skipping function OnServerCreateMatch(none), it contains poisonous unsupported syntaxes

// Skipping function OnServerCancelMatch(none), it contains poisonous unsupported syntaxes

// Skipping function OnServerStartMatch(none), it contains poisonous unsupported syntaxes

// Skipping function OnServerJoinMatch(none, none), it contains poisonous unsupported syntaxes

// Skipping function OnClientMatchMessage(none, none), it contains poisonous unsupported syntaxes

// Skipping function ShowLobbyView(), it contains poisonous unsupported syntaxes

func @_Mirror.Examples.MultipleMatch.CanvasController.ShowRoomView$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :635 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :637 :12) // Not a variable of known type: lobbyView
%1 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :637 :32) // false
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :637 :12) // lobbyView.SetActive(false) (InvocationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :638 :12) // Not a variable of known type: roomView
%4 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :638 :31) // true
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\MultipleMatches\\Scripts\\CanvasController.cs" :638 :12) // roomView.SetActive(true) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function RefreshMatchList(), it contains poisonous unsupported syntaxes

