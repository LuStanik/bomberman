// Skipping function SetupKcp(i1, none, i32, i1, none, none), it contains poisonous unsupported syntaxes

func @_kcp2k.KcpConnection.HandleTimeout$uint$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :97 :8) {
^entry (%_time : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :97 :27)
cbde.store %_time, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :97 :27)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :101 :16) // Not a variable of known type: time
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :101 :24) // Not a variable of known type: lastReceiveTime
%3 = constant 10000 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :101 :42)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :101 :24) // Binary expression on unsupported types lastReceiveTime + TIMEOUT
%5 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :101 :16) // comparison of unknown type: time >= lastReceiveTime + TIMEOUT
cond_br %5, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :101 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Log
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :103 :16) // Log.Warning (SimpleMemberAccessExpression)
%7 = constant 10000 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :103 :63)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :103 :28) // $"KCP: Connection timed out after {TIMEOUT}ms. Disconnecting." (InterpolatedStringExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :103 :16) // Log.Warning($"KCP: Connection timed out after {TIMEOUT}ms. Disconnecting.") (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Disconnect
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :104 :16) // Disconnect() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_kcp2k.KcpConnection.HandleDeadLink$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :108 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :111 :16) // Not a variable of known type: kcp
%1 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :111 :16) // kcp.state (SimpleMemberAccessExpression)
%2 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :111 :30)
%3 = cbde.neg %2 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :111 :29)
%4 = cmpi "eq", %1, %3 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :111 :16)
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :111 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Log
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :113 :16) // Log.Warning (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :113 :28) // "KCP Connection dead_link detected. Disconnecting." (StringLiteralExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :113 :16) // Log.Warning("KCP Connection dead_link detected. Disconnecting.") (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Disconnect
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :114 :16) // Disconnect() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_kcp2k.KcpConnection.HandlePing$uint$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :119 :8) {
^entry (%_time : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :119 :24)
cbde.store %_time, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :119 :24)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :122 :16) // Not a variable of known type: time
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :122 :24) // Not a variable of known type: lastPingTime
%3 = constant 1000 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :122 :39)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :122 :24) // Binary expression on unsupported types lastPingTime + PING_INTERVAL
%5 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :122 :16) // comparison of unknown type: time >= lastPingTime + PING_INTERVAL
cond_br %5, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :122 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Send
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :126 :21) // Not a variable of known type: Ping
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :126 :16) // Send(Ping) (InvocationExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :127 :31) // Not a variable of known type: time
br ^2

^2: // ExitBlock
return

}
func @_kcp2k.KcpConnection.HandleChoked$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :131 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :135 :24) // Not a variable of known type: kcp
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :135 :24) // kcp.rcv_queue (SimpleMemberAccessExpression)
%2 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :135 :24) // kcp.rcv_queue.Count (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :135 :46) // Not a variable of known type: kcp
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :135 :46) // kcp.snd_queue (SimpleMemberAccessExpression)
%5 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :135 :46) // kcp.snd_queue.Count (SimpleMemberAccessExpression)
%6 = addi %2, %5 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :135 :24)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :136 :24) // Not a variable of known type: kcp
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :136 :24) // kcp.rcv_buf (SimpleMemberAccessExpression)
%9 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :136 :24) // kcp.rcv_buf.Count (SimpleMemberAccessExpression)
%10 = addi %6, %9 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :135 :24)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :136 :44) // Not a variable of known type: kcp
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :136 :44) // kcp.snd_buf (SimpleMemberAccessExpression)
%13 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :136 :44) // kcp.snd_buf.Count (SimpleMemberAccessExpression)
%14 = addi %10, %13 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :135 :24)
%15 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :135 :16) // total
cbde.store %14, %15 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :135 :16)
%16 = cbde.load %15 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :137 :16)
%17 = constant 10000 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :137 :25)
%18 = cmpi "sge", %16, %17 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :137 :16)
cond_br %18, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :137 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Log
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :139 :16) // Log.Warning (SimpleMemberAccessExpression)
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :139 :28) // $"KCP: disconnecting connection because it can't process data fast enough.\n" (InterpolatedStringExpression)
%21 = cbde.load %15 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :140 :48)
%22 = constant 10000 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :140 :56)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :140 :94) // Not a variable of known type: kcp
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :140 :94) // kcp.rcv_queue (SimpleMemberAccessExpression)
%25 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :140 :94) // kcp.rcv_queue.Count (SimpleMemberAccessExpression)
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :140 :126) // Not a variable of known type: kcp
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :140 :126) // kcp.snd_queue (SimpleMemberAccessExpression)
%28 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :140 :126) // kcp.snd_queue.Count (SimpleMemberAccessExpression)
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :140 :156) // Not a variable of known type: kcp
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :140 :156) // kcp.rcv_buf (SimpleMemberAccessExpression)
%31 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :140 :156) // kcp.rcv_buf.Count (SimpleMemberAccessExpression)
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :140 :184) // Not a variable of known type: kcp
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :140 :184) // kcp.snd_buf (SimpleMemberAccessExpression)
%34 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :140 :184) // kcp.snd_buf.Count (SimpleMemberAccessExpression)
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :140 :33) // $"Queue total {total}>{QueueDisconnectThreshold}. rcv_queue={kcp.rcv_queue.Count} snd_queue={kcp.snd_queue.Count} rcv_buf={kcp.rcv_buf.Count} snd_buf={kcp.snd_buf.Count}\n" (InterpolatedStringExpression)
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :139 :28) // Binary expression on unsupported types $"KCP: disconnecting connection because it can't process data fast enough.\n" +                                  $"Queue total {total}>{QueueDisconnectThreshold}. rcv_queue={kcp.rcv_queue.Count} snd_queue={kcp.snd_queue.Count} rcv_buf={kcp.rcv_buf.Count} snd_buf={kcp.snd_buf.Count}\n"
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :141 :33) // $"* Try to Enable NoDelay, decrease INTERVAL, disable Congestion Window (= enable NOCWND!), increase SEND/RECV WINDOW or compress data.\n" (InterpolatedStringExpression)
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :139 :28) // Binary expression on unsupported types $"KCP: disconnecting connection because it can't process data fast enough.\n" +                                  $"Queue total {total}>{QueueDisconnectThreshold}. rcv_queue={kcp.rcv_queue.Count} snd_queue={kcp.snd_queue.Count} rcv_buf={kcp.rcv_buf.Count} snd_buf={kcp.snd_buf.Count}\n" +                                  $"* Try to Enable NoDelay, decrease INTERVAL, disable Congestion Window (= enable NOCWND!), increase SEND/RECV WINDOW or compress data.\n"
%39 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :142 :33) // $"* Or perhaps the network is simply too slow on our end, or on the other end.\n" (InterpolatedStringExpression)
%40 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :139 :28) // Binary expression on unsupported types $"KCP: disconnecting connection because it can't process data fast enough.\n" +                                  $"Queue total {total}>{QueueDisconnectThreshold}. rcv_queue={kcp.rcv_queue.Count} snd_queue={kcp.snd_queue.Count} rcv_buf={kcp.rcv_buf.Count} snd_buf={kcp.snd_buf.Count}\n" +                                  $"* Try to Enable NoDelay, decrease INTERVAL, disable Congestion Window (= enable NOCWND!), increase SEND/RECV WINDOW or compress data.\n" +                                  $"* Or perhaps the network is simply too slow on our end, or on the other end.\n"
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :139 :16) // Log.Warning($"KCP: disconnecting connection because it can't process data fast enough.\n" +                                  $"Queue total {total}>{QueueDisconnectThreshold}. rcv_queue={kcp.rcv_queue.Count} snd_queue={kcp.snd_queue.Count} rcv_buf={kcp.rcv_buf.Count} snd_buf={kcp.snd_buf.Count}\n" +                                  $"* Try to Enable NoDelay, decrease INTERVAL, disable Congestion Window (= enable NOCWND!), increase SEND/RECV WINDOW or compress data.\n" +                                  $"* Or perhaps the network is simply too slow on our end, or on the other end.\n") (InvocationExpression)
%42 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :148 :16) // Not a variable of known type: kcp
%43 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :148 :16) // kcp.snd_queue (SimpleMemberAccessExpression)
%44 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :148 :16) // kcp.snd_queue.Clear() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Disconnect
%45 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :150 :16) // Disconnect() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_kcp2k.KcpConnection.ReceiveNext$outSystem.ArraySegment$byte$$(none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :155 :8) {
^entry (%_message : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :155 :25)
cbde.store %_message, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :155 :25)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :158 :26) // Not a variable of known type: kcp
%2 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :158 :26) // kcp.PeekSize() (InvocationExpression)
%3 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :158 :16) // msgSize
cbde.store %2, %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :158 :16)
%4 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :159 :16)
%5 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :159 :26)
%6 = cmpi "sgt", %4, %5 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :159 :16)
cond_br %6, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :159 :16)

^1: // BinaryBranchBlock
%7 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :163 :20)
%8 = constant 149352 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :163 :31)
%9 = cmpi "sle", %7, %8 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :163 :20)
cond_br %9, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :163 :20)

^3: // BinaryBranchBlock
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :165 :35) // Not a variable of known type: kcp
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :165 :47) // Not a variable of known type: kcpMessageBuffer
%12 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :165 :65)
%13 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :165 :35) // kcp.Receive(kcpMessageBuffer, msgSize) (InvocationExpression)
%14 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :165 :24) // received
cbde.store %13, %14 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :165 :24)
%15 = cbde.load %14 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :166 :24)
%16 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :166 :36)
%17 = cmpi "sge", %15, %16 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :166 :24)
cond_br %17, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :166 :24)

^5: // BinaryBranchBlock
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :168 :57) // Not a variable of known type: kcpMessageBuffer
%19 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :168 :75)
%20 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :168 :78)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :168 :34) // new ArraySegment<byte>(kcpMessageBuffer, 0, msgSize) (ObjectCreationExpression)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :169 :48) // Not a variable of known type: refTime
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :169 :48) // refTime.ElapsedMilliseconds (SimpleMemberAccessExpression)
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :169 :42) // (uint)refTime.ElapsedMilliseconds (CastExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Utils
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :172 :48) // Not a variable of known type: message
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :172 :57) // Not a variable of known type: Ping
%27 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :172 :28) // Utils.SegmentsEqual(message, Ping) (InvocationExpression)
cond_br %27, ^7, ^8 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :172 :28)

^7: // JumpBlock
%28 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :175 :35) // false
return %28 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :175 :28)

^8: // JumpBlock
%29 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :177 :31) // true
return %29 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :177 :24)

^6: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Log
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :182 :24) // Log.Warning (SimpleMemberAccessExpression)
%31 = cbde.load %14 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :182 :65)
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :182 :36) // $"Receive failed with error={received}. closing connection." (InterpolatedStringExpression)
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :182 :24) // Log.Warning($"Receive failed with error={received}. closing connection.") (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Disconnect
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :183 :24) // Disconnect() (InvocationExpression)
br ^2

^4: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Log
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :190 :20) // Log.Warning (SimpleMemberAccessExpression)
%36 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :190 :79)
%37 = constant 149352 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :190 :95)
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :190 :32) // $"KCP: possible allocation attack for msgSize {msgSize} > max {MaxMessageSize}. Disconnecting the connection." (InterpolatedStringExpression)
%39 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :190 :20) // Log.Warning($"KCP: possible allocation attack for msgSize {msgSize} > max {MaxMessageSize}. Disconnecting the connection.") (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Disconnect
%40 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :191 :20) // Disconnect() (InvocationExpression)
br ^2

^2: // JumpBlock
%41 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :194 :19) // false
return %41 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :194 :12)

^9: // ExitBlock
cbde.unreachable

}
// Skipping function TickConnected(none), it contains poisonous unsupported syntaxes

// Skipping function TickAuthenticated(none), it contains poisonous unsupported syntaxes

// Skipping function Tick(), it contains poisonous unsupported syntaxes

func @_kcp2k.KcpConnection.RawInput$byte$$.int$(none, i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :314 :8) {
^entry (%_buffer : none, %_msgLength : i32):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :314 :29)
cbde.store %_buffer, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :314 :29)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :314 :44)
cbde.store %_msgLength, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :314 :44)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :316 :24) // Not a variable of known type: kcp
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :316 :34) // Not a variable of known type: buffer
%4 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :316 :42)
%5 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :316 :24) // kcp.Input(buffer, msgLength) (InvocationExpression)
%6 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :316 :16) // input
cbde.store %5, %6 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :316 :16)
%7 = cbde.load %6 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :317 :16)
%8 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :317 :25)
%9 = cmpi "ne", %7, %8 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :317 :16)
cond_br %9, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :317 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Log
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :319 :16) // Log.Warning (SimpleMemberAccessExpression)
%11 = cbde.load %6 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :319 :55)
%12 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :319 :86)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :319 :28) // $"Input failed with error={input} for buffer with length={msgLength}" (InterpolatedStringExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :319 :16) // Log.Warning($"Input failed with error={input} for buffer with length={msgLength}") (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_kcp2k.KcpConnection.Send$System.ArraySegment$byte$$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :325 :8) {
^entry (%_data : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :325 :25)
cbde.store %_data, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :325 :25)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :329 :16) // Not a variable of known type: data
%2 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :329 :16) // data.Count (SimpleMemberAccessExpression)
%3 = constant 149352 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :329 :30)
%4 = cmpi "sle", %2, %3 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :329 :16)
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :329 :16)

^1: // BinaryBranchBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :331 :27) // Not a variable of known type: kcp
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :331 :36) // Not a variable of known type: data
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :331 :36) // data.Array (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :331 :48) // Not a variable of known type: data
%9 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :331 :48) // data.Offset (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :331 :61) // Not a variable of known type: data
%11 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :331 :61) // data.Count (SimpleMemberAccessExpression)
%12 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :331 :27) // kcp.Send(data.Array, data.Offset, data.Count) (InvocationExpression)
%13 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :331 :20) // sent
cbde.store %12, %13 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :331 :20)
%14 = cbde.load %13 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :332 :20)
%15 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :332 :27)
%16 = cmpi "slt", %14, %15 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :332 :20)
cond_br %16, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :332 :20)

^3: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Log
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :334 :20) // Log.Warning (SimpleMemberAccessExpression)
%18 = cbde.load %13 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :334 :58)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :334 :89) // Not a variable of known type: data
%20 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :334 :89) // data.Count (SimpleMemberAccessExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :334 :32) // $"Send failed with error={sent} for segment with length={data.Count}" (InterpolatedStringExpression)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :334 :20) // Log.Warning($"Send failed with error={sent} for segment with length={data.Count}") (InvocationExpression)
br ^4

^2: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Log
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :337 :17) // Log.Error (SimpleMemberAccessExpression)
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :337 :61) // Not a variable of known type: data
%25 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :337 :61) // data.Count (SimpleMemberAccessExpression)
%26 = constant 149352 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :337 :114)
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :337 :27) // $"Failed to send message of size {data.Count} because it's larger than MaxMessageSize={MaxMessageSize}" (InterpolatedStringExpression)
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :337 :17) // Log.Error($"Failed to send message of size {data.Count} because it's larger than MaxMessageSize={MaxMessageSize}") (InvocationExpression)
br ^4

^4: // ExitBlock
return

}
func @_kcp2k.KcpConnection.SendHandshake$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :345 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Log
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :347 :12) // Log.Info (SimpleMemberAccessExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :347 :21) // "KcpConnection: sending Handshake to other end!" (StringLiteralExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :347 :12) // Log.Info("KcpConnection: sending Handshake to other end!") (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Send
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :348 :17) // Not a variable of known type: Hello
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :348 :12) // Send(Hello) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_kcp2k.KcpConnection.Dispose$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpConnection.cs" :351 :8) {
^entry :
br ^0

^0: // ExitBlock
return

}
// Skipping function Disconnect(), it contains poisonous unsupported syntaxes

