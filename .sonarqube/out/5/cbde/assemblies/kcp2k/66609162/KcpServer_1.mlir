func @_kcp2k.KcpServer.Start$ushort$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpServer.cs" :80 :8) {
^entry (%_port : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpServer.cs" :80 :26)
cbde.store %_port, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpServer.cs" :80 :26)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpServer.cs" :83 :16) // Not a variable of known type: socket
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpServer.cs" :83 :26) // null (NullLiteralExpression)
%3 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpServer.cs" :83 :16) // comparison of unknown type: socket != null
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpServer.cs" :83 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Log
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpServer.cs" :85 :16) // Log.Warning (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpServer.cs" :85 :28) // "KCP: server already started!" (StringLiteralExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpServer.cs" :85 :16) // Log.Warning("KCP: server already started!") (InvocationExpression)
br ^2

^2: // SimpleBlock
// Entity from another assembly: AddressFamily
%7 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpServer.cs" :89 :32) // AddressFamily.InterNetworkV6 (SimpleMemberAccessExpression)
// Entity from another assembly: SocketType
%8 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpServer.cs" :89 :62) // SocketType.Dgram (SimpleMemberAccessExpression)
// Entity from another assembly: ProtocolType
%9 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpServer.cs" :89 :80) // ProtocolType.Udp (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpServer.cs" :89 :21) // new Socket(AddressFamily.InterNetworkV6, SocketType.Dgram, ProtocolType.Udp) (ObjectCreationExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpServer.cs" :90 :12) // Not a variable of known type: socket
%12 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpServer.cs" :90 :12) // socket.DualMode (SimpleMemberAccessExpression)
%13 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpServer.cs" :90 :30) // true
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpServer.cs" :91 :12) // Not a variable of known type: socket
// Entity from another assembly: IPAddress
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpServer.cs" :91 :39) // IPAddress.IPv6Any (SimpleMemberAccessExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpServer.cs" :91 :58) // Not a variable of known type: port
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpServer.cs" :91 :24) // new IPEndPoint(IPAddress.IPv6Any, port) (ObjectCreationExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpServer.cs" :91 :12) // socket.Bind(new IPEndPoint(IPAddress.IPv6Any, port)) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function Send(i32, none), it contains poisonous unsupported syntaxes

// Skipping function Disconnect(i32), it contains poisonous unsupported syntaxes

// Skipping function GetClientAddress(i32), it contains poisonous unsupported syntaxes

// Skipping function Tick(), it contains poisonous unsupported syntaxes

// Skipping function Stop(), it contains poisonous unsupported syntaxes

