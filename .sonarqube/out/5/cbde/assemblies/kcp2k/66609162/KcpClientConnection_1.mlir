func @_kcp2k.KcpClientConnection.Connect$string.ushort.bool.uint.int.bool.uint.uint$(none, none, i1, none, i32, i1, none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :13 :8) {
^entry (%_host : none, %_port : none, %_noDelay : i1, %_interval : none, %_fastResend : i32, %_congestionWindow : i1, %_sendWindowSize : none, %_receiveWindowSize : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :13 :28)
cbde.store %_host, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :13 :28)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :13 :41)
cbde.store %_port, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :13 :41)
%2 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :13 :54)
cbde.store %_noDelay, %2 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :13 :54)
%3 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :13 :68)
cbde.store %_interval, %3 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :13 :68)
%4 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :13 :98)
cbde.store %_fastResend, %4 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :13 :98)
%5 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :13 :118)
cbde.store %_congestionWindow, %5 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :13 :118)
%6 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :13 :148)
cbde.store %_sendWindowSize, %6 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :13 :148)
%7 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :13 :183)
cbde.store %_receiveWindowSize, %7 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :13 :183)
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Log
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :15 :12) // Log.Info (SimpleMemberAccessExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :15 :46) // Not a variable of known type: host
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :15 :53) // Not a variable of known type: port
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :15 :21) // $"KcpClient: connect to {host}:{port}" (InterpolatedStringExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :15 :12) // Log.Info($"KcpClient: connect to {host}:{port}") (InvocationExpression)
// Entity from another assembly: Dns
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :16 :57) // Not a variable of known type: host
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :16 :36) // Dns.GetHostAddresses(host) (InvocationExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :17 :16) // Not a variable of known type: ipAddress
%17 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :17 :16) // ipAddress.Length (SimpleMemberAccessExpression)
%18 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :17 :35)
%19 = cmpi "slt", %17, %18 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :17 :16)
cond_br %19, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :17 :16)

^1: // JumpBlock
// Entity from another assembly: SocketError
%20 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :18 :47) // SocketError.HostNotFound (SimpleMemberAccessExpression)
%21 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :18 :42) // (int)SocketError.HostNotFound (CastExpression)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :18 :22) // new SocketException((int)SocketError.HostNotFound) (ObjectCreationExpression)
cbde.throw %22 :  none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :18 :16)

^2: // SimpleBlock
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :20 :44) // Not a variable of known type: ipAddress
%24 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :20 :54)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :20 :44) // ipAddress[0] (ElementAccessExpression)
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :20 :58) // Not a variable of known type: port
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :20 :29) // new IPEndPoint(ipAddress[0], port) (ObjectCreationExpression)
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :21 :32) // Not a variable of known type: remoteEndpoint
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :21 :32) // remoteEndpoint.AddressFamily (SimpleMemberAccessExpression)
// Entity from another assembly: SocketType
%30 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :21 :62) // SocketType.Dgram (SimpleMemberAccessExpression)
// Entity from another assembly: ProtocolType
%31 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :21 :80) // ProtocolType.Udp (SimpleMemberAccessExpression)
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :21 :21) // new Socket(remoteEndpoint.AddressFamily, SocketType.Dgram, ProtocolType.Udp) (ObjectCreationExpression)
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :22 :12) // Not a variable of known type: socket
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :22 :27) // Not a variable of known type: remoteEndpoint
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :22 :12) // socket.Connect(remoteEndpoint) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SetupKcp
%36 = cbde.load %2 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :23 :21)
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :23 :30) // Not a variable of known type: interval
%38 = cbde.load %4 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :23 :40)
%39 = cbde.load %5 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :23 :52)
%40 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :23 :70) // Not a variable of known type: sendWindowSize
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :23 :86) // Not a variable of known type: receiveWindowSize
%42 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :23 :12) // SetupKcp(noDelay, interval, fastResend, congestionWindow, sendWindowSize, receiveWindowSize) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SendHandshake
%43 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :26 :12) // SendHandshake() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: RawReceive
%44 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :28 :12) // RawReceive() (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function RawReceive(), it contains poisonous unsupported syntaxes

func @_kcp2k.KcpClientConnection.Dispose$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :62 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :64 :12) // Not a variable of known type: socket
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :64 :12) // socket.Close() (InvocationExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :65 :21) // null (NullLiteralExpression)
br ^1

^1: // ExitBlock
return

}
func @_kcp2k.KcpClientConnection.RawSend$byte$$.int$(none, i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :68 :8) {
^entry (%_data : none, %_length : i32):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :68 :40)
cbde.store %_data, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :68 :40)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :68 :53)
cbde.store %_length, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :68 :53)
br ^0

^0: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :70 :12) // Not a variable of known type: socket
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :70 :24) // Not a variable of known type: data
%4 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :70 :30)
// Entity from another assembly: SocketFlags
%5 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :70 :38) // SocketFlags.None (SimpleMemberAccessExpression)
%6 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClientConnection.cs" :70 :12) // socket.Send(data, length, SocketFlags.None) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
