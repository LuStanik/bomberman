func @_kcp2k.Kcp.SegmentNew$$() -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :114 :8) {
^entry :
br ^0

^0: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Segment
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :116 :19) // Segment.Take() (InvocationExpression)
return %0 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :116 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_kcp2k.Kcp.SegmentDelete$kcp2k.Segment$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :122 :8) {
^entry (%_seg : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :122 :34)
cbde.store %_seg, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :122 :34)
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Segment
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :124 :27) // Not a variable of known type: seg
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :124 :12) // Segment.Return(seg) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function Receive(none, i32), it contains poisonous unsupported syntaxes

// Skipping function PeekSize(), it contains poisonous unsupported syntaxes

// Skipping function Send(none, i32, i32), it contains poisonous unsupported syntaxes

func @_kcp2k.Kcp.UpdateAck$int$(i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :287 :8) {
^entry (%_rtt : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :287 :23)
cbde.store %_rtt, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :287 :23)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :290 :16) // Not a variable of known type: rx_srtt
%2 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :290 :27)
%3 = cmpi "eq", %1, %2 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :290 :16)
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :290 :16)

^1: // SimpleBlock
%4 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :292 :26)
%5 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :293 :28)
%6 = constant 2 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :293 :34)
%7 = divis %5, %6 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :293 :28)
br ^3

^2: // BinaryBranchBlock
%8 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :297 :28)
%9 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :297 :34) // Not a variable of known type: rx_srtt
%10 = subi %8, %9 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :297 :28)
%11 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :297 :20) // delta
cbde.store %10, %11 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :297 :20)
%12 = cbde.load %11 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :298 :20)
%13 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :298 :28)
%14 = cmpi "slt", %12, %13 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :298 :20)
cond_br %14, ^4, ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :298 :20)

^4: // SimpleBlock
%15 = cbde.load %11 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :298 :40)
%16 = cbde.neg %15 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :298 :39)
cbde.store %16, %11 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :298 :31)
br ^5

^5: // BinaryBranchBlock
%17 = constant 3 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :299 :29)
%18 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :299 :33) // Not a variable of known type: rx_rttval
%19 = muli %17, %18 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :299 :29)
%20 = cbde.load %11 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :299 :45)
%21 = addi %19, %20 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :299 :29)
%22 = constant 4 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :299 :54)
%23 = divis %21, %22 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :299 :28)
%24 = constant 7 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :300 :27)
%25 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :300 :31) // Not a variable of known type: rx_srtt
%26 = muli %24, %25 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :300 :27)
%27 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :300 :41)
%28 = addi %26, %27 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :300 :27)
%29 = constant 8 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :300 :48)
%30 = divis %28, %29 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :300 :26)
%31 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :301 :20) // Not a variable of known type: rx_srtt
%32 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :301 :30)
%33 = cmpi "slt", %31, %32 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :301 :20)
cond_br %33, ^6, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :301 :20)

^6: // SimpleBlock
%34 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :301 :43)
br ^3

^3: // SimpleBlock
%35 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :303 :22) // Not a variable of known type: rx_srtt
// Entity from another assembly: Math
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :303 :46) // Not a variable of known type: interval
%37 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :303 :41) // (int)interval (CastExpression)
%38 = constant 4 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :303 :56)
%39 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :303 :60) // Not a variable of known type: rx_rttval
%40 = muli %38, %39 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :303 :56)
%41 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :303 :32) // Math.Max((int)interval, 4 * rx_rttval) (InvocationExpression)
%42 = addi %35, %41 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :303 :22)
%43 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :303 :16) // rto
cbde.store %42, %43 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :303 :16)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Utils
%44 = cbde.load %43 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :304 :33)
%45 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :304 :38) // Not a variable of known type: rx_minrto
%46 = constant 60000 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :304 :49)
%47 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :304 :21) // Utils.Clamp(rto, rx_minrto, RTO_MAX) (InvocationExpression)
br ^7

^7: // ExitBlock
return

}
func @_kcp2k.Kcp.ShrinkBuf$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :308 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :310 :16) // Not a variable of known type: snd_buf
%1 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :310 :16) // snd_buf.Count (SimpleMemberAccessExpression)
%2 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :310 :32)
%3 = cmpi "sgt", %1, %2 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :310 :16)
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :310 :16)

^1: // SimpleBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :312 :30) // Not a variable of known type: snd_buf
%5 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :312 :38)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :312 :30) // snd_buf[0] (ElementAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :313 :26) // Not a variable of known type: seg
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :313 :26) // seg.sn (SimpleMemberAccessExpression)
br ^3

^2: // SimpleBlock
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :317 :26) // Not a variable of known type: snd_nxt
br ^3

^3: // ExitBlock
return

}
// Skipping function ParseAck(none), it contains poisonous unsupported syntaxes

// Skipping function ParseUna(none), it contains poisonous unsupported syntaxes

// Skipping function ParseFastack(none, none), it contains poisonous unsupported syntaxes

func @_kcp2k.Kcp.AckPush$uint.uint$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :392 :8) {
^entry (%_sn : none, %_ts : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :392 :21)
cbde.store %_sn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :392 :21)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :392 :30)
cbde.store %_ts, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :392 :30)
br ^0

^0: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :394 :12) // Not a variable of known type: acklist
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :394 :24) // new AckItem{ serialNumber = sn, timestamp = ts } (ObjectCreationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :394 :52) // Not a variable of known type: sn
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :394 :68) // Not a variable of known type: ts
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :394 :12) // acklist.Add(new AckItem{ serialNumber = sn, timestamp = ts }) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function ParseData(none), it contains poisonous unsupported syntaxes

func @_kcp2k.Kcp.InsertSegmentInReceiveBuffer$kcp2k.Segment$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :420 :8) {
^entry (%_newseg : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :420 :51)
cbde.store %_newseg, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :420 :51)
br ^0

^0: // ForInitializerBlock
%1 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :422 :26) // false
%2 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :422 :17) // repeat
cbde.store %1, %2 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :422 :17)
%3 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :425 :16) // i
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :426 :21) // Not a variable of known type: rcv_buf
%5 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :426 :21) // rcv_buf.Count (SimpleMemberAccessExpression)
%6 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :426 :37)
%7 = subi %5, %6 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :426 :21)
cbde.store %7, %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :426 :17)
br ^1

^1: // BinaryBranchBlock
%8 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :426 :40)
%9 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :426 :45)
%10 = cmpi "sge", %8, %9 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :426 :40)
cond_br %10, ^2, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :426 :40)

^2: // BinaryBranchBlock
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :428 :30) // Not a variable of known type: rcv_buf
%12 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :428 :38)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :428 :30) // rcv_buf[i] (ElementAccessExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :429 :20) // Not a variable of known type: seg
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :429 :20) // seg.sn (SimpleMemberAccessExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :429 :30) // Not a variable of known type: newseg
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :429 :30) // newseg.sn (SimpleMemberAccessExpression)
%19 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :429 :20) // comparison of unknown type: seg.sn == newseg.sn
cond_br %19, ^4, ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :429 :20)

^4: // JumpBlock
%20 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :432 :29) // true
cbde.store %20, %2 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :432 :20)
br ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :433 :20) // break

^5: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Utils
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :435 :35) // Not a variable of known type: newseg
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :435 :35) // newseg.sn (SimpleMemberAccessExpression)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :435 :46) // Not a variable of known type: seg
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :435 :46) // seg.sn (SimpleMemberAccessExpression)
%25 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :435 :20) // Utils.TimeDiff(newseg.sn, seg.sn) (InvocationExpression)
%26 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :435 :56)
%27 = cmpi "sgt", %25, %26 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :435 :20)
cond_br %27, ^6, ^7 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :435 :20)

^6: // JumpBlock
br ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :438 :20) // break

^7: // SimpleBlock
%28 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :426 :48)
%29 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :426 :48)
%30 = subi %28, %29 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :426 :48)
cbde.store %30, %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :426 :48)
br ^1

^3: // BinaryBranchBlock
%31 = cbde.load %2 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :443 :17)
%32 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :443 :16) // !repeat (LogicalNotExpression)
cond_br %32, ^8, ^9 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :443 :16)

^8: // SimpleBlock
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :445 :16) // Not a variable of known type: rcv_buf
%34 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :445 :31)
%35 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :445 :35)
%36 = addi %34, %35 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :445 :31)
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :445 :38) // Not a variable of known type: newseg
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :445 :16) // rcv_buf.Insert(i + 1, newseg) (InvocationExpression)
br ^10

^9: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SegmentDelete
%39 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :450 :30) // Not a variable of known type: newseg
%40 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :450 :16) // SegmentDelete(newseg) (InvocationExpression)
br ^10

^10: // ExitBlock
return

}
// Skipping function MoveReceiveBufferDataToReceiveQueue(), it contains poisonous unsupported syntaxes

// Skipping function Input(none, i32), it contains poisonous unsupported syntaxes

func @_kcp2k.Kcp.WndUnused$$() -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :638 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :640 :16) // Not a variable of known type: rcv_queue
%1 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :640 :16) // rcv_queue.Count (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :640 :34) // Not a variable of known type: rcv_wnd
%3 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :640 :16) // comparison of unknown type: rcv_queue.Count < rcv_wnd
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :640 :16)

^1: // JumpBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :641 :23) // Not a variable of known type: rcv_wnd
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :641 :39) // Not a variable of known type: rcv_queue
%6 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :641 :39) // rcv_queue.Count (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :641 :33) // (uint)rcv_queue.Count (CastExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :641 :23) // Binary expression on unsupported types rcv_wnd - (uint)rcv_queue.Count
return %8 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :641 :16)

^2: // JumpBlock
%9 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :642 :19)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :642 :19) // Dummy variable because type of %9 is incompatible
return %10 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :642 :12)

^3: // ExitBlock
cbde.unreachable

}
// Skipping function Flush(), it contains poisonous unsupported syntaxes

// Skipping function Update(none), it contains poisonous unsupported syntaxes

// Skipping function Check(none), it contains poisonous unsupported syntaxes

func @_kcp2k.Kcp.SetInterval$uint$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :979 :8) {
^entry (%_interval : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :979 :32)
cbde.store %_interval, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :979 :32)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :981 :16) // Not a variable of known type: interval
%2 = constant 5000 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :981 :27)
%3 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :981 :16) // comparison of unknown type: interval > 5000
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :981 :16)

^1: // SimpleBlock
%4 = constant 5000 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :981 :44)
br ^3

^2: // BinaryBranchBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :982 :21) // Not a variable of known type: interval
%6 = constant 10 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :982 :32)
%7 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :982 :21) // comparison of unknown type: interval < 10
cond_br %7, ^4, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :982 :21)

^4: // SimpleBlock
%8 = constant 10 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :982 :47)
br ^3

^3: // SimpleBlock
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :983 :12) // this (ThisExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :983 :12) // this.interval (SimpleMemberAccessExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :983 :28) // Not a variable of known type: interval
br ^5

^5: // ExitBlock
return

}
func @_kcp2k.Kcp.SetNoDelay$uint.uint.int.bool$(none, none, i32, i1) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :994 :8) {
^entry (%_nodelay : none, %_interval : none, %_resend : i32, %_nocwnd : i1):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :994 :31)
cbde.store %_nodelay, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :994 :31)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :994 :45)
cbde.store %_interval, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :994 :45)
%2 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :994 :71)
cbde.store %_resend, %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :994 :71)
%3 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :994 :87)
cbde.store %_nocwnd, %3 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :994 :87)
br ^0

^0: // BinaryBranchBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :996 :12) // this (ThisExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :996 :12) // this.nodelay (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :996 :27) // Not a variable of known type: nodelay
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :997 :16) // Not a variable of known type: nodelay
%8 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :997 :27)
%9 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :997 :16) // comparison of unknown type: nodelay != 0
cond_br %9, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :997 :16)

^1: // SimpleBlock
%10 = constant 30 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :999 :28)
br ^3

^2: // SimpleBlock
%11 = constant 100 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1003 :28)
br ^3

^3: // BinaryBranchBlock
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1006 :16) // Not a variable of known type: interval
%13 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1006 :28)
%14 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1006 :16) // comparison of unknown type: interval >= 0
cond_br %14, ^4, ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1006 :16)

^4: // BinaryBranchBlock
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1008 :20) // Not a variable of known type: interval
%16 = constant 5000 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1008 :31)
%17 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1008 :20) // comparison of unknown type: interval > 5000
cond_br %17, ^6, ^7 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1008 :20)

^6: // SimpleBlock
%18 = constant 5000 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1008 :48)
br ^8

^7: // BinaryBranchBlock
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1009 :25) // Not a variable of known type: interval
%20 = constant 10 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1009 :36)
%21 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1009 :25) // comparison of unknown type: interval < 10
cond_br %21, ^9, ^8 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1009 :25)

^9: // SimpleBlock
%22 = constant 10 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1009 :51)
br ^8

^8: // SimpleBlock
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1010 :16) // this (ThisExpression)
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1010 :16) // this.interval (SimpleMemberAccessExpression)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1010 :32) // Not a variable of known type: interval
br ^5

^5: // BinaryBranchBlock
%26 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1013 :16)
%27 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1013 :26)
%28 = cmpi "sge", %26, %27 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1013 :16)
cond_br %28, ^10, ^11 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1013 :16)

^10: // SimpleBlock
%29 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1015 :29)
br ^11

^11: // SimpleBlock
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1018 :12) // this (ThisExpression)
%31 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1018 :12) // this.nocwnd (SimpleMemberAccessExpression)
%32 = cbde.load %3 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1018 :26)
br ^12

^12: // ExitBlock
return

}
func @_kcp2k.Kcp.SetWindowSize$uint.uint$(none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1022 :8) {
^entry (%_sendWindow : none, %_receiveWindow : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1022 :34)
cbde.store %_sendWindow, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1022 :34)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1022 :51)
cbde.store %_receiveWindow, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1022 :51)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1024 :16) // Not a variable of known type: sendWindow
%3 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1024 :29)
%4 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1024 :16) // comparison of unknown type: sendWindow > 0
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1024 :16)

^1: // SimpleBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1026 :26) // Not a variable of known type: sendWindow
br ^2

^2: // BinaryBranchBlock
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1029 :16) // Not a variable of known type: receiveWindow
%7 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1029 :32)
%8 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1029 :16) // comparison of unknown type: receiveWindow > 0
cond_br %8, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1029 :16)

^3: // SimpleBlock
// Entity from another assembly: Math
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1032 :35) // Not a variable of known type: receiveWindow
%10 = constant 128 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1032 :50)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Kcp.cs" :1032 :26) // Math.Max(receiveWindow, WND_RCV) (InvocationExpression)
br ^4

^4: // ExitBlock
return

}
