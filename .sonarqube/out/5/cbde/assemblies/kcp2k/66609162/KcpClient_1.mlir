// Skipping function Connect(none, none, i1, none, i32, i1, none, none), it contains poisonous unsupported syntaxes

func @_kcp2k.KcpClient.Send$System.ArraySegment$byte$$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClient.cs" :68 :8) {
^entry (%_segment : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClient.cs" :68 :25)
cbde.store %_segment, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClient.cs" :68 :25)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClient.cs" :70 :16) // Not a variable of known type: connected
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClient.cs" :70 :16)

^1: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClient.cs" :72 :16) // Not a variable of known type: connection
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClient.cs" :72 :32) // Not a variable of known type: segment
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClient.cs" :72 :16) // connection.Send(segment) (InvocationExpression)
br ^3

^2: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Log
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClient.cs" :74 :17) // Log.Warning (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClient.cs" :74 :29) // "KCP: can't send because client not connected!" (StringLiteralExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClient.cs" :74 :17) // Log.Warning("KCP: can't send because client not connected!") (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function Disconnect(), it contains poisonous unsupported syntaxes

func @_kcp2k.KcpClient.Tick$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClient.cs" :91 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClient.cs" :94 :16) // Not a variable of known type: connection
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClient.cs" :94 :30) // null (NullLiteralExpression)
%2 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClient.cs" :94 :16) // comparison of unknown type: connection != null
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClient.cs" :94 :16)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClient.cs" :97 :16) // Not a variable of known type: connection
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClient.cs" :97 :16) // connection.RawReceive() (InvocationExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClient.cs" :99 :16) // Not a variable of known type: connection
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\highlevel\\KcpClient.cs" :99 :16) // connection.Tick() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
