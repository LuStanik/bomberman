func @_kcp2k.Utils.Clamp$int.int.int$(i32, i32, i32) -> i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :7 :8) {
^entry (%_value : i32, %_min : i32, %_max : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :7 :32)
cbde.store %_value, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :7 :32)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :7 :43)
cbde.store %_min, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :7 :43)
%2 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :7 :52)
cbde.store %_max, %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :7 :52)
br ^0

^0: // BinaryBranchBlock
%3 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :9 :16)
%4 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :9 :24)
%5 = cmpi "slt", %3, %4 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :9 :16)
cond_br %5, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :9 :16)

^1: // JumpBlock
%6 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :9 :36)
return %6 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :9 :29)

^2: // BinaryBranchBlock
%7 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :10 :16)
%8 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :10 :24)
%9 = cmpi "sgt", %7, %8 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :10 :16)
cond_br %9, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :10 :16)

^3: // JumpBlock
%10 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :10 :36)
return %10 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :10 :29)

^4: // JumpBlock
%11 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :11 :19)
return %11 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :11 :12)

^5: // ExitBlock
cbde.unreachable

}
func @_kcp2k.Utils.Encode8u$byte$$.int.byte$(none, i32, none) -> i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :15 :8) {
^entry (%_p : none, %_offset : i32, %_c : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :15 :35)
cbde.store %_p, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :15 :35)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :15 :45)
cbde.store %_offset, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :15 :45)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :15 :57)
cbde.store %_c, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :15 :57)
br ^0

^0: // JumpBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :17 :12) // Not a variable of known type: p
%4 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :17 :14)
%5 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :17 :18)
%6 = addi %4, %5 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :17 :14)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :17 :12) // p[0 + offset] (ElementAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :17 :28) // Not a variable of known type: c
%9 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :18 :19)
return %9 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :18 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_kcp2k.Utils.Decode8u$byte$$.int.refbyte$(none, i32, none) -> i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :22 :8) {
^entry (%_p : none, %_offset : i32, %_c : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :22 :35)
cbde.store %_p, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :22 :35)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :22 :45)
cbde.store %_offset, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :22 :45)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :22 :57)
cbde.store %_c, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :22 :57)
br ^0

^0: // JumpBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :24 :16) // Not a variable of known type: p
%4 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :24 :18)
%5 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :24 :22)
%6 = addi %4, %5 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :24 :18)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :24 :16) // p[0 + offset] (ElementAccessExpression)
%8 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :25 :19)
return %8 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :25 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_kcp2k.Utils.Encode16U$byte$$.int.ushort$(none, i32, none) -> i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :29 :8) {
^entry (%_p : none, %_offset : i32, %_w : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :29 :36)
cbde.store %_p, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :29 :36)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :29 :46)
cbde.store %_offset, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :29 :46)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :29 :58)
cbde.store %_w, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :29 :58)
br ^0

^0: // JumpBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :31 :12) // Not a variable of known type: p
%4 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :31 :14)
%5 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :31 :18)
%6 = addi %4, %5 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :31 :14)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :31 :12) // p[0 + offset] (ElementAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :31 :35) // Not a variable of known type: w
%9 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :31 :40)
%10 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :31 :35) // Binary expression on unsupported types w >> 0
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :31 :28) // (byte)(w >> 0) (CastExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :32 :12) // Not a variable of known type: p
%13 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :32 :14)
%14 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :32 :18)
%15 = addi %13, %14 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :32 :14)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :32 :12) // p[1 + offset] (ElementAccessExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :32 :35) // Not a variable of known type: w
%18 = constant 8 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :32 :40)
%19 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :32 :35) // Binary expression on unsupported types w >> 8
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :32 :28) // (byte)(w >> 8) (CastExpression)
%21 = constant 2 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :33 :19)
return %21 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :33 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_kcp2k.Utils.Decode16U$byte$$.int.refushort$(none, i32, none) -> i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :37 :8) {
^entry (%_p : none, %_offset : i32, %_c : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :37 :36)
cbde.store %_p, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :37 :36)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :37 :46)
cbde.store %_offset, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :37 :46)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :37 :58)
cbde.store %_c, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :37 :58)
br ^0

^0: // JumpBlock
%3 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :39 :28)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :40 :12) // Not a variable of known type: result
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :40 :22) // Not a variable of known type: p
%7 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :40 :24)
%8 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :40 :28)
%9 = addi %7, %8 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :40 :24)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :40 :22) // p[0 + offset] (ElementAccessExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :40 :12) // Binary expression on unsupported types result |= p[0 + offset]
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :41 :12) // Not a variable of known type: result
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :41 :31) // Not a variable of known type: p
%14 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :41 :33)
%15 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :41 :37)
%16 = addi %14, %15 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :41 :33)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :41 :31) // p[1 + offset] (ElementAccessExpression)
%18 = constant 8 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :41 :48)
%19 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :41 :31) // Binary expression on unsupported types p[1 + offset] << 8
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :41 :22) // (ushort)(p[1 + offset] << 8) (CastExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :41 :12) // Binary expression on unsupported types result |= (ushort)(p[1 + offset] << 8)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :42 :16) // Not a variable of known type: result
%23 = constant 2 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :43 :19)
return %23 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :43 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_kcp2k.Utils.Encode32U$byte$$.int.uint$(none, i32, none) -> i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :47 :8) {
^entry (%_p : none, %_offset : i32, %_l : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :47 :36)
cbde.store %_p, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :47 :36)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :47 :46)
cbde.store %_offset, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :47 :46)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :47 :58)
cbde.store %_l, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :47 :58)
br ^0

^0: // JumpBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :49 :12) // Not a variable of known type: p
%4 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :49 :14)
%5 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :49 :18)
%6 = addi %4, %5 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :49 :14)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :49 :12) // p[0 + offset] (ElementAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :49 :35) // Not a variable of known type: l
%9 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :49 :40)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :49 :35) // Binary expression on unsupported types l >> 0
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :49 :28) // (byte)(l >> 0) (CastExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :50 :12) // Not a variable of known type: p
%13 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :50 :14)
%14 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :50 :18)
%15 = addi %13, %14 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :50 :14)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :50 :12) // p[1 + offset] (ElementAccessExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :50 :35) // Not a variable of known type: l
%18 = constant 8 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :50 :40)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :50 :35) // Binary expression on unsupported types l >> 8
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :50 :28) // (byte)(l >> 8) (CastExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :51 :12) // Not a variable of known type: p
%22 = constant 2 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :51 :14)
%23 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :51 :18)
%24 = addi %22, %23 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :51 :14)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :51 :12) // p[2 + offset] (ElementAccessExpression)
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :51 :35) // Not a variable of known type: l
%27 = constant 16 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :51 :40)
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :51 :35) // Binary expression on unsupported types l >> 16
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :51 :28) // (byte)(l >> 16) (CastExpression)
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :52 :12) // Not a variable of known type: p
%31 = constant 3 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :52 :14)
%32 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :52 :18)
%33 = addi %31, %32 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :52 :14)
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :52 :12) // p[3 + offset] (ElementAccessExpression)
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :52 :35) // Not a variable of known type: l
%36 = constant 24 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :52 :40)
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :52 :35) // Binary expression on unsupported types l >> 24
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :52 :28) // (byte)(l >> 24) (CastExpression)
%39 = constant 4 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :53 :19)
return %39 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :53 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_kcp2k.Utils.Decode32U$byte$$.int.refuint$(none, i32, none) -> i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :57 :8) {
^entry (%_p : none, %_offset : i32, %_c : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :57 :36)
cbde.store %_p, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :57 :36)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :57 :46)
cbde.store %_offset, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :57 :46)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :57 :58)
cbde.store %_c, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :57 :58)
br ^0

^0: // JumpBlock
%3 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :59 :26)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :60 :12) // Not a variable of known type: result
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :60 :22) // Not a variable of known type: p
%7 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :60 :24)
%8 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :60 :28)
%9 = addi %7, %8 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :60 :24)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :60 :22) // p[0 + offset] (ElementAccessExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :60 :12) // Binary expression on unsupported types result |= p[0 + offset]
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :61 :12) // Not a variable of known type: result
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :61 :29) // Not a variable of known type: p
%14 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :61 :31)
%15 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :61 :35)
%16 = addi %14, %15 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :61 :31)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :61 :29) // p[1 + offset] (ElementAccessExpression)
%18 = constant 8 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :61 :46)
%19 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :61 :29) // Binary expression on unsupported types p[1 + offset] << 8
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :61 :22) // (uint)(p[1 + offset] << 8) (CastExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :61 :12) // Binary expression on unsupported types result |= (uint)(p[1 + offset] << 8)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :62 :12) // Not a variable of known type: result
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :62 :29) // Not a variable of known type: p
%24 = constant 2 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :62 :31)
%25 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :62 :35)
%26 = addi %24, %25 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :62 :31)
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :62 :29) // p[2 + offset] (ElementAccessExpression)
%28 = constant 16 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :62 :46)
%29 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :62 :29) // Binary expression on unsupported types p[2 + offset] << 16
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :62 :22) // (uint)(p[2 + offset] << 16) (CastExpression)
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :62 :12) // Binary expression on unsupported types result |= (uint)(p[2 + offset] << 16)
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :63 :12) // Not a variable of known type: result
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :63 :29) // Not a variable of known type: p
%34 = constant 3 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :63 :31)
%35 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :63 :35)
%36 = addi %34, %35 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :63 :31)
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :63 :29) // p[3 + offset] (ElementAccessExpression)
%38 = constant 24 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :63 :46)
%39 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :63 :29) // Binary expression on unsupported types p[3 + offset] << 24
%40 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :63 :22) // (uint)(p[3 + offset] << 24) (CastExpression)
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :63 :12) // Binary expression on unsupported types result |= (uint)(p[3 + offset] << 24)
%42 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :64 :16) // Not a variable of known type: result
%43 = constant 4 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :65 :19)
return %43 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :65 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_kcp2k.Utils.TimeDiff$uint.uint$(none, none) -> i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :69 :8) {
^entry (%_later : none, %_earlier : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :70 :35)
cbde.store %_later, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :70 :35)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :70 :47)
cbde.store %_earlier, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :70 :47)
br ^0

^0: // JumpBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :72 :25) // Not a variable of known type: later
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :72 :33) // Not a variable of known type: earlier
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :72 :25) // Binary expression on unsupported types later - earlier
%5 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :72 :19) // (int)(later - earlier) (CastExpression)
return %5 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\KCP\\kcp2k\\kcp\\Utils.cs" :72 :12)

^1: // ExitBlock
cbde.unreachable

}
