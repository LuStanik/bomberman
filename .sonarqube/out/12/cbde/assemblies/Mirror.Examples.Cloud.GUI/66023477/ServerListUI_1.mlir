func @_Mirror.Cloud.Example.ServerListUI.OnValidate$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListUI.cs" :16 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListUI.cs" :18 :16) // Not a variable of known type: parent
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListUI.cs" :18 :26) // null (NullLiteralExpression)
%2 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListUI.cs" :18 :16) // comparison of unknown type: parent == null
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListUI.cs" :18 :16)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListUI.cs" :20 :25) // Identifier from another assembly: transform
br ^2

^2: // ExitBlock
return

}
func @_Mirror.Cloud.Example.ServerListUI.UpdateList$Mirror.Cloud.ListServerService.ServerCollectionJson$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListUI.cs" :24 :8) {
^entry (%_serverCollection : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListUI.cs" :24 :31)
cbde.store %_serverCollection, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListUI.cs" :24 :31)
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: DeleteOldItems
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListUI.cs" :26 :12) // DeleteOldItems() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CreateNewItems
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListUI.cs" :27 :27) // Not a variable of known type: serverCollection
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListUI.cs" :27 :27) // serverCollection.servers (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListUI.cs" :27 :12) // CreateNewItems(serverCollection.servers) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function CreateNewItems(none), it contains poisonous unsupported syntaxes

// Skipping function DeleteOldItems(), it contains poisonous unsupported syntaxes

