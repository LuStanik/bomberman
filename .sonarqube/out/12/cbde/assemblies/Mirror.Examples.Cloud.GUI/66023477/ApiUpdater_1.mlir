func @_Mirror.Cloud.Example.ApiUpdater.Awake$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :17 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :19 :16) // Not a variable of known type: manager
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :19 :27) // null (NullLiteralExpression)
%2 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :19 :16) // comparison of unknown type: manager == null
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :19 :16)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :21 :26) // FindObjectOfType<NetworkManagerListServer>() (InvocationExpression)
br ^2

^2: // BinaryBranchBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :23 :16) // Not a variable of known type: connector
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :23 :29) // null (NullLiteralExpression)
%6 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :23 :16) // comparison of unknown type: connector == null
cond_br %6, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :23 :16)

^3: // SimpleBlock
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :25 :28) // Not a variable of known type: manager
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :25 :28) // manager.GetComponent<ApiConnector>() (InvocationExpression)
br ^4

^4: // SimpleBlock
// Entity from another assembly: Debug
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :28 :25) // Not a variable of known type: manager
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :28 :36) // null (NullLiteralExpression)
%11 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :28 :25) // comparison of unknown type: manager != null
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :28 :42) // "ApiUpdater could not find NetworkManagerListServer" (StringLiteralExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :28 :12) // Debug.Assert(manager != null, "ApiUpdater could not find NetworkManagerListServer") (InvocationExpression)
// Entity from another assembly: Debug
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :29 :25) // Not a variable of known type: connector
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :29 :38) // null (NullLiteralExpression)
%16 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :29 :25) // comparison of unknown type: connector != null
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :29 :44) // "ApiUpdater could not find ApiConnector" (StringLiteralExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :29 :12) // Debug.Assert(connector != null, "ApiUpdater could not find ApiConnector") (InvocationExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :31 :12) // Not a variable of known type: manager
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :31 :12) // manager.onPlayerListChanged (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: onPlayerListChanged
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :31 :12) // Binary expression on unsupported types manager.onPlayerListChanged += onPlayerListChanged
// No identifier name for binary assignment expression
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :32 :12) // Not a variable of known type: manager
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :32 :12) // manager.onServerStarted (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ServerStartedHandler
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :32 :12) // Binary expression on unsupported types manager.onServerStarted += ServerStartedHandler
// No identifier name for binary assignment expression
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :33 :12) // Not a variable of known type: manager
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :33 :12) // manager.onServerStopped (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ServerStoppedHandler
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :33 :12) // Binary expression on unsupported types manager.onServerStopped += ServerStoppedHandler
// No identifier name for binary assignment expression
br ^5

^5: // ExitBlock
return

}
func @_Mirror.Cloud.Example.ApiUpdater.OnDestroy$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :36 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :38 :16) // Not a variable of known type: manager
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :38 :27) // null (NullLiteralExpression)
%2 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :38 :16) // comparison of unknown type: manager != null
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :38 :16)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :40 :16) // Not a variable of known type: manager
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :40 :16) // manager.onPlayerListChanged (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: onPlayerListChanged
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :40 :16) // Binary expression on unsupported types manager.onPlayerListChanged -= onPlayerListChanged
// No identifier name for binary assignment expression
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :41 :16) // Not a variable of known type: manager
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :41 :16) // manager.onServerStarted (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ServerStartedHandler
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :41 :16) // Binary expression on unsupported types manager.onServerStarted -= ServerStartedHandler
// No identifier name for binary assignment expression
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :42 :16) // Not a variable of known type: manager
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :42 :16) // manager.onServerStopped (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ServerStoppedHandler
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :42 :16) // Binary expression on unsupported types manager.onServerStopped -= ServerStoppedHandler
// No identifier name for binary assignment expression
br ^2

^2: // ExitBlock
return

}
func @_Mirror.Cloud.Example.ApiUpdater.onPlayerListChanged$int$(i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :46 :8) {
^entry (%_playerCount : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :46 :33)
cbde.store %_playerCount, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :46 :33)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :48 :16) // Not a variable of known type: connector
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :48 :16) // connector.ListServer (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :48 :16) // connector.ListServer.ServerApi (SimpleMemberAccessExpression)
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :48 :16) // connector.ListServer.ServerApi.ServerInList (SimpleMemberAccessExpression)
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :48 :16)

^1: // BinaryBranchBlock
%5 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :51 :20)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :51 :34) // Not a variable of known type: manager
%7 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :51 :34) // manager.maxConnections (SimpleMemberAccessExpression)
%8 = cmpi "slt", %5, %7 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :51 :20)
cond_br %8, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :51 :20)

^3: // BinaryBranchBlock
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :53 :24) // Not a variable of known type: logger
%10 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :53 :24) // logger.LogEnabled() (InvocationExpression)
cond_br %10, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :53 :24)

^5: // SimpleBlock
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :53 :45) // Not a variable of known type: logger
%12 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :53 :90)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :53 :56) // $"Updating Server, player count: {playerCount} " (InterpolatedStringExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :53 :45) // logger.Log($"Updating Server, player count: {playerCount} ") (InvocationExpression)
br ^6

^6: // SimpleBlock
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :54 :20) // Not a variable of known type: connector
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :54 :20) // connector.ListServer (SimpleMemberAccessExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :54 :20) // connector.ListServer.ServerApi (SimpleMemberAccessExpression)
%18 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :54 :64)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :54 :20) // connector.ListServer.ServerApi.UpdateServer(playerCount) (InvocationExpression)
br ^7

^4: // BinaryBranchBlock
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :59 :24) // Not a variable of known type: logger
%21 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :59 :24) // logger.LogEnabled() (InvocationExpression)
cond_br %21, ^8, ^9 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :59 :24)

^8: // SimpleBlock
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :59 :45) // Not a variable of known type: logger
%23 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :59 :90)
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :59 :56) // $"Removing Server, player count: {playerCount}" (InterpolatedStringExpression)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :59 :45) // logger.Log($"Removing Server, player count: {playerCount}") (InvocationExpression)
br ^9

^9: // SimpleBlock
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :60 :20) // Not a variable of known type: connector
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :60 :20) // connector.ListServer (SimpleMemberAccessExpression)
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :60 :20) // connector.ListServer.ServerApi (SimpleMemberAccessExpression)
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :60 :20) // connector.ListServer.ServerApi.RemoveServer() (InvocationExpression)
br ^7

^2: // BinaryBranchBlock
%30 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :66 :20)
%31 = constant 2 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :66 :34)
%32 = cmpi "slt", %30, %31 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :66 :20)
cond_br %32, ^10, ^7 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :66 :20)

^10: // BinaryBranchBlock
%33 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :68 :24) // Not a variable of known type: logger
%34 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :68 :24) // logger.LogEnabled() (InvocationExpression)
cond_br %34, ^11, ^12 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :68 :24)

^11: // SimpleBlock
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :68 :45) // Not a variable of known type: logger
%36 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :68 :88)
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :68 :56) // $"Adding Server, player count: {playerCount}" (InterpolatedStringExpression)
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :68 :45) // logger.Log($"Adding Server, player count: {playerCount}") (InvocationExpression)
br ^12

^12: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: AddServer
%39 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :69 :30)
%40 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :69 :20) // AddServer(playerCount) (InvocationExpression)
br ^7

^7: // ExitBlock
return

}
func @_Mirror.Cloud.Example.ApiUpdater.ServerStartedHandler$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :74 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: AddServer
%0 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :76 :22)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :76 :12) // AddServer(0) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Cloud.Example.ApiUpdater.AddServer$int$(i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :79 :8) {
^entry (%_playerCount : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :79 :23)
cbde.store %_playerCount, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :79 :23)
br ^0

^0: // SimpleBlock
// Entity from another assembly: Transport
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :81 :34) // Transport.activeTransport (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :83 :22) // Not a variable of known type: transport
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :83 :22) // transport.ServerUri() (InvocationExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :84 :23) // Not a variable of known type: uri
%7 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :84 :23) // uri.Port (SimpleMemberAccessExpression)
%8 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :84 :16) // port
cbde.store %7, %8 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :84 :16)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :85 :30) // Not a variable of known type: uri
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :85 :30) // uri.Scheme (SimpleMemberAccessExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :87 :12) // Not a variable of known type: connector
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :87 :12) // connector.ListServer (SimpleMemberAccessExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :87 :12) // connector.ListServer.ServerApi (SimpleMemberAccessExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :87 :53) // new ServerJson             {                 displayName = $"{gameName} {(UnityEngine.Random.value * 1000).ToString("0")}",                 protocol = protocol,                 port = port,                 maxPlayerCount = NetworkManager.singleton.maxConnections,                 playerCount = playerCount             } (ObjectCreationExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :89 :33) // Not a variable of known type: gameName
// Entity from another assembly: UnityEngine
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :89 :45) // UnityEngine.Random (SimpleMemberAccessExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :89 :45) // UnityEngine.Random.value (SimpleMemberAccessExpression)
%19 = constant 1000 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :89 :72)
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :89 :45) // Binary expression on unsupported types UnityEngine.Random.value * 1000
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :89 :87) // "0" (StringLiteralExpression)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :89 :44) // (UnityEngine.Random.value * 1000).ToString("0") (InvocationExpression)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :89 :30) // $"{gameName} {(UnityEngine.Random.value * 1000).ToString("0")}" (InterpolatedStringExpression)
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :90 :27) // Not a variable of known type: protocol
%25 = cbde.load %8 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :91 :23)
// Entity from another assembly: NetworkManager
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :92 :33) // NetworkManager.singleton (SimpleMemberAccessExpression)
%27 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :92 :33) // NetworkManager.singleton.maxConnections (SimpleMemberAccessExpression)
%28 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :93 :30)
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :87 :12) // connector.ListServer.ServerApi.AddServer(new ServerJson             {                 displayName = $"{gameName} {(UnityEngine.Random.value * 1000).ToString("0")}",                 protocol = protocol,                 port = port,                 maxPlayerCount = NetworkManager.singleton.maxConnections,                 playerCount = playerCount             }) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Cloud.Example.ApiUpdater.ServerStoppedHandler$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :97 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :99 :12) // Not a variable of known type: connector
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :99 :12) // connector.ListServer (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :99 :12) // connector.ListServer.ServerApi (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ApiUpdater.cs" :99 :12) // connector.ListServer.ServerApi.RemoveServer() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
