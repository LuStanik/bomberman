func @_Mirror.Cloud.Example.QuickListServerDebug.Start$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuickListServerDebug.cs" :13 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: NetworkManager
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuickListServerDebug.cs" :15 :37) // NetworkManager.singleton (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuickListServerDebug.cs" :16 :24) // Not a variable of known type: manager
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuickListServerDebug.cs" :16 :24) // manager.GetComponent<ApiConnector>() (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuickListServerDebug.cs" :18 :12) // Not a variable of known type: connector
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuickListServerDebug.cs" :18 :12) // connector.ListServer (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuickListServerDebug.cs" :18 :12) // connector.ListServer.ClientApi (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuickListServerDebug.cs" :18 :12) // connector.ListServer.ClientApi.onServerListUpdated (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ClientApi_onServerListUpdated
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuickListServerDebug.cs" :18 :12) // Binary expression on unsupported types connector.ListServer.ClientApi.onServerListUpdated += ClientApi_onServerListUpdated
// No identifier name for binary assignment expression
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Cloud.Example.QuickListServerDebug.ClientApi_onServerListUpdated$Mirror.Cloud.ListServerService.ServerCollectionJson$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuickListServerDebug.cs" :21 :8) {
^entry (%_arg0 : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuickListServerDebug.cs" :21 :51)
cbde.store %_arg0, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuickListServerDebug.cs" :21 :51)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuickListServerDebug.cs" :23 :25) // Not a variable of known type: arg0
br ^1

^1: // ExitBlock
return

}
// Skipping function OnGUI(), it contains poisonous unsupported syntaxes

