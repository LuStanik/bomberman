func @_Mirror.Cloud.Examples.InstantiateNetworkManager.Awake$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\InstantiateNetworkManager.cs" :11 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: NetworkManager
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\InstantiateNetworkManager.cs" :13 :16) // NetworkManager.singleton (SimpleMemberAccessExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\InstantiateNetworkManager.cs" :13 :44) // null (NullLiteralExpression)
%2 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\InstantiateNetworkManager.cs" :13 :16) // comparison of unknown type: NetworkManager.singleton == null
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\InstantiateNetworkManager.cs" :13 :16)

^1: // SimpleBlock
// Entity from another assembly: Instantiate
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\InstantiateNetworkManager.cs" :15 :28) // Not a variable of known type: prefab
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\InstantiateNetworkManager.cs" :15 :16) // Instantiate(prefab) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
