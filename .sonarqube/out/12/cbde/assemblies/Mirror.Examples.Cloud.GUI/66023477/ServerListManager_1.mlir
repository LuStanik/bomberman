func @_Mirror.Cloud.Example.ServerListManager.Start$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :24 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: NetworkManager
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :26 :37) // NetworkManager.singleton (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :27 :24) // Not a variable of known type: manager
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :27 :24) // manager.GetComponent<ApiConnector>() (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :29 :12) // Not a variable of known type: connector
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :29 :12) // connector.ListServer (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :29 :12) // connector.ListServer.ClientApi (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :29 :12) // connector.ListServer.ClientApi.onServerListUpdated (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :29 :66) // Not a variable of known type: listUI
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :29 :12) // Binary expression on unsupported types connector.ListServer.ClientApi.onServerListUpdated += listUI.UpdateList
// No identifier name for binary assignment expression
%10 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :31 :16) // Not a variable of known type: autoRefreshServerlist
cond_br %10, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :31 :16)

^1: // SimpleBlock
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :33 :16) // Not a variable of known type: connector
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :33 :16) // connector.ListServer (SimpleMemberAccessExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :33 :16) // connector.ListServer.ClientApi (SimpleMemberAccessExpression)
%14 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :33 :72) // Not a variable of known type: refreshinterval
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :33 :16) // connector.ListServer.ClientApi.StartGetServerListRepeat(refreshinterval) (InvocationExpression)
br ^2

^2: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: AddButtonHandlers
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :36 :12) // AddButtonHandlers() (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Cloud.Example.ServerListManager.AddButtonHandlers$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :39 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :41 :12) // Not a variable of known type: refreshButton
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :41 :12) // refreshButton.onClick (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: RefreshButtonHandler
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :41 :12) // refreshButton.onClick.AddListener(RefreshButtonHandler) (InvocationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :42 :12) // Not a variable of known type: startServerButton
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :42 :12) // startServerButton.onClick (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: StartServerButtonHandler
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :42 :12) // startServerButton.onClick.AddListener(StartServerButtonHandler) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Cloud.Example.ServerListManager.OnDestroy$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :45 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :47 :16) // Not a variable of known type: connector
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :47 :29) // null (NullLiteralExpression)
%2 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :47 :16) // comparison of unknown type: connector == null
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :47 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :48 :16)

^2: // BinaryBranchBlock
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :50 :16) // Not a variable of known type: autoRefreshServerlist
cond_br %3, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :50 :16)

^3: // SimpleBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :52 :16) // Not a variable of known type: connector
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :52 :16) // connector.ListServer (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :52 :16) // connector.ListServer.ClientApi (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :52 :16) // connector.ListServer.ClientApi.StopGetServerListRepeat() (InvocationExpression)
br ^4

^4: // SimpleBlock
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :55 :12) // Not a variable of known type: connector
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :55 :12) // connector.ListServer (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :55 :12) // connector.ListServer.ClientApi (SimpleMemberAccessExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :55 :12) // connector.ListServer.ClientApi.onServerListUpdated (SimpleMemberAccessExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :55 :66) // Not a variable of known type: listUI
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :55 :12) // Binary expression on unsupported types connector.ListServer.ClientApi.onServerListUpdated -= listUI.UpdateList
// No identifier name for binary assignment expression
br ^5

^5: // ExitBlock
return

}
func @_Mirror.Cloud.Example.ServerListManager.RefreshButtonHandler$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :58 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :60 :12) // Not a variable of known type: connector
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :60 :12) // connector.ListServer (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :60 :12) // connector.ListServer.ClientApi (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :60 :12) // connector.ListServer.ClientApi.GetServerList() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Cloud.Example.ServerListManager.StartServerButtonHandler$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :63 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: NetworkManager
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :65 :12) // NetworkManager.singleton (SimpleMemberAccessExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\ServerListManager.cs" :65 :12) // NetworkManager.singleton.StartServer() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
