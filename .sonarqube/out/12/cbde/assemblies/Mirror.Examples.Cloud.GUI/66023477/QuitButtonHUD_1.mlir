func @_Mirror.Examples.Pong.QuitButtonHUD.OnGUI$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :9 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: NetworkManager
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :11 :37) // NetworkManager.singleton (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :12 :16) // Not a variable of known type: manager
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :12 :27) // null (NullLiteralExpression)
%4 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :12 :16) // comparison of unknown type: manager == null
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :12 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :13 :16)

^2: // BinaryBranchBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :15 :16) // Not a variable of known type: manager
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :15 :16) // manager.mode (SimpleMemberAccessExpression)
// Entity from another assembly: NetworkManagerMode
%7 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :15 :32) // NetworkManagerMode.ServerOnly (SimpleMemberAccessExpression)
%8 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :15 :16) // comparison of unknown type: manager.mode == NetworkManagerMode.ServerOnly
cond_br %8, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :15 :16)

^3: // BinaryBranchBlock
// Entity from another assembly: GUILayout
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :17 :37) // "Stop Server" (StringLiteralExpression)
%10 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :17 :20) // GUILayout.Button("Stop Server") (InvocationExpression)
cond_br %10, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :17 :20)

^5: // SimpleBlock
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :19 :20) // Not a variable of known type: manager
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :19 :20) // manager.StopServer() (InvocationExpression)
br ^6

^4: // BinaryBranchBlock
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :22 :21) // Not a variable of known type: manager
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :22 :21) // manager.mode (SimpleMemberAccessExpression)
// Entity from another assembly: NetworkManagerMode
%15 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :22 :37) // NetworkManagerMode.Host (SimpleMemberAccessExpression)
%16 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :22 :21) // comparison of unknown type: manager.mode == NetworkManagerMode.Host
cond_br %16, ^7, ^8 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :22 :21)

^7: // BinaryBranchBlock
// Entity from another assembly: GUILayout
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :24 :37) // "Stop Host" (StringLiteralExpression)
%18 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :24 :20) // GUILayout.Button("Stop Host") (InvocationExpression)
cond_br %18, ^9, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :24 :20)

^9: // SimpleBlock
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :26 :20) // Not a variable of known type: manager
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :26 :20) // manager.StopHost() (InvocationExpression)
br ^6

^8: // BinaryBranchBlock
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :29 :21) // Not a variable of known type: manager
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :29 :21) // manager.mode (SimpleMemberAccessExpression)
// Entity from another assembly: NetworkManagerMode
%23 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :29 :37) // NetworkManagerMode.ClientOnly (SimpleMemberAccessExpression)
%24 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :29 :21) // comparison of unknown type: manager.mode == NetworkManagerMode.ClientOnly
cond_br %24, ^10, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :29 :21)

^10: // BinaryBranchBlock
// Entity from another assembly: GUILayout
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :31 :37) // "Stop Client" (StringLiteralExpression)
%26 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :31 :20) // GUILayout.Button("Stop Client") (InvocationExpression)
cond_br %26, ^11, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :31 :20)

^11: // SimpleBlock
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :33 :20) // Not a variable of known type: manager
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Examples\\Cloud\\GUI\\Scripts\\QuitButtonHUD.cs" :33 :20) // manager.StopClient() (InvocationExpression)
br ^6

^6: // ExitBlock
return

}
