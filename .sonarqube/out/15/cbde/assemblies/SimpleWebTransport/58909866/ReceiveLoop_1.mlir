func @_Mirror.SimpleWeb.ReceiveLoop.Config.Deconstruct$outMirror.SimpleWeb.Connection.outint.outbool.outSystem.Collections.Concurrent.ConcurrentQueue$Mirror.SimpleWeb.Message$.outMirror.SimpleWeb.BufferPool$(none, i32, i1, none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :29 :12) {
^entry (%_conn : none, %_maxMessageSize : i32, %_expectMask : i1, %_queue : none, %_bufferPool : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :29 :36)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :29 :36)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :29 :57)
cbde.store %_maxMessageSize, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :29 :57)
%2 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :29 :81)
cbde.store %_expectMask, %2 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :29 :81)
%3 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :29 :102)
cbde.store %_queue, %3 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :29 :102)
%4 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :29 :138)
cbde.store %_bufferPool, %4 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :29 :138)
br ^0

^0: // SimpleBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :31 :23) // this (ThisExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :31 :23) // this.conn (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :32 :33) // this (ThisExpression)
%8 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :32 :33) // this.maxMessageSize (SimpleMemberAccessExpression)
cbde.store %8, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :32 :16)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :33 :29) // this (ThisExpression)
%10 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :33 :29) // this.expectMask (SimpleMemberAccessExpression)
cbde.store %10, %2 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :33 :16)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :34 :24) // this (ThisExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :34 :24) // this.queue (SimpleMemberAccessExpression)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :35 :29) // this (ThisExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :35 :29) // this.bufferPool (SimpleMemberAccessExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function Loop(none), it contains poisonous unsupported syntaxes

// Skipping function ReadOneMessage(none, none), it contains poisonous unsupported syntaxes

// Skipping function HandleArrayMessage(none, none, i32, i32), it contains poisonous unsupported syntaxes

// Skipping function HandleCloseMessage(none, none, i32, i32), it contains poisonous unsupported syntaxes

func @_Mirror.SimpleWeb.ReceiveLoop.GetCloseMessage$byte$$.int.int$(none, i32, i32) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :189 :8) {
^entry (%_buffer : none, %_msgOffset : i32, %_payloadLength : i32):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :189 :38)
cbde.store %_buffer, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :189 :38)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :189 :53)
cbde.store %_msgOffset, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :189 :53)
%2 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :189 :68)
cbde.store %_payloadLength, %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :189 :68)
br ^0

^0: // JumpBlock
// Entity from another assembly: Encoding
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :191 :19) // Encoding.UTF8 (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :191 :43) // Not a variable of known type: buffer
%5 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :191 :51)
%6 = constant 2 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :191 :63)
%7 = addi %5, %6 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :191 :51)
%8 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :191 :66)
%9 = constant 2 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :191 :82)
%10 = subi %8, %9 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :191 :66)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :191 :19) // Encoding.UTF8.GetString(buffer, msgOffset + 2, payloadLength - 2) (InvocationExpression)
return %11 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :191 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.SimpleWeb.ReceiveLoop.GetCloseCode$byte$$.int$(none, i32) -> i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :194 :8) {
^entry (%_buffer : none, %_msgOffset : i32):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :194 :32)
cbde.store %_buffer, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :194 :32)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :194 :47)
cbde.store %_msgOffset, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :194 :47)
br ^0

^0: // JumpBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :196 :19) // Not a variable of known type: buffer
%3 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :196 :26)
%4 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :196 :38)
%5 = addi %3, %4 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :196 :26)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :196 :19) // buffer[msgOffset + 0] (ElementAccessExpression)
%7 = constant 8 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :196 :44)
%8 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :196 :19) // Binary expression on unsupported types buffer[msgOffset + 0] << 8
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :196 :48) // Not a variable of known type: buffer
%10 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :196 :55)
%11 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :196 :67)
%12 = addi %10, %11 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :196 :55)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :196 :48) // buffer[msgOffset + 1] (ElementAccessExpression)
%14 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :196 :19) // Binary expression on unsupported types buffer[msgOffset + 0] << 8 | buffer[msgOffset + 1]
return %14 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\ReceiveLoop.cs" :196 :12)

^1: // ExitBlock
cbde.unreachable

}
