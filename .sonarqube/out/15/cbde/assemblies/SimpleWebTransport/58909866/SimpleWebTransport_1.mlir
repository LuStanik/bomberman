func @_Mirror.SimpleWeb.SimpleWebTransport.OnValidate$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :77 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :79 :16) // Not a variable of known type: maxMessageSize
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :79 :33) // ushort (PredefinedType)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :79 :33) // ushort.MaxValue (SimpleMemberAccessExpression)
%3 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :79 :16) // comparison of unknown type: maxMessageSize > ushort.MaxValue
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :79 :16)

^1: // SimpleBlock
// Entity from another assembly: Debug
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :81 :78) // ushort (PredefinedType)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :81 :78) // ushort.MaxValue (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :81 :33) // $"max supported value for maxMessageSize is {ushort.MaxValue}" (InterpolatedStringExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :81 :16) // Debug.LogWarning($"max supported value for maxMessageSize is {ushort.MaxValue}") (InvocationExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :82 :33) // ushort (PredefinedType)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :82 :33) // ushort.MaxValue (SimpleMemberAccessExpression)
br ^2

^2: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Log
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :85 :12) // Log.level (SimpleMemberAccessExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :85 :24) // Not a variable of known type: _logLevels
br ^3

^3: // ExitBlock
return

}
func @_Mirror.SimpleWeb.SimpleWebTransport.Available$$() -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :93 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :95 :19) // true
return %0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :95 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.SimpleWeb.SimpleWebTransport.GetMaxPacketSize$int$(i32) -> i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :97 :8) {
^entry (%_channelId : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :97 :45)
cbde.store %_channelId, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :97 :45)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :99 :19) // Not a variable of known type: maxMessageSize
return %1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :99 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.SimpleWeb.SimpleWebTransport.Awake$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :102 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Log
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :104 :12) // Log.level (SimpleMemberAccessExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :104 :24) // Not a variable of known type: _logLevels
br ^1

^1: // ExitBlock
return

}
// Skipping function Shutdown(), it contains poisonous unsupported syntaxes

func @_Mirror.SimpleWeb.SimpleWebTransport.LateUpdate$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :114 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ProcessMessages
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :116 :12) // ProcessMessages() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function ProcessMessages(), it contains poisonous unsupported syntaxes

// Skipping function ClientConnected(), it contains poisonous unsupported syntaxes

// Skipping function ClientConnect(none), it contains poisonous unsupported syntaxes

// Skipping function ClientDisconnect(), it contains poisonous unsupported syntaxes

func @_Mirror.SimpleWeb.SimpleWebTransport.ClientSend$int.System.ArraySegment$byte$$(i32, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :183 :8) {
^entry (%_channelId : i32, %_segment : none):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :183 :40)
cbde.store %_channelId, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :183 :40)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :183 :55)
cbde.store %_segment, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :183 :55)
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ClientConnected
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :185 :17) // ClientConnected() (InvocationExpression)
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :185 :16) // !ClientConnected() (LogicalNotExpression)
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :185 :16)

^1: // JumpBlock
// Entity from another assembly: Debug
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :187 :31) // "Not Connected" (StringLiteralExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :187 :16) // Debug.LogError("Not Connected") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :188 :16)

^2: // BinaryBranchBlock
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :191 :16) // Not a variable of known type: segment
%7 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :191 :16) // segment.Count (SimpleMemberAccessExpression)
%8 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :191 :32) // Not a variable of known type: maxMessageSize
%9 = cmpi "sgt", %7, %8 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :191 :16)
cond_br %9, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :191 :16)

^3: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Log
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :193 :26) // "Message greater than max size" (StringLiteralExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :193 :16) // Log.Error("Message greater than max size") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :194 :16)

^4: // BinaryBranchBlock
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :197 :16) // Not a variable of known type: segment
%13 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :197 :16) // segment.Count (SimpleMemberAccessExpression)
%14 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :197 :33)
%15 = cmpi "eq", %13, %14 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :197 :16)
cond_br %15, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :197 :16)

^5: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Log
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :199 :26) // "Message count was zero" (StringLiteralExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :199 :16) // Log.Error("Message count was zero") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :200 :16)

^6: // SimpleBlock
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :203 :12) // Not a variable of known type: client
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :203 :24) // Not a variable of known type: segment
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :203 :12) // client.Send(segment) (InvocationExpression)
br ^7

^7: // ExitBlock
return

}
// Skipping function ServerActive(), it contains poisonous unsupported syntaxes

// Skipping function ServerStart(), it contains poisonous unsupported syntaxes

func @_Mirror.SimpleWeb.SimpleWebTransport.ServerStop$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :234 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ServerActive
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :236 :17) // ServerActive() (InvocationExpression)
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :236 :16) // !ServerActive() (LogicalNotExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :236 :16)

^1: // SimpleBlock
// Entity from another assembly: Debug
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :238 :31) // "SimpleWebServer Not Active" (StringLiteralExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :238 :16) // Debug.LogError("SimpleWebServer Not Active") (InvocationExpression)
br ^2

^2: // SimpleBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :241 :12) // Not a variable of known type: server
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :241 :12) // server.Stop() (InvocationExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :242 :21) // null (NullLiteralExpression)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.SimpleWeb.SimpleWebTransport.ServerDisconnect$int$(i32) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :245 :8) {
^entry (%_connectionId : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :245 :46)
cbde.store %_connectionId, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :245 :46)
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ServerActive
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :247 :17) // ServerActive() (InvocationExpression)
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :247 :16) // !ServerActive() (LogicalNotExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :247 :16)

^1: // JumpBlock
// Entity from another assembly: Debug
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :249 :31) // "SimpleWebServer Not Active" (StringLiteralExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :249 :16) // Debug.LogError("SimpleWebServer Not Active") (InvocationExpression)
%5 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :250 :23) // false
return %5 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :250 :16)

^2: // JumpBlock
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :253 :19) // Not a variable of known type: server
%7 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :253 :37)
%8 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :253 :19) // server.KickClient(connectionId) (InvocationExpression)
return %8 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :253 :12)

^3: // ExitBlock
cbde.unreachable

}
func @_Mirror.SimpleWeb.SimpleWebTransport.ServerSend$int.int.System.ArraySegment$byte$$(i32, i32, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :256 :8) {
^entry (%_connectionId : i32, %_channelId : i32, %_segment : none):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :256 :40)
cbde.store %_connectionId, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :256 :40)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :256 :58)
cbde.store %_channelId, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :256 :58)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :256 :73)
cbde.store %_segment, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :256 :73)
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ServerActive
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :258 :17) // ServerActive() (InvocationExpression)
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :258 :16) // !ServerActive() (LogicalNotExpression)
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :258 :16)

^1: // JumpBlock
// Entity from another assembly: Debug
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :260 :31) // "SimpleWebServer Not Active" (StringLiteralExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :260 :16) // Debug.LogError("SimpleWebServer Not Active") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :261 :16)

^2: // BinaryBranchBlock
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :264 :16) // Not a variable of known type: segment
%8 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :264 :16) // segment.Count (SimpleMemberAccessExpression)
%9 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :264 :32) // Not a variable of known type: maxMessageSize
%10 = cmpi "sgt", %8, %9 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :264 :16)
cond_br %10, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :264 :16)

^3: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Log
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :266 :26) // "Message greater than max size" (StringLiteralExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :266 :16) // Log.Error("Message greater than max size") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :267 :16)

^4: // BinaryBranchBlock
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :270 :16) // Not a variable of known type: segment
%14 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :270 :16) // segment.Count (SimpleMemberAccessExpression)
%15 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :270 :33)
%16 = cmpi "eq", %14, %15 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :270 :16)
cond_br %16, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :270 :16)

^5: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Log
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :272 :26) // "Message count was zero" (StringLiteralExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :272 :16) // Log.Error("Message count was zero") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :273 :16)

^6: // JumpBlock
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :276 :12) // Not a variable of known type: server
%20 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :276 :27)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :276 :41) // Not a variable of known type: segment
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :276 :12) // server.SendOne(connectionId, segment) (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :277 :12)

^7: // ExitBlock
return

}
func @_Mirror.SimpleWeb.SimpleWebTransport.ServerGetClientAddress$int$(i32) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :280 :8) {
^entry (%_connectionId : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :280 :54)
cbde.store %_connectionId, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :280 :54)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :282 :19) // Not a variable of known type: server
%2 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :282 :43)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :282 :19) // server.GetClientAddress(connectionId) (InvocationExpression)
return %3 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :282 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.SimpleWeb.SimpleWebTransport.ServerUri$$() -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :285 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :287 :33) // new UriBuilder             {                 Scheme = GetServerScheme(),                 Host = Dns.GetHostName(),                 Port = port             } (ObjectCreationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetServerScheme
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :289 :25) // GetServerScheme() (InvocationExpression)
// Entity from another assembly: Dns
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :290 :23) // Dns.GetHostName() (InvocationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :291 :23) // Not a variable of known type: port
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :293 :19) // Not a variable of known type: builder
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :293 :19) // builder.Uri (SimpleMemberAccessExpression)
return %6 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\SimpleWebTransport.cs" :293 :12)

^1: // ExitBlock
cbde.unreachable

}
