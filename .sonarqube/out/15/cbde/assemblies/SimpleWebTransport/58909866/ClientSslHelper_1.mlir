// Skipping function TryCreateStream(none, none), it contains poisonous unsupported syntaxes

func @_Mirror.SimpleWeb.ClientSslHelper.CreateStream$System.Net.Sockets.NetworkStream.System.Uri$(none, none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\StandAlone\\ClientSslHelper.cs" :31 :8) {
^entry (%_stream : none, %_uri : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\StandAlone\\ClientSslHelper.cs" :31 :28)
cbde.store %_stream, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\StandAlone\\ClientSslHelper.cs" :31 :28)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\StandAlone\\ClientSslHelper.cs" :31 :50)
cbde.store %_uri, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\StandAlone\\ClientSslHelper.cs" :31 :50)
br ^0

^0: // JumpBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\StandAlone\\ClientSslHelper.cs" :33 :48) // Not a variable of known type: stream
%3 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\StandAlone\\ClientSslHelper.cs" :33 :56) // true
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ValidateServerCertificate
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\StandAlone\\ClientSslHelper.cs" :33 :34) // new SslStream(stream, true, ValidateServerCertificate) (ObjectCreationExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\StandAlone\\ClientSslHelper.cs" :34 :12) // Not a variable of known type: sslStream
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\StandAlone\\ClientSslHelper.cs" :34 :43) // Not a variable of known type: uri
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\StandAlone\\ClientSslHelper.cs" :34 :43) // uri.Host (SimpleMemberAccessExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\StandAlone\\ClientSslHelper.cs" :34 :12) // sslStream.AuthenticateAsClient(uri.Host) (InvocationExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\StandAlone\\ClientSslHelper.cs" :35 :19) // Not a variable of known type: sslStream
return %10 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\StandAlone\\ClientSslHelper.cs" :35 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.SimpleWeb.ClientSslHelper.ValidateServerCertificate$object.System.Security.Cryptography.X509Certificates.X509Certificate.System.Security.Cryptography.X509Certificates.X509Chain.System.Net.Security.SslPolicyErrors$(none, none, none, none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\StandAlone\\ClientSslHelper.cs" :38 :8) {
^entry (%_sender : none, %_certificate : none, %_chain : none, %_sslPolicyErrors : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\StandAlone\\ClientSslHelper.cs" :38 :46)
cbde.store %_sender, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\StandAlone\\ClientSslHelper.cs" :38 :46)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\StandAlone\\ClientSslHelper.cs" :38 :61)
cbde.store %_certificate, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\StandAlone\\ClientSslHelper.cs" :38 :61)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\StandAlone\\ClientSslHelper.cs" :38 :90)
cbde.store %_chain, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\StandAlone\\ClientSslHelper.cs" :38 :90)
%3 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\StandAlone\\ClientSslHelper.cs" :38 :107)
cbde.store %_sslPolicyErrors, %3 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\StandAlone\\ClientSslHelper.cs" :38 :107)
br ^0

^0: // JumpBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\StandAlone\\ClientSslHelper.cs" :43 :19) // Not a variable of known type: sslPolicyErrors
// Entity from another assembly: SslPolicyErrors
%5 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\StandAlone\\ClientSslHelper.cs" :43 :38) // SslPolicyErrors.None (SimpleMemberAccessExpression)
%6 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\StandAlone\\ClientSslHelper.cs" :43 :19) // comparison of unknown type: sslPolicyErrors == SslPolicyErrors.None
return %6 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\StandAlone\\ClientSslHelper.cs" :43 :12)

^1: // ExitBlock
cbde.unreachable

}
