func @_Mirror.SimpleWeb.SimpleWebServer.Start$ushort$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :30 :8) {
^entry (%_port : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :30 :26)
cbde.store %_port, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :30 :26)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :32 :12) // Not a variable of known type: server
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :32 :26) // Not a variable of known type: port
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :32 :12) // server.Listen(port) (InvocationExpression)
%4 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :33 :21) // true
br ^1

^1: // ExitBlock
return

}
func @_Mirror.SimpleWeb.SimpleWebServer.Stop$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :36 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :38 :12) // Not a variable of known type: server
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :38 :12) // server.Stop() (InvocationExpression)
%2 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :39 :21) // false
br ^1

^1: // ExitBlock
return

}
// Skipping function SendAll(none, none), it contains poisonous unsupported syntaxes

func @_Mirror.SimpleWeb.SimpleWebServer.SendOne$int.System.ArraySegment$byte$$(i32, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :54 :8) {
^entry (%_connectionId : i32, %_source : none):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :54 :28)
cbde.store %_connectionId, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :54 :28)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :54 :46)
cbde.store %_source, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :54 :46)
br ^0

^0: // SimpleBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :56 :33) // Not a variable of known type: bufferPool
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :56 :49) // Not a variable of known type: source
%4 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :56 :49) // source.Count (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :56 :33) // bufferPool.Take(source.Count) (InvocationExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :57 :12) // Not a variable of known type: buffer
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :57 :28) // Not a variable of known type: source
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :57 :12) // buffer.CopyFrom(source) (InvocationExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :59 :12) // Not a variable of known type: server
%11 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :59 :24)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :59 :38) // Not a variable of known type: buffer
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :59 :12) // server.Send(connectionId, buffer) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.SimpleWeb.SimpleWebServer.KickClient$int$(i32) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :62 :8) {
^entry (%_connectionId : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :62 :31)
cbde.store %_connectionId, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :62 :31)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :64 :19) // Not a variable of known type: server
%2 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :64 :42)
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :64 :19) // server.CloseConnection(connectionId) (InvocationExpression)
return %3 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :64 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.SimpleWeb.SimpleWebServer.GetClientAddress$int$(i32) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :67 :8) {
^entry (%_connectionId : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :67 :39)
cbde.store %_connectionId, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :67 :39)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :69 :19) // Not a variable of known type: server
%2 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :69 :43)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :69 :19) // server.GetClientAddress(connectionId) (InvocationExpression)
return %3 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\SimpleWebServer.cs" :69 :12)

^1: // ExitBlock
cbde.unreachable

}
// Skipping function ProcessMessageQueue(none), it contains poisonous unsupported syntaxes

