func @_Mirror.SimpleWeb.WebSocketClientWebGl.Connect$System.Uri$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :24 :8) {
^entry (%_serverAddress : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :24 :37)
cbde.store %_serverAddress, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :24 :37)
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SimpleWebJSLib
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :26 :43) // Not a variable of known type: serverAddress
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :26 :43) // serverAddress.ToString() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OpenCallback
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CloseCallBack
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: MessageCallback
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ErrorCallback
%3 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :26 :20) // SimpleWebJSLib.Connect(serverAddress.ToString(), OpenCallback, CloseCallBack, MessageCallback, ErrorCallback) (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :27 :12) // Not a variable of known type: instances
%5 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :27 :26) // Not a variable of known type: index
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :27 :33) // this (ThisExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :27 :12) // instances.Add(index, this) (InvocationExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :28 :20) // Not a variable of known type: ClientState
%9 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :28 :20) // ClientState.Connecting (SimpleMemberAccessExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.SimpleWeb.WebSocketClientWebGl.Disconnect$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :31 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :33 :20) // Not a variable of known type: ClientState
%1 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :33 :20) // ClientState.Disconnecting (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SimpleWebJSLib
%2 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :35 :38) // Not a variable of known type: index
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :35 :12) // SimpleWebJSLib.Disconnect(index) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.SimpleWeb.WebSocketClientWebGl.Send$System.ArraySegment$byte$$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :38 :8) {
^entry (%_segment : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :38 :34)
cbde.store %_segment, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :38 :34)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :40 :16) // Not a variable of known type: segment
%2 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :40 :16) // segment.Count (SimpleMemberAccessExpression)
%3 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :40 :32) // Not a variable of known type: maxMessageSize
%4 = cmpi "sgt", %2, %3 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :40 :16)
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :40 :16)

^1: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Log
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :42 :59) // Not a variable of known type: segment
%6 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :42 :59) // segment.Count (SimpleMemberAccessExpression)
%7 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :42 :110) // Not a variable of known type: maxMessageSize
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :42 :26) // $"Cant send message with length {segment.Count} because it is over the max size of {maxMessageSize}" (InterpolatedStringExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :42 :16) // Log.Error($"Cant send message with length {segment.Count} because it is over the max size of {maxMessageSize}") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :43 :16)

^2: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SimpleWebJSLib
%10 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :46 :32) // Not a variable of known type: index
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :46 :39) // Not a variable of known type: segment
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :46 :39) // segment.Array (SimpleMemberAccessExpression)
%13 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :46 :54)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :46 :57) // Not a variable of known type: segment
%15 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :46 :57) // segment.Count (SimpleMemberAccessExpression)
%16 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :46 :12) // SimpleWebJSLib.Send(index, segment.Array, 0, segment.Count) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.SimpleWeb.WebSocketClientWebGl.onOpen$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :49 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :51 :12) // Not a variable of known type: receiveQueue
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :51 :45) // Not a variable of known type: EventType
%2 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :51 :45) // EventType.Connected (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :51 :33) // new Message(EventType.Connected) (ObjectCreationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :51 :12) // receiveQueue.Enqueue(new Message(EventType.Connected)) (InvocationExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :52 :20) // Not a variable of known type: ClientState
%6 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :52 :20) // ClientState.Connected (SimpleMemberAccessExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.SimpleWeb.WebSocketClientWebGl.onClose$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :55 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :59 :12) // Not a variable of known type: receiveQueue
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :59 :45) // Not a variable of known type: EventType
%2 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :59 :45) // EventType.Disconnected (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :59 :33) // new Message(EventType.Disconnected) (ObjectCreationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :59 :12) // receiveQueue.Enqueue(new Message(EventType.Disconnected)) (InvocationExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :60 :20) // Not a variable of known type: ClientState
%6 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :60 :20) // ClientState.NotConnected (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :61 :12) // Not a variable of known type: instances
%8 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :61 :29) // Not a variable of known type: index
%9 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :61 :12) // instances.Remove(index) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function onMessage(none, i32), it contains poisonous unsupported syntaxes

func @_Mirror.SimpleWeb.WebSocketClientWebGl.onErr$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :80 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :82 :12) // Not a variable of known type: receiveQueue
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :82 :59) // "Javascript Websocket error" (StringLiteralExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :82 :45) // new Exception("Javascript Websocket error") (ObjectCreationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :82 :33) // new Message(new Exception("Javascript Websocket error")) (ObjectCreationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :82 :12) // receiveQueue.Enqueue(new Message(new Exception("Javascript Websocket error"))) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Disconnect
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\Webgl\\WebSocketClientWebGl.cs" :83 :12) // Disconnect() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
