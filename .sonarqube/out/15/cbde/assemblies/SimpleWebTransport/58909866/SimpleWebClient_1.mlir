func @_Mirror.SimpleWeb.SimpleWebClient.Create$int.int.Mirror.SimpleWeb.TcpConfig$(i32, i32, none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\SimpleWebClient.cs" :19 :8) {
^entry (%_maxMessageSize : i32, %_maxMessagesPerTick : i32, %_tcpConfig : none):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\SimpleWebClient.cs" :19 :45)
cbde.store %_maxMessageSize, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\SimpleWebClient.cs" :19 :45)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\SimpleWebClient.cs" :19 :65)
cbde.store %_maxMessagesPerTick, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\SimpleWebClient.cs" :19 :65)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\SimpleWebClient.cs" :19 :89)
cbde.store %_tcpConfig, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\SimpleWebClient.cs" :19 :89)
br ^0

^0: // JumpBlock
%3 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\SimpleWebClient.cs" :24 :49)
%4 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\SimpleWebClient.cs" :24 :65)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\SimpleWebClient.cs" :24 :85) // Not a variable of known type: tcpConfig
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\SimpleWebClient.cs" :24 :19) // new WebSocketClientStandAlone(maxMessageSize, maxMessagesPerTick, tcpConfig) (ObjectCreationExpression)
return %6 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Client\\SimpleWebClient.cs" :24 :12)

^1: // ExitBlock
cbde.unreachable

}
// Skipping function ProcessMessageQueue(none), it contains poisonous unsupported syntaxes

