func @_Mirror.SimpleWeb.SendLoop.Config.Deconstruct$outMirror.SimpleWeb.Connection.outint.outbool$(none, i32, i1) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :29 :12) {
^entry (%_conn : none, %_bufferSize : i32, %_setMask : i1):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :29 :36)
cbde.store %_conn, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :29 :36)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :29 :57)
cbde.store %_bufferSize, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :29 :57)
%2 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :29 :77)
cbde.store %_setMask, %2 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :29 :77)
br ^0

^0: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :31 :23) // this (ThisExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :31 :23) // this.conn (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :32 :29) // this (ThisExpression)
%6 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :32 :29) // this.bufferSize (SimpleMemberAccessExpression)
cbde.store %6, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :32 :16)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :33 :26) // this (ThisExpression)
%8 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :33 :26) // this.setMask (SimpleMemberAccessExpression)
cbde.store %8, %2 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :33 :16)
br ^1

^1: // ExitBlock
return

}
// Skipping function Loop(none), it contains poisonous unsupported syntaxes

func @_Mirror.SimpleWeb.SendLoop.SendMessage$byte$$.int.Mirror.SimpleWeb.ArrayBuffer.bool.Mirror.SimpleWeb.SendLoop.MaskHelper$(none, i32, none, i1, none) -> i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :123 :8) {
^entry (%_buffer : none, %_startOffset : i32, %_msg : none, %_setMask : i1, %_maskHelper : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :123 :31)
cbde.store %_buffer, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :123 :31)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :123 :46)
cbde.store %_startOffset, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :123 :46)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :123 :63)
cbde.store %_msg, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :123 :63)
%3 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :123 :80)
cbde.store %_setMask, %3 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :123 :80)
%4 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :123 :94)
cbde.store %_maskHelper, %4 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :123 :94)
br ^0

^0: // BinaryBranchBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :125 :28) // Not a variable of known type: msg
%6 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :125 :28) // msg.count (SimpleMemberAccessExpression)
%7 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :125 :16) // msgLength
cbde.store %6, %7 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :125 :16)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WriteHeader
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :126 :37) // Not a variable of known type: buffer
%9 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :126 :45)
%10 = cbde.load %7 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :126 :58)
%11 = cbde.load %3 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :126 :69)
%12 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :126 :25) // WriteHeader(buffer, startOffset, msgLength, setMask) (InvocationExpression)
%13 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :126 :16) // offset
cbde.store %12, %13 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :126 :16)
%14 = cbde.load %3 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :128 :16)
cond_br %14, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :128 :16)

^1: // SimpleBlock
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :130 :25) // Not a variable of known type: maskHelper
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :130 :46) // Not a variable of known type: buffer
%17 = cbde.load %13 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :130 :54)
%18 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :130 :25) // maskHelper.WriteMask(buffer, offset) (InvocationExpression)
cbde.store %18, %13 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :130 :16)
br ^2

^2: // BinaryBranchBlock
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :133 :12) // Not a variable of known type: msg
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :133 :23) // Not a variable of known type: buffer
%21 = cbde.load %13 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :133 :31)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :133 :12) // msg.CopyTo(buffer, offset) (InvocationExpression)
%23 = cbde.load %13 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :134 :12)
%24 = cbde.load %7 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :134 :22)
%25 = addi %23, %24 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :134 :12)
cbde.store %25, %13 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :134 :12)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Log
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :137 :27) // "Send" (StringLiteralExpression)
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :137 :35) // Not a variable of known type: buffer
%28 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :137 :43)
%29 = cbde.load %13 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :137 :56)
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :137 :12) // Log.DumpBuffer("Send", buffer, startOffset, offset) (InvocationExpression)
%31 = cbde.load %3 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :139 :16)
cond_br %31, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :139 :16)

^3: // SimpleBlock
%32 = cbde.load %13 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :141 :36)
%33 = cbde.load %7 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :141 :45)
%34 = subi %32, %33 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :141 :36)
%35 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :141 :20) // messageOffset
cbde.store %34, %35 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :141 :20)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: MessageProcessor
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :142 :44) // Not a variable of known type: buffer
%37 = cbde.load %35 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :142 :52)
%38 = cbde.load %7 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :142 :67)
%39 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :142 :78) // Not a variable of known type: buffer
%40 = cbde.load %35 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :142 :86)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Constants
%41 = constant 4 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :142 :102)
%42 = subi %40, %41 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :142 :86)
%43 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :142 :16) // MessageProcessor.ToggleMask(buffer, messageOffset, msgLength, buffer, messageOffset - Constants.MaskSize) (InvocationExpression)
br ^4

^4: // JumpBlock
%44 = cbde.load %13 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :145 :19)
return %44 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :145 :12)

^5: // ExitBlock
cbde.unreachable

}
func @_Mirror.SimpleWeb.SendLoop.WriteHeader$byte$$.int.int.bool$(none, i32, i32, i1) -> i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :148 :8) {
^entry (%_buffer : none, %_startOffset : i32, %_msgLength : i32, %_setMask : i1):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :148 :31)
cbde.store %_buffer, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :148 :31)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :148 :46)
cbde.store %_startOffset, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :148 :46)
%2 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :148 :63)
cbde.store %_msgLength, %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :148 :63)
%3 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :148 :78)
cbde.store %_setMask, %3 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :148 :78)
br ^0

^0: // BinaryBranchBlock
%4 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :150 :29)
%5 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :150 :16) // sendLength
cbde.store %4, %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :150 :16)
%6 = constant 128 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :151 :34)
%8 = constant 2 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :152 :36)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :154 :12) // Not a variable of known type: buffer
%11 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :154 :19)
%12 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :154 :33)
%13 = addi %11, %12 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :154 :19)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :154 :12) // buffer[startOffset + 0] (ElementAccessExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :154 :38) // Not a variable of known type: finished
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :154 :49) // Not a variable of known type: byteOpCode
%17 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :154 :38) // Binary expression on unsupported types finished | byteOpCode
%18 = cbde.load %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :155 :12)
%19 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :155 :12)
%20 = addi %18, %19 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :155 :12)
cbde.store %20, %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :155 :12)
%21 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :157 :16)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Constants
%22 = constant 125 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :157 :29)
%23 = cmpi "sle", %21, %22 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :157 :16)
cond_br %23, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :157 :16)

^1: // SimpleBlock
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :159 :16) // Not a variable of known type: buffer
%25 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :159 :23)
%26 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :159 :37)
%27 = addi %25, %26 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :159 :23)
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :159 :16) // buffer[startOffset + 1] (ElementAccessExpression)
%29 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :159 :48)
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :159 :42) // (byte)msgLength (CastExpression)
%31 = cbde.load %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :160 :16)
%32 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :160 :16)
%33 = addi %31, %32 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :160 :16)
cbde.store %33, %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :160 :16)
br ^3

^2: // BinaryBranchBlock
%34 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :162 :21)
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :162 :34) // ushort (PredefinedType)
%36 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :162 :34) // ushort.MaxValue (SimpleMemberAccessExpression)
%37 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :162 :21) // comparison of unknown type: msgLength <= ushort.MaxValue
cond_br %37, ^4, ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :162 :21)

^4: // SimpleBlock
%38 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :164 :16) // Not a variable of known type: buffer
%39 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :164 :23)
%40 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :164 :37)
%41 = addi %39, %40 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :164 :23)
%42 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :164 :16) // buffer[startOffset + 1] (ElementAccessExpression)
%43 = constant 126 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :164 :42)
%44 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :165 :16) // Not a variable of known type: buffer
%45 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :165 :23)
%46 = constant 2 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :165 :37)
%47 = addi %45, %46 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :165 :23)
%48 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :165 :16) // buffer[startOffset + 2] (ElementAccessExpression)
%49 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :165 :49)
%50 = constant 8 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :165 :62)
%51 = cbde.neg %50 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :165 :49)
%52 = shlis %49, %51 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :165 :49)
%53 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :165 :42) // (byte)(msgLength >> 8) (CastExpression)
%54 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :166 :16) // Not a variable of known type: buffer
%55 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :166 :23)
%56 = constant 3 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :166 :37)
%57 = addi %55, %56 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :166 :23)
%58 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :166 :16) // buffer[startOffset + 3] (ElementAccessExpression)
%59 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :166 :48)
%60 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :166 :42) // (byte)msgLength (CastExpression)
%61 = cbde.load %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :167 :16)
%62 = constant 3 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :167 :30)
%63 = addi %61, %62 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :167 :16)
cbde.store %63, %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :167 :16)
br ^3

^5: // JumpBlock
%64 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :171 :87) // ushort (PredefinedType)
%65 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :171 :87) // ushort.MaxValue (SimpleMemberAccessExpression)
%66 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :171 :47) // $"Trying to send a message larger than {ushort.MaxValue} bytes" (InterpolatedStringExpression)
%67 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :171 :22) // new InvalidDataException($"Trying to send a message larger than {ushort.MaxValue} bytes") (ObjectCreationExpression)
cbde.throw %67 :  none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :171 :16)

^3: // BinaryBranchBlock
%68 = cbde.load %3 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :174 :16)
cond_br %68, ^6, ^7 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :174 :16)

^6: // SimpleBlock
%69 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :176 :16) // Not a variable of known type: buffer
%70 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :176 :23)
%71 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :176 :37)
%72 = addi %70, %71 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :176 :23)
%73 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :176 :16) // buffer[startOffset + 1] (ElementAccessExpression)
%74 = constant 128 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :176 :43)
%75 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :176 :16) // Binary expression on unsupported types buffer[startOffset + 1] |= 0b1000_0000
// No identifier name for binary assignment expression
br ^7

^7: // JumpBlock
%76 = cbde.load %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :179 :19)
%77 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :179 :32)
%78 = addi %76, %77 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :179 :19)
return %78 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :179 :12)

^8: // ExitBlock
cbde.unreachable

}
func @_Mirror.SimpleWeb.SendLoop.MaskHelper.Dispose$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :192 :12) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :194 :16) // Not a variable of known type: random
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :194 :16) // random.Dispose() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.SimpleWeb.SendLoop.MaskHelper.WriteMask$byte$$.int$(none, i32) -> i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :197 :12) {
^entry (%_buffer : none, %_offset : i32):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :197 :33)
cbde.store %_buffer, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :197 :33)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :197 :48)
cbde.store %_offset, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :197 :48)
br ^0

^0: // JumpBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :199 :16) // Not a variable of known type: random
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :199 :32) // Not a variable of known type: maskBuffer
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :199 :16) // random.GetBytes(maskBuffer) (InvocationExpression)
// Entity from another assembly: Buffer
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :200 :33) // Not a variable of known type: maskBuffer
%6 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :200 :45)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :200 :48) // Not a variable of known type: buffer
%8 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :200 :56)
%9 = constant 4 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :200 :64)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :200 :16) // Buffer.BlockCopy(maskBuffer, 0, buffer, offset, 4) (InvocationExpression)
%11 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :202 :23)
%12 = constant 4 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :202 :32)
%13 = addi %11, %12 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :202 :23)
return %13 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\SendLoop.cs" :202 :16)

^1: // ExitBlock
cbde.unreachable

}
