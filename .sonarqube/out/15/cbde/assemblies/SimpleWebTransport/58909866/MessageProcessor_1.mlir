func @_Mirror.SimpleWeb.MessageProcessor.NeedToReadShortLength$byte$$$(none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :10 :8) {
^entry (%_buffer : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :10 :49)
cbde.store %_buffer, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :10 :49)
br ^0

^0: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: FirstLengthByte
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :12 :43) // Not a variable of known type: buffer
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :12 :27) // FirstLengthByte(buffer) (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :14 :19) // Not a variable of known type: lenByte
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Constants
%5 = constant 126 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :14 :30)
%6 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :14 :19) // comparison of unknown type: lenByte >= Constants.UshortPayloadLength
return %6 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :14 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.SimpleWeb.MessageProcessor.GetOpcode$byte$$$(none) -> i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :17 :8) {
^entry (%_buffer : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :17 :36)
cbde.store %_buffer, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :17 :36)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :19 :19) // Not a variable of known type: buffer
%2 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :19 :26)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :19 :19) // buffer[0] (ElementAccessExpression)
%4 = constant 15 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :19 :31)
%5 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :19 :19) // Binary expression on unsupported types buffer[0] & 0b0000_1111
return %5 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :19 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.SimpleWeb.MessageProcessor.GetPayloadLength$byte$$$(none) -> i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :22 :8) {
^entry (%_buffer : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :22 :43)
cbde.store %_buffer, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :22 :43)
br ^0

^0: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: FirstLengthByte
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :24 :43) // Not a variable of known type: buffer
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :24 :27) // FirstLengthByte(buffer) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetMessageLength
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :25 :36) // Not a variable of known type: buffer
%5 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :25 :44)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :25 :47) // Not a variable of known type: lenByte
%7 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :25 :19) // GetMessageLength(buffer, 0, lenByte) (InvocationExpression)
return %7 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :25 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.SimpleWeb.MessageProcessor.ValidateHeader$byte$$.int.bool$(none, i32, i1) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :28 :8) {
^entry (%_buffer : none, %_maxLength : i32, %_expectMask : i1):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :28 :42)
cbde.store %_buffer, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :28 :42)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :28 :57)
cbde.store %_maxLength, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :28 :57)
%2 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :28 :72)
cbde.store %_expectMask, %2 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :28 :72)
br ^0

^0: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :30 :29) // Not a variable of known type: buffer
%4 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :30 :36)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :30 :29) // buffer[0] (ElementAccessExpression)
%6 = constant 128 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :30 :41)
%7 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :30 :29) // Binary expression on unsupported types buffer[0] & 0b1000_0000
%8 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :30 :57)
%9 = cmpi "ne", %7, %8 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :30 :28)
%10 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :30 :17) // finished
cbde.store %9, %10 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :30 :17)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :31 :28) // Not a variable of known type: buffer
%12 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :31 :35)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :31 :28) // buffer[1] (ElementAccessExpression)
%14 = constant 128 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :31 :40)
%15 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :31 :28) // Binary expression on unsupported types buffer[1] & 0b1000_0000
%16 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :31 :56)
%17 = cmpi "ne", %15, %16 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :31 :27)
%18 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :31 :17) // hasMask
cbde.store %17, %18 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :31 :17)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :33 :25) // Not a variable of known type: buffer
%20 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :33 :32)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :33 :25) // buffer[0] (ElementAccessExpression)
%22 = constant 15 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :33 :37)
%23 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :33 :25) // Binary expression on unsupported types buffer[0] & 0b0000_1111
%24 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :33 :16) // opcode
cbde.store %23, %24 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :33 :16)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: FirstLengthByte
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :34 :43) // Not a variable of known type: buffer
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :34 :27) // FirstLengthByte(buffer) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ThrowIfNotFinished
%28 = cbde.load %10 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :36 :31)
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :36 :12) // ThrowIfNotFinished(finished) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ThrowIfMaskNotExpected
%30 = cbde.load %18 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :37 :35)
%31 = cbde.load %2 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :37 :44)
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :37 :12) // ThrowIfMaskNotExpected(hasMask, expectMask) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ThrowIfBadOpCode
%33 = cbde.load %24 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :38 :29)
%34 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :38 :12) // ThrowIfBadOpCode(opcode) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetMessageLength
%35 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :40 :42) // Not a variable of known type: buffer
%36 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :40 :50)
%37 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :40 :53) // Not a variable of known type: lenByte
%38 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :40 :25) // GetMessageLength(buffer, 0, lenByte) (InvocationExpression)
%39 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :40 :16) // msglen
cbde.store %38, %39 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :40 :16)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ThrowIfLengthZero
%40 = cbde.load %39 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :42 :30)
%41 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :42 :12) // ThrowIfLengthZero(msglen) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ThrowIfMsgLengthTooLong
%42 = cbde.load %39 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :43 :36)
%43 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :43 :44)
%44 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :43 :12) // ThrowIfMsgLengthTooLong(msglen, maxLength) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.SimpleWeb.MessageProcessor.ToggleMask$byte$$.int.int.byte$$.int$(none, i32, i32, none, i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :46 :8) {
^entry (%_src : none, %_sourceOffset : i32, %_messageLength : i32, %_maskBuffer : none, %_maskOffset : i32):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :46 :38)
cbde.store %_src, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :46 :38)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :46 :50)
cbde.store %_sourceOffset, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :46 :50)
%2 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :46 :68)
cbde.store %_messageLength, %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :46 :68)
%3 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :46 :87)
cbde.store %_maskBuffer, %3 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :46 :87)
%4 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :46 :106)
cbde.store %_maskOffset, %4 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :46 :106)
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ToggleMask
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :48 :23) // Not a variable of known type: src
%6 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :48 :28)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :48 :42) // Not a variable of known type: src
%8 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :48 :47)
%9 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :48 :61)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :48 :76) // Not a variable of known type: maskBuffer
%11 = cbde.load %4 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :48 :88)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :48 :12) // ToggleMask(src, sourceOffset, src, sourceOffset, messageLength, maskBuffer, maskOffset) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.SimpleWeb.MessageProcessor.ToggleMask$byte$$.int.Mirror.SimpleWeb.ArrayBuffer.int.byte$$.int$(none, i32, none, i32, none, i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :51 :8) {
^entry (%_src : none, %_sourceOffset : i32, %_dst : none, %_messageLength : i32, %_maskBuffer : none, %_maskOffset : i32):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :51 :38)
cbde.store %_src, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :51 :38)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :51 :50)
cbde.store %_sourceOffset, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :51 :50)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :51 :68)
cbde.store %_dst, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :51 :68)
%3 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :51 :85)
cbde.store %_messageLength, %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :51 :85)
%4 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :51 :104)
cbde.store %_maskBuffer, %4 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :51 :104)
%5 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :51 :123)
cbde.store %_maskOffset, %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :51 :123)
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ToggleMask
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :53 :23) // Not a variable of known type: src
%7 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :53 :28)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :53 :42) // Not a variable of known type: dst
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :53 :42) // dst.array (SimpleMemberAccessExpression)
%10 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :53 :53)
%11 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :53 :56)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :53 :71) // Not a variable of known type: maskBuffer
%13 = cbde.load %5 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :53 :83)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :53 :12) // ToggleMask(src, sourceOffset, dst.array, 0, messageLength, maskBuffer, maskOffset) (InvocationExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :54 :12) // Not a variable of known type: dst
%16 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :54 :12) // dst.count (SimpleMemberAccessExpression)
%17 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :54 :24)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.SimpleWeb.MessageProcessor.ToggleMask$byte$$.int.byte$$.int.int.byte$$.int$(none, i32, none, i32, i32, none, i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :57 :8) {
^entry (%_src : none, %_srcOffset : i32, %_dst : none, %_dstOffset : i32, %_messageLength : i32, %_maskBuffer : none, %_maskOffset : i32):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :57 :38)
cbde.store %_src, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :57 :38)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :57 :50)
cbde.store %_srcOffset, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :57 :50)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :57 :65)
cbde.store %_dst, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :57 :65)
%3 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :57 :77)
cbde.store %_dstOffset, %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :57 :77)
%4 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :57 :92)
cbde.store %_messageLength, %4 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :57 :92)
%5 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :57 :111)
cbde.store %_maskBuffer, %5 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :57 :111)
%6 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :57 :130)
cbde.store %_maskOffset, %6 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :57 :130)
br ^0

^0: // ForInitializerBlock
%7 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :59 :25)
%8 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :59 :21) // i
cbde.store %7, %8 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :59 :21)
br ^1

^1: // BinaryBranchBlock
%9 = cbde.load %8 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :59 :28)
%10 = cbde.load %4 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :59 :32)
%11 = cmpi "slt", %9, %10 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :59 :28)
cond_br %11, ^2, ^3 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :59 :28)

^2: // SimpleBlock
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :61 :32) // Not a variable of known type: maskBuffer
%13 = cbde.load %6 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :61 :43)
%14 = cbde.load %8 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :61 :56)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Constants
%15 = constant 4 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :61 :60)
%16 = remis %14, %15 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :61 :56)
%17 = addi %13, %16 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :61 :43)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :61 :32) // maskBuffer[maskOffset + i % Constants.MaskSize] (ElementAccessExpression)
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :62 :16) // Not a variable of known type: dst
%21 = cbde.load %3 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :62 :20)
%22 = cbde.load %8 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :62 :32)
%23 = addi %21, %22 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :62 :20)
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :62 :16) // dst[dstOffset + i] (ElementAccessExpression)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :62 :44) // Not a variable of known type: src
%26 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :62 :48)
%27 = cbde.load %8 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :62 :60)
%28 = addi %26, %27 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :62 :48)
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :62 :44) // src[srcOffset + i] (ElementAccessExpression)
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :62 :65) // Not a variable of known type: maskByte
%31 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :62 :44) // Binary expression on unsupported types src[srcOffset + i] ^ maskByte
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :62 :37) // (byte)(src[srcOffset + i] ^ maskByte) (CastExpression)
br ^4

^4: // SimpleBlock
%33 = cbde.load %8 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :59 :47)
%34 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :59 :47)
%35 = addi %33, %34 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :59 :47)
cbde.store %35, %8 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :59 :47)
br ^1

^3: // ExitBlock
return

}
func @_Mirror.SimpleWeb.MessageProcessor.GetMessageLength$byte$$.int.byte$(none, i32, none) -> i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :67 :8) {
^entry (%_buffer : none, %_offset : i32, %_lenByte : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :67 :36)
cbde.store %_buffer, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :67 :36)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :67 :51)
cbde.store %_offset, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :67 :51)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :67 :63)
cbde.store %_lenByte, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :67 :63)
br ^0

^0: // BinaryBranchBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :69 :16) // Not a variable of known type: lenByte
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Constants
%4 = constant 126 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :69 :27)
%5 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :69 :16) // comparison of unknown type: lenByte == Constants.UshortPayloadLength
cond_br %5, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :69 :16)

^1: // JumpBlock
%6 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :72 :31)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :73 :16) // Not a variable of known type: value
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :73 :34) // Not a variable of known type: buffer
%10 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :73 :41)
%11 = constant 2 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :73 :50)
%12 = addi %10, %11 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :73 :41)
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :73 :34) // buffer[offset + 2] (ElementAccessExpression)
%14 = constant 8 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :73 :56)
%15 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :73 :34) // Binary expression on unsupported types buffer[offset + 2] << 8
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :73 :25) // (ushort)(buffer[offset + 2] << 8) (CastExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :73 :16) // Binary expression on unsupported types value |= (ushort)(buffer[offset + 2] << 8)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :74 :16) // Not a variable of known type: value
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :74 :25) // Not a variable of known type: buffer
%20 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :74 :32)
%21 = constant 3 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :74 :41)
%22 = addi %20, %21 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :74 :32)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :74 :25) // buffer[offset + 3] (ElementAccessExpression)
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :74 :16) // Binary expression on unsupported types value |= buffer[offset + 3]
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :76 :23) // Not a variable of known type: value
%26 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :76 :23) // Dummy variable because type of %25 is incompatible
return %26 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :76 :16)

^2: // BinaryBranchBlock
%27 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :78 :21) // Not a variable of known type: lenByte
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Constants
%28 = constant 127 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :78 :32)
%29 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :78 :21) // comparison of unknown type: lenByte == Constants.UlongPayloadLength
cond_br %29, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :78 :21)

^3: // JumpBlock
%30 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :80 :47) // "Max length is longer than allowed in a single message" (StringLiteralExpression)
%31 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :80 :22) // new InvalidDataException("Max length is longer than allowed in a single message") (ObjectCreationExpression)
cbde.throw %31 :  none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :80 :16)

^4: // JumpBlock
%32 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :85 :23) // Not a variable of known type: lenByte
%33 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :85 :23) // Dummy variable because type of %32 is incompatible
return %33 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :85 :16)

^5: // ExitBlock
cbde.unreachable

}
func @_Mirror.SimpleWeb.MessageProcessor.ThrowIfNotFinished$bool$(i1) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :90 :8) {
^entry (%_finished : i1):
%0 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :90 :39)
cbde.store %_finished, %0 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :90 :39)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.load %0 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :92 :17)
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :92 :16) // !finished (LogicalNotExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :92 :16)

^1: // JumpBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :94 :47) // "Full message should have been sent, if the full message wasn't sent it wasn't sent from this trasnport" (StringLiteralExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :94 :22) // new InvalidDataException("Full message should have been sent, if the full message wasn't sent it wasn't sent from this trasnport") (ObjectCreationExpression)
cbde.throw %4 :  none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :94 :16)

^2: // ExitBlock
return

}
func @_Mirror.SimpleWeb.MessageProcessor.ThrowIfMaskNotExpected$bool.bool$(i1, i1) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :99 :8) {
^entry (%_hasMask : i1, %_expectMask : i1):
%0 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :99 :43)
cbde.store %_hasMask, %0 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :99 :43)
%1 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :99 :57)
cbde.store %_expectMask, %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :99 :57)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.load %0 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :101 :16)
%3 = cbde.load %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :101 :27)
%4 = cmpi "ne", %2, %3 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :101 :16)
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :101 :16)

^1: // JumpBlock
%5 = cbde.load %1 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :103 :78)
%6 = cbde.load %0 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :103 :99)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :103 :47) // $"Message expected mask to be {expectMask} but was {hasMask}" (InterpolatedStringExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :103 :22) // new InvalidDataException($"Message expected mask to be {expectMask} but was {hasMask}") (ObjectCreationExpression)
cbde.throw %8 :  none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :103 :16)

^2: // ExitBlock
return

}
// Skipping function ThrowIfBadOpCode(i32), it contains poisonous unsupported syntaxes

func @_Mirror.SimpleWeb.MessageProcessor.ThrowIfLengthZero$int$(i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :119 :8) {
^entry (%_msglen : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :119 :38)
cbde.store %_msglen, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :119 :38)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :121 :16)
%2 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :121 :26)
%3 = cmpi "eq", %1, %2 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :121 :16)
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :121 :16)

^1: // JumpBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :123 :47) // "Message length was zero" (StringLiteralExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :123 :22) // new InvalidDataException("Message length was zero") (ObjectCreationExpression)
cbde.throw %5 :  none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :123 :16)

^2: // ExitBlock
return

}
func @_Mirror.SimpleWeb.MessageProcessor.ThrowIfMsgLengthTooLong$int.int$(i32, i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :131 :8) {
^entry (%_msglen : i32, %_maxLength : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :131 :44)
cbde.store %_msglen, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :131 :44)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :131 :56)
cbde.store %_maxLength, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :131 :56)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :133 :16)
%3 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :133 :25)
%4 = cmpi "sgt", %2, %3 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :133 :16)
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :133 :16)

^1: // JumpBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :135 :47) // "Message length is greater than max length" (StringLiteralExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :135 :22) // new InvalidDataException("Message length is greater than max length") (ObjectCreationExpression)
cbde.throw %6 :  none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\MessageProcessor.cs" :135 :16)

^2: // ExitBlock
return

}
