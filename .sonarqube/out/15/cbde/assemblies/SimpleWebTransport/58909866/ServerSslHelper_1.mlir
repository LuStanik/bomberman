// Skipping function TryCreateStream(none), it contains poisonous unsupported syntaxes

func @_Mirror.SimpleWeb.ServerSslHelper.CreateStream$System.Net.Sockets.NetworkStream$(none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\ServerSslHelper.cs" :59 :8) {
^entry (%_stream : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\ServerSslHelper.cs" :59 :28)
cbde.store %_stream, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\ServerSslHelper.cs" :59 :28)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\ServerSslHelper.cs" :61 :48) // Not a variable of known type: stream
%2 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\ServerSslHelper.cs" :61 :56) // true
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: acceptClient
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\ServerSslHelper.cs" :61 :34) // new SslStream(stream, true, acceptClient) (ObjectCreationExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\ServerSslHelper.cs" :62 :12) // Not a variable of known type: sslStream
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\ServerSslHelper.cs" :62 :43) // Not a variable of known type: certificate
%7 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\ServerSslHelper.cs" :62 :56) // false
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\ServerSslHelper.cs" :62 :63) // Not a variable of known type: config
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\ServerSslHelper.cs" :62 :63) // config.sslProtocols (SimpleMemberAccessExpression)
%10 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\ServerSslHelper.cs" :62 :84) // false
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\ServerSslHelper.cs" :62 :12) // sslStream.AuthenticateAsServer(certificate, false, config.sslProtocols, false) (InvocationExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\ServerSslHelper.cs" :64 :19) // Not a variable of known type: sslStream
return %12 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\ServerSslHelper.cs" :64 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.SimpleWeb.ServerSslHelper.acceptClient$object.System.Security.Cryptography.X509Certificates.X509Certificate.System.Security.Cryptography.X509Certificates.X509Chain.System.Net.Security.SslPolicyErrors$(none, none, none, none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\ServerSslHelper.cs" :67 :8) {
^entry (%_sender : none, %_certificate : none, %_chain : none, %_sslPolicyErrors : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\ServerSslHelper.cs" :67 :26)
cbde.store %_sender, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\ServerSslHelper.cs" :67 :26)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\ServerSslHelper.cs" :67 :41)
cbde.store %_certificate, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\ServerSslHelper.cs" :67 :41)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\ServerSslHelper.cs" :67 :70)
cbde.store %_chain, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\ServerSslHelper.cs" :67 :70)
%3 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\ServerSslHelper.cs" :67 :87)
cbde.store %_sslPolicyErrors, %3 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\ServerSslHelper.cs" :67 :87)
br ^0

^0: // JumpBlock
%4 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\ServerSslHelper.cs" :70 :19) // true
return %4 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\ServerSslHelper.cs" :70 :12)

^1: // ExitBlock
cbde.unreachable

}
