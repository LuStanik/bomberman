func @_Mirror.SimpleWeb.ArrayBuffer.SetReleasesRequired$int$(i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :28 :8) {
^entry (%_required : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :28 :40)
cbde.store %_required, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :28 :40)
br ^0

^0: // SimpleBlock
%1 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :30 :31)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.SimpleWeb.ArrayBuffer.Release$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :48 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: Interlocked
%0 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :50 :53) // Not a variable of known type: releasesRequired
%1 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :50 :27) // Interlocked.Decrement(ref releasesRequired) (InvocationExpression)
%2 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :50 :16) // newValue
cbde.store %1, %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :50 :16)
%3 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :51 :16)
%4 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :51 :28)
%5 = cmpi "sle", %3, %4 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :51 :16)
cond_br %5, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :51 :16)

^1: // SimpleBlock
%6 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :53 :24)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :54 :16) // Not a variable of known type: owner
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :54 :29) // this (ThisExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :54 :16) // owner.Return(this) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.SimpleWeb.ArrayBuffer.Dispose$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :57 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Release
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :59 :12) // Release() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.SimpleWeb.ArrayBuffer.CopyTo$byte$$.int$(none, i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :63 :8) {
^entry (%_target : none, %_offset : i32):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :63 :27)
cbde.store %_target, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :63 :27)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :63 :42)
cbde.store %_offset, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :63 :42)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :65 :16) // Not a variable of known type: count
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :65 :25) // Not a variable of known type: target
%4 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :65 :25) // target.Length (SimpleMemberAccessExpression)
%5 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :65 :41)
%6 = addi %4, %5 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :65 :25)
%7 = cmpi "sgt", %2, %6 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :65 :16)
cond_br %7, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :65 :16)

^1: // JumpBlock
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :65 :81) // nameof(count) (InvocationExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :65 :114) // nameof(target) (InvocationExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :65 :78) // $"{nameof(count)} was greater than {nameof(target)}.length" (InterpolatedStringExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :65 :139) // nameof(target) (InvocationExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :65 :56) // new ArgumentException($"{nameof(count)} was greater than {nameof(target)}.length", nameof(target)) (ObjectCreationExpression)
cbde.throw %12 :  none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :65 :50)

^2: // SimpleBlock
// Entity from another assembly: Buffer
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :67 :29) // Not a variable of known type: array
%14 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :67 :36)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :67 :39) // Not a variable of known type: target
%16 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :67 :47)
%17 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :67 :55) // Not a variable of known type: count
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :67 :12) // Buffer.BlockCopy(array, 0, target, offset, count) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.SimpleWeb.ArrayBuffer.CopyFrom$System.ArraySegment$byte$$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :70 :8) {
^entry (%_segment : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :70 :29)
cbde.store %_segment, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :70 :29)
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CopyFrom
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :72 :21) // Not a variable of known type: segment
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :72 :21) // segment.Array (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :72 :36) // Not a variable of known type: segment
%4 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :72 :36) // segment.Offset (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :72 :52) // Not a variable of known type: segment
%6 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :72 :52) // segment.Count (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :72 :12) // CopyFrom(segment.Array, segment.Offset, segment.Count) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.SimpleWeb.ArrayBuffer.CopyFrom$byte$$.int.int$(none, i32, i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :75 :8) {
^entry (%_source : none, %_offset : i32, %_length : i32):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :75 :29)
cbde.store %_source, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :75 :29)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :75 :44)
cbde.store %_offset, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :75 :44)
%2 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :75 :56)
cbde.store %_length, %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :75 :56)
br ^0

^0: // BinaryBranchBlock
%3 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :77 :16)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :77 :25) // Not a variable of known type: array
%5 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :77 :25) // array.Length (SimpleMemberAccessExpression)
%6 = cmpi "sgt", %3, %5 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :77 :16)
cond_br %6, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :77 :16)

^1: // JumpBlock
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :77 :70) // nameof(length) (InvocationExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :77 :104) // nameof(array) (InvocationExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :77 :67) // $"{nameof(length)} was greater than {nameof(array)}.length" (InterpolatedStringExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :77 :128) // nameof(length) (InvocationExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :77 :45) // new ArgumentException($"{nameof(length)} was greater than {nameof(array)}.length", nameof(length)) (ObjectCreationExpression)
cbde.throw %11 :  none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :77 :39)

^2: // SimpleBlock
%12 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :79 :20)
// Entity from another assembly: Buffer
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :80 :29) // Not a variable of known type: source
%14 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :80 :37)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :80 :45) // Not a variable of known type: array
%16 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :80 :52)
%17 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :80 :55)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :80 :12) // Buffer.BlockCopy(source, offset, array, 0, length) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.SimpleWeb.ArrayBuffer.CopyFrom$System.IntPtr.int$(none, i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :83 :8) {
^entry (%_bufferPtr : none, %_length : i32):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :83 :29)
cbde.store %_bufferPtr, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :83 :29)
%1 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :83 :47)
cbde.store %_length, %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :83 :47)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :85 :16)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :85 :25) // Not a variable of known type: array
%4 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :85 :25) // array.Length (SimpleMemberAccessExpression)
%5 = cmpi "sgt", %2, %4 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :85 :16)
cond_br %5, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :85 :16)

^1: // JumpBlock
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :85 :70) // nameof(length) (InvocationExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :85 :104) // nameof(array) (InvocationExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :85 :67) // $"{nameof(length)} was greater than {nameof(array)}.length" (InterpolatedStringExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :85 :128) // nameof(length) (InvocationExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :85 :45) // new ArgumentException($"{nameof(length)} was greater than {nameof(array)}.length", nameof(length)) (ObjectCreationExpression)
cbde.throw %10 :  none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :85 :39)

^2: // SimpleBlock
%11 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :87 :20)
// Entity from another assembly: Marshal
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :88 :25) // Not a variable of known type: bufferPtr
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :88 :36) // Not a variable of known type: array
%14 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :88 :43)
%15 = cbde.load %1 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :88 :46)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :88 :12) // Marshal.Copy(bufferPtr, array, 0, length) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.SimpleWeb.ArrayBuffer.ToSegment$$() -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :91 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :93 :42) // Not a variable of known type: array
%1 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :93 :49)
%2 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :93 :52) // Not a variable of known type: count
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :93 :19) // new ArraySegment<byte>(array, 0, count) (ObjectCreationExpression)
return %3 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :93 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.SimpleWeb.ArrayBuffer.Validate$int$(i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :96 :8) {
^entry (%_arraySize : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :97 :31)
cbde.store %_arraySize, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :97 :31)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :99 :16) // Not a variable of known type: array
%2 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :99 :16) // array.Length (SimpleMemberAccessExpression)
%3 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :99 :32)
%4 = cmpi "ne", %2, %3 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :99 :16)
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :99 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Log
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :101 :26) // "Buffer that was returned had an array of the wrong size" (StringLiteralExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :101 :16) // Log.Error("Buffer that was returned had an array of the wrong size") (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
// Skipping function Take(), it contains poisonous unsupported syntaxes

func @_Mirror.SimpleWeb.BufferBucket.Return$Mirror.SimpleWeb.ArrayBuffer$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :136 :8) {
^entry (%_buffer : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :136 :27)
cbde.store %_buffer, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :136 :27)
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: DecrementCreated
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :138 :12) // DecrementCreated() (InvocationExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :139 :12) // Not a variable of known type: buffer
%3 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :139 :28) // Not a variable of known type: arraySize
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :139 :12) // buffer.Validate(arraySize) (InvocationExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :140 :12) // Not a variable of known type: buffers
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :140 :28) // Not a variable of known type: buffer
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :140 :12) // buffers.Enqueue(buffer) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.SimpleWeb.BufferBucket.IncrementCreated$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :143 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: Interlocked
%0 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :146 :49) // Not a variable of known type: _current
%1 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :146 :23) // Interlocked.Increment(ref _current) (InvocationExpression)
%2 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :146 :16) // next
cbde.store %1, %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :146 :16)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Log
%3 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :147 :40) // Not a variable of known type: arraySize
%4 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :147 :59)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :147 :24) // $"BufferBucket({arraySize}) count:{next}" (InterpolatedStringExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :147 :12) // Log.Verbose($"BufferBucket({arraySize}) count:{next}") (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.SimpleWeb.BufferBucket.DecrementCreated$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :149 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: Interlocked
%0 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :152 :49) // Not a variable of known type: _current
%1 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :152 :23) // Interlocked.Decrement(ref _current) (InvocationExpression)
%2 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :152 :16) // next
cbde.store %1, %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :152 :16)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Log
%3 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :153 :40) // Not a variable of known type: arraySize
%4 = cbde.load %2 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :153 :59)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :153 :24) // $"BufferBucket({arraySize}) count:{next}" (InterpolatedStringExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :153 :12) // Log.Verbose($"BufferBucket({arraySize}) count:{next}") (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function Validate(), it contains poisonous unsupported syntaxes

func @_Mirror.SimpleWeb.BufferPool.Take$int$(i32) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :249 :8) {
^entry (%_size : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :249 :32)
cbde.store %_size, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :249 :32)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :251 :16)
%2 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :251 :23) // Not a variable of known type: largest
%3 = cmpi "sgt", %1, %2 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :251 :16)
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :251 :16)

^1: // JumpBlock
%4 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :251 :71)
%5 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :251 :105) // Not a variable of known type: largest
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :251 :62) // $"Size ({size}) is greatest that largest ({largest})" (InterpolatedStringExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :251 :40) // new ArgumentException($"Size ({size}) is greatest that largest ({largest})") (ObjectCreationExpression)
cbde.throw %7 :  none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :251 :34)

^2: // ForInitializerBlock
%8 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :253 :25)
%9 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :253 :21) // i
cbde.store %8, %9 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :253 :21)
br ^3

^3: // BinaryBranchBlock
%10 = cbde.load %9 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :253 :28)
%11 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :253 :32) // Not a variable of known type: bucketCount
%12 = cmpi "slt", %10, %11 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :253 :28)
cond_br %12, ^4, ^5 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :253 :28)

^4: // BinaryBranchBlock
%13 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :255 :20)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :255 :28) // Not a variable of known type: buckets
%15 = cbde.load %9 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :255 :36)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :255 :28) // buckets[i] (ElementAccessExpression)
%17 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :255 :28) // buckets[i].arraySize (SimpleMemberAccessExpression)
%18 = cmpi "sle", %13, %17 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :255 :20)
cond_br %18, ^6, ^7 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :255 :20)

^6: // JumpBlock
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :257 :27) // Not a variable of known type: buckets
%20 = cbde.load %9 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :257 :35)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :257 :27) // buckets[i] (ElementAccessExpression)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :257 :27) // buckets[i].Take() (InvocationExpression)
return %22 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :257 :20)

^7: // SimpleBlock
%23 = cbde.load %9 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :253 :45)
%24 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :253 :45)
%25 = addi %23, %24 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :253 :45)
cbde.store %25, %9 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :253 :45)
br ^3

^5: // JumpBlock
%26 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :261 :49)
%27 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :261 :83) // Not a variable of known type: largest
%28 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :261 :40) // $"Size ({size}) is greatest that largest ({largest})" (InterpolatedStringExpression)
%29 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :261 :18) // new ArgumentException($"Size ({size}) is greatest that largest ({largest})") (ObjectCreationExpression)
cbde.throw %29 :  none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Common\\BufferPool.cs" :261 :12)

^8: // ExitBlock
cbde.unreachable

}
