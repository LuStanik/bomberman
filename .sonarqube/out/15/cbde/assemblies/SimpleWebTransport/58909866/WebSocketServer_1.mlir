func @_Mirror.SimpleWeb.WebSocketServer.Listen$int$(i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\WebSocketServer.cs" :35 :8) {
^entry (%_port : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\WebSocketServer.cs" :35 :27)
cbde.store %_port, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\WebSocketServer.cs" :35 :27)
br ^0

^0: // SimpleBlock
// Entity from another assembly: TcpListener
%1 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\WebSocketServer.cs" :37 :42)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\WebSocketServer.cs" :37 :23) // TcpListener.Create(port) (InvocationExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\WebSocketServer.cs" :38 :12) // Not a variable of known type: listener
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\WebSocketServer.cs" :38 :12) // listener.Start() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Log
%5 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\WebSocketServer.cs" :40 :51)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\WebSocketServer.cs" :40 :21) // $"Server has started on port {port}" (InterpolatedStringExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\WebSocketServer.cs" :40 :12) // Log.Info($"Server has started on port {port}") (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: acceptLoop
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\WebSocketServer.cs" :42 :27) // new Thread(acceptLoop) (ObjectCreationExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\WebSocketServer.cs" :43 :12) // Not a variable of known type: acceptThread
%10 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\WebSocketServer.cs" :43 :12) // acceptThread.IsBackground (SimpleMemberAccessExpression)
%11 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\WebSocketServer.cs" :43 :40) // true
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\WebSocketServer.cs" :44 :12) // Not a variable of known type: acceptThread
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Runtime\\Transport\\SimpleWebTransport\\Server\\WebSocketServer.cs" :44 :12) // acceptThread.Start() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function Stop(), it contains poisonous unsupported syntaxes

// Skipping function acceptLoop(), it contains poisonous unsupported syntaxes

// Skipping function HandshakeAndReceiveLoop(none), it contains poisonous unsupported syntaxes

// Skipping function AfterConnectionDisposed(none), it contains poisonous unsupported syntaxes

// Skipping function Send(i32, none), it contains poisonous unsupported syntaxes

// Skipping function CloseConnection(i32), it contains poisonous unsupported syntaxes

// Skipping function GetClientAddress(i32), it contains poisonous unsupported syntaxes

