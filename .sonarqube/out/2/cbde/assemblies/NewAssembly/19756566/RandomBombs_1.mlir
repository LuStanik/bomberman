func @_RandomBombs.Start$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\RandomBombs.cs" :15 :4) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: StartCoroutine
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Countdown
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\RandomBombs.cs" :17 :23) // Countdown() (InvocationExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\RandomBombs.cs" :17 :8) // StartCoroutine(Countdown()) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_RandomBombs.Update$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\RandomBombs.cs" :20 :4) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: UpdateTimer
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\RandomBombs.cs" :22 :8) // UpdateTimer() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_RandomBombs.UpdateTimer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\RandomBombs.cs" :25 :4) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\RandomBombs.cs" :27 :12) // Not a variable of known type: gameStarted
cond_br %0, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\RandomBombs.cs" :27 :12)

^1: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\RandomBombs.cs" :29 :12) // Not a variable of known type: totalTime
// Entity from another assembly: Time
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\RandomBombs.cs" :29 :25) // Time.deltaTime (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\RandomBombs.cs" :29 :12) // Binary expression on unsupported types totalTime += Time.deltaTime
// Entity from another assembly: Mathf
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\RandomBombs.cs" :31 :45) // Not a variable of known type: totalTime
%5 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\RandomBombs.cs" :31 :28) // Mathf.FloorToInt(totalTime) (InvocationExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\RandomBombs.cs" :32 :34) // Not a variable of known type: totalTime
%8 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\RandomBombs.cs" :32 :46)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\RandomBombs.cs" :32 :34) // Binary expression on unsupported types totalTime % 1
%10 = constant 100 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\RandomBombs.cs" :32 :51)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\RandomBombs.cs" :32 :33) // Binary expression on unsupported types (totalTime % 1) * 100
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\RandomBombs.cs" :34 :12) // Not a variable of known type: timer
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\RandomBombs.cs" :34 :12) // timer.text (SimpleMemberAccessExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\RandomBombs.cs" :34 :25) // string (PredefinedType)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\RandomBombs.cs" :34 :39) // "Time: {0:00}:{1:00}" (StringLiteralExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\RandomBombs.cs" :34 :62) // Not a variable of known type: seconds
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\RandomBombs.cs" :34 :71) // Not a variable of known type: milliSeconds
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\RandomBombs.cs" :34 :25) // string.Format("Time: {0:00}:{1:00}", seconds, milliSeconds) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
// Skipping function Countdown(), it contains poisonous unsupported syntaxes

// Skipping function RainOfBombs(), it contains poisonous unsupported syntaxes

func @_RandomBombs.ExitToMenu$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\RandomBombs.cs" :68 :4) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: SceneManager
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\RandomBombs.cs" :70 :31) // "Menu" (StringLiteralExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\RandomBombs.cs" :70 :8) // SceneManager.LoadScene("Menu") (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_RandomBombs.OnDestroy$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\RandomBombs.cs" :73 :4) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CurrentScore
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\RandomBombs.cs" :75 :8) // CurrentScore.TotalTime (SimpleMemberAccessExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\RandomBombs.cs" :75 :33) // Not a variable of known type: totalTime
br ^1

^1: // ExitBlock
return

}
