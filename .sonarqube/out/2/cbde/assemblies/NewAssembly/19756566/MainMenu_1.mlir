func @_MainMenu.QuitGame$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\MainMenu.cs" :8 :4) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: Debug
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\MainMenu.cs" :10 :18) // "QUIT" (StringLiteralExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\MainMenu.cs" :10 :8) // Debug.Log("QUIT") (InvocationExpression)
// Entity from another assembly: Application
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\MainMenu.cs" :11 :8) // Application.Quit() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
