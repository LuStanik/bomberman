func @_SaveScores.SaveScore$RandomBombs$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :6 :4) {
^entry (%_run : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :6 :33)
cbde.store %_run, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :6 :33)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :8 :36) // new BinaryFormatter() (ObjectCreationExpression)
// Entity from another assembly: Application
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :9 :22) // Application.persistentDataPath (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :9 :55) // "highscores.txt" (StringLiteralExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :9 :22) // Binary expression on unsupported types Application.persistentDataPath + "highscores.txt"
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :10 :43) // Not a variable of known type: path
// Entity from another assembly: FileMode
%8 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :10 :49) // FileMode.Create (SimpleMemberAccessExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :10 :28) // new FileStream(path, FileMode.Create) (ObjectCreationExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :12 :47) // Not a variable of known type: run
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :12 :30) // new PlayerScores(run) (ObjectCreationExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :14 :8) // Not a variable of known type: formatter
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :14 :28) // Not a variable of known type: stream
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :14 :36) // Not a variable of known type: scores
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :14 :8) // formatter.Serialize(stream, scores) (InvocationExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :15 :8) // Not a variable of known type: stream
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :15 :8) // stream.Close() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_SaveScores.LoadScore$$() -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :18 :4) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: Application
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :20 :22) // Application.persistentDataPath (SimpleMemberAccessExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :20 :55) // "highscores.txt" (StringLiteralExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :20 :22) // Binary expression on unsupported types Application.persistentDataPath + "highscores.txt"
// Entity from another assembly: File
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :21 :24) // Not a variable of known type: path
%5 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :21 :12) // File.Exists(path) (InvocationExpression)
cond_br %5, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :21 :12)

^1: // JumpBlock
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :23 :40) // new BinaryFormatter() (ObjectCreationExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :24 :47) // Not a variable of known type: path
// Entity from another assembly: FileMode
%9 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :24 :53) // FileMode.Open (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :24 :32) // new FileStream(path, FileMode.Open) (ObjectCreationExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :26 :34) // Not a variable of known type: formatter
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :26 :56) // Not a variable of known type: stream
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :26 :34) // formatter.Deserialize(stream) (InvocationExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :26 :34) // formatter.Deserialize(stream) as PlayerScores (AsExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :27 :12) // Not a variable of known type: stream
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :27 :12) // stream.Close() (InvocationExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :29 :19) // Not a variable of known type: scores
return %19 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :29 :12)

^2: // JumpBlock
// Entity from another assembly: Debug
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :33 :27) // "No file found" (StringLiteralExpression)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :33 :12) // Debug.LogError("No file found") (InvocationExpression)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :34 :19) // null (NullLiteralExpression)
return %22 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\SaveScores.cs" :34 :12)

^3: // ExitBlock
cbde.unreachable

}
