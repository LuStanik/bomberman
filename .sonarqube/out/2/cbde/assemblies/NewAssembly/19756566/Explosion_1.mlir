func @_Explosion.Start$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Explosion.cs" :4 :4) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: Destroy
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Explosion.cs" :6 :16) // Identifier from another assembly: gameObject
%1 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Explosion.cs" :6 :28) // 0.75f (NumericLiteralExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Explosion.cs" :6 :8) // Destroy(gameObject, 0.75f) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
