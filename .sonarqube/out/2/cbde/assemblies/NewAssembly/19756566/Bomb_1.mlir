func @_Bomb.Start$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :11 :4) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :13 :19) // Not a variable of known type: bomb
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :13 :19) // bomb.transform (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :13 :19) // bomb.transform.position (SimpleMemberAccessExpression)
// Entity from another assembly: Invoke
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :14 :15) // nameof(Collision) (InvocationExpression)
%4 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :14 :34) // 0.5f (NumericLiteralExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :14 :8) // Invoke(nameof(Collision), 0.5f) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Bomb.Explode$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :18 :4) {
^entry :
br ^0

^0: // SimpleBlock
%0 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :20 :19) // true
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Collision
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :21 :8) // Collision() (InvocationExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :22 :8) // Identifier from another assembly: gameObject
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :22 :8) // gameObject.GetComponent<SpriteRenderer>() (InvocationExpression)
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :22 :8) // gameObject.GetComponent<SpriteRenderer>().enabled (SimpleMemberAccessExpression)
%5 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :22 :60) // false
// Entity from another assembly: Destroy
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :23 :16) // Identifier from another assembly: gameObject
%7 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :23 :28) // .2f (NumericLiteralExpression)
%8 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :23 :34) // Not a variable of known type: power
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :23 :28) // Binary expression on unsupported types .2f * power
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :23 :8) // Destroy(gameObject, .2f * power) (InvocationExpression)
// Entity from another assembly: Instantiate
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :24 :20) // Not a variable of known type: explosion
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :24 :31) // Not a variable of known type: position
// Entity from another assembly: Quaternion
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :24 :41) // Quaternion.identity (SimpleMemberAccessExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :24 :8) // Instantiate(explosion, position, Quaternion.identity) (InvocationExpression)
// Entity from another assembly: StartCoroutine
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CreateExplosion
// Entity from another assembly: Vector2
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :25 :39) // Vector2.right (SimpleMemberAccessExpression)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :25 :23) // CreateExplosion(Vector2.right) (InvocationExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :25 :8) // StartCoroutine(CreateExplosion(Vector2.right)) (InvocationExpression)
// Entity from another assembly: StartCoroutine
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CreateExplosion
// Entity from another assembly: Vector2
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :26 :39) // Vector2.left (SimpleMemberAccessExpression)
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :26 :23) // CreateExplosion(Vector2.left) (InvocationExpression)
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :26 :8) // StartCoroutine(CreateExplosion(Vector2.left)) (InvocationExpression)
// Entity from another assembly: StartCoroutine
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CreateExplosion
// Entity from another assembly: Vector2
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :27 :39) // Vector2.up (SimpleMemberAccessExpression)
%22 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :27 :23) // CreateExplosion(Vector2.up) (InvocationExpression)
%23 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :27 :8) // StartCoroutine(CreateExplosion(Vector2.up)) (InvocationExpression)
// Entity from another assembly: StartCoroutine
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CreateExplosion
// Entity from another assembly: Vector2
%24 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :28 :39) // Vector2.down (SimpleMemberAccessExpression)
%25 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :28 :23) // CreateExplosion(Vector2.down) (InvocationExpression)
%26 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :28 :8) // StartCoroutine(CreateExplosion(Vector2.down)) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function CreateExplosion(none), it contains poisonous unsupported syntaxes

func @_Bomb.Collision$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :50 :4) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :52 :8) // Identifier from another assembly: gameObject
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :52 :8) // gameObject.GetComponent<BoxCollider2D>() (InvocationExpression)
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :52 :8) // gameObject.GetComponent<BoxCollider2D>().enabled (SimpleMemberAccessExpression)
%3 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :52 :59) // true
br ^1

^1: // ExitBlock
return

}
func @_Bomb.Boom$int$(i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :55 :4) {
^entry (%_pow : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :55 :21)
cbde.store %_pow, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :55 :21)
br ^0

^0: // SimpleBlock
%1 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :57 :16)
// Entity from another assembly: Invoke
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :58 :15) // nameof(Explode) (InvocationExpression)
%3 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :58 :32) // 3f (NumericLiteralExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :58 :8) // Invoke(nameof(Explode), 3f) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function OnTriggerEnter2D(none), it contains poisonous unsupported syntaxes

func @_Bomb.GetPower$$() -> i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :71 :4) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :73 :15) // Not a variable of known type: power
return %0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Bomb.cs" :73 :8)

^1: // ExitBlock
cbde.unreachable

}
