func @_Destructible.OnTriggerEnter2D$UnityEngine.Collider2D$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Destructible.cs" :7 :4) {
^entry (%_other : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Destructible.cs" :7 :34)
cbde.store %_other, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Destructible.cs" :7 :34)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Destructible.cs" :9 :12) // Not a variable of known type: other
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Destructible.cs" :9 :29) // "Soda" (StringLiteralExpression)
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Destructible.cs" :9 :12) // other.CompareTag("Soda") (InvocationExpression)
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Destructible.cs" :9 :12)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: DestroyBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Destructible.cs" :11 :12) // DestroyBlock() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Destructible.DestroyBlock$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Destructible.cs" :15 :4) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: Random
%0 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Destructible.cs" :17 :25)
%1 = constant 10 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Destructible.cs" :17 :28)
%2 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Destructible.cs" :17 :12) // Random.Range(0, 10) (InvocationExpression)
%3 = constant 7 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Destructible.cs" :17 :35)
%4 = cmpi "sge", %2, %3 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Destructible.cs" :17 :12)
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Destructible.cs" :17 :12)

^1: // SimpleBlock
// Entity from another assembly: Instantiate
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Destructible.cs" :19 :24) // Not a variable of known type: coll
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Destructible.cs" :19 :30) // Identifier from another assembly: gameObject
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Destructible.cs" :19 :30) // gameObject.transform (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Destructible.cs" :19 :30) // gameObject.transform.position (SimpleMemberAccessExpression)
// Entity from another assembly: Quaternion
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Destructible.cs" :19 :61) // Quaternion.identity (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Destructible.cs" :19 :12) // Instantiate(coll, gameObject.transform.position, Quaternion.identity) (InvocationExpression)
br ^2

^2: // SimpleBlock
// Entity from another assembly: Destroy
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Destructible.cs" :21 :16) // Identifier from another assembly: gameObject
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Destructible.cs" :21 :8) // Destroy(gameObject) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
