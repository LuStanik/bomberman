func @_Collectible.Start$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Collectible.cs" :11 :4) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: Random
%0 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Collectible.cs" :13 :31)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Collectible.cs" :13 :34) // Not a variable of known type: graphicsArr
%2 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Collectible.cs" :13 :34) // graphicsArr.Length (SimpleMemberAccessExpression)
%3 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Collectible.cs" :13 :18) // Random.Range(0, graphicsArr.Length) (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Collectible.cs" :14 :8) // Identifier from another assembly: gameObject
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Collectible.cs" :14 :8) // gameObject.GetComponent<SpriteRenderer>() (InvocationExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Collectible.cs" :14 :8) // gameObject.GetComponent<SpriteRenderer>().sprite (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Collectible.cs" :14 :59) // Not a variable of known type: graphicsArr
%8 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Collectible.cs" :14 :71) // Not a variable of known type: version
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Collectible.cs" :14 :59) // graphicsArr[version] (ElementAccessExpression)
br ^1

^1: // ExitBlock
return

}
func @_Collectible.OnTriggerEnter2D$UnityEngine.Collider2D$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Collectible.cs" :17 :4) {
^entry (%_other : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Collectible.cs" :17 :34)
cbde.store %_other, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Collectible.cs" :17 :34)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Collectible.cs" :19 :13) // Not a variable of known type: other
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Collectible.cs" :19 :30) // "Player" (StringLiteralExpression)
%3 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Collectible.cs" :19 :13) // other.CompareTag("Player") (InvocationExpression)
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Collectible.cs" :19 :12) // !other.CompareTag("Player") (LogicalNotExpression)
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Collectible.cs" :19 :12)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Collectible.cs" :19 :41)

^2: // SimpleBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Collectible.cs" :21 :8) // Not a variable of known type: other
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Collectible.cs" :21 :26) // "GetBonus" (StringLiteralExpression)
%7 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Collectible.cs" :21 :38) // Not a variable of known type: version
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Collectible.cs" :21 :8) // other.SendMessage("GetBonus", version) (InvocationExpression)
// Entity from another assembly: Destroy
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Collectible.cs" :22 :16) // Identifier from another assembly: gameObject
%10 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Collectible.cs" :22 :28) // 0.1f (NumericLiteralExpression)
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Collectible.cs" :22 :8) // Destroy(gameObject, 0.1f) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
