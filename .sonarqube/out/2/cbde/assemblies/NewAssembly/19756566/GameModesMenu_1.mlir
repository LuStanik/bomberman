func @_GameModesMenu.PlaySinglePlayer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\GameModesMenu.cs" :5 :4) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: SceneManager
// Entity from another assembly: SceneManager
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\GameModesMenu.cs" :7 :31) // SceneManager.GetActiveScene() (InvocationExpression)
%1 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\GameModesMenu.cs" :7 :31) // SceneManager.GetActiveScene().buildIndex (SimpleMemberAccessExpression)
%2 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\GameModesMenu.cs" :7 :74)
%3 = addi %1, %2 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\GameModesMenu.cs" :7 :31)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\GameModesMenu.cs" :7 :8) // SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_GameModesMenu.PlayMultiPlayer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\GameModesMenu.cs" :9 :4) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: SceneManager
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\GameModesMenu.cs" :12 :31) // "MultiGame" (StringLiteralExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\GameModesMenu.cs" :12 :8) // SceneManager.LoadScene("MultiGame") (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
