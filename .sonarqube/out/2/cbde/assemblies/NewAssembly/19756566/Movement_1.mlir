func @_Movement.Start$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Movement.cs" :20 :4) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Movement.cs" :21 :17) // GetComponent<Rigidbody2D>() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function Update(), it contains poisonous unsupported syntaxes

func @_Movement.FixedUpdate$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Movement.cs" :62 :4) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Movement.cs" :64 :19) // Not a variable of known type: player
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Movement.cs" :64 :19) // player.position (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Movement.cs" :64 :37) // Not a variable of known type: move
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Movement.cs" :64 :45) // Not a variable of known type: speed
// Entity from another assembly: Time
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Movement.cs" :64 :53) // Time.fixedDeltaTime (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Movement.cs" :64 :45) // Binary expression on unsupported types speed * Time.fixedDeltaTime
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Movement.cs" :64 :37) // Binary expression on unsupported types move * (speed * Time.fixedDeltaTime)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Movement.cs" :64 :19) // Binary expression on unsupported types player.position + move * (speed * Time.fixedDeltaTime)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Movement.cs" :65 :8) // Not a variable of known type: player
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Movement.cs" :65 :28) // Not a variable of known type: position
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Movement.cs" :65 :8) // player.MovePosition(position) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function OnTriggerEnter2D(none), it contains poisonous unsupported syntaxes

func @_Movement.IncrementBombs$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Movement.cs" :82 :4) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Movement.cs" :84 :8) // Not a variable of known type: availableBombs
%1 = constant 1 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Movement.cs" :84 :26)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Scripts\\Movement.cs" :84 :8) // Binary expression on unsupported types availableBombs += 1
br ^1

^1: // ExitBlock
return

}
// Skipping function GetBonus(i32), it contains poisonous unsupported syntaxes

