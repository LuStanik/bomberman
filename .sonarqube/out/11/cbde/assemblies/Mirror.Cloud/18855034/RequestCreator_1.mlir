func @_Mirror.Cloud.RequestCreator.CreateUri$string$(none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :41 :8) {
^entry (%_page : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :41 :22)
cbde.store %_page, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :41 :22)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :43 :27) // string (PredefinedType)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :43 :41) // "{0}/{1}?key={2}" (StringLiteralExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :43 :60) // Not a variable of known type: baseAddress
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :43 :73) // Not a variable of known type: page
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :43 :79) // Not a variable of known type: apiKey
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :43 :27) // string.Format("{0}/{1}?key={2}", baseAddress, page, apiKey) (InvocationExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :43 :19) // new Uri(string.Format("{0}/{1}?key={2}", baseAddress, page, apiKey)) (ObjectCreationExpression)
return %7 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :43 :12)

^1: // ExitBlock
cbde.unreachable

}
// Skipping function CreateWebRequest(none, none, none), it contains poisonous unsupported syntaxes

func @_Mirror.Cloud.RequestCreator.Get$string$(none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :76 :8) {
^entry (%_page : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :76 :35)
cbde.store %_page, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :76 :35)
br ^0

^0: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CreateWebRequest
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :78 :36) // Not a variable of known type: page
%2 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :78 :42) // GET (IdentifierName)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :78 :19) // CreateWebRequest(page, GET) (InvocationExpression)
return %3 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :78 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.Cloud.RequestCreator.Post$T$$string.T$(none, none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :88 :8) {
^entry (%_page : none, %_json : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :88 :39)
cbde.store %_page, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :88 :39)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :88 :52)
cbde.store %_json, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :88 :52)
br ^0

^0: // JumpBlock
// Entity from another assembly: JsonUtility
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :90 :51) // Not a variable of known type: json
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :90 :32) // JsonUtility.ToJson(json) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CreateWebRequest
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :91 :36) // Not a variable of known type: page
%6 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :91 :42) // POST (IdentifierName)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :91 :48) // Not a variable of known type: jsonString
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :91 :19) // CreateWebRequest(page, POST, jsonString) (InvocationExpression)
return %8 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :91 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.Cloud.RequestCreator.Patch$T$$string.T$(none, none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :101 :8) {
^entry (%_page : none, %_json : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :101 :40)
cbde.store %_page, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :101 :40)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :101 :53)
cbde.store %_json, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :101 :53)
br ^0

^0: // JumpBlock
// Entity from another assembly: JsonUtility
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :103 :51) // Not a variable of known type: json
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :103 :32) // JsonUtility.ToJson(json) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CreateWebRequest
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :104 :36) // Not a variable of known type: page
%6 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :104 :42) // PATCH (IdentifierName)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :104 :49) // Not a variable of known type: jsonString
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :104 :19) // CreateWebRequest(page, PATCH, jsonString) (InvocationExpression)
return %8 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :104 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.Cloud.RequestCreator.Delete$string$(none) -> none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :112 :8) {
^entry (%_page : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :112 :38)
cbde.store %_page, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :112 :38)
br ^0

^0: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CreateWebRequest
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :114 :36) // Not a variable of known type: page
%2 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :114 :42) // DELETE (IdentifierName)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :114 :19) // CreateWebRequest(page, DELETE) (InvocationExpression)
return %3 : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :114 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.Cloud.RequestCreator.SendRequest$UnityEngine.Networking.UnityWebRequest.Mirror.Cloud.RequestSuccess.Mirror.Cloud.RequestFail$(none, none, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :118 :8) {
^entry (%_request : none, %_onSuccess : none, %_onFail : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :118 :32)
cbde.store %_request, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :118 :32)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :118 :57)
cbde.store %_onSuccess, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :118 :57)
%2 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :118 :90)
cbde.store %_onFail, %2 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :118 :90)
br ^0

^0: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :120 :12) // Not a variable of known type: runner
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SendRequestEnumerator
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :120 :56) // Not a variable of known type: request
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :120 :65) // Not a variable of known type: onSuccess
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :120 :76) // Not a variable of known type: onFail
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :120 :34) // SendRequestEnumerator(request, onSuccess, onFail) (InvocationExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\RequestCreator.cs" :120 :12) // runner.StartCoroutine(SendRequestEnumerator(request, onSuccess, onFail)) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function SendRequestEnumerator(none, none, none), it contains poisonous unsupported syntaxes

