func @_Mirror.Cloud.Logger.LogRequest$string.string.bool.string$(none, none, i1, none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :11 :8) {
^entry (%_page : none, %_method : none, %_hasJson : i1, %_json : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :11 :38)
cbde.store %_page, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :11 :38)
%1 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :11 :51)
cbde.store %_method, %1 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :11 :51)
%2 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :11 :66)
cbde.store %_hasJson, %2 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :11 :66)
%3 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :11 :80)
cbde.store %_json, %3 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :11 :80)
br ^0

^0: // BinaryBranchBlock
%4 = cbde.load %2 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :13 :16)
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :13 :16)

^1: // SimpleBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :15 :16) // Not a variable of known type: logger
// Entity from another assembly: LogType
%6 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :15 :33) // LogType.Log (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :15 :46) // "Request: {0} {1} {2}" (StringLiteralExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :15 :70) // Not a variable of known type: method
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :15 :78) // Not a variable of known type: page
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :15 :84) // Not a variable of known type: json
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :15 :16) // logger.LogFormat(LogType.Log, "Request: {0} {1} {2}", method, page, json) (InvocationExpression)
br ^3

^2: // SimpleBlock
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :19 :16) // Not a variable of known type: logger
// Entity from another assembly: LogType
%13 = constant unit loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :19 :33) // LogType.Log (SimpleMemberAccessExpression)
%14 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :19 :46) // "Request: {0} {1}" (StringLiteralExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :19 :66) // Not a variable of known type: method
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :19 :74) // Not a variable of known type: page
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :19 :16) // logger.LogFormat(LogType.Log, "Request: {0} {1}", method, page) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function LogResponse(none), it contains poisonous unsupported syntaxes

func @_Mirror.Cloud.Logger.Log$string$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :47 :8) {
^entry (%_msg : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :47 :33)
cbde.store %_msg, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :47 :33)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :49 :16) // Not a variable of known type: logger
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :49 :16) // logger.LogEnabled() (InvocationExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :49 :16)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :50 :16) // Not a variable of known type: logger
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :50 :27) // Not a variable of known type: msg
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :50 :16) // logger.Log(msg) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.Cloud.Logger.LogWarning$string$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :53 :8) {
^entry (%_msg : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :53 :40)
cbde.store %_msg, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :53 :40)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :55 :16) // Not a variable of known type: logger
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :55 :16) // logger.WarnEnabled() (InvocationExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :55 :16)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :56 :16) // Not a variable of known type: logger
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :56 :34) // Not a variable of known type: msg
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :56 :16) // logger.LogWarning(msg) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Mirror.Cloud.Logger.LogError$string$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :59 :8) {
^entry (%_msg : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :59 :38)
cbde.store %_msg, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :59 :38)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :61 :16) // Not a variable of known type: logger
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :61 :16) // logger.ErrorEnabled() (InvocationExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :61 :16)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :62 :16) // Not a variable of known type: logger
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :62 :32) // Not a variable of known type: msg
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\Logger.cs" :62 :16) // logger.LogError(msg) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
// Skipping function Verbose(none), it contains poisonous unsupported syntaxes

