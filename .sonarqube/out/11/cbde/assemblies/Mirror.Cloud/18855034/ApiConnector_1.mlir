func @_Mirror.Cloud.ApiConnector.Awake$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ApiConnector.cs" :40 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ApiConnector.cs" :42 :48) // Not a variable of known type: ApiAddress
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ApiConnector.cs" :42 :60) // Not a variable of known type: ApiKey
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ApiConnector.cs" :42 :68) // this (ThisExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ApiConnector.cs" :42 :29) // new RequestCreator(ApiAddress, ApiKey, this) (ObjectCreationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: InitListServer
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ApiConnector.cs" :44 :12) // InitListServer() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Cloud.ApiConnector.InitListServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ApiConnector.cs" :47 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ApiConnector.cs" :49 :69) // this (ThisExpression)
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ApiConnector.cs" :49 :75) // Not a variable of known type: requestCreator
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ApiConnector.cs" :49 :45) // new ListServerServerApi(this, requestCreator) (ObjectCreationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ApiConnector.cs" :50 :69) // this (ThisExpression)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ApiConnector.cs" :50 :75) // Not a variable of known type: requestCreator
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ApiConnector.cs" :50 :91) // Not a variable of known type: _onServerListUpdated
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ApiConnector.cs" :50 :45) // new ListServerClientApi(this, requestCreator, _onServerListUpdated) (ObjectCreationExpression)
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ApiConnector.cs" :51 :40) // Not a variable of known type: serverApi
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ApiConnector.cs" :51 :51) // Not a variable of known type: clientApi
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ApiConnector.cs" :51 :25) // new ListServer(serverApi, clientApi) (ObjectCreationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function OnDestroy(), it contains poisonous unsupported syntaxes

