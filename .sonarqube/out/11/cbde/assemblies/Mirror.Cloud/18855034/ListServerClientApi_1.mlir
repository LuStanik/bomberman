func @_Mirror.Cloud.ListServerService.ListServerClientApi.Shutdown$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerClientApi.cs" :24 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: StopGetServerListRepeat
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerClientApi.cs" :26 :12) // StopGetServerListRepeat() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Cloud.ListServerService.ListServerClientApi.GetServerList$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerClientApi.cs" :29 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerClientApi.cs" :31 :12) // Not a variable of known type: runner
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: getServerList
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerClientApi.cs" :31 :34) // getServerList() (InvocationExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerClientApi.cs" :31 :12) // runner.StartCoroutine(getServerList()) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Mirror.Cloud.ListServerService.ListServerClientApi.StartGetServerListRepeat$int$(i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerClientApi.cs" :34 :8) {
^entry (%_interval : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerClientApi.cs" :34 :45)
cbde.store %_interval, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerClientApi.cs" :34 :45)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerClientApi.cs" :36 :43) // Not a variable of known type: runner
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetServerListRepeat
%2 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerClientApi.cs" :36 :85)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerClientApi.cs" :36 :65) // GetServerListRepeat(interval) (InvocationExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerClientApi.cs" :36 :43) // runner.StartCoroutine(GetServerListRepeat(interval)) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function StopGetServerListRepeat(), it contains poisonous unsupported syntaxes

// Skipping function GetServerListRepeat(i32), it contains poisonous unsupported syntaxes

// Skipping function getServerList(), it contains poisonous unsupported syntaxes

