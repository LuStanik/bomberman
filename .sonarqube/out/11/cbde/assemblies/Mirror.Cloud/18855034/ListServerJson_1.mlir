func @_Mirror.Cloud.ListServerService.ServerJson.SetCustomData$System.Collections.Generic.Dictionary$string.string$$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :60 :8) {
^entry (%_data : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :60 :34)
cbde.store %_data, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :60 :34)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :62 :16) // Not a variable of known type: data
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :62 :24) // null (NullLiteralExpression)
%3 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :62 :16) // comparison of unknown type: data == null
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :62 :16)

^1: // SimpleBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :64 :29) // null (NullLiteralExpression)
br ^3

^2: // SimpleBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :68 :29) // Not a variable of known type: data
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :68 :29) // data.ToKeyValueArray() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CustomDataHelper
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :69 :52) // Not a variable of known type: customData
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :69 :16) // CustomDataHelper.ValidateCustomData(customData) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Cloud.ListServerService.ServerJson.Validate$$() -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :73 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CustomDataHelper
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :75 :48) // Not a variable of known type: customData
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :75 :12) // CustomDataHelper.ValidateCustomData(customData) (InvocationExpression)
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :77 :16) // string (PredefinedType)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :77 :37) // Not a variable of known type: protocol
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :77 :16) // string.IsNullOrEmpty(protocol) (InvocationExpression)
cond_br %4, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :77 :16)

^1: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Logger
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :79 :32) // "ServerJson should not have empty protocol" (StringLiteralExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :79 :16) // Logger.LogError("ServerJson should not have empty protocol") (InvocationExpression)
%7 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :80 :23) // false
return %7 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :80 :16)

^2: // BinaryBranchBlock
%8 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :83 :16) // Not a variable of known type: port
%9 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :83 :24)
%10 = cmpi "eq", %8, %9 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :83 :16)
cond_br %10, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :83 :16)

^3: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Logger
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :85 :32) // "ServerJson should not have port equal 0" (StringLiteralExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :85 :16) // Logger.LogError("ServerJson should not have port equal 0") (InvocationExpression)
%13 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :86 :23) // false
return %13 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :86 :16)

^4: // BinaryBranchBlock
%14 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :89 :16) // Not a variable of known type: maxPlayerCount
%15 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :89 :34)
%16 = cmpi "eq", %14, %15 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :89 :16)
cond_br %16, ^5, ^6 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :89 :16)

^5: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Logger
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :91 :32) // "ServerJson should not have maxPlayerCount equal 0" (StringLiteralExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :91 :16) // Logger.LogError("ServerJson should not have maxPlayerCount equal 0") (InvocationExpression)
%19 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :92 :23) // false
return %19 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :92 :16)

^6: // JumpBlock
%20 = constant 1 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :95 :19) // true
return %20 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :95 :12)

^7: // ExitBlock
cbde.unreachable

}
func @_Mirror.Cloud.ListServerService.PartialServerJson.SetCustomData$System.Collections.Generic.Dictionary$string.string$$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :124 :8) {
^entry (%_data : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :124 :34)
cbde.store %_data, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :124 :34)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :126 :16) // Not a variable of known type: data
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :126 :24) // null (NullLiteralExpression)
%3 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :126 :16) // comparison of unknown type: data == null
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :126 :16)

^1: // SimpleBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :128 :29) // null (NullLiteralExpression)
br ^3

^2: // SimpleBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :132 :29) // Not a variable of known type: data
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :132 :29) // data.ToKeyValueArray() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CustomDataHelper
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :133 :52) // Not a variable of known type: customData
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :133 :16) // CustomDataHelper.ValidateCustomData(customData) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Cloud.ListServerService.PartialServerJson.Validate$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :137 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CustomDataHelper
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :139 :48) // Not a variable of known type: customData
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :139 :12) // CustomDataHelper.ValidateCustomData(customData) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function ToDictionary(none), it contains poisonous unsupported syntaxes

// Skipping function ToKeyValueArray(none), it contains poisonous unsupported syntaxes

// Skipping function ValidateCustomData(none), it contains poisonous unsupported syntaxes

func @_Mirror.Cloud.ListServerService.KeyValue.Validate$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :191 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :193 :16) // Not a variable of known type: key
%1 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :193 :16) // key.Length (SimpleMemberAccessExpression)
%2 = constant 32 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :193 :29)
%3 = cmpi "sgt", %1, %2 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :193 :16)
cond_br %3, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :193 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Logger
%4 = constant 32 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :195 :83)
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :195 :32) // $"Custom Data must have key with length less than {MaxKeySize}" (InterpolatedStringExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :195 :16) // Logger.LogError($"Custom Data must have key with length less than {MaxKeySize}") (InvocationExpression)
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :196 :22) // Not a variable of known type: key
%8 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :196 :36)
%9 = constant 32 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :196 :39)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :196 :22) // key.Substring(0, MaxKeySize) (InvocationExpression)
br ^2

^2: // BinaryBranchBlock
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :199 :16) // Not a variable of known type: value
%12 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :199 :16) // value.Length (SimpleMemberAccessExpression)
%13 = constant 256 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :199 :31)
%14 = cmpi "sgt", %12, %13 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :199 :16)
cond_br %14, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :199 :16)

^3: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Logger
%15 = constant 256 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :201 :85)
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :201 :32) // $"Custom Data must have value with length less than {MaxValueSize}" (InterpolatedStringExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :201 :16) // Logger.LogError($"Custom Data must have value with length less than {MaxValueSize}") (InvocationExpression)
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :202 :24) // Not a variable of known type: value
%19 = constant 0 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :202 :40)
%20 = constant 256 : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :202 :43)
%21 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerJson.cs" :202 :24) // value.Substring(0, MaxValueSize) (InvocationExpression)
br ^4

^4: // ExitBlock
return

}
