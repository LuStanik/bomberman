func @_Mirror.Cloud.ListServerService.ListServerServerApi.Shutdown$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :38 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: stopPingCoroutine
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :40 :12) // stopPingCoroutine() (InvocationExpression)
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :41 :16) // Not a variable of known type: added
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :41 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: removeServerWithoutCoroutine
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :43 :16) // removeServerWithoutCoroutine() (InvocationExpression)
br ^2

^2: // SimpleBlock
%3 = constant 0 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :45 :20) // false
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Cloud.ListServerService.ListServerServerApi.AddServer$Mirror.Cloud.ListServerService.ServerJson$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :48 :8) {
^entry (%_server : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :48 :30)
cbde.store %_server, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :48 :30)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :50 :16) // Not a variable of known type: added
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :50 :16)

^1: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Logger
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :50 :43) // "AddServer called when server was already adding or added" (StringLiteralExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :50 :25) // Logger.LogWarning("AddServer called when server was already adding or added") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :50 :104)

^2: // BinaryBranchBlock
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :51 :25) // Not a variable of known type: server
%5 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :51 :25) // server.Validate() (InvocationExpression)
%6 = cbde.alloca i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :51 :17) // valid
cbde.store %5, %6 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :51 :17)
%7 = cbde.load %6 : memref<i1> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :52 :17)
%8 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :52 :16) // !valid (LogicalNotExpression)
cond_br %8, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :52 :16)

^3: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :52 :26)

^4: // SimpleBlock
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :54 :12) // Not a variable of known type: runner
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: addServer
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :54 :44) // Not a variable of known type: server
%11 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :54 :34) // addServer(server) (InvocationExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :54 :12) // runner.StartCoroutine(addServer(server)) (InvocationExpression)
br ^5

^5: // ExitBlock
return

}
func @_Mirror.Cloud.ListServerService.ListServerServerApi.UpdateServer$int$(i32) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :57 :8) {
^entry (%_newPlayerCount : i32):
%0 = cbde.alloca i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :57 :33)
cbde.store %_newPlayerCount, %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :57 :33)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :59 :17) // Not a variable of known type: added
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :59 :16) // !added (LogicalNotExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :59 :16)

^1: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Logger
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :59 :44) // "UpdateServer called when before server was added" (StringLiteralExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :59 :26) // Logger.LogWarning("UpdateServer called when before server was added") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :59 :97)

^2: // SimpleBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :61 :12) // Not a variable of known type: currentServer
%6 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :61 :12) // currentServer.playerCount (SimpleMemberAccessExpression)
%7 = cbde.load %0 : memref<i32> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :61 :40)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: UpdateServer
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :62 :25) // Not a variable of known type: currentServer
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :62 :12) // UpdateServer(currentServer) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Cloud.ListServerService.ListServerServerApi.UpdateServer$Mirror.Cloud.ListServerService.ServerJson$(none) -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :65 :8) {
^entry (%_server : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :65 :33)
cbde.store %_server, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :65 :33)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :68 :17) // Not a variable of known type: added
%2 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :68 :16) // !added (LogicalNotExpression)
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :68 :16)

^1: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Logger
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :68 :44) // "UpdateServer called when before server was added" (StringLiteralExpression)
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :68 :26) // Logger.LogWarning("UpdateServer called when before server was added") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :68 :97)

^2: // SimpleBlock
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :70 :46) // new PartialServerJson             {                 displayName = server.displayName,                 playerCount = server.playerCount,                 maxPlayerCount = server.maxPlayerCount,                 customData = server.customData,             } (ObjectCreationExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :72 :30) // Not a variable of known type: server
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :72 :30) // server.displayName (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :73 :30) // Not a variable of known type: server
%9 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :73 :30) // server.playerCount (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :74 :33) // Not a variable of known type: server
%11 = cbde.unknown : i32 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :74 :33) // server.maxPlayerCount (SimpleMemberAccessExpression)
%12 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :75 :29) // Not a variable of known type: server
%13 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :75 :29) // server.customData (SimpleMemberAccessExpression)
%15 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :77 :12) // Not a variable of known type: partialServer
%16 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :77 :12) // partialServer.Validate() (InvocationExpression)
%17 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :79 :12) // Not a variable of known type: runner
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: updateServer
%18 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :79 :47) // Not a variable of known type: partialServer
%19 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :79 :34) // updateServer(partialServer) (InvocationExpression)
%20 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :79 :12) // runner.StartCoroutine(updateServer(partialServer)) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_Mirror.Cloud.ListServerService.ListServerServerApi.RemoveServer$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :82 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :84 :17) // Not a variable of known type: added
%1 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :84 :16) // !added (LogicalNotExpression)
cond_br %1, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :84 :16)

^1: // JumpBlock
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :84 :26)

^2: // BinaryBranchBlock
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :86 :16) // string (PredefinedType)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :86 :37) // Not a variable of known type: serverId
%4 = cbde.unknown : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :86 :16) // string.IsNullOrEmpty(serverId) (InvocationExpression)
cond_br %4, ^3, ^4 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :86 :16)

^3: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Logger
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :88 :34) // "Can not remove server because serverId was empty" (StringLiteralExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :88 :16) // Logger.LogWarning("Can not remove server because serverId was empty") (InvocationExpression)
return loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :89 :16)

^4: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: stopPingCoroutine
%7 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :92 :12) // stopPingCoroutine() (InvocationExpression)
%8 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :93 :12) // Not a variable of known type: runner
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: removeServer
%9 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :93 :34) // removeServer() (InvocationExpression)
%10 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :93 :12) // runner.StartCoroutine(removeServer()) (InvocationExpression)
br ^5

^5: // ExitBlock
return

}
func @_Mirror.Cloud.ListServerService.ListServerServerApi.stopPingCoroutine$$() -> () loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :96 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :98 :16) // Not a variable of known type: _pingCoroutine
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :98 :34) // null (NullLiteralExpression)
%2 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :98 :16) // comparison of unknown type: _pingCoroutine != null
cond_br %2, ^1, ^2 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :98 :16)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :100 :16) // Not a variable of known type: runner
%4 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :100 :37) // Not a variable of known type: _pingCoroutine
%5 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :100 :16) // runner.StopCoroutine(_pingCoroutine) (InvocationExpression)
%6 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\ListServer\\ListServerServerApi.cs" :101 :33) // null (NullLiteralExpression)
br ^2

^2: // ExitBlock
return

}
// Skipping function addServer(none), it contains poisonous unsupported syntaxes

// Skipping function updateServer(none), it contains poisonous unsupported syntaxes

// Skipping function ping(), it contains poisonous unsupported syntaxes

// Skipping function removeServer(), it contains poisonous unsupported syntaxes

// Skipping function removeServerWithoutCoroutine(), it contains poisonous unsupported syntaxes

