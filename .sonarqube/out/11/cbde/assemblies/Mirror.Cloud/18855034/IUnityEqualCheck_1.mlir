func @_Mirror.Cloud.UnityEqualCheckExtension.IsNull$Mirror.Cloud.IUnityEqualCheck$(none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\IUnityEqualCheck.cs" :15 :8) {
^entry (%_obj : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\IUnityEqualCheck.cs" :15 :34)
cbde.store %_obj, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\IUnityEqualCheck.cs" :15 :34)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\IUnityEqualCheck.cs" :17 :20) // Not a variable of known type: obj
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\IUnityEqualCheck.cs" :17 :20) // obj as Object (AsExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\IUnityEqualCheck.cs" :17 :38) // null (NullLiteralExpression)
%4 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\IUnityEqualCheck.cs" :17 :19) // comparison of unknown type: (obj as Object) == null
return %4 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\IUnityEqualCheck.cs" :17 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Mirror.Cloud.UnityEqualCheckExtension.IsNotNull$Mirror.Cloud.IUnityEqualCheck$(none) -> i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\IUnityEqualCheck.cs" :20 :8) {
^entry (%_obj : none):
%0 = cbde.alloca none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\IUnityEqualCheck.cs" :20 :37)
cbde.store %_obj, %0 : memref<none> loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\IUnityEqualCheck.cs" :20 :37)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\IUnityEqualCheck.cs" :22 :20) // Not a variable of known type: obj
%2 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\IUnityEqualCheck.cs" :22 :20) // obj as Object (AsExpression)
%3 = cbde.unknown : none loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\IUnityEqualCheck.cs" :22 :38) // null (NullLiteralExpression)
%4 = cbde.unknown : i1  loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\IUnityEqualCheck.cs" :22 :19) // comparison of unknown type: (obj as Object) != null
return %4 : i1 loc("D:\\Unity\\2019.4.16f1\\Bomberman\\Assets\\Mirror\\Cloud\\Core\\IUnityEqualCheck.cs" :22 :12)

^1: // ExitBlock
cbde.unreachable

}
