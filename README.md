Bomberman Project
Creators: Michał Worsowicz, Kamil Tomaszewski, Łukasz Stanik

To start the game download BombermanV0.62 directory and doubleclick Bomberman.exe file.

Technical documentation and user manual are in pdf files in main project directory.

All elements created for the project are kept in the 'Assets' directory.

Contact: lukaszstanik99@gmail.com

Copyright (c) 2021, Dream Team Semestr V2
All rights reserved.
